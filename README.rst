########
Musicbox 
########


Overview
========

This project is an ongoing work to implement a software running a multimedia 
enabled device like digital radio, alarm clock or musicplayer based on some kind of 
embedded platform. It was started from an existing project creating an 
Si4688 based radio when a new hardware was designed:

https://gitlab.com/amesser-group/electronic-devices/radio-shield

Status
======

Building the project will generate some firmwares to be usable with:

- custom ATSAM4S MCU combined with a first version of a Si4688 adapter
  and various types of displays (A bare metal system)
- `ESP32 based radio clock <doc/realizations/esp32-radio-clock/esp32-radio-clock.rst>`_ using
  Bastelino ESP32-S2 platform combined with radio shield and
  a graphic LCD display (Running on top on ESP-IDF/FreeRTOS)

Although these firmwares are qlready quite usable for listening radio
and providing an alarm clock. There are still many open points to
do. Also the project serves a bit as my personal playground to try
different design patterns with embedded devices

Licensing
=========

GPLv3 for all files in src/application subfolder

Custom License for all files in src/atmel-sam4s/src and
src/atmel-sam4s/include provided as comment in each file.

Custom License for all files in src/arm-cmsis/src
provided as comment in each file.

Custom License for all files in src/dhara_ftl/src. License conditions
given in src/dhara_ftl/src/LICENSE

Copyright
=========

For all files in firmware subfolder the particular copyright
is provided as comment in first lines of a file itself. Holders
of copyright are:

- src/application:
  Andreas Messer <andi@bastelmap.de>

- src/atmel-sam4s/src, src/atmel-sam4s/include:
  Atmel Corporation

- src/arm-cmsis/src:
  ARM LIMITED

- src/dhara_ftl/src:
  Daniel Beer <dlbeer@gmail.com>

- src/elmchan_fatfs/src:
  ChaN, http://elm-chan.org/fsw/ff/00index_e.html






