/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_SNOOZECONTROLLER_HPP_
#define MUSICBOX_SNOOZECONTROLLER_HPP_

#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/msm_grammar.hpp>

namespace MusicBox
{
  class SnoozeController_ : public boost::msm::front::state_machine_def<SnoozeController_>
  {
  protected:
    struct Idle   : public boost::msm::front::state<> {};
    struct Waking : public boost::msm::front::state<> {};
    struct Snooze : public boost::msm::front::state<> {};
    struct Asleep : public boost::msm::front::state<> {};

    struct Alarm {};
    struct SnoozeKey {};
    struct UserShutdown {};
    struct Timeout {};

    void PlayOneHour(const SnoozeKey&);
    void MuteAlarm(const SnoozeKey&);
    void RestartAlarm(const Timeout&);
    void RadioOff(const Timeout&);
    void RadioOff(const SnoozeKey&);

    struct transition_table : boost::mpl::vector<
      a_row<Idle,   SnoozeKey,     Asleep, &SnoozeController_::PlayOneHour>,
       _row<Idle,    Alarm,         Waking>,
       _row<Waking,  UserShutdown,  Idle>,
       _row<Waking,  Timeout,       Idle>,
      a_row<Waking, SnoozeKey,     Snooze, &SnoozeController_::MuteAlarm>,
       _row<Snooze,  UserShutdown,  Idle>,
      a_row<Snooze, Timeout,       Waking, &SnoozeController_::RestartAlarm>,
       _row<Asleep, UserShutdown,  Idle>,
      a_row<Asleep, Timeout,       Idle,   &SnoozeController_::RadioOff>,
      a_row<Asleep, SnoozeKey,     Idle,   &SnoozeController_::RadioOff>
    > {};

  public:
    using initial_state = Idle;
    typedef int no_exception_thrown;
    typedef int no_message_queue;

    /* ignore transitions not defined in table above */
    template <class FSM,class Event>  void no_transition(Event const& ,FSM&, int )  {}
  };

  class SnoozeController : protected boost::msm::back::state_machine<SnoozeController_>
  {
  public:
    void HandleSnoozeKeyPress();
    void HandleUserTimeout();
    void HandleAlarm();
    void HandleUserShutdown();
  };

}

#endif