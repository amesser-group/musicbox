/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Alarm.hpp"

using namespace MusicBox;

Alarm::Setpoint::Setpoint(const ecpp_sys::Chrono::SystemClock::Timepoint & tp)
{
  ecpp_sys::Chrono::LocalClock::Timepoint lt = tp;

  mask   = 0x80 | ToMask(lt.wday());
  hour   = lt.time().hours();
  minute = lt.time().minutes();
}

void
Alarm::Check(const Alarm::Setpoint &last_checked, const Alarm::Setpoint &current) const
{
  if((current.mask & setpoint_.mask) == current.mask)
  {
    if(current >= setpoint_)
    {
      bool day_changed = (last_checked.mask != current.mask);

      if(day_changed || (last_checked < setpoint_))
        ::signals::Raise(*this);
    }
  }
}