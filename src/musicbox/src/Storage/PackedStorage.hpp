/*
 *  Copyright 2020-2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_STORAGE_PACKEDSTORAGE_HPP_
#define MUSICBOX_STORAGE_PACKEDSTORAGE_HPP_

#include "Storage/PackedBinaryFormat.hpp"
#include "Database/ServiceDatabase.hpp"
#include "ecpp/BinaryStruct.hpp"
#include <cstdint>

namespace MusicBox::Storage::PackedStorage
{

  
  class Reader : protected PackedBinaryFormat
  {
  public:
    constexpr Reader() {};

    void      Load();

    constexpr Database::ServiceHandle GetLastService() const { return Database::ServiceHandle(raw_service_handle_); }

  protected:
    class TLVReader;

    bool ReadHeader(TLVReader   &tlv);
    void ReadServices(TLVReader &tlv);

    uint_least8_t   raw_service_handle_ {0};
  };

  class Writer : protected PackedBinaryFormat
  {
  public:
    constexpr Writer() {};
    void      Save();
    void      SaveState();
  protected:
    class TLVWriter;

    void WriteHeader(TLVWriter &writer);
    void WriteServices(TLVWriter &writer);
  };
};
#endif