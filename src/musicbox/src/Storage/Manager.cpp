/*
 *  Copyright 2018 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Storage/Manager.hpp"
#include "Database/ServiceDatabase.hpp"
#include "ecpp/Container.hpp"
#include "ecpp/Datatypes.hpp"
#include "ecpp/Units/Time.hpp"
#include "System.hpp"

using namespace MusicBox::Storage;
using namespace std;

using ecpp::Units::MilliSecond;

void StorageManager::setSupressSaves(bool suppress)
{
  if (SuppressSaves == suppress)
    return;

  SuppressSaves = suppress;

  if(!suppress)
  {
    saveStoredState();
    databaseChanged();
  }
}

void StorageManager::databaseChanged()
{
  StoreDatabase.Enqueue();
}

ECPP_JOB_DEF(StorageManager, StoreDatabase)
{
  store_database();
}

void StorageManager::LoadDatabase()
{
  ::sys::Bsp::StorageReader reader;

  reader.Load();

  ::sys::GetSi468xDriver(*this).tuneChannel(reader.GetLastService());
}

void StorageManager::store_database()
{
  if(!SuppressSaves)
  {
    ::sys::Bsp::StorageWriter writer;
    writer.Save();
  }
}

void
StorageManager::saveState()
{
  ::sys::Bsp::StorageWriter writer;
  writer.SaveState();
}

ECPP_JOB_DEF(StorageManager, StoreState)
{
  saveState();
}

void
StorageManager::saveStoredState()
{
  StoreState.Start(MilliSecond<>(10000));
}



