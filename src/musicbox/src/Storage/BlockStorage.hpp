/*
 *  Copyright 2020-2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_STORAGE_BLOCKSTORAGE_HPP_
#define MUSICBOX_STORAGE_BLOCKSTORAGE_HPP_

#include "Storage/PackedBinaryFormat.hpp"
#include "Database/ServiceDatabase.hpp"
#include "ecpp/BinaryStruct.hpp"
#include <cstdint>

namespace MusicBox::Storage::BlockStorage
{
  template<typename ValueType, typename Argument>
  class IteratorProxy
  {
  public:
    constexpr IteratorProxy(Argument *arg) : arg_(arg) {}

    ValueType operator *()  { return ValueType(*arg_);  };
    constexpr void operator ++ () const {};
  protected:
    Argument *arg_;
  };

  class Reader : protected PackedBinaryFormat
  {
  public:
    constexpr Reader() {};

    void      Load();

    constexpr Database::ServiceHandle GetLastService() const { return Database::ServiceHandle(raw_service_handle_); }

  protected:
    class BlockReader;

    void ReadHeader();
    void ReadServices();

    uint_least8_t   services_block_nr_  {0};
    uint_least8_t   raw_service_handle_ {0};
  };

  class Reader::BlockReader 
  {
  public:
    BlockReader(uint_least8_t block_nr);

    class ItemDeserializer;
    class Deserializer;

    Deserializer  begin();
    constexpr Deserializer end() const;

    constexpr BlockTypes GetBlockType()    const { return static_cast<BlockTypes>(buffer_[kHeaderTypeOffset]); }
    constexpr uint8_t    GetBlockVersion() const { return buffer_[kHeaderVersionOffset]; }
    constexpr size_t     GetPayloadLen()   const { return ((size_t)buffer_[kHeaderLengthOffset]) + (((size_t)buffer_[kHeaderLengthOffset+1]) << 8); }
  protected:
    size_t          offset_ {0};
    uint8_t         buffer_[512];
  };

  class Reader::BlockReader::Deserializer : public IteratorProxy<ItemDeserializer, BlockReader>
  {
  public:
    using IteratorProxy<ItemDeserializer, BlockReader>::IteratorProxy;
    
    constexpr bool operator != (const IteratorProxy&) const { return arg_->offset_ < (arg_->GetPayloadLen() + 4); }
  };

  constexpr Reader::BlockReader::Deserializer
  Reader::BlockReader::end() const
  {
    return Deserializer(nullptr);
  }

  class Reader::BlockReader::ItemDeserializer : public ::ecpp::Deserializer<::ecpp::BinaryStruct>
  {
  protected:
    using BaseClass = ::ecpp::Deserializer<::ecpp::BinaryStruct>;

  public:
    constexpr ItemDeserializer(Reader::BlockReader &reader) : BaseClass(reader.buffer_, reader.offset_), reader_(reader) {};

    ~ItemDeserializer()
    {
      reader_.offset_ = BaseClass::offset_;
    }


    Reader::BlockReader & reader_;
  };


  class Writer : protected PackedBinaryFormat
  {
  public:
    constexpr Writer() {};
    void      Save();
    void      SaveState();
  protected:
    class BlockWriter;

    void WriteHeader();
    void WriteServices();

    uint_least8_t   next_block_nr_     {0};
    uint_least8_t   services_block_nr_ {0};
  };

  class Writer::BlockWriter 
  {
  public:
    BlockWriter(uint_least8_t block_nr, BlockTypes typ, uint8_t version);
    ~BlockWriter();

    class   ItemSerializer;
    class   Serializer;
    
    constexpr Serializer  begin();
    constexpr Serializer  end() const;

  protected:
    size_t          offset_       {0};
    uint_least8_t   block_nr_     {0};
    uint_least8_t   block_filled_ {false};
    uint8_t         buffer_       [512];
  };

  class Writer::BlockWriter::Serializer : public IteratorProxy<ItemSerializer, BlockWriter>
  {
  public:
    using IteratorProxy<ItemSerializer, BlockWriter>::IteratorProxy;
    constexpr bool operator != (const IteratorProxy&) const { return !((arg_->offset_ >= sizeof(buffer_)) || (arg_->block_filled_)); }
  };

  class Writer::BlockWriter::ItemSerializer : public ::ecpp::Serializer<::ecpp::BinaryStruct >
  {
  protected:
    using BaseClass = ::ecpp::Serializer<::ecpp::BinaryStruct >;

  public:
    constexpr ItemSerializer(Writer::BlockWriter &writer) : BaseClass( writer.buffer_, writer.offset_), writer_(writer) {};

    ~ItemSerializer()
    {
      if(BaseClass::OverflowOccured())
        writer_.block_filled_ = true;
      else
        writer_.offset_ = BaseClass::offset_;
    }
  
    Writer::BlockWriter & writer_;
  };

  constexpr Writer::BlockWriter::Serializer
  Writer::BlockWriter::begin()
  {
    return Serializer(this);
  }

  constexpr Writer::BlockWriter::Serializer
  Writer::BlockWriter::end() const
  {
    return Serializer(nullptr);
  }

};
#endif