/*
 *  Copyright 2020-2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_STORAGE_PACKEDBINARYFORMAT_HPP_
#define MUSICBOX_STORAGE_PACKEDBINARYFORMAT_HPP_

#include "Database/ServiceDatabase.hpp"
#include <cstdint>
#include <functional>

namespace MusicBox::Storage
{
  class PackedBinaryFormat
  {
  public:
    enum class BlockTypes   : uint8_t
    {
      Undefined   = 0,
      Header      = 1,
      Reserved    = 2,
      Services    = 3,
    };

    static constexpr size_t kHeaderVersionOffset = 0;
    static constexpr size_t kHeaderTypeOffset    = 1;
    static constexpr size_t kHeaderLengthOffset  = 2;
    static constexpr size_t kPayloadOffset       = 4;


    enum ServiceFlags : uint8_t
    {
      Unset   = 0x0,
      Preset  = 0x1,
    };

    template<typename Writer>
    void Pack(Writer &writer, const Database::ServiceValue &service_value)
    {
      writer << (uint16_t)service_value.ServiceId
            << (uint16_t)service_value.CId
            << (uint16_t)service_value.FMFreq
            << (uint8_t)service_value.DabChannel.getRaw()
            << service_value.Name.AsArray()
            << (uint8_t)(service_value.bPreset ? ServiceFlags::Preset : ServiceFlags::Unset);
    }

    template<typename Reader>
    bool Unpack(Reader &reader, Database::ServiceValue &service_value)
    {
      uint8_t flags = 0, channel = 0;

      reader >> (uint16_t&)service_value.ServiceId
             >> (uint16_t&)service_value.CId
             >> (uint16_t&)service_value.FMFreq
             >> (uint8_t&)channel
             >> (service_value.Name.AsArray())
             >> (uint8_t&)flags;

      if(reader.UnderflowOccured())
        return false;

      service_value.DabChannel.setRaw(channel);

      if (flags & ServiceFlags::Preset)
        service_value.bPreset = true;
      else
        service_value.bPreset = false;

      /* Sanity checks to cover typicall storage issues */
      if(service_value.FMFreq > (108000 / 10))
        service_value.FMFreq = 0;
      else if(service_value.FMFreq < (87500 / 10))
        service_value.FMFreq = 0;


      return true;
    }


  };
}

#endif