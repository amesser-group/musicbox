/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Storage/BlockStorage.hpp"
#include "Database/Service.hpp"
#include "Database/ServiceHandle.hpp"
#include "ecpp/Datatypes.hpp"
#include "System.hpp"
#include <cstring>
#include <cstddef>

using namespace ::MusicBox::Storage::BlockStorage;

void
Reader::Load()
{
  ReadHeader();

  if(services_block_nr_ > 0)
    ReadServices();
}

void
Reader::ReadHeader()
{
  BlockReader b(0);

  if (b.GetBlockType() != BlockTypes::Header)
    return;

  if (b.GetBlockVersion() > 2)
    return;

  for(auto deserializer : b)
  {
    uint8_t dummy;

    deserializer >> (uint8_t&)dummy
                 >> (uint8_t&)services_block_nr_
                 >> (uint8_t&)raw_service_handle_
                 >> (uint8_t&)dummy;
  }
}

void
Reader::ReadServices()
{
  auto & db = Database::ServiceDatabase::instance();
  uint_fast8_t i;

  auto next_block_nr = services_block_nr_;

  i = 1;
  while(db.IsValidServiceHandle(Database::ServiceHandle(i)))
  {
    BlockReader b(next_block_nr);
    
    if (b.GetBlockType() != BlockTypes::Services)
      break;
  
    if (b.GetBlockVersion() != 1)
      break;

    for(auto deserializer : b)
    {
      Database::ServiceValue val {};

      if(!Unpack(deserializer, val))
        break;

      db.UpdateService(val, Database::ServiceHandle(i));
      ++i;
    }

    next_block_nr++;
  }
}


void
Writer::SaveState()
{
  auto & storage = sys::Bsp::instance().storage_provider_;

  services_block_nr_ = 1;
  WriteHeader();

  storage.Sync();
}


void
Writer::Save()
{
  auto & storage = sys::Bsp::instance().storage_provider_;

  /* the first block (block_nr = 0) will be written the
   * header at the end */
  next_block_nr_ = 1;
  WriteServices();

  WriteHeader();

  storage.TrimUntilEnd(next_block_nr_);
}

void
Writer::WriteHeader()
{
  BlockWriter b(0, BlockTypes::Header, 2);
  auto serializer = *(b.begin());

  serializer << (uint8_t)0
             << (uint8_t)services_block_nr_
             << (uint8_t)::sys::globals().si468x_driver_.GetCurrentServiceHandle().GetRaw()
             << (uint8_t)0;
}


void
Writer::WriteServices()
{
  auto const & db = Database::ServiceDatabase::instance();
  uint_fast8_t i;

  services_block_nr_ = next_block_nr_;

  i = 1;
  for(auto const * ptr = db.GetService(Database::ServiceHandle(i)); ptr != nullptr;)
  {
    for(auto serializer : BlockWriter(next_block_nr_, BlockTypes::Services, 1))
    {
      Pack(serializer, *ptr);

      if (serializer.OverflowOccured())
        break;

      ++i;
      if((ptr = db.GetService(Database::ServiceHandle(i))) == nullptr)
        break;
    }
    next_block_nr_++;
  }
}

Writer::BlockWriter::BlockWriter(uint_least8_t block_nr, BlockTypes typ, uint8_t version)
{
  block_nr_ = block_nr;
  buffer_[kHeaderTypeOffset]    = static_cast<uint8_t>(typ);
  buffer_[kHeaderVersionOffset] = version;
  offset_ = kPayloadOffset;
}

Writer::BlockWriter::~BlockWriter()
{
  if (offset_ < sizeof(buffer_))
    memset(buffer_ + offset_, 0xff, sizeof(buffer_) - offset_);

  buffer_[kHeaderLengthOffset]   = static_cast<uint8_t>((offset_ - kPayloadOffset) & 0xFF);
  buffer_[kHeaderLengthOffset+1] = static_cast<uint8_t>(((offset_ - kPayloadOffset) >> 8) & 0xFF);

  sys::Bsp::instance().storage_provider_.WriteSector(block_nr_, buffer_);
}

Reader::BlockReader::BlockReader(uint_least8_t block_nr)
{
  sys::Bsp::instance().storage_provider_.ReadSector(block_nr, buffer_);
  offset_ = kPayloadOffset;
}

Reader::BlockReader::Deserializer
Reader::BlockReader::begin()
{
  offset_ = kPayloadOffset;
  return Deserializer(this);
}
