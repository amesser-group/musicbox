/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Storage/PackedStorage.hpp"
#include "Database/Service.hpp"
#include "Database/ServiceHandle.hpp"
#include "ecpp/Datatypes.hpp"
#include "System.hpp"
#include <cstring>
#include <cstddef>

using namespace ::MusicBox::Storage::PackedStorage;


class Reader::TLVReader
{
public:
  TLVReader();

  class ReadIterator;

  ReadIterator begin();
  constexpr ReadIterator end();

  bool  ReadNextTLV();

  constexpr auto GetBlockType()    const { return tlv_type_; }
  constexpr auto GetBlockVersion() const { return tlv_version_; }
protected:
  size_t          storage_offset_ {0};
  uint_least8_t   tlv_version_    {0};
  BlockTypes      tlv_type_       {0};

  /** this is the full tlv length of currently loaded tlv */
  uint_least16_t  tlv_length_     {0};
};

Reader::TLVReader::TLVReader()
{
  ReadNextTLV();
}

bool
Reader::TLVReader::ReadNextTLV()
{
  auto & s = ::sys::Bsp::instance().storage_provider_;

  storage_offset_ += tlv_length_;
  tlv_type_       =  BlockTypes::Undefined;
  tlv_version_    =  0;
  tlv_length_     =  0;

  /* read header of tlv */
  if(s.Read(storage_offset_, kPayloadOffset) != kPayloadOffset)
    return false;

  auto const & data = s.GetStruct();

  tlv_version_ = data.GetField8(kHeaderVersionOffset);
  tlv_type_    = static_cast<BlockTypes>(data.GetField8(kHeaderTypeOffset));
  tlv_length_  = data.GetField16(kHeaderLengthOffset) + kPayloadOffset;

  return true;
}


class Reader::TLVReader::ReadIterator
{
public:
  class ItemDeserializer;

  constexpr ReadIterator(size_t storage_offset, size_t storage_len) : 
    storage_offset_(storage_offset), storage_len_(storage_len) {}

  ItemDeserializer operator*();
  constexpr bool operator !=(const ReadIterator& rhs) { return storage_len_ > 0; }
  constexpr void operator ++() const {};

protected:
  void Refill();

  size_t    storage_offset_;
  size_t    storage_len_;
  size_t    deserializer_offset_ {0};
};

Reader::TLVReader::ReadIterator 
Reader::TLVReader::begin()
{
  auto & s = ::sys::Bsp::instance().storage_provider_;

  if(tlv_length_ >= kPayloadOffset)
  {
    s.Read(storage_offset_ + kPayloadOffset, tlv_length_ - kPayloadOffset);
    return ReadIterator(storage_offset_ + kPayloadOffset, tlv_length_ - kPayloadOffset);
  }
  else
  {
    return ReadIterator(storage_offset_, 0);
  }
}

constexpr Reader::TLVReader::ReadIterator 
Reader::TLVReader::end()
{
  return ReadIterator(storage_offset_, 0);
}

class Reader::TLVReader::ReadIterator::ItemDeserializer : public ::ecpp::Deserializer<::ecpp::BinaryStruct>
{
protected:
  using Base = ::ecpp::Deserializer<::ecpp::BinaryStruct>;
public:
  constexpr ItemDeserializer(TLVReader::ReadIterator &read_iterator, const ::ecpp::BinaryStruct & data):
    Base(data, read_iterator.deserializer_offset_), read_iterator_(read_iterator) {}
  ~ItemDeserializer();

protected:
  TLVReader::ReadIterator & read_iterator_;
};

Reader::TLVReader::ReadIterator::ItemDeserializer::~ItemDeserializer()
{
  if(Base::offset_ >= Base::GetBufferSize())
    read_iterator_.Refill();
  else
    read_iterator_.deserializer_offset_ = Base::offset_;
}

Reader::TLVReader::ReadIterator::ItemDeserializer
Reader::TLVReader::ReadIterator::operator*()
{
  return { *this, ::sys::Bsp::instance().storage_provider_.GetStruct() };
}

void
Reader::TLVReader::ReadIterator::Refill()
{
  if(deserializer_offset_ >= storage_len_)
  {
    storage_offset_ += storage_len_;
    storage_len_     = 0;
  }
  else
  {
    storage_offset_ += deserializer_offset_;
    storage_len_    -= deserializer_offset_;
    deserializer_offset_ = 0;

    if(::sys::Bsp::instance().storage_provider_.Read(storage_offset_, storage_len_) == 0)
      storage_len_ = 0;
  }
}

void
Reader::Load()
{
  TLVReader reader;
  
  if(!ReadHeader(reader))
    return;

  reader.ReadNextTLV();

  ReadServices(reader);
}

bool
Reader::ReadHeader(TLVReader &tlv)
{
  if (tlv.GetBlockType() != BlockTypes::Header)
    return false;

  if (tlv.GetBlockVersion() > 2)
    return false;

  {
    auto it = tlv.begin();
    auto deserializer = *it;
    uint8_t dummy;

    deserializer >> (uint8_t&)dummy
                 >> (uint8_t&)dummy
                 >> (uint8_t&)raw_service_handle_
                 >> (uint8_t&)dummy;
  }

  return true;
}

void
Reader::ReadServices(TLVReader &tlv)
{
  auto & db = Database::ServiceDatabase::instance();
  uint_fast8_t i;

  if (tlv.GetBlockType() != BlockTypes::Services)
    return;

  if (tlv.GetBlockVersion() != 1)
    return;

  i = 1;
  for(auto deserializer : tlv)
  {
    Database::ServiceValue val {};

    if(!db.IsValidServiceHandle(Database::ServiceHandle(i)))
      break;

    if(Unpack(deserializer, val))
    {
      db.UpdateService(val, Database::ServiceHandle(i));
      ++i;
    }
  }
}


class Writer::TLVWriter
{
public:
  constexpr TLVWriter() {};
  class TLV;
protected:
  size_t          storage_offset_ {0};
};

class Writer::TLVWriter::TLV
{
public:
  constexpr TLV(TLVWriter& parent, BlockTypes tlv_type, uint_least8_t tlv_version) :
    parent_(parent), tlv_type_(tlv_type), tlv_version_(tlv_version) {}

  ~TLV();

  class WriteIterator;

  WriteIterator begin();
  WriteIterator end();
protected:
  TLVWriter&      parent_;

  BlockTypes      tlv_type_       {0};
  uint_least8_t   tlv_version_    {0};

  /* this is the length of the whole tlv including the header */
  uint_least16_t  tlv_len_        {0}; 
};

Writer::TLVWriter::TLV::~TLV()
{
  auto & s = ::sys::Bsp::instance().storage_provider_;

  /* write header if required */
  if (tlv_len_ > kPayloadOffset)
  {
    s.GetStruct().SetField8(kHeaderVersionOffset, tlv_version_);
    s.GetStruct().SetField8(kHeaderTypeOffset,    static_cast<uint8_t>(tlv_type_));
    s.GetStruct().SetField16(kHeaderLengthOffset,  tlv_len_ - kPayloadOffset);

    s.Write(parent_.storage_offset_, kPayloadOffset);
    parent_.storage_offset_ += tlv_len_;
  }
}
class Writer::TLVWriter::TLV::WriteIterator
{
public:
  class ItemSerializer;

  constexpr WriteIterator(TLV& tlv, size_t serializer_offset): tlv_(tlv), serializer_offset_(serializer_offset) {}
  ~WriteIterator();

  constexpr void operator ++() const {};
  /** TODO: we need a length check for storage here */
  constexpr bool operator != (const WriteIterator& rhs) { return true; }

  ItemSerializer operator*();
protected:
  void         Write();

  TLV&         tlv_;
  size_t       serializer_offset_;
};

class Writer::TLVWriter::TLV::WriteIterator::ItemSerializer : public ::ecpp::Serializer<::ecpp::BinaryStruct>
{
protected:
  using Base = ::ecpp::Serializer<::ecpp::BinaryStruct>;
public:
  constexpr ItemSerializer(TLV::WriteIterator &write_iterator, const ::ecpp::BinaryStruct & data):
    Base(data, write_iterator.serializer_offset_), write_iterator_(write_iterator) {}

  ~ItemSerializer();
protected:
  TLV::WriteIterator & write_iterator_;
};


Writer::TLVWriter::TLV::WriteIterator::~WriteIterator()
{
  if(serializer_offset_ > 0)
    Write();
}

void
Writer::TLVWriter::TLV::WriteIterator::Write()
{
  auto & s = ::sys::Bsp::instance().storage_provider_;

  s.Write(tlv_.parent_.storage_offset_ + tlv_.tlv_len_ ,serializer_offset_);

  tlv_.tlv_len_ += serializer_offset_;
  serializer_offset_ = 0;
}

Writer::TLVWriter::TLV::WriteIterator::ItemSerializer
Writer::TLVWriter::TLV::WriteIterator::operator*()
{
  return {*this, ::sys::Bsp::instance().storage_provider_.GetStruct()};
}

Writer::TLVWriter::TLV::WriteIterator::ItemSerializer::~ItemSerializer()
{
  if(Base::OverflowOccured())
  {
    write_iterator_.Write();
  }
  else
  {
    write_iterator_.serializer_offset_ = Base::offset_;
  }
}

Writer::TLVWriter::TLV::WriteIterator
Writer::TLVWriter::TLV::begin()
{
  tlv_len_ = kPayloadOffset;
  return WriteIterator(*this, 0);
}

Writer::TLVWriter::TLV::WriteIterator
Writer::TLVWriter::TLV::end()
{
  return WriteIterator(*this, 0);
}


void
Writer::SaveState()
{
  TLVWriter writer;

  WriteHeader(writer);
}


void
Writer::Save()
{
  TLVWriter writer;

  WriteHeader(writer);

  WriteServices(writer);
}

void
Writer::WriteHeader(TLVWriter& writer)
{
  auto tlv = TLVWriter::TLV(writer, BlockTypes::Header, 2);
  auto it  = tlv.begin();

  (*it) << (uint8_t)0
        << (uint8_t)0
        << (uint8_t)::sys::globals().si468x_driver_.GetCurrentServiceHandle().GetRaw()
        << (uint8_t)0;
}

void
Writer::WriteServices(TLVWriter& writer)
{
  auto const & db = Database::ServiceDatabase::instance();
  auto tlv = TLVWriter::TLV(writer, BlockTypes::Services, 1);
  uint_fast8_t i;

  i = 1;
  for(auto serializer : tlv)
  {
    auto const * ptr = db.GetService(Database::ServiceHandle(i));

    if(ptr == nullptr)
      break;

    Pack(serializer, *ptr);

    if (!serializer.OverflowOccured())
      ++i;
  }
}
