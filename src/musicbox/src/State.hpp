/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_STATE_HPP_
#define MUSICBOX_STATE_HPP_

#include <cstdint>

namespace MusicBox
{
  class OperatingState
  {
  public:
    static const OperatingState kStandBy;
    static const OperatingState kPlayRadio;

    constexpr OperatingState(const OperatingState & init) : state_val_(init.state_val_) {}

    constexpr bool operator==(const OperatingState & rhs) { return state_val_ == rhs.state_val_; }

    /* enables switch case */
    constexpr operator uint_least8_t() const { return state_val_; }
    explicit operator bool() const = delete;

  protected:
    constexpr OperatingState(uint_least8_t state_val) : state_val_(state_val) {}
    uint_least8_t state_val_;
  };

  constexpr OperatingState OperatingState::kStandBy     {0};
  constexpr OperatingState OperatingState::kPlayRadio   {1};
}    

#endif