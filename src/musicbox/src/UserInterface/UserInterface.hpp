/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_UI_USERINTERFACE_HPP_
#define MUSICBOX_UI_USERINTERFACE_HPP_

#include "ecpp/System/TickTimer.hpp"

#include "State.hpp"

#include "UserInterface/events.hpp"
#include "UserInterface/Widgets.hpp"

#include <array>

namespace MusicBox::UserInterface
{  
  using ::ecpp::Units::MilliSecond;
  using ::ecpp::Units::Second;

  class UserInterface : public WidgetEnvironment
  {
  public:
    static constexpr MilliSecond<>   kPollUserInputInterval   {40};
    static constexpr MilliSecond<>  kUserInputInactiveTimeout {10000};

    void DispatchEventValue(WidgetEnvironment::EventValue event_value);

    void Refresh();
    void Refresh(::ecpp::System::FlexibleTimeout delay);

    void HandleUserInput();
    void ResetInteractionTime();

    void HandleEvent(const typename WidgetEnvironment::Event &event);

    bool IsUserIdle() const { return !user_input_active_; };

    void Start();
    void Reset();

    void HandleChange(OperatingState state);
    void HandleKey(events::KeyEvent       key);
    void HandleKey(events::KeyEvent::Back key) { PopWidget(); Refresh(); };

    static void EnterStandbyAction(const WidgetEnvironment::Context &ctx);
  protected:
    ::std::array<WidgetStackItem, 4> widget_stack_;
    uint_least8_t                    widget_stack_depth_ {0};

    ::ecpp::System::TickTimer        user_last_active_tmr_ {};
    uint_least8_t                    user_input_active_    {false};

    ECPP_JOB_TIMER_DECL(DrawScreen); 
    ECPP_JOB_TIMER_DECL(PollUserInput);

    void PopWidget();

    WidgetEnvironment::Widget& GetCurrentWidget();

    friend WidgetEnvironment::Context;
  };

  template<typename Widget, typename... ARGS>
  void WidgetEnvironment::Context::PushWidget(ARGS &&... args) const
  {
    if((ui_.widget_stack_depth_ + 1U) >= ui_.widget_stack_.max_size())
      return;

    if((ui_.widget_stack_depth_ > 0) ||
        ::std::get_if<Model::IdleWidget>(&(ui_.widget_stack_[0])) == nullptr)
      ui_.widget_stack_depth_ += 1;

    EmplaceWidget<Widget>(::std::forward<ARGS>(args)...);
  }

  template<typename Widget, typename... ARGS>
  void WidgetEnvironment::Context::EmplaceWidget(ARGS && ... args) const
  {
    ui_.widget_stack_[ui_.widget_stack_depth_].template emplace<Widget>(::std::forward<ARGS>(args)...);
    ui_.Refresh();
  }
}

namespace signals
{
  void UserShutdown();
}

#endif

