/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface;

WidgetEnvironment::Widget&
UserInterface::GetCurrentWidget()
{
  return std::visit([](auto &&arg) -> WidgetEnvironment::Widget & { 
    return arg;
  }, widget_stack_[widget_stack_depth_]);
}  

void
UserInterface::Refresh()
{
  DrawScreen.Enqueue();
}


void 
UserInterface::Refresh(::ecpp::System::FlexibleTimeout delay)
{
  DrawScreen.Start(delay);
}

void
UserInterface::HandleUserInput()
{
  PollUserInput.Start(MilliSecond<>(0));
}

void
UserInterface::ResetInteractionTime()
{
  user_last_active_tmr_.Start();
  user_input_active_ = true;

  if(!PollUserInput.IsRunning())
    PollUserInput.Start(kPollUserInputInterval);
}

void
UserInterface::DispatchEventValue(WidgetEnvironment::EventValue event_value)
{
  auto const painter = CreatePainter();
  GetCurrentWidget().HandleEvent({event_value}, { {0,0, painter.width(), painter.height()}, *this});
}

void
UserInterface::PopWidget()
{
  if(widget_stack_depth_ > 0 )
  {
    widget_stack_[widget_stack_depth_].template emplace<::MusicBox::UserInterface::Model::DummyWidget>();
    widget_stack_depth_--;
  }

  if(widget_stack_depth_ == 0 )
  {
    if (::sys::globals().operating_state() == OperatingState::kStandBy)
      widget_stack_[0].emplace<Model::StandByWidget>();
    else
      widget_stack_[0].emplace<Model::IdleWidget>();
  }
}

void
UserInterface::Reset()
{
  do {
    PopWidget();
  } while(widget_stack_depth_ > 0 );
  
  Refresh();
}

void 
UserInterface::HandleEvent(const WidgetEnvironment::Event &event)
{
  if(event.GetIf<events::UserInteractionTimeout>())
    Reset();
}

void
UserInterface::Start()
{
  widget_stack_[0].emplace<Model::IdleWidget>();

  PollUserInput.Enqueue();
  DrawScreen.Enqueue();
}


ECPP_JOB_DEF(UserInterface, DrawScreen)
{
  if (::sys::bsp().display_.IsUpdatePending())
  {
    DrawScreen.Start(MilliSecond<>(40));
  }
  else
  {
    auto painter = CreatePainter();

    painter.SetEditLocation({0, 0},{0, 0}, Widget::EditingMode::kEditingModeNone);
    painter.Clear();
    //RedrawTimeout = 0;

    GetCurrentWidget().HandleEvent({events::Refresh()}, { {0,0, painter.width(), painter.height()}, *this});
    GetCurrentWidget().Draw(painter);

    //LastDrawTime = ::sys::GetTicks();

    ::sys::bsp().display_.Update();
    DrawScreen.Start(Seconds(1));
  }  
}

void 
UserInterface::HandleKey(events::KeyEvent key)
{
  if(key == key.kVolumePlus)
  {
    /* actual volume change is currently handled by bsp itself */
  }
  else if(key == key.kVolumeMinus)
  {
    /* actual volume change is currently handled by bsp itself */
  }
  else
  {
    user_last_active_tmr_.Start();
    user_input_active_ = true;
    DispatchEventValue(key);
    Refresh();
  }
}

ECPP_JOB_DEF(UserInterface, PollUserInput)
{
  auto ticks = ::sys::bsp().HandleUserInput(::sys::globals());

  if (user_input_active_)
  {
    if (user_last_active_tmr_.IsExpired(kUserInputInactiveTimeout))
    {
      user_input_active_ = false;
      DispatchEventValue(events::UserInteractionTimeout());
    }
    else
    {
      if (!ticks)
        ticks = kPollUserInputInterval;
    }
  }

  if(ticks > 0)
    PollUserInput.Continue(kPollUserInputInterval);
}

void
UserInterface::HandleChange(OperatingState state)
{
  Reset();
}

void UserInterface::EnterStandbyAction(const WidgetEnvironment::Context &ctx)
{
  signals::UserShutdown();
}