/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Model/WakeAlarmWidgets.hpp"
#include "ecpp/ArrayString.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;
using namespace ::MusicBox::UserInterface;


void 
WakeAlarmSettingsWidget::DrawDayList(typename WidgetEnvironment::Painter &painter, const Alarm::Setpoint & sp, State state)
{
  using Font = WidgetEnvironment::Font<WakeAlarmSettingsWidget>;
  static const char daychars[] = "SMDMDFS";
  uint_least8_t selected_day;

  switch(state)
  {
  case kSelectSunday:    selected_day = 0; break;
  case kSelectMonday:    selected_day = 1; break;
  case kSelectTuesday:   selected_day = 2; break;
  case kSelectWednesday: selected_day = 3; break;
  case kSelectThursday:  selected_day = 4; break;
  case kSelectFriday:    selected_day = 5; break;
  case kSelectSaturday:  selected_day = 6; break;
  default: selected_day = 7; break;
  }

  for(uint_least8_t day = 0; day < 7 ; ++day)
  {
    auto f = painter.CreateField({day*10,0,(day+1)*10,painter.height()});
    bool enabled = sp.IsEnabled(::ecpp::Chrono::WeekDay(day));
    ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8,2> text;

    text = daychars[day];
    f.CenterText<Font>(text);

    if (selected_day == day)
    {
      f.CreateField({1,1,f.width() - 1, f.height() - 1}).Invert();

      if(!enabled)
        f.Invert();
    }
    else
    {
      if(enabled)
        f.Invert();
    }
  }  
}


void 
WakeAlarmSettingsWidget::Draw(typename WidgetEnvironment::Painter &painter) const
{
  using Font = WidgetEnvironment::Font<decltype(*this)>;

  ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8,18> text;
  const auto & sp = alarm_.setpoint();


  painter.CreateField({0,0, painter.width(), Font::GetHeight()}).CenterText<Font>("Wecker");

  {
    auto row = painter.CreateField({0, Font::GetHeight(), painter.width(), Font::GetHeight()*2});
    uint_least8_t split_pos = 60;

    if(sp.IsEnabled())
    {
      auto f = row.CreateField({split_pos,0,row.width(), row.height()});
      text = "Alarm <an>";

      DrawDayList(f, sp, state_);
    }
    else
    {
      text = "Alarm <aus>";
    }

    row.PutText<Font>(text);

    if(kSelectEnable == state_)
      row.SetEditLocation({0,0,split_pos, row.height()}, EditingMode::kEditingModeSelected);
  }


  {
    auto row = painter.CreateField({0, Font::GetHeight()*2, painter.width(), Font::GetHeight()*3});
    constexpr uint_least8_t clock_center = 64;

    text = "Zeit";
    row.PutText<Font>(text);

    text = ":";
    auto w = row.GetStringRect<Font>(text).GetWidth() + 1;
    row.CreateField({clock_center - w/2, 0 , clock_center - w/2 + w, row.GetHeight()}).PutText<Font>(text);

    text.IPrintF("%02d", sp.hour);
    auto wh = row.GetStringRect<Font>(text).GetWidth();
    row.CreateField({clock_center - wh - w/2, 0 , clock_center - w/2, row.GetHeight()}).PutText<Font>(text);

    if(state_ == kSelectHour)
      row.SetEditLocation({clock_center - wh - w/2-1, 0 , clock_center - w/2, row.GetHeight()}, EditingMode::kEditingModeSelected);
    else if(state_ == kAdjustHour)
      row.SetEditLocation({clock_center - wh - w/2-1, 0 , clock_center - w/2, row.GetHeight()}, EditingMode::kEditingModeAdjusting);

    text.IPrintF("%02d", sp.minute);
    auto wm = row.GetStringRect<Font>(text).GetWidth();
    row.CreateField({clock_center - w/2 + w, 0 , clock_center - w/2 + w + wm, row.GetHeight()}).PutText<Font>(text);

    if(state_ == kSelectMinute)
      row.SetEditLocation({clock_center - w/2 + w-1, 0 , clock_center - w/2 + w + wm, row.GetHeight()}, EditingMode::kEditingModeSelected);
    else if(state_ == kAdjustMinute)
      row.SetEditLocation({clock_center - w/2 + w-1, 0 , clock_center - w/2 + w + wm, row.GetHeight()}, EditingMode::kEditingModeAdjusting);
  }

  {
    auto row = painter.CreateField({0, painter.height() - Font::GetHeight(), painter.width(), painter.height()});
    row.DrawTextItem<WidgetEnvironment, Font>("Speichern", state_ == kSelectBack);
  }
}
