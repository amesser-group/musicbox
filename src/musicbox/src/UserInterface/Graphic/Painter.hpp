/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_GRAPHIC_TEXTPAINTER_HPP_
#define MUSICBOX_USERINTERFACE_GRAPHIC_TEXTPAINTER_HPP_

#include "ecpp/Drivers/BufferedDisplayDriver.hpp"

namespace MusicBox::UserInterface
{
  /* some forward declarations */
  namespace Model
  {
    class Si468xAutoAntCapTuningWidget;
    class Si468xManualAntCapTuningWidget;
    class ServiceListItem;
  }

  namespace Graphic
  {
    template<typename BasePainter>
    class Painter : public BasePainter
    {
    public:
      using typename BasePainter::Rect;

      using BasePainter::BasePainter;

      using BasePainter::CreateField;
      using BasePainter::GetWidth;
      using BasePainter::GetHeight;
      using BasePainter::Invert;
      using BasePainter::operator[];


      template<typename Base>
      constexpr Painter(const Base & init) : BasePainter{init} {}

      constexpr Painter CreateField(Rect rect) 
      { 
        return {BasePainter::CreateField(rect)};
      }      

      void Draw(const Model::Si468xAutoAntCapTuningWidget &widget);
      void Draw(const Model::Si468xManualAntCapTuningWidget &widget);
      void Draw(const Model::ServiceListItem &item, bool selected);

      using BasePainter::Draw;
    };
  }
}

#endif

