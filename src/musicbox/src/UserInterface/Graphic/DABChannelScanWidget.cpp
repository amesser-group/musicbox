/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"
#include "Drivers/SI468x/Actions/ScanDAB.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;

using MusicBox::Drivers::SI468x::Actions::Si468xScanDABAction;
using MusicBox::Drivers::SI468x::Si468xOperationMode;

void
DABChannelScanWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  auto p = ::sys::globals().si468x_actions_.Action<Si468xScanDABAction>().Get();
  int16_t rssi = 0;

  ::sys::globals().user_interface_.Refresh(MilliSecond<>(100));

  using Font = WidgetEnvironment::Font<decltype(*this)>;

  painter.CreateField({0,0, painter.GetWidth(), 16}).CenterText<Font>("DAB Kanalsuche");

  auto const & s = ::sys::globals().si468x_driver_;

  if (s.getOpMode() == Si468xOperationMode::DABRadio)
  {
    auto const & r  = s.getDABManager();
    rssi = static_cast<int16_t>(r.getRssi());
  }

  if(p != nullptr)
  {
    const auto & transponder_list = ::MusicBox::Database::ServiceDatabase::instance().getDabChannelList();
    unsigned offset = 16;

    for(auto & entry : p->GetHistory())
    {
      auto idx = entry.channel_index_;

      if( (idx >= 0) && (idx < transponder_list.MaxTransponders))
      {
        auto t = transponder_list[idx].Handle.ToString();
        auto field = painter.CreateField({0,offset, painter.GetWidth(), offset + 16});

        if (entry.service_cnt_ < 0)
          field.IPrintF<Font>("%s: RSSI %3d ...", t.data(), rssi);
        else
          field.IPrintF<Font>("%s: RSSI %3d Sender: %2d", t.data(), entry.rssi_, entry.service_cnt_);

        offset += 16;
      }

      if(offset >= 48)
        break;
    }

    auto field = painter.CreateField({0,48, painter.GetWidth(), 64});

    if( p->IsScanning())
      field.IPrintF<Font>("Scanne (%d Sender) ...", p->GetServicesFoundCnt());
    else
      field.IPrintF<Font>("Fertig (%d Sender)", p->GetServicesFoundCnt());
  }

}
