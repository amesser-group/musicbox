/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Graphic/Painter.hpp"
#include "UserInterface/WidgetEnvironment.hpp"
#include "UserInterface/Model/ServiceListModel.hpp"

using namespace MusicBox::UserInterface;
using MusicBox::UserInterface::Graphic::Painter;

template<typename BasePainter>
void Painter<BasePainter>::Draw(const Model::ServiceListItem &item, bool selected)
{
  using Font = WidgetEnvironment::Font<::std::decay_t<decltype(item)>>;

  auto & value = (*item);

  CreateField({1, 0, GetWidth() - 1, GetHeight()}).template PutText<Font>(value.Name);

  if (selected)
    Invert();
}

template class Painter<::ecpp::UserInterface::Graphic::Painter<::sys::Bsp::Display::Painter>>;