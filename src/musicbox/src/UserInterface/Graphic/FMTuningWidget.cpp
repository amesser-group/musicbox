/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"
#include "System.hpp"

#include "ecpp/ArrayString.hpp"

using namespace MusicBox::UserInterface::Model;
using MusicBox::Drivers::SI468x::TunerStateType;

void
FMTuningWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  auto & si = sys::globals().si468x_driver_;

  using Font = WidgetEnvironment::Font<decltype(*this)>;

  {
    char buf[20];
    auto freq = si.getTunedFreq();

    snprintf(buf, sizeof(buf), "%3u.%02u MHz", (unsigned)(freq/1000), (unsigned)(freq%1000/10));
    painter.CreateField( {0,0 , painter.GetWidth(), 16}).CenterText<Font>(buf);
  }

  if (TunerStateType::Tuned == si.getTunerState())
  {
    const auto & rds_state = si.getRDSState();

    if(rds_state.isProgramServiceNameValid())
    {
      ::ecpp::ArrayString<ecpp::StringEncodings::Utf8, 16> rds_name = rds_state.program_service_name ;
      painter.CreateField( {0,24 , painter.GetWidth(), 40}).CenterText<Font>(rds_name);
    }
    else
    {
      painter.CreateField( {0,24 , painter.GetWidth(), 40}).CenterText<Font>("<Unbekannt>");
    }
  }
  else
  {
    painter.CreateField( {0,24 , painter.GetWidth(), 40}).CenterText<Font>("<Kein Signal>");
  }
}
