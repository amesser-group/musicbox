/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"
#include "System.hpp"

using namespace ::MusicBox::UserInterface::Model;

using ::MusicBox::Drivers::SI468x::Si468xOperationMode;
using ::MusicBox::OperatingState;

const ActionListItem IdleWidget::kRadioQuickActions[2] =
{
  { u8"Hauptmenü",  [](const WidgetEnvironment::Context &ctx)  { ctx.SpawnMainMenu(); }},
  { u8"Abschalten", UserInterface::EnterStandbyAction },
};

const ActionListItem IdleWidget::kDABQuickActions[2] =
{
  { u8"Hauptmenü",  [](const WidgetEnvironment::Context &ctx)  { ctx.SpawnMainMenu(); }},
  { u8"Abschalten", UserInterface::EnterStandbyAction },
};

const ActionListItem IdleWidget::kFMQuickActions[4] =
{
  { "Suche vorwärts",  [](const WidgetEnvironment::Context &ctx) { sys::globals().si468x_driver_.seek(true); }},
  { "Suche rückwärts", [](const WidgetEnvironment::Context &ctx) { sys::globals().si468x_driver_.seek(false); }},
  { "Hauptmenü",       [](const WidgetEnvironment::Context &ctx) { ctx.SpawnMainMenu(); }},
  { "Abschalten",      UserInterface::EnterStandbyAction },
};

IdleWidget::IdleWidget() : WidgetEnvironment::Widget(), quick_actions_{ActionListModel(kRadioQuickActions)}
{

}

void
IdleWidget::HandleEvent(const WidgetEnvironment::Event &event, const WidgetEnvironment::Context &ctx)
{
  event.Visit([this,&ctx](auto &&arg) { 
    using T = std::decay_t<decltype(arg)>;

    if constexpr (std::is_base_of<events::StatusChange, T>::value)
    {
      ctx.Invalidate(this);
    }
    else if constexpr (std::is_base_of<events::UserInteractionTimeout, T>::value)
    {
      ctx.Invalidate(this);
    }
    else if constexpr (std::is_base_of<events::Refresh, T>::value)
    {
      UpdateQuickActions();
    }
  });

  WidgetEnvironment::Widget::HandleEvent(event, ctx);
}

void
IdleWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  auto user_idle = ::sys::globals().user_interface_.IsUserIdle();

  switch(user_input)
  {
  case events::KeyEvent::kClicked:
    if(user_idle)
      ctx.SpawnMainMenu();
    else
      quick_actions_.GetSelectedItem()->Activate(ctx);
    break;
  case events::KeyEvent::kUp:
    if(!user_idle)
      quick_actions_.SetSelectedItem(++quick_actions_.GetSelectedItem());
    break;
  case events::KeyEvent::kDown:
    if(!user_idle)
      quick_actions_.SetSelectedItem(--quick_actions_.GetSelectedItem());
    break;
  default:
    break;
  }
}

void IdleWidget::UpdateQuickActions()
{
  const auto & mgr = sys::globals().si468x_driver_;
  auto model = quick_actions_;
    
  switch(mgr.getMode())
  {
  case Si468xOperationMode::FMRadio: 
    model = Model::ActionListModel(kFMQuickActions);
    break;
  case Si468xOperationMode::DABRadio:
    model = Model::ActionListModel(kDABQuickActions);
    break;
  default:
    model = Model::ActionListModel(kRadioQuickActions);
    break;
  }

  if(model != quick_actions_)
    quick_actions_ = model;
}