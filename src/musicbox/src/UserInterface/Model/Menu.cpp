/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"
#include "System.hpp"

namespace MusicBox::UserInterface::Model
{

  static const ActionListItem kRadioMenuItems[] =
  {
    {"Favoriten",       [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<ServiceListWidget>("Favoriten",   ServiceListModel::Filters::Preset); } },
    {"Alle Sender",     [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<ServiceListWidget>("Alle Sender", ServiceListModel::Filters::Any); } },
    {"FM Sender",       [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<ServiceListWidget>("FM Sender", ServiceListModel::Filters::FM); } },
    {"DAB Sender",      [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<ServiceListWidget>("DAB Sender", ServiceListModel::Filters::DAB); } },
    {"FM Sendersuche",  [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<FMTuningWidget>(); } },
    {"Zurück",          WidgetEnvironment::Context::Back },
  };

  void
  ContextMenuMixin::SpawnRadioMenu() const
  {
    auto & ctx = static_cast<const WidgetEnvironment::Context&>(*this);
    ctx.PushWidget<ActionListWidget>("Radio", kRadioMenuItems);

  }

  void
  ContextMenuMixin::SpawnAlarmClockMenu() const
  {
    auto & ctx = static_cast<const WidgetEnvironment::Context&>(*this);
    ctx.PushWidget<AlarmListWidget> ("Wecker", ::sys::GetApp(*this).wakealarms_);
  }

}