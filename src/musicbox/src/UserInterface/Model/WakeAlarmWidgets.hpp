/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_MODEL_ALARMLISTWIDGET_HPP_
#define MUSICBOX_USERINTERFACE_MODEL_ALARMLISTWIDGET_HPP_

#include "config/MusicBox.hpp"
#include "ecpp/UserInterface/Model/IterableListModel.hpp"
#include "ecpp/UserInterface/Model/ListWidget.hpp"
#include "UserInterface/WidgetEnvironment.hpp"

namespace MusicBox::UserInterface::Model
{
  class WakeAlarmSettingsWidget : public WidgetEnvironment::Widget
  {
  protected:
    enum State : uint_least8_t 
    {
      kSelectEnable,
      kSelectSunday,
      kSelectMonday,
      kSelectTuesday,
      kSelectWednesday,
      kSelectThursday,
      kSelectFriday,
      kSelectSaturday,
      kSelectHour,
      kAdjustHour,
      kSelectMinute,
      kAdjustMinute,
      kSelectBack,
    };
  public:
    WakeAlarmSettingsWidget(Alarm &alarm) : WidgetEnvironment::Widget(), alarm_(alarm) {}

    virtual void Draw(typename WidgetEnvironment::Painter &ctx) const override;
    virtual void HandleEvent(const events::KeyEvent user_input, const typename WidgetEnvironment::Context &ctx) override;

    static void DrawDayList(typename WidgetEnvironment::Painter &painter, const Alarm::Setpoint & sp, State state);
    static void FormatDayList(const Alarm::Setpoint & sp, ::ecpp::StringSpan<::ecpp::StringEncodings::Utf8>::Iterator begin, ::ecpp::StringSpan<::ecpp::StringEncodings::Utf8>::Iterator end);

  protected:
    Alarm &alarm_;
    State  state_{kSelectEnable};
  };

  class AlarmListEntry;


  class AlarmListEntry
  {
  public:
    constexpr AlarmListEntry():
      alarm_(kDummy) {}

    constexpr AlarmListEntry(Alarm &alarm):
      alarm_(alarm) {}

    template<typename Model>
    void Draw(WidgetEnvironment::Painter &painter, Model &model, bool selected);

    void Activate(const WidgetEnvironment::Context &ctx)
    {
      if(&alarm_ == &kDummy)
        ctx.PopWidget();
      else
        ctx.PushWidget<WakeAlarmSettingsWidget>(alarm_);
    }

  protected:
    Alarm &alarm_;

    static Alarm kDummy;
  };

  template<class Model>
  void 
  AlarmListEntry::Draw(WidgetEnvironment::Painter &painter, Model &model, bool selected)
  {
    ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8,18> text;

    if(&alarm_ == &kDummy)
    {
      text = "Zurück";
    }
    else
    {
      const auto & sp = alarm_.setpoint();

      if(sp.IsEnabled())
      {
        text.IPrintF("%02d:%02d ", sp.hour, sp.minute);
        WakeAlarmSettingsWidget::FormatDayList(sp, text.begin() + 6, text.begin() + 18);
      }
      else
      {
        text.IPrintF("%02d:%02d <aus>", sp.hour, sp.minute);
      }
    }

    painter.DrawTextItem<WidgetEnvironment>(text, selected);
  }

  template<class AlarmList>
  using AlarmListModel = ::ecpp::UserInterface::Model::IterableListModel<AlarmList, AlarmListEntry>;

  using AlarmListWidget = ::ecpp::UserInterface::Model::ListWidget<AlarmListModel<config::MusicBox::WakeAlarmList>, WidgetEnvironment>;
}
#endif