/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"
#include "System.hpp"

using namespace ::MusicBox::UserInterface::Model;
using ::MusicBox::UserInterface::WidgetEnvironment;

using ::MusicBox::Drivers::SI468x::Si468xOperationMode;

FMTuningWidget::FMTuningWidget() : WidgetEnvironment::Widget{}
{
  auto & mgr = ::sys::globals().si468x_driver_;
  mgr.activateMode(Si468xOperationMode::FMRadio);
}

void
FMTuningWidget::HandleEvent(const WidgetEnvironment::Event& event, const WidgetEnvironment::Context &ctx)
{
  event.Visit([this,&ctx](auto &&arg) { 
    using T = std::decay_t<decltype(arg)>;

    if constexpr (std::is_base_of<events::StatusChange, T>::value)
      ctx.Invalidate(this);
  });

  WidgetEnvironment::Widget::HandleEvent(event, ctx);
}

void
FMTuningWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  auto & si = sys::globals().si468x_driver_;
  switch(user_input)
  {
  case events::KeyEvent::kUp:
    si.seek(false);
    break;
  case events::KeyEvent::kDown:
    si.seek(true);
    break;
  case events::KeyEvent::kClicked:
    break;
  default:
    WidgetEnvironment::Widget::HandleEvent(user_input, ctx);
    break;
  }
}
