/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "UserInterface/Model/Si468xAntCapTuningWidget.hpp"
#include "Drivers/SI468x/Actions/MeasureAntCap.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;
using ::MusicBox::Drivers::SI468x::Si468xOperationMode;
using ::MusicBox::Drivers::SI468x::Actions::Si468xMeasureAntCapAction;

void
Si468xManualAntCapTuningWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  auto & s = sys::GetSi468xDriver(ctx);

  bool allow = (s.getOpMode() == Si468xOperationMode::DABRadio) ||
                (s.getOpMode() == Si468xOperationMode::FMRadio);

  switch(user_input)
  {
  case events::KeyEvent::kUp:      if (allow) s.setAntCap(s.getAntCapValue() - 1); break;
  case events::KeyEvent::kDown:    if (allow) s.setAntCap(s.getAntCapValue() + 1); break;
  case events::KeyEvent::kClicked: ctx.PopWidget(); break;
  default: WidgetEnvironment::Widget::HandleEvent(user_input, ctx);
  }
}


Si468xAutoAntCapTuningWidget::Si468xAutoAntCapTuningWidget() :
  Si468xManualAntCapTuningWidget()
{
  auto a = ::sys::globals().si468x_actions_.Action<Si468xMeasureAntCapAction>();
  auto p = a.Aquire();

  if(nullptr != p)
    p->StartMeasurement();
}

Si468xAutoAntCapTuningWidget::~Si468xAutoAntCapTuningWidget()
{
  auto a = ::sys::globals().si468x_actions_.Action<Si468xMeasureAntCapAction>();
  auto p = a.Get();

  if(nullptr != p)
    p->StopMeasurement();

  a.Release();
}