/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_MODEL_WIDGETS_HPP_
#define MUSICBOX_USERINTERFACE_MODEL_WIDGETS_HPP_

#include "UserInterface/WidgetEnvironment.hpp"

#include "UserInterface/Model/ServiceListModel.hpp"
#include "UserInterface/Model/DABChannelScanWidget.hpp"
#include "UserInterface/Model/FMTuningWidget.hpp"
#include "UserInterface/Model/IdleWidget.hpp"
#include "UserInterface/Model/Lists.hpp"
#include "UserInterface/Model/Menu.hpp"
#include "UserInterface/Model/Si468xAntCapTuningWidget.hpp"
#include "UserInterface/Model/StandbyWidget.hpp"
#include "UserInterface/Model/WakeAlarmWidgets.hpp"

namespace MusicBox::UserInterface::Model
{
  class DummyWidget : public WidgetEnvironment::Widget
  {
  public:
    void Draw(typename WidgetEnvironment::Painter &ctx) const override {};
  };
}

#endif