/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Model/ServiceListModel.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;
using typename ::MusicBox::Database::ServiceDatabase;

bool
ServiceListModel::Filters::Any(const ServiceValue&)
{
  return true;
}

bool
ServiceListModel::Filters::Preset(const ServiceValue& value)
{
  return value.bPreset != 0;
}

bool
ServiceListModel::Filters::FM(const ServiceValue& value)
{
  return value.isFMService();
}

bool
ServiceListModel::Filters::DAB(const ServiceValue& value)
{
  return value.isDabService();
}


ServiceListModel::ServiceListModel(FilterCallback filter) :
  selected_service_{filter}, filter_cb_{filter}
{

}

bool 
ServiceListModel::SetSelectedItem(const Iterator & it)
{ 
  if(selected_service_.service_ == it.service_)
    return false;

  if (it.service_ == Database::ServiceDatabase::instance().end())
  {
    if(selected_service_.service_ == Database::ServiceDatabase::instance().end())
      return false;
  }

  
  selected_service_ = it;
  return true;
}

void ServiceListItem::Draw(WidgetEnvironment::Painter &painter, const ServiceListModel&, bool selected) const
{
  if((*this) == ecpp::EndIterationTag())
    painter.DrawTextItem<WidgetEnvironment>("<Zurück>", selected);
  else
    painter.Draw(*this, selected);
}

void ServiceListItem::Activate(const MusicBox::UserInterface::WidgetEnvironment::Context& ctx) const
{
  if((*this) == ecpp::EndIterationTag())
    ctx.PopWidget();
  else
    ::sys::globals().si468x_driver_.tuneChannel(static_cast<const MusicBox::Database::ServiceValueIterator&>(*this));
}

ServiceListModel::Iterator::Iterator(FilterCallback filter) :
  service_{Database::ServiceDatabase::instance().begin()}, filter_cb_{filter}
{
  while(service_ != Database::ServiceDatabase::instance().end() && !filter_cb_(*service_))
    ++service_;
}


ServiceListModel::Iterator&
ServiceListModel::Iterator::operator=(const ServiceListModel::Iterator& init)
{
  service_ = init.service_;
  return *this;
}


ServiceListModel::Iterator&
ServiceListModel::Iterator::operator++()
{
  while(service_ != Database::ServiceDatabase::instance().end())
  {
    ++service_;

    if(filter_cb_(*service_))
      break;
  }

  return *this;
}

ServiceListModel::Iterator&
ServiceListModel::Iterator::operator--()
{
  auto s = service_;

  --s;
  while(s != Database::ServiceDatabase::instance().rend())
  {
    if(filter_cb_(*service_))
    {
      service_ = s;
      break;
    }
  }

  return *this;
}

#if 0
void
ServiceListWidget::HandleEvent(WidgetContext &ctx, const WidgetContext::Event& event)
{
  if(event.GetIf<Events::DatabaseChanged>())
    ctx.Invalidate(this);
  else
    WidgetContext::Widget::HandleEvent(ctx, event);
}
#endif