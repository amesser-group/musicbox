/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_MODEL_LISTS_HPP_
#define MUSICBOX_USERINTERFACE_MODEL_LISTS_HPP_

#include "UserInterface/WidgetEnvironment.hpp"

#include "UserInterface/Model/ServiceListModel.hpp"

#include "ecpp/UserInterface/Model/ListWidget.hpp"
#include "ecpp/UserInterface/Model/ArrayListModel.hpp"
#include "ecpp/UserInterface/Model/ActionListItem.hpp"


namespace MusicBox::UserInterface::Model
{
  using ActionListItem             = ::ecpp::UserInterface::Model::ActionListItem<WidgetEnvironment>;
  using FilterableActionListItem   = ::ecpp::UserInterface::Model::FilterableActionListItem<WidgetEnvironment>;

  using ActionListModel            = ::ecpp::UserInterface::Model::ArrayListModel<ActionListItem>;

  using ActionListWidget           = ::ecpp::UserInterface::Model::ListWidget<ActionListModel, WidgetEnvironment> ;
  using FilterableActionListWidget = ::ecpp::UserInterface::Model::ListWidget<::ecpp::UserInterface::Model::ArrayListModel<FilterableActionListItem>, WidgetEnvironment> ;  

  using ServiceListWidget          = ::ecpp::UserInterface::Model::ListWidget<Model::ServiceListModel, WidgetEnvironment>;
}

#endif