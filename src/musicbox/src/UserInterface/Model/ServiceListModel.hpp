/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_MODEL_CHANNELISTWIDGET_HPP_
#define MUSICBOX_USERINTERFACE_MODEL_CHANNELISTWIDGET_HPP_

#include "Database/ServiceDatabase.hpp"
#include "UserInterface/WidgetEnvironment.hpp"

namespace MusicBox::UserInterface::Model
{
  class ServiceListModel;

  class ServiceListItem : public ::MusicBox::Database::ServiceValueIterator
  {
  public:
    void Draw(WidgetEnvironment::Painter &painter, const ServiceListModel&, bool selected) const;
    void Activate(const MusicBox::UserInterface::WidgetEnvironment::Context& ctx) const;
  };


  class ServiceListModel
  {
  protected:
    using ServiceValueIterator = ::MusicBox::Database::ServiceValueIterator;
    using ServiceIterator      = ::MusicBox::Database::ServiceIterator;
    using ServiceValue         = ::MusicBox::Database::ServiceValue;

  public:
    using Item = ServiceListItem;
    
    typedef bool (*FilterCallback)(const ServiceValue& value);

    struct Filters
    {
      static bool Any   (const ServiceValue& value);
      static bool Preset(const ServiceValue& value);
      static bool FM    (const ServiceValue& value);
      static bool DAB   (const ServiceValue& value);
    };

    class Iterator
    {
    public:
      Iterator& operator=(const Iterator & init);

      constexpr const Item & operator*()  const { return service_; }
      constexpr const Item * operator->() const { return &service_; }

      Iterator& operator++() ;
      Iterator& operator--() ;

      constexpr bool operator < (::ecpp::EndIterationTag) const
      {
        return (service_ != Database::ServiceDatabase::instance().end());
      }

      Iterator(const Iterator &init) = default;
    protected:
      Iterator(FilterCallback filter);

      Item                  service_;
      const FilterCallback  filter_cb_;

      friend class ServiceListModel;
    };

    ServiceListModel(FilterCallback filter = Filters::Any);

    Iterator GetSelectedItem() const { return selected_service_; }

    bool SetSelectedItem(const Iterator & it);

    constexpr ::ecpp::EndIterationTag end() const { return ::ecpp::EndIterationTag(); }

  protected:
    Iterator                  selected_service_;
    const FilterCallback      filter_cb_;
  };

};
#endif
