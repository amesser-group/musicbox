/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Model/DABChannelScanWidget.hpp"

#include "System.hpp"

using namespace ::MusicBox::UserInterface::Model;

using ::MusicBox::Drivers::SI468x::Si468xOperationMode;
using ::MusicBox::Drivers::SI468x::Actions::Si468xScanDABAction;

DABChannelScanWidget::DABChannelScanWidget(): WidgetEnvironment::Widget{}
{
  auto a = ::sys::globals().si468x_actions_.Action<Si468xScanDABAction>();
  auto p = a.Aquire();

  if(nullptr != p)
    p->StartScan();
}

DABChannelScanWidget::~DABChannelScanWidget()
{
  auto a = ::sys::globals().si468x_actions_.Action<Si468xScanDABAction>();
  auto p = a.Get();

  if(nullptr != p)
    p->StopScan();

  a.Release();
}

void
DABChannelScanWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  switch(user_input)
  {
  case events::KeyEvent::kConfirm:
    ctx.PopWidget();
    break;
  default:
    WidgetEnvironment::Widget::HandleEvent(user_input, ctx);
    break;
  }
}

void
DABChannelScanWidget::HandleEvent(const WidgetEnvironment::Event &event, const WidgetEnvironment::Context &ctx)
{
  if(!event.GetIf<events::UserInteractionTimeout>())
    WidgetEnvironment::Widget::HandleEvent(event, ctx);
}
