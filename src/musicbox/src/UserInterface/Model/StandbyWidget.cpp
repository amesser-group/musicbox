/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"
#include "System.hpp"

using namespace ::MusicBox::UserInterface::Model;

using ::MusicBox::Drivers::SI468x::Si468xOperationMode;
using ::MusicBox::OperatingState;

const char StandByWidget::kDayNames[19] = "??SoMoDiMiDoFrSaXX";

const ActionListItem StandByWidget::kOffQuickActions[1] =
{
  { "Radio",  [](const WidgetEnvironment::Context &ctx)      { sys::globals().PlayLastStation(); }},
};


StandByWidget::StandByWidget() : WidgetEnvironment::Widget(), actions_{ActionListModel(kOffQuickActions)}
{

}

void
StandByWidget::HandleEvent(const WidgetEnvironment::Event &event, const WidgetEnvironment::Context &ctx)
{
  event.Visit([this,&ctx](auto &&arg) { 
    using T = std::decay_t<decltype(arg)>;

    if constexpr (std::is_base_of<events::StatusChange, T>::value)
    {
      ctx.Invalidate(this);
    }
    else if constexpr (std::is_base_of<events::UserInteractionTimeout, T>::value)
    {
      ctx.Invalidate(this);
    }
  });

  WidgetEnvironment::Widget::HandleEvent(event, ctx);
}

void
StandByWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  auto user_idle = ::sys::globals().user_interface_.IsUserIdle();

  switch(user_input)
  {
  case events::KeyEvent::kClicked:
    if(!user_idle)
      actions_.GetSelectedItem()->Activate(ctx);
    break;
  case events::KeyEvent::kUp:
    if(!user_idle)
      actions_.SetSelectedItem(++actions_.GetSelectedItem());
    break;
  case events::KeyEvent::kDown:
    if(!user_idle)
      actions_.SetSelectedItem(--actions_.GetSelectedItem());
    break;
  default:
    break;
  }
}
