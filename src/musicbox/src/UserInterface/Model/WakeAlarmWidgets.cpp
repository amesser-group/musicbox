/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Model/WakeAlarmWidgets.hpp"
#include "ecpp/ArrayString.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;
using namespace ::MusicBox::UserInterface;

MusicBox::Alarm AlarmListEntry::kDummy;

void WakeAlarmSettingsWidget::FormatDayList(const Alarm::Setpoint & sp, ::ecpp::StringSpan<::ecpp::StringEncodings::Utf8>::Iterator it, ::ecpp::StringSpan<::ecpp::StringEncodings::Utf8>::Iterator end)
{
  static const char daychars[] = "SMDMDFS";

  for(uint_least8_t day = 0; (day < 7) && (it < end); ++day)
  {
    if(sp.IsEnabled(::ecpp::Chrono::WeekDay(day)))
      (*it) = daychars[day];
    else
      (*it) = '_';

    ++it;
  }
}

void WakeAlarmSettingsWidget::HandleEvent(const events::KeyEvent user_input, const typename WidgetEnvironment::Context &ctx)
{
  if ( (user_input == events::KeyEvent::kClicked) &&
      (state_ == kSelectBack) )
  {
    ctx.PopWidget();
  }
  else
  {
    auto sp = alarm_.setpoint();

    switch(user_input)
    {
    case events::KeyEvent::kNext:
      switch(state_)
      {
      case kSelectEnable:    state_ = sp.IsEnabled() ? kSelectSunday : kSelectBack; break;
      case kSelectSunday:    state_ = kSelectMonday;    break;
      case kSelectMonday:    state_ = kSelectTuesday;    break;
      case kSelectTuesday:   state_ = kSelectWednesday;   break;
      case kSelectWednesday: state_ = kSelectThursday; break;
      case kSelectThursday:  state_ = kSelectFriday;  break;
      case kSelectFriday:    state_ = kSelectSaturday;    break;
      case kSelectSaturday:  state_ = kSelectHour;    break;
      case kSelectHour:      state_ = kSelectMinute;    break;
      case kAdjustHour:      sp.hour = (sp.hour < 23) ? (sp.hour + 1) : 0; break;
      case kSelectMinute:    state_ = kSelectBack;    break;
      case kAdjustMinute:    sp.minute = (sp.minute < 60) ? (sp.minute + 1) : 0; break;
      case kSelectBack:      break;
      }
      break;
    case events::KeyEvent::kPrev:
      switch(state_)
      {
      case kSelectEnable:    break;
      case kSelectSunday:    state_ = kSelectEnable;    break;
      case kSelectMonday:    state_ = kSelectSunday;    break;
      case kSelectTuesday:   state_ = kSelectMonday;    break;
      case kSelectWednesday: state_ = kSelectTuesday;    break;
      case kSelectThursday:  state_ = kSelectWednesday;   break;
      case kSelectFriday:    state_ = kSelectThursday; break;
      case kSelectSaturday:  state_ = kSelectFriday;  break;
      case kSelectHour:      state_ = kSelectSaturday;  break;
      case kAdjustHour:      sp.hour = (sp.hour > 0) ? (sp.hour - 1) : 23; break;
      case kSelectMinute:    state_ = kSelectHour;  break;
      case kAdjustMinute:    sp.minute = (sp.minute > 0) ? (sp.minute - 1) : 59; break;
      case kSelectBack:      state_ = sp.IsEnabled() ? kSelectMinute : kSelectEnable; break;
      }
      break;
    case events::KeyEvent::kClicked:
      switch(state_)
      {
      case kSelectEnable:    sp.ToggleEnabled();  break;
      case kSelectSunday:    sp.ToggleEnabled(::ecpp::Chrono::WeekDay::Sunday);    break;
      case kSelectMonday:    sp.ToggleEnabled(::ecpp::Chrono::WeekDay::Monday);    break;
      case kSelectTuesday:   sp.ToggleEnabled(::ecpp::Chrono::WeekDay::Tuesday);   break;
      case kSelectWednesday: sp.ToggleEnabled(::ecpp::Chrono::WeekDay::Wednesday); break;
      case kSelectThursday:  sp.ToggleEnabled(::ecpp::Chrono::WeekDay::Thursday);  break;
      case kSelectFriday:    sp.ToggleEnabled(::ecpp::Chrono::WeekDay::Friday);    break;
      case kSelectSaturday:  sp.ToggleEnabled(::ecpp::Chrono::WeekDay::Saturday);  break;
      case kSelectHour:      state_ = kAdjustHour;  break;
      case kAdjustHour:      state_ = kSelectHour;  break;
      case kSelectMinute:    state_ = kAdjustMinute;  break;
      case kAdjustMinute:    state_ = kSelectMinute;  break;
      case kSelectBack:      break;
      }
      break;
    default:
      break;
    }

    alarm_.set_setpoint(sp);
  }
}
