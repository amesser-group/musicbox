/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_WIDGETCONTEXT_HPP_
#define MUSICBOX_USERINTERFACE_WIDGETCONTEXT_HPP_

#include "ecpp/UserInterface/Widget.hpp"
#include "UserInterface/Model/Menu.hpp"
#include "UserInterface/events.hpp"
#include "UserInterface/Target.hpp"

#include <variant>

namespace MusicBox::UserInterface
{
  class UserInterface;

  class WidgetEnvironment::Widget : public ::ecpp::UserInterface::Widget<WidgetEnvironment>
  {
  public:
    using EditingMode = WidgetEnvironment::Painter::EditingMode;

    void         HandleEvent(const typename WidgetEnvironment::Event &event, const typename WidgetEnvironment::Context &ctx) override;
    virtual void HandleEvent(const events::KeyEvent user_input, const typename WidgetEnvironment::Context &ctx);
  };

  class WidgetEnvironment::Context : public Model::ContextTargetMixin,
                                     public Model::ContextMenuMixin,
                                     public WidgetEnvironment::Painter::Rect
  {
  public:
    constexpr Context(WidgetEnvironment::Painter::Rect rect, UserInterface &ui) : WidgetEnvironment::Painter::Rect{rect}, ui_{ui} {}

    void Invalidate(const Widget* widget) const;
    
    template<typename _Widget, typename... ARGS>
    void PushWidget(ARGS &&... args) const;

    template<typename _Widget, typename... ARGS>
    void EmplaceWidget(ARGS &&... args) const;

    void PopWidget() const;

    template<typename Item>
    constexpr auto GetVisibleItemCount(const Item & item) const
    {
      return WidgetEnvironment::Painter::GetVisibleItemCount(*this, item);
    }

    static void Back(const WidgetEnvironment::Context &ctx);
  protected:
    UserInterface& ui_;
  };

  class WidgetEnvironment::Event : public EventValue
  {
  public:
    using KeyEvent = events::KeyEvent;


    template<typename TheEvent>
    constexpr Event(TheEvent val) : EventValue{val} {}

    template<typename EventValue>
    constexpr const EventValue* GetIf() const { 
      return ::std::visit( [] (auto &&arg) -> const EventValue* {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_base_of<EventValue, T>::value)
          return &arg;
        else
          return nullptr;
      }, AsVariant());
    }

    constexpr const events::KeyEvent* GetIfUserInput() const {
      return GetIf<events::KeyEvent>();
    }

    template<typename _Visitor>
    constexpr decltype(auto)
    Visit(_Visitor&& __visitor) const
    {
      return ::std::visit(__visitor, AsVariant());
    }

  protected:

    const EventValue& AsVariant() const { return *this; }
  };
}

#endif