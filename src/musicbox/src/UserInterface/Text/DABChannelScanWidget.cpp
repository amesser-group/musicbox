/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"
#include "Drivers/SI468x/Actions/ScanDAB.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;

using MusicBox::Drivers::SI468x::Actions::Si468xScanDABAction;
using MusicBox::Drivers::SI468x::Si468xOperationMode;

void
DABChannelScanWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  auto p = ::sys::globals().si468x_actions_.Action<Si468xScanDABAction>().Get();
  auto & s = ::sys::globals().si468x_driver_;

  ::sys::globals().user_interface_.Refresh(MilliSecond<>(100));

  painter[0].CenterText("DAB Kanalsuche");

  if (s.getOpMode() == Si468xOperationMode::DABRadio)
  {
    auto const & sc = sys::GetSi468xDriver(this);
    auto const & r  = sc.getDABManager();
    auto rssi = ::ecpp::Math::FixedPoint<int32_t, 1000>(r.getRssi());

    painter[1].IPrintF("Freq: %3d.%03d MHz", s.getTunedFreq() / 1000, s.getTunedFreq() % 1000);
    painter[2].IPrintF("Rssi: %3d.%03d", rssi.raw().value / 1000, rssi.raw().value % 1000);
  }

  if(nullptr != p)
  { /* should be never the case */
    if( p->IsScanning())
      painter[3].CenterText("Scanne..");
    else
      painter[3].CenterText("Fertig");
  }

}
