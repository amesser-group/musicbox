/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_TEXT_TEXTDISPLAYDRIVERFACTORY_
#define MUSICBOX_USERINTERFACE_TEXT_TEXTDISPLAYDRIVERFACTORY_

#include "ecpp/Drivers/BufferedDisplayDriver.hpp"
#include "UserInterface/Text/TextPainter.hpp"

namespace MusicBox::UserInterface::Text
{
  /** Defines behavior / api expecteded by text display user interface from
   *  display driver */
  template<typename DisplayDriver>
  class TextDisplayDriverFactory : protected ::ecpp::Drivers::BufferedDisplayDriverFactory<DisplayDriver>
  {
  private:
    using BufferedDriver = ::ecpp::Drivers::BufferedDisplayDriverFactory<DisplayDriver>;

    using typename BufferedDriver::CursorMode;
  public:
    enum EditingMode : decltype(CursorMode)
    {
      kEditingModeNone      = CursorMode::kCursorOff,
      kEditingModeSelected  = CursorMode::kCursorUnderline,
      kEditingModeAdjusting = CursorMode::kCursorBlink,
    };
    
    using typename BufferedDriver::BufferIndex;
    using typename BufferedDriver::Point;
    using typename BufferedDriver::Rect;
    using typename BufferedDriver::DisplayEncoding;
    
    using BufferedDriver::operator[];

    using BufferedDriver::rows;
    using BufferedDriver::columns;

    using BufferedDriver::Init;

    using CharacterMapper = ::ecpp::UserInterface::Text::CharacterMapper<TextDisplayDriverFactory>;
    using Painter         = ::MusicBox::UserInterface::Text::TextPainter<CharacterMapper>;

    constexpr Painter CreatePainter() { return Painter(*this); };

    void SetEditLocation(Point upper_left, Point lower_right, EditingMode mode)  
    { 
      edit_mode_ = mode;
      if(upper_left != lower_right)
        edit_location_ = Point(lower_right.x, lower_right.y-1);
      else
        edit_location_ = lower_right;
    }

    void Update();

    constexpr bool IsUpdatePending() { return false; }
  protected:
    /** Location in display where user is currently edditing */
    Point       edit_location_ {0,0};
    EditingMode edit_mode_     {kEditingModeNone};

    friend class ::ecpp::UserInterface::Text::CharacterMapper<TextDisplayDriverFactory>;
  };

  template<typename DisplayDriver>
  void
  TextDisplayDriverFactory<DisplayDriver>::Update()
  {
    Point p(0,0);

    BufferedDriver::SetCursorMode(CursorMode::kCursorOff);
    for(p.y = 0; p.y < rows(); ++p.y)
    {
      BufferedDriver::LocateCursor(0, p.y);
      BufferedDriver::WriteDDRAM( BufferedDriver::buffer_ + BufferIndex(p).index, columns());
    }

    /* place cursor at edit locatin if wihtin display area */
    if( (edit_location_.x < columns()) && (edit_location_.y < rows()))
    {
      BufferedDriver::LocateCursor(edit_location_.x, edit_location_.y);
      BufferedDriver::SetCursorMode(static_cast<CursorMode>(edit_mode_));
    }
  }

}

#endif
