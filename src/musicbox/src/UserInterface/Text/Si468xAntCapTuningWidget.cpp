/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Actions/MeasureAntCap.hpp"
#include "UserInterface/Model/Si468xAntCapTuningWidget.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;

using MusicBox::Drivers::SI468x::Si468xOperationMode;
using MusicBox::Drivers::SI468x::Actions::Si468xMeasureAntCapAction;

void
Si468xManualAntCapTuningWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  auto & s = sys::globals().si468x_driver_;

  ::sys::globals().user_interface_.Refresh(MilliSecond<>(100));

  painter[0].CenterText("AntCap Optimierung");

  if ( (s.getOpMode() == Si468xOperationMode::DABRadio) ||
       (s.getOpMode() == Si468xOperationMode::FMRadio) )
  {
    auto const & r = s.getRadioManager();
    auto rssi = ::ecpp::Math::FixedPoint<int32_t, 1000>(r.getRssi());

    painter[1].IPrintF("Freq:   %3d.%3d MHz", s.getTunedFreq() / 1000, s.getTunedFreq() % 1000);
    painter[2].IPrintF("AC: %3d Rssi: %3d.%03d", s.getAntCapValue(), rssi.raw().value / 1000, rssi.raw().value % 1000);
  }
}

void
Si468xAutoAntCapTuningWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  Si468xManualAntCapTuningWidget::Draw(painter);

  auto a = ::sys::globals().si468x_actions_.Action<Si468xMeasureAntCapAction>();
  auto p = a.Get();

  if(nullptr != p)
  {
    if( p->IsMeasuring())
      painter[2].CenterText("Erstelle Messreihe..");
    else
      painter[2].CenterText("Fertig");
  }
}
