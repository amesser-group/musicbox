/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Model/WakeAlarmWidgets.hpp"
#include "ecpp/ArrayString.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;
using namespace ::MusicBox::UserInterface;

void 
WakeAlarmSettingsWidget::Draw(typename WidgetEnvironment::Painter &painter) const
{
  ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8,18> text;
  const auto & sp = alarm_.setpoint();

  painter[0].CenterText("Wecker");

  {
    bool selected = (kSelectEnable <= state_) && (state_ <= kSelectSaturday);

    if(sp.IsEnabled())
    {
      text = "Alarm <an> ";

      FormatDayList(sp, text.begin() + 11, text.begin() + 18);

      switch(state_)
      {
      case kSelectEnable:    painter[1].SetEditLocation({7,0,9,painter[1].height()}, EditingMode::kEditingModeAdjusting); break;
      case kSelectSunday:    painter[1].SetEditLocation({11,0,12,painter[1].height()}, EditingMode::kEditingModeAdjusting); break;
      case kSelectMonday:    painter[1].SetEditLocation({12,0,13,painter[1].height()}, EditingMode::kEditingModeAdjusting); break;
      case kSelectTuesday:   painter[1].SetEditLocation({13,0,14,painter[1].height()}, EditingMode::kEditingModeAdjusting); break;
      case kSelectWednesday: painter[1].SetEditLocation({14,0,15,painter[1].height()}, EditingMode::kEditingModeAdjusting); break;
      case kSelectThursday:  painter[1].SetEditLocation({15,0,16,painter[1].height()}, EditingMode::kEditingModeAdjusting); break;
      case kSelectFriday:    painter[1].SetEditLocation({16,0,17,painter[1].height()}, EditingMode::kEditingModeAdjusting); break;
      case kSelectSaturday:  painter[1].SetEditLocation({17,0,18,painter[1].height()}, EditingMode::kEditingModeAdjusting); break;
      default: break;
      }

    }
    else
    {
      text = "Alarm <aus>";
      
      if(state_ == kSelectEnable)
        painter[1].SetEditLocation({7,0,10,painter[1].height()}, EditingMode::kEditingModeAdjusting);
    }

    painter[1].DrawTextItem<WidgetEnvironment>(text, selected);
  }


  {
    bool selected =  (kSelectHour <= state_) && (state_ <= kAdjustMinute);

    text.IPrintF(" Zeit %02d:%02d", sp.hour, sp.minute);
    painter[2].DrawTextItem<WidgetEnvironment>(text, selected);

    if(selected)
    {
      if(kSelectHour == state_)
        painter[2].SetEditLocation({6,0,8,painter[2].height()}, EditingMode::kEditingModeSelected);
      else if(kAdjustHour == state_)
        painter[2].SetEditLocation({6,0,8,painter[2].height()}, EditingMode::kEditingModeAdjusting);
      else if(kSelectMinute == state_)
        painter[2].SetEditLocation({9,0,11,painter[2].height()}, EditingMode::kEditingModeSelected);
      else if(kAdjustMinute == state_)
        painter[2].SetEditLocation({9,0,11,painter[2].height()}, EditingMode::kEditingModeAdjusting);
    }
  }

  painter[3].DrawTextItem<WidgetEnvironment>("Speichern", state_ == kSelectBack);
}
