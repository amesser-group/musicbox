/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Text/TextPainter.hpp"
#include "UserInterface/Model/ServiceListModel.hpp"

using namespace MusicBox::UserInterface;
using MusicBox::UserInterface::Text::TextPainter;

namespace MusicBox::UserInterface::Text
{
  template<typename FrameBufferAdapter>
  void TextPainter<FrameBufferAdapter>::Draw(const Model::ServiceListItem &item, bool selected)
  {
    const auto & value = (*item);

    CreateField({1, 0, width() - 1, 1}).PutText(value.Name);

    if (selected)
    {
      operator[](0)[0] = '[';
      operator[](0)[width()-1] = ']';
    }
  }

  template class MusicBox::UserInterface::Text::TextPainter<::sys::Bsp::Display::CharacterMapper>;
}
