/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_TEXTPAINTER_TEXTPAINTER_HPP_
#define MUSICBOX_USERINTERFACE_TEXTPAINTER_TEXTPAINTER_HPP_

#include "ecpp/UserInterface/Text/Painter.hpp"

namespace MusicBox::UserInterface
{
  /* some forward declarations */
  namespace Model
  {
    class ServiceListItem;
  }

  namespace Text
  {
    template<typename FrameBufferAdapter>
    class TextPainter : public ::ecpp::UserInterface::Text::TextPainter<FrameBufferAdapter>
    {
    private:
      using Base = ::ecpp::UserInterface::Text::TextPainter<FrameBufferAdapter>;
    public:
      using typename Base::Rect;

      constexpr TextPainter(const FrameBufferAdapter &init) : Base(init) {};
      TextPainter(const TextPainter &init) = default;

      using Base::Base;

      using Base::Draw;
      void Draw(const Model::ServiceListItem &item, bool selected);

      using Base::width;
      using Base::operator[];

      constexpr TextPainter CreateField(Rect rect)   { return TextPainter(*this, rect); }
    };
  }
}

#endif

