/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/WidgetEnvironment.hpp"
#include "System.hpp"

using namespace ::MusicBox::UserInterface;

void 
WidgetEnvironment::Widget::HandleEvent(const typename WidgetEnvironment::Event &event, const typename WidgetEnvironment::Context &ctx)
{
  auto* p = event.GetIfUserInput();

  if(p != nullptr)
    HandleEvent(*p, ctx);
  else
    ::sys::globals().user_interface_.HandleEvent(event);
}

void 
WidgetEnvironment::Widget::HandleEvent(const events::KeyEvent user_input, const typename WidgetEnvironment::Context &ctx)
{
}
