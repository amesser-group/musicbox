/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_COMMONS_HPP_
#define MUSICBOX_USERINTERFACE_COMMONS_HPP_

#include "Drivers/SI468x/Common.hpp"
#include "Database/ServiceDatabase.hpp"

namespace MusicBox::UserInterface::events
{
  struct KeyEvent
  {
  protected:
    constexpr KeyEvent (uint_least8_t keycode) : keycode_{keycode} {}
    const uint_least8_t keycode_;

  public:
    struct Back;
    struct Snooze;

    static const KeyEvent  kNone;
    static const KeyEvent  kConfirm;
    static const KeyEvent  kPrev;
    static const KeyEvent  kNext;
    static const Snooze    kSnooze;
    static const Back      kBack; 

    static const KeyEvent  kVolumePlus;
    static const KeyEvent  kVolumeMinus;

    /* aliases for common keys */
    static constexpr const KeyEvent & kUp      = kPrev;
    static constexpr const KeyEvent & kDown    = kNext;
    static constexpr const KeyEvent & kClicked = kConfirm;

    constexpr operator uint_least8_t () const { return keycode_; }
    constexpr bool operator==(const KeyEvent & rhs) const { return keycode_ == rhs.keycode_;}
  };

  struct KeyEvent::Back   : public KeyEvent { using KeyEvent::KeyEvent; };
  struct KeyEvent::Snooze : public KeyEvent { using KeyEvent::KeyEvent; };

  constexpr const KeyEvent          KeyEvent::kNone        {0};
  constexpr const KeyEvent          KeyEvent::kConfirm     {1};
  constexpr const KeyEvent          KeyEvent::kPrev        {2};
  constexpr const KeyEvent          KeyEvent::kNext        {3};
  constexpr const KeyEvent::Snooze  KeyEvent::kSnooze      {4};
  constexpr const KeyEvent::Back    KeyEvent::kBack        {5};
  constexpr const KeyEvent          KeyEvent::kVolumePlus  {6};
  constexpr const KeyEvent          KeyEvent::kVolumeMinus {7};
  
  struct Refresh
  {
  };

  struct UserInteractionTimeout
  {
  };


  struct StatusChange
  {
  };

  struct DatabaseChanged : public StatusChange
  {
    constexpr DatabaseChanged(const MusicBox::Database::ServiceHandle handle) : StatusChange{}, handle{handle} {}

    const MusicBox::Database::ServiceHandle handle;
  };

  struct OperationModeChanged : public StatusChange
  {
    constexpr OperationModeChanged(MusicBox::Drivers::SI468x::Si468xOperationMode _mode) : StatusChange{}, mode{_mode} {}
    MusicBox::Drivers::SI468x::Si468xOperationMode mode;
  };
}
#endif