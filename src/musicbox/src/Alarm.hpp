/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_ALARM_HPP_
#define MUSICBOX_ALARM_HPP_

#include "ecpp_sys/Chrono.hpp"
#include <array>

namespace MusicBox
{
  class Alarm;
}

namespace signals
{
  void Raise(const MusicBox::Alarm& Alarm);
}

namespace MusicBox
{
  class Alarm
  {
  public:
    using Timepoint = ecpp_sys::Chrono::SystemClock::Timepoint;

    struct Setpoint
    {
      uint_least8_t  mask   {0};
      uint_least8_t  hour   {0};
      uint_least8_t  minute {0};

      Setpoint() = default;
      Setpoint(const Setpoint &) = default;
      Setpoint(const ecpp_sys::Chrono::SystemClock::Timepoint & tp);

      void ToggleEnabled()                                       {mask = (mask == 0) ? (0xFF) : (mask ^ 0x80);}
      void ToggleEnabled(ecpp::Chrono::WeekDay wday)             {mask ^= ToMask(wday);}

      constexpr bool IsEnabled() const                           { return mask & 0x80U;}
      constexpr bool IsEnabled(ecpp::Chrono::WeekDay wday) const { return mask & ToMask(wday);}

      constexpr bool operator < (const Setpoint & rhs) const { return (hour < rhs.hour) || (hour == rhs.hour && minute < rhs.minute);}
      constexpr bool operator >=(const Setpoint & rhs) const { return (hour > rhs.hour) || (hour == rhs.hour && minute >= rhs.minute);}

    private:
      constexpr static uint_least8_t ToMask(ecpp::Chrono::WeekDay wday) { return 0x40U >> (unsigned)wday;}
    };

    constexpr const Setpoint & setpoint() const        { return setpoint_; }
    void set_setpoint(const Setpoint &val) { setpoint_ = val;  }
    
    void Check(const Setpoint &last_checked, const Setpoint &current) const;
    
  protected:
    Setpoint       setpoint_;
  };

  template<size_t N>
  class WakeAlarmList : public ::std::array<Alarm, N>
  {
  public:
    void            CheckAlarms();
    Alarm::Setpoint FindNextAlarm() const;

    using ::std::array<Alarm, N>::begin;
    using ::std::array<Alarm, N>::end;
  protected:
    Alarm::Setpoint  last_alarm_check_sp_;
  };

  template<size_t N>
  void WakeAlarmList<N>::CheckAlarms()
  {
    Alarm::Setpoint now(::ecpp_sys::Chrono::SystemClock::now());

    for(auto it = begin(); it != end(); ++it)
      it->Check(last_alarm_check_sp_, now);

    last_alarm_check_sp_ = now;
  }

  template<size_t N>
  Alarm::Setpoint WakeAlarmList<N>::FindNextAlarm() const
  {
    Alarm::Setpoint now(::ecpp_sys::Chrono::SystemClock::now());
    uint_fast8_t     next_sp_days_ahead = 0;
    Alarm::Setpoint  next_sp;

    for(auto it = begin(); it != end(); ++it)
    {
      const Alarm::Setpoint & sp = it->setpoint();

      if(sp.IsEnabled())
      {
        uint_fast8_t days_ahead = 0;
        uint_fast16_t test;

        test = (now.mask & 0x7F);
        test |= test << 7;

        if(now >= sp)
        {
          test >>= 1;
          days_ahead += 1;
        }

        for(;days_ahead < 7; test >>= 1, days_ahead++)
          if( (test & 0x7F) & sp.mask)
            break;

        if( (next_sp.mask == 0) ||
            (next_sp_days_ahead > days_ahead) || 
            ((next_sp_days_ahead == days_ahead) && (sp < next_sp)))
        {
          next_sp            = sp;

          /* let next_sp point to next day of week when alarm will occu */
          next_sp.mask = (test | 0x80) & next_sp.mask;

          next_sp_days_ahead = days_ahead;
        }
      }
    }

    return next_sp;
  }

}

#endif
