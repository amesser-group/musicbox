/*
 *  Copyright 2017-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RDS/RDS.hpp"
#include <cstring>
#include "ecpp/StringEncodings/Unicode.hpp"

using namespace std;
using ::ecpp::String;
using ::ecpp::StringEncoding;
using ::ecpp::StringEncodings::Utf8;
using ::ecpp::StringEncodings::Unicode;

const RDSStringEncoding::BufferElement*
RDSStringEncoding::Decoder::NextChar(const BufferElement* end)
{
  if(end == nullptr)
    end = buf_ + 1;

  if (buf_ >= end)
    return end;

  if((buf_ < end) && (*buf_ != 0))
    buf_++;

  return buf_;
}

RDSStringEncoding::Codepoint
RDSStringEncoding::Decoder::DecodeChar(const BufferElement* end) const
{
  if(buf_ < end)
    return *buf_;
  else
    return Codepoint::kStringEnd;
}

RDSStringEncoding::Codepoint::operator Unicode::Codepoint() const
{
  char32_t cp = Unicode::Codepoint::kMappingFailed;


  if (val_ >= 0x20 && val_ <=0x7D)
    cp = val_;

  switch(val_)
  {
  case 0x00: cp = U'\0';break;
  case 0x24: cp = U'#'; break;
  case 0x25: cp = U'¤'; break;
  case 0x5E: cp = U'?'; break;
  case 0x5F: cp = U'?'; break;
  case 0x60: cp = U'?'; break;
  case 0x7E: cp = U'?'; break;
  case 0x7F: cp = U'?'; break;
  case 0x80: cp = U'á'; break;
  case 0x81: cp = U'à'; break;
  case 0x82: cp = U'é'; break;
  case 0x83: cp = U'è'; break;
  case 0x84: cp = U'í'; break;
  case 0x85: cp = U'ì'; break;
  case 0x86: cp = U'ó'; break;
  case 0x87: cp = U'ò'; break;
  case 0x88: cp = U'ú'; break;
  case 0x89: cp = U'ù'; break;
  case 0x8A: cp = U'Ñ'; break;
  case 0x8B: cp = U'Ç'; break;
  case 0x8C: break;
  case 0x8D: cp = U'ß'; break;
  case 0x8E: cp = U'¡'; break;
  case 0x8F: break;
  case 0x90: cp = U'â'; break;
  case 0x91: cp = U'ä'; break;
  case 0x92: cp = U'ê'; break;
  case 0x93: cp = U'ë'; break;
  case 0x94: cp = U'î'; break;
  case 0x95: cp = U'ï'; break;
  case 0x96: cp = U'ô'; break;
  case 0x97: cp = U'ö'; break;
  case 0x98: cp = U'û'; break;
  case 0x99: cp = U'ü'; break;
  case 0x9A: cp = U'ñ'; break;
  case 0x9B: cp = U'ç'; break;
  default: break;
  }

  return {cp};
}

bool
RDSManager::handleFrame(const RDSFrame & Frame)
{
  bool changed = false;

  if(Frame.ValidMask & Frame.BLOCKA_VALID)
  {
    if( m_State.program_identifier != Frame.BlockA)
    {
      m_State.program_identifier = Frame.BlockA;
      m_State.valid_flags       |= m_State.RDS_PI_VALID;
      changed = true;
    }
  }

  if(Frame.ValidMask & Frame.BLOCKB_VALID)
  {
    uint8_t GroupType      = (Frame.BlockB & 0xF000) >> 12;
    //uint8_t GroupVersion   = (Frame.BlockB & 0x0800) >> 11;

    /* Frame contains Program Service */
    if(GroupType == 0x0 && (Frame.ValidMask & Frame.BLOCKD_VALID))
    {
      uint8_t SegmentAddress = Frame.BlockB & 0x3;

      TempProgramServiceName[SegmentAddress * 2]     = (Frame.BlockD >> 8) & 0xFF;
      TempProgramServiceName[SegmentAddress * 2 + 1] = (Frame.BlockD >> 0) & 0xFF;

      if(NULL == memchr(TempProgramServiceName, '\0', 8))
      {
        if (memcmp(m_State.program_service_name.data(), TempProgramServiceName, 8))
        {
          memcpy(m_State.program_service_name.data(), TempProgramServiceName, 8);

          m_State.valid_flags |= m_State.RDS_PS_NAME_VALID;
          changed = true;
        }

        memset(TempProgramServiceName, '\0', 8);
      }
    }
  }

  return changed;
}

void RDSManager::invalidate()
{
  m_State = {};
}
