/*
 *  Copyright 2017-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RDS_HPP_
#define RDS_HPP_

#include <cstdint>
#include <iterator>
#include <array>
#include "ecpp/ArrayString.hpp"
#include "ecpp/String.hpp"
#include "ecpp/StringEncoding.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

struct RDSFrame
{
  enum : uint_least8_t
  {
    BLOCKA_VALID = 0x40,
    BLOCKB_VALID = 0x10,
    BLOCKC_VALID = 0x04,
    BLOCKD_VALID = 0x01,
  };

  uint16_t      BlockA     {0};
  uint16_t      BlockB     {0};
  uint16_t      BlockC     {0};
  uint16_t      BlockD     {0};
  uint_least8_t ValidMask  {0};
};

class RDSStringEncoding : public ecpp::StringEncoding
{
public:
  using BufferElement = uint8_t;

  class Codepoint;
  class Decoder;
};

class RDSStringEncoding::Codepoint
{
public:
  constexpr Codepoint(uint8_t val) : val_(val) {}

  operator ecpp::StringEncodings::Unicode::Codepoint() const;

  constexpr bool operator != (const Codepoint &rhs) const
  {
    return val_ != rhs.val_;
  }

  enum : uint8_t
  {
    kStringEnd = 0,
  };
protected:
  uint_least8_t val_;

  friend class Decoder;
};


class RDSStringEncoding::Decoder
{
public:
  constexpr Decoder(const BufferElement* buf) : buf_{buf} {}

  const BufferElement* NextChar(const BufferElement* end);
  Codepoint DecodeChar (const BufferElement* end) const;

protected:
  const BufferElement *buf_;
};


class RDSManager;

class RDSState
{
public:
  using ProgramServiceName = ::ecpp::ArrayString<RDSStringEncoding, 8>;

  enum : uint_least8_t
  {
    RDS_PI_VALID      = 0x01,
    RDS_PS_NAME_VALID = 0x02,
  };

  constexpr bool isProgramIdentifierValid() const
  {
    return 0 != (valid_flags & RDS_PI_VALID);
  }

  constexpr bool isProgramServiceNameValid() const
  {
    return 0 != (valid_flags & RDS_PS_NAME_VALID);
  }

  /** Flag field indicating which fields are valid */
  uint_least8_t      valid_flags           {0};
  /** RDS Program Identifier (PI) */
  uint_least16_t     program_identifier    {0};
  /** RDS Program Service Name (PS) */
  ProgramServiceName program_service_name  {};
};

class RDSManager
{
public:
  bool handleFrame(const RDSFrame & Frame);
  void invalidate();

  const RDSState & getState() const {return m_State;};

private:
  RDSState        m_State;
  char            TempProgramServiceName[8] {};
};


#endif /* RDS_HPP_ */
