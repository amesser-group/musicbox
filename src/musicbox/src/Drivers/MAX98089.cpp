/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/MAX98089.hpp"
#include "ecpp/Datatypes.hpp"
#include "System.hpp"

using namespace MusicBox;
using namespace MusicBox::Drivers::MAX98089;
using namespace ecpp;

static uint8_t Max98089UnShutdownData[] = { RegDef::Max98089::REG_SYSTEM_SHUTDOWN, 0xC0};




#if 0
void
Max98089ManagerStateWriteDataStart::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.WriteDataStart, *this);
  m.writeData(Max98089ShutdownData, ElementCount(Max98089ShutdownData));
};

void
Max98089ManagerStateWriteDataStart::handleTwiTransferDone()
{
  Max98089Manager & m = container_of(Max98089Manager, State.WriteDataStart, *this);
  m.changeState<Max98089ManagerStateWriteData>(*this);
}

void
Max98089ManagerStateWriteData::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.WriteData, *this);

  if ((offset + 1U) < length)
  {
    buffer[0] = data[0] + offset;
    buffer[1] = data[offset + 1];

    m.writeData(buffer, 2);
    offset++;
  }
  else
  {
    if(m.ShutdownReq)
      m.changeState<Max98089ManagerStateEnterShutdown>();
    else
      m.changeState<Max98089ManagerStateActivate>();
  }
};

void
Max98089ManagerStateWriteData::handleTwiTransferDone()
{
  handlePendingActions();
};
#endif

static const int_least16_t s_LevelTocB [] =
{
  [0x00] = (int)(-62 * 10.),
  [0x01] = (int)(-58 * 10.),
  [0x02] = (int)(-54 * 10.),
  [0x03] = (int)(-50 * 10.),
  [0x04] = (int)(-46 * 10.),
  [0x05] = (int)(-42 * 10.),
  [0x06] = (int)(-38 * 10.),
  [0x07] = (int)(-35 * 10.),
  [0x08] = (int)(-32 * 10.),
  [0x09] = (int)(-29 * 10.),
  [0x0A] = (int)(-26 * 10.),
  [0x0B] = (int)(-23 * 10.),
  [0x0C] = (int)(-20 * 10.),
  [0x0D] = (int)(-17 * 10.),
  [0x0E] = (int)(-14 * 10.),
  [0x0F] = (int)(-12 * 10.),
  [0x10] = (int)(-10 * 10.),
  [0x11] = (int)(-8 * 10.),
  [0x12] = (int)(-6 * 10.),
  [0x13] = (int)(-4 * 10.),
  [0x14] = (int)(-2 * 10.),
  [0x15] = (int)(0 * 10.),
  [0x16] = (int)(+1 * 10.),
  [0x17] = (int)(+2 * 10.),
  [0x18] = (int)(+3 * 10.),
  [0x19] = (int)(+4 * 10.),
  [0x1A] = (int)(+5 * 10.),
  [0x1B] = (int)(+6 * 10.),
  [0x1C] = (int)(+6.5 * 10.),
  [0x1D] = (int)(+7 * 10.),
  [0x1E] = (int)(+7.5 * 10.),
  [0x1F] = (int)(+8 * 10.),
};

Max98089Driver::Volume
Max98089Driver::Volume::FromLevel(uint_fast8_t level)
{
  return Volume(s_LevelTocB[level & 0x1F]);
}

bool
Max98089Driver::WriteRegisters(uint8_t start_register, const uint8_t* values, size_t cnt)
{
  auto & trb = ::sys::bsp().GetTwiRequestBlock(*this);

  if(trb.IsActive())
    return false;

  trb.SetUp(MusicBox::RegDef::Max98089::kTwiAddr);
  trb.GetStruct().SetField8(0, start_register);
  trb.GetStruct().Pad(1, values, cnt);
  
  trb.Submit(cnt + 1, 0);

  return true;
}
