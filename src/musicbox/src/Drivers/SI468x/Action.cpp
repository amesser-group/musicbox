/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Action.hpp"
#include "System.hpp"

using namespace MusicBox::Drivers::SI468x;

Si468xDABManager* 
Si468xAction::GetDABManager()
{
  auto & drv = ::sys::globals().si468x_driver_;

  if (drv.getOpMode() == Si468xOperationMode::DABRadio)
    return &drv.GetDABManager();
  else
    return nullptr;
}

Si468xManagerBase&
Si468xAction::GetManager()
{
  auto & drv = ::sys::globals().si468x_driver_;
  return drv.getMgr();
}