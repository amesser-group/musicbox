/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Driver.hpp"
#include "System.hpp"

using namespace MusicBox::Drivers::SI468x;

Si468xDriver::~Si468xDriver()
{

}

Si468xDriver::Blub::~Blub()
{

}

void
Si468xDriver::seek(bool forward)
{
  if( getMgr().getOpMode() != Si468xOperationMode::FMRadio)
    return;

  if(forward)
    OpMgr.FM.seekForward();
  else
    OpMgr.FM.seekBackward();
}

void
Si468xDriver::activateMode(Si468xOperationMode opmode, bool force)
{
  if( force || (getMgr().getOpMode() != opmode))
  {
    getMgr().~Si468xManagerBase();

    service_info_stable_ = false;
    new (&OpMgr.Firmware) Si4688FirmwareManager(opmode);

    pollAsync();
  }
}



void
Si468xDriver::notifyStatus(uint_fast8_t last_command, const Si4688Status& status)
{
  getMgr().handleCommandFinished(last_command, status);
}

void
Si468xDriver::changeMgr(Si468xOperationMode mode)
{
  switch(mode)
  {
  case Si468xOperationMode::DABRadio:
    changeMgr<Si468xDABManager>();
    break;
  case Si468xOperationMode::FMRadio:
    changeMgr<Si468xFMManager>();
    break;
  default:
    changeMgr<Si4688FirmwareManager>(Si468xOperationMode::Disabled);
    break;
  }
}

void
Si468xDriver::changedMgr()
{
  ::signals::Changed(*this, getMgr().getOpMode());
  pollAsync();
}

void 
Si468xDriver::tuneChannel(const Database::ServiceValue &service, Si468xOperationMode opmode)
{
  current_service_        = service;
  current_service_handle_ = {};
  tuneChannel(opmode);
}

void 
Si468xDriver::tuneChannel(MusicBox::Database::DabChannelHandle channel_handle)
{
  current_service_        = {};
  current_service_handle_ = {};

  current_service_.DabChannel = channel_handle;
  tuneChannel(Si468xOperationMode::DABRadio);
}

void
Si468xDriver::tuneChannel(Database::ServiceHandle handle, Si468xOperationMode opmode)
{
  auto const & db = Database::ServiceDatabase::instance();
  auto const * value = db.GetService(handle);

  if(value == nullptr)
    return;
  
  current_service_handle_ = handle;
  current_service_        = *value;
  tuneChannel(opmode);
}

void
Si468xDriver::tuneChannel(Si468xOperationMode opmode)
{
  if( (opmode != Si468xOperationMode::FMRadio) &&
      (opmode != Si468xOperationMode::DABRadio) )
  { /* choose best */
    opmode = Si468xOperationMode::Disabled;

    if(current_service_.isFMService())
      opmode = Si468xOperationMode::FMRadio;
    else if (current_service_.DabChannel.isValid())
      opmode = Si468xOperationMode::DABRadio;

    if(current_service_.isDabService())
      opmode = Si468xOperationMode::DABRadio;
  }

  if(getMgr().getOpMode() != opmode)
    activateMode(opmode);
  else if (opmode == Si468xOperationMode::FMRadio)
    GetFMManager().tune();
  else if (opmode == Si468xOperationMode::DABRadio)
    GetDABManager().tune();
}

void
Si468xDriver::setAntCap(uint8_t value)
{
  if(value > 128)
    return;

  AntCapVal = value;

  if ( (getOpMode() == Si468xOperationMode::DABRadio) ||
       (getOpMode() == Si468xOperationMode::FMRadio))
    OpMgr.DAB.tune(true);
}

ECPP_JOB_DEF(Si468xDriver,Poll)
{
  getMgr().poll();
}

ECPP_JOB_DEF(Si468xDriver,Timeout)
{
  getMgr().timeout();
}

ECPP_JOB_DEF(Si468xDriver, TransactionCompleteAsync)
{
  auto & ta   = sys::GetSi468xTransaction(*this);

  getMgr().handleCommandFinished(ta.GetLastCommand(), ta.GetStatus());

  auto action = sys::GetSi468xActions(*this).VirtualAction().Get();
  if(action != nullptr)
    action->HandleCommandFinished(ta.GetLastCommand(), ta.GetStatus());
}