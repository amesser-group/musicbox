/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DRIVERS_SI468X_TRANSACTION_HPP_
#define MUSICBOX_DRIVERS_SI468X_TRANSACTION_HPP_

#include "ecpp/System/TickTimer.hpp"

#include "Platform.hpp"
#include "Drivers/SI468x/Status.hpp"
#include "Devices/SI4688_Registers.hpp"
#include "ecpp/BinaryStruct.hpp"
#include <cstdint>

namespace MusicBox::Drivers::SI468x
{
  using ::ecpp::BinaryStruct;
  using ::ecpp::Units::MilliSecond;

  template<typename RequestBlock>
  class Si468xTransaction : protected RequestBlock, protected ::MusicBox::RegDef::Si4688
  {
  public:
    using RequestBlock::GetStruct;

    bool SendCommand        (uint_fast16_t len);
    bool SendSimpleCommand  (uint_fast8_t command, uint_fast16_t len);
    bool ReadResponse       (uint_fast16_t len = 4);
    bool SetProperty        (uint_fast16_t property, uint_fast16_t value);

    using RequestBlock::GetReadLen;
    using RequestBlock::GetWriteLen;
    using RequestBlock::IsSame;

    uint8_t        GetLastCommand() const   { return last_command_;}
    const Si4688Status & GetStatus() const  { return last_command_status_;}

    bool IsReady()   const { return state_ == State::IDLE; }
    bool IsPending() const { return state_ != State::IDLE; }

    ECPP_JOB_DECL(RequestComplete);

  private:
    enum class State : uint8_t
    {
      IDLE,
      W_START,
      W_TWI_WCNF,
      W_TWI_RCNF,
    };

    bool SendReadCommand (uint_fast16_t len);

    ::ecpp::System::TickTimer last_command_tmr_;
    Si4688Status              last_command_status_  {};
    uint8_t                   last_command_         {0};
    State                     state_                {State::IDLE };
  };

  template<typename RequestBlock> bool
  Si468xTransaction<RequestBlock>::SendCommand(uint_fast16_t len)
  {
    if (len < 1)
      return false;

    if (state_ != State::IDLE)
      return false;

    state_        = State::W_TWI_WCNF;
    last_command_ = GetStruct().GetField8(0);

    RequestBlock::SetUp();
    RequestBlock::Submit(len, 0);

    return true;
  }

  template<typename RequestBlock> bool
  Si468xTransaction<RequestBlock>::SendSimpleCommand(uint_fast8_t command, uint_fast16_t len)
  {
    if (state_ != State::IDLE)
      return false;

    GetStruct().SetField32(0, command);
    return SendCommand(len);
  }

  template<typename RequestBlock> bool
  Si468xTransaction<RequestBlock>::SendReadCommand(uint_fast16_t len)
  {
    if (len < 4)
      len = 4;

    state_ = State::W_TWI_RCNF;

    RequestBlock::SetUp();
    RequestBlock::GetStruct().SetField8(0, CMD_RD_REPLY);
    RequestBlock::Submit(1, len);

    return true;
  }

  template<typename RequestBlock> bool
  Si468xTransaction<RequestBlock>::ReadResponse(uint_fast16_t len)
  {
    if (state_ != State::IDLE)
      return false;

    return SendReadCommand(len);
  }

  template<typename RequestBlock> bool 
  Si468xTransaction<RequestBlock>::SetProperty(uint_fast16_t property, uint_fast16_t value)
  {
    auto s = RequestBlock::GetStruct();

    if (state_ != State::IDLE)
      return false;

    s.SetField16(0, CMD_SET_PROPERTY);
    s.SetField16(2, property);
    s.SetField16(4, value);

    return SendCommand(6);
  }

  
  ECPP_TEMPLATE_JOB_DEF(template<typename RequestBlock>, Si468xTransaction<RequestBlock>, RequestComplete)
  {
    if( (State::W_TWI_WCNF != state_) &&
        (State::W_TWI_RCNF != state_) )
      return;

    if(!RequestBlock::WasSuccessful())
    {
      last_command_status_ = 0xFFFFFFFF;
      state_ = State::IDLE;

      RequestBlock::GetDriver().TransactionComplete();
    }
    else if (State::W_TWI_WCNF == state_)
    {
      /* write is now done, start polling the status */
      SendReadCommand(5);
      last_command_tmr_.Start();
    }
    else
    {
      auto s = RequestBlock::GetStruct();

      if(RequestBlock::GetReadLen() >= 4)
        last_command_status_.update(s.template GetBlock<uint8_t[4]>(0));

      if((s.GetField8(0) & MSK_STATUS0_CTS))
      {
        state_ = State::IDLE;
        RequestBlock::GetDriver().TransactionComplete();
      }
      else if(last_command_tmr_.IsExpired(MilliSecond<>(1000)))
      { /* looks like chip lookup */
        last_command_status_ = 0xFFFFFFFF;
        state_ = State::IDLE;
        RequestBlock::GetDriver().TransactionComplete();
      }
      else
      { /* not yet ready, continue polling */
        SendReadCommand(5);
      }
    }
  }
}

#endif