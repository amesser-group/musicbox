/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Driver.hpp"
#include "System.hpp"

using namespace MusicBox::Drivers::SI468x;

void
Si4688FirmwareManager::resetDevice()
{
  auto & drv = GetDriver();
  auto & ta  = ::sys::GetSi468xTransaction(drv);

  if(ta.IsReady())
  {
    if(sys::bsp().AssertReset(drv))
    {
      m_State = StateType::STATE_W_RESET_ASSERTED;
      drv.startTimeout(MilliSecond<>(100));
    }
    else
    {
      drv.startTimeout(MilliSecond<>(10));
    }
  }
  else
  {
    drv.startTimeout(MilliSecond<>(10));
  }
}

/* Using a CX3225SB crystal 19.2 Mhz CL=8pf*/
constexpr uint8_t  clk_mode  = 1;
constexpr uint8_t  tr_size   = 0x7;        /* for 19.2 Mhz crystal */
constexpr uint8_t  ibias     = 600 / 10 ;  /* 600 µA startup bias */
constexpr uint8_t  ibias_run = 200 / 10;   /* 200 µA runtime bias */
constexpr uint32_t freq      = 19200000;   /* crystal freq */
constexpr uint8_t  ctun      = static_cast<uint8_t>(2*(8. - 3.) / 0.381); /* tuning capacitance occording to manual */

const uint8_t Si4688FirmwareManager::s_PowerUpCmd[16] =
{
  CMD_POWER_UP,
  0,
  (clk_mode << 4) | tr_size,
  0x7f & ibias,
  freq & 0xFF,
  (freq >> 8) & 0xFF,
  (freq >> 16) & 0xFF,
  (freq >> 24) & 0xFF,
  0x3f & ctun,
  0x10,
  0,
  0,
  0,
  0x7F & ibias_run,
  0,
  0
};

void Si4688FirmwareManager::bootChip()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());
  BinaryStruct s = ta.GetStruct();

  if((TargetMode != Si468xOperationMode::FMRadio) &&
     (TargetMode != Si468xOperationMode::DABRadio) &&
     (TargetMode != Si468xOperationMode::UpdateFlash))
    return;

  m_State = StateType::STATE_W_CMD_POWERUP;

  s.SetBlock(0, s_PowerUpCmd);
  ta.SendCommand(sizeof(s_PowerUpCmd));
}

void
Si4688FirmwareManager::handleLoadPatch()
{
  auto & drv = GetDriver();
  auto & ta  = ::sys::GetSi468xTransaction(drv);

  if(m_State == StateType::STATE_W_CMD_POWERUP)
  {
    m_State = StateType::STATE_W_CMD_LOAD_PATCH;
    m_Offset = 0;

    ta.SendSimpleCommand(CMD_LOAD_INIT, 2);
  }
  else if(m_State == StateType::STATE_W_CMD_LOAD_PATCH)
  {
    if(m_Offset < sizeof(Rom00Patch016))
    {
      BinaryStruct s = ta.GetStruct();

      uint_fast8_t DataLen;
      s.SetField32(0, CMD_HOST_LOAD);

      DataLen = s.Pad(4, &(Rom00Patch016[m_Offset]), sizeof(Rom00Patch016) - m_Offset);
      /* Datalength must be 4 byte multiples */
      DataLen -= DataLen % 4;

      m_Offset += DataLen;
      ta.SendCommand(4 + DataLen);
    }
    else
    {
      m_State = StateType::STATE_W_LOAD_PATCH;
      drv.startTimeout(MilliSecond<>(5));
    }
  }
}

void Si4688FirmwareManager::handleLoadFirmware()
{
  auto & drv = GetDriver();
  auto & ta  = ::sys::GetSi468xTransaction(drv);

  if(m_State == StateType::STATE_W_LOAD_PATCH)
  {
    m_State = StateType::STATE_W_CMD_LOAD_FIRMWARE;
    m_Offset = 0;
    ta.SendSimpleCommand(CMD_LOAD_INIT, 2);
  }
  else if(m_State == StateType::STATE_W_CMD_LOAD_FIRMWARE)
  {
    BinaryStruct s = ta.GetStruct();

    switch(TargetMode)
    {
    case Si468xOperationMode::Reset:
    case Si468xOperationMode::Disabled:
    case Si468xOperationMode::Booting:
      break;
    case Si468xOperationMode::FMRadio:
      m_State = StateType::STATE_W_LOAD_FIRMWARE;
      s.SetField32(4, ::sys::bsp().kSi468x_FlashAddr_FMFirmware);
      s.SetField32(8, 0);
      ta.SendSimpleCommand(CMD_FLASH_PASS_THROUGH, 12);
      break;
    case Si468xOperationMode::DABRadio:
      m_State = StateType::STATE_W_LOAD_FIRMWARE;
      s.SetField32(4, ::sys::bsp().kSi468x_FlashAddr_DABFirmware);
      s.SetField32(8, 0);
      ta.SendSimpleCommand(CMD_FLASH_PASS_THROUGH, 12);
      break;
    case Si468xOperationMode::UpdateFlash:
      break; /* handled independendly */
    }
  }
  else if(m_State == StateType::STATE_W_LOAD_FIRMWARE)
  {
    m_State = StateType::STATE_W_CMD_BOOT;
    ta.SendSimpleCommand(CMD_BOOT, 2);
  }
  else if(m_State == StateType::STATE_W_CMD_BOOT)
  {
    m_State  = StateType::STATE_W_SET_PROPERTIES;
    m_Offset = 0;
  }

  if(m_State == StateType::STATE_W_SET_PROPERTIES)
  {
    auto properties = sys::bsp().GetConfiguration(drv);

    if(0xFFFF != properties[m_Offset])
    {
      ta.SetProperty(properties[m_Offset], properties[m_Offset+1]);
      m_Offset += 2;
    }
    else
    {
      drv.changeMgr(TargetMode);
    }
  }
}

void Si4688FirmwareManager::handleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  bool success = !status.isErrorBitSet();

  switch(m_State)
  {
  case StateType::STATE_W_CMD_POWERUP:
    if(success)
      handleLoadPatch();
    else
      resetDevice();
    break;
  case StateType::STATE_W_CMD_LOAD_PATCH:
    if(success)
      handleLoadPatch();
    else
      resetDevice();
      /* m_State = StateType::STATE_ERROR; */
    break;
    
  case StateType::STATE_W_CMD_LOAD_FIRMWARE:
  case StateType::STATE_W_LOAD_FIRMWARE:
  case StateType::STATE_W_CMD_BOOT:
  case StateType::STATE_W_SET_PROPERTIES:
    if( (TargetMode == Si468xOperationMode::FMRadio) ||
        (TargetMode == Si468xOperationMode::DABRadio) )
    {
      if(success)
        handleLoadFirmware();
      else
        m_State = StateType::STATE_ERROR;      
    }
    else if(TargetMode == Si468xOperationMode::UpdateFlash)
    {
      m_State = StateType::STATE_RUN;
    }
    break;
  default:
    resetDevice();
    break;
  }
}

void
Si4688FirmwareManager::poll()
{
  switch(m_State)
  {
  case StateType::STATE_POWERON:
    resetDevice();
    break;
  default:
    break;
  }
}

void
Si4688FirmwareManager::timeout()
{
  auto & drv = GetDriver();
  auto & ta  = ::sys::GetSi468xTransaction(drv);

  if(!ta.IsReady())
  {
    drv.startTimeout(MilliSecond<>(5));
    return;
  }

  switch(m_State)
  {
  case StateType::STATE_POWERON:
    resetDevice();
    break;
  case StateType::STATE_W_RESET_ASSERTED:
    m_State = StateType::STATE_W_RESET_DEASSERTED;

    sys::bsp().DeassertReset(drv);
    drv.startTimeout(MilliSecond<>(100));
    break;
  case StateType::STATE_W_RESET_DEASSERTED:
    sys::bsp().ConnectI2C(drv);
    bootChip();
    break;
  case StateType::STATE_W_LOAD_PATCH:
    handleLoadFirmware();
    break;
  default:
    break;
  }
}

