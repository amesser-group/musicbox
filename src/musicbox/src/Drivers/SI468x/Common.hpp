/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SI4688_MANAGERBASE_HPP_
#define SI4688_MANAGERBASE_HPP_

#include <cstdint>
#include <cstring>

#include "Platform.hpp"

#include "ecpp/Math/FixedPoint.hpp"
#include "ecpp/Math/Statistics.hpp"

#include "ecpp/Container.hpp"

#include "Devices/SI4688_Registers.hpp"
#include "Drivers/SI468x/Status.hpp"

namespace MusicBox::Drivers::SI468x
{
  using ::std::memcpy;
  using ::std::uint_least8_t;
  using ::std::uint_least32_t;
  using Drivers::Si4688::Si4688Status;

  enum class Si468xOperationMode : uint_least8_t
  {
    Reset,
    Disabled,
    Booting,
    FMRadio,
    DABRadio,
    UpdateFlash,
  };

  enum class TunerStateType : uint_least8_t
  {
    Unknown,
    Tuned,
    Failed,
  };


  struct Si468xEvent
  {
    struct OpmodeChanged;
  };

  struct Si468xEvent::OpmodeChanged : public Si468xEvent
  {
    constexpr OpmodeChanged(Si468xOperationMode m) : Si468xEvent(), mode(m) {};

    Si468xOperationMode mode;
  };

  class Si468xDriver;
  
  class Si468xManagerBase : protected ::MusicBox::RegDef::Si4688
  {
  public:
    constexpr Si468xManagerBase(Si468xOperationMode mode) : OpMode { mode } {}
    virtual  ~Si468xManagerBase() {};

    constexpr Si468xOperationMode getOpMode()     const { return OpMode; }

    virtual void handleCommandFinished(uint_fast8_t command, const Si4688Status& status) = 0;
    virtual void poll()              = 0;
    virtual void timeout()           = 0;
    virtual bool ModeReached() const = 0;

  protected:
    Si468xDriver & GetDriver();

    const          Si468xOperationMode OpMode;
  };


  enum class Si468xDriverCmd : uint_least8_t
  {
    None = 0,
    SeekForward,
    SeekBackWard,
    Retune,
    Tune,
  };

  typedef ::ecpp::Math::FixedPoint<uint_least32_t, 256> Si468xRssiType;

  class Si468xRadioManager : public Si468xManagerBase
  {
  public:
    constexpr Si468xRadioManager(Si468xOperationMode mode) : Si468xManagerBase{mode} {};

    constexpr TunerStateType      getTunerState() const { return TunerState; }
    constexpr uint_least32_t      getTunedFreq()  const { return TunedFrequency; }
    constexpr Si468xRssiType      getRssi()       const { return AverageRssi.getValue(); }

    void tune(bool force = false);
    void seekForward();
    void seekBackward();
  protected:

    void changeTunerState(TunerStateType state);

    TunerStateType                                     TunerState      { TunerStateType::Unknown };
    Si468xDriverCmd                                    RequestedAction { Si468xDriverCmd::Tune };
    uint_least32_t                                     TunedFrequency  {0};
    ::ecpp::Math::Statistics::PT1<Si468xRssiType, 16>  AverageRssi     {-128};
  };
};

#endif
