/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Common.hpp"
#include "System.hpp"

using namespace MusicBox::Drivers::SI468x;

void
Si468xRadioManager::changeTunerState(TunerStateType state)
{
  if( (TunerStateType::Failed == state) &&
      (state == TunerState))
    return;

  TunerState = state;
  signals::Changed(GetDriver(), state);
}


void
Si468xRadioManager::tune(bool force)
{
  if (force)
    RequestedAction = Si468xDriverCmd::Retune;
  else
    RequestedAction = Si468xDriverCmd::Tune;

  GetDriver().pollAsync();
}

void
Si468xRadioManager::seekForward()
{
  RequestedAction = Si468xDriverCmd::SeekForward;
  GetDriver().pollAsync();
}

void
Si468xRadioManager::seekBackward()
{
  RequestedAction = Si468xDriverCmd::SeekBackWard;
  GetDriver().pollAsync();
}
