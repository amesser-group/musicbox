/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SI4688_DABRADIOMANAGER_HPP_
#define SI4688_DABRADIOMANAGER_HPP_

#include "ecpp/System/TickTimer.hpp"

#include "Database/DabTransponder.hpp"
#include "Drivers/SI468x/Common.hpp"

namespace MusicBox::Drivers::SI468x
{
  class Si468xDABManager : public Si468xRadioManager
  {
  public:
    enum class StateType : uint8_t
    {
      STATE_W_SET_PROPERTIES,
      STATE_W_SET_CHANNELS,
      STATE_READY,
      STATE_TUNING,
      STATE_TUNED,
      STATE_PLAY,
      STATE_ERROR,
    };

    constexpr Si468xDABManager() : Si468xRadioManager(Si468xOperationMode::DABRadio) {}
    virtual  ~Si468xDABManager() {}

    constexpr int_fast8_t getServiceCount() const {return ServiceCount;}

  private:
    StateType                 m_State  = StateType::STATE_W_SET_PROPERTIES;

    volatile uint_least16_t  m_Offset     {0};
    ::MusicBox::Database::DabChannelHandle m_DabChannel {};

    unsigned long   m_Debug       {0};
    int_least8_t   FFTOffset      {0};
    int_least8_t    ServiceCount {-1};
    uint_least16_t  service_list_version_ {0};

    ::ecpp::System::TickTimer service_list_changed_timer_;

    static const uint16_t s_BootupProperties[];

    void        sendCommandGetEventStatus();

    void        handleDABDigradStatusCommand();
    void        handleDABGetEventStatusCommand();
    void        handleTestGetRssi();
    void        handleGetTime();
    void        handleDABSetFreqListStatusCommand();
    void        handleDABStartDigitalServiceCommand(bool success);
    void        handleGetDigitalServiceListCommand();

    void        handleDABBoot();
    void        setChannelsList();
    void        sendCommandTestGetRssi();
    void        handleAction();
    void        handleTuning();
    void        handleTimeout();
  public:
    unsigned long  getDebugVal() const {return m_Debug;}
    int8_t   getFFTOffset() const {return FFTOffset; }

    void handleCommandFinished(uint_fast8_t command, const Si4688Status& status) override;
    void timeout() override;
    void poll() override;
    bool ModeReached() const override { return m_State >= StateType::STATE_READY; }

  };
}

#endif