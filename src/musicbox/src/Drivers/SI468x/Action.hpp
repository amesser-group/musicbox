/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DRIVERS_SI468X_ACTION_HPP_
#define MUSICBOX_DRIVERS_SI468X_ACTION_HPP_  

#include "Drivers/SI468x/Driver.hpp"

namespace MusicBox::Drivers::SI468x
{
  class Si468xAction
  {
  protected:
    constexpr    Si468xAction () {}

    virtual void HandleCommandFinished(uint_fast8_t command, const Si4688Status& status) {};

    Si468xDABManager*  GetDABManager();
    Si468xManagerBase& GetManager();

    friend class Si468xDriver;
  };
}

#endif