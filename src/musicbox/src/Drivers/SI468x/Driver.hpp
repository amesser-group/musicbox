/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SI4688_DRIVER_HPP_
#define SI4688_DRIVER_HPP_

#include "Drivers/SI468x/Common.hpp"
#include "Drivers/SI468x/DABManager.hpp"
#include "Drivers/SI468x/FirmwareManager.hpp"
#include "Drivers/SI468x/FMManager.hpp"

#include "Database/ServiceDatabase.hpp"
#include "Database/ServiceIterator.hpp"
#include "Database/ServiceValueIterator.hpp"
#include "Database/DabTransponder.hpp"

#include "ecpp/Units/Time.hpp"
#include "Platform.hpp"

namespace MusicBox::Drivers::SI468x
{

  class Si468xDriver
  {
  protected:
    using ServiceValue         = ::MusicBox::Database::ServiceValue;
    using ServiceIterator      = ::MusicBox::Database::ServiceIterator;
    using ServiceValueIterator = ::MusicBox::Database::ServiceValueIterator;

  public:
    constexpr Si468xDriver() {}
    ~Si468xDriver();

    const     Si468xManagerBase & getMgr()        const { return OpMgr.Firmware; }
    Si468xOperationMode  getOpMode()     const { return getMgr().getOpMode(); }
    TunerStateType       getTunerState() const { return getRadioManager().getTunerState(); }
    uint_least32_t       getTunedFreq()  const { return getRadioManager().getTunedFreq(); }
    const     RDSState&  getRDSState()   const { return getFMManager().getRDSState(); }

    const Si468xRadioManager       & getRadioManager()        const  { return OpMgr.DAB;}
    const Si468xDABManager         & getDABManager()          const  { return OpMgr.DAB;}
    const Si468xFMManager          & getFMManager()           const  { return OpMgr.FM;}

    void  TransactionComplete()    { TransactionCompleteAsync.Enqueue(); };

    void pollAsync()
    {
      Poll.Enqueue();
    }

    constexpr bool service_info_stable() const { return service_info_stable_; }

  private:
    Si468xDABManager   & GetDABManager()   { return OpMgr.DAB;}
    Si468xFMManager    & GetFMManager()    { return OpMgr.FM;}

    void changedMgr();

    ECPP_JOB_DECL(Poll);
    ECPP_JOB_TIMER_DECL(Timeout);
    ECPP_JOB_DECL(TransactionCompleteAsync);

    Database::ServiceValue   current_service_       {};
    Database::ServiceHandle  current_service_handle_{};
    uint_least8_t            service_info_stable_   {false};

    uint_least8_t                  AntCapVal     {0};

    union Blub {
      ~Blub();
      Si4688FirmwareManager          Firmware;
      Si468xDABManager               DAB;
      Si468xFMManager                FM;
    } OpMgr = { .Firmware = { Si468xOperationMode::Disabled } };


    template<typename T>
    void startTimeout(T timeout)
    {
      Timeout.Start(timeout);
    }

    /** getters for casted container */
    Si468xManagerBase       & getMgr()       { return OpMgr.Firmware; }

    template<typename T, typename ...ARGS>
    void changeMgr(ARGS ...args)
    {
      getMgr().~Si468xManagerBase();
      new (&OpMgr) T(args...);
      changedMgr();
    }

    void changeMgr(Si468xOperationMode mode);

    void notifyStatus(uint_fast8_t last_command, const Si4688Status& status);
    void notifyOperationMode(Si468xOperationMode Mode);

    static Si468xDriver & GetDriver(Si468xManagerBase& ref)
    {
      return container_of(Si468xDriver, OpMgr, ref);
    }

    static const Si468xDriver & GetDriver(const Si468xManagerBase& ref)
    {
      return container_of(Si468xDriver, OpMgr, ref);
    }
  public:
    void start() { pollAsync();}
    void seek(bool forward);

    void activateMode(Si468xOperationMode opmode, bool force = false);

    Si468xOperationMode getMode()       const {return getMgr().getOpMode(); }

    constexpr const Database::ServiceValue   & GetCurrentService()       const {return current_service_; }
    constexpr const Database::ServiceHandle    GetCurrentServiceHandle() const {return current_service_handle_; }

    void tuneChannel(const Database::ServiceValue    &service, Si468xOperationMode opmode = Si468xOperationMode::Booting);
    void tuneChannel(MusicBox::Database::DabChannelHandle channel_handle);
    void tuneChannel(Database::ServiceHandle   handle,  Si468xOperationMode opmode = Si468xOperationMode::Booting);

    void tuneChannel(Database::ServiceIterator it,  Si468xOperationMode opmode = Si468xOperationMode::Booting)
      { tuneChannel(it.GetHandle(), opmode); }

    void handleRDSFrame(const RDSFrame &Frame);

    uint8_t getAntCapValue() const {return AntCapVal; }
    void              setAntCap(uint8_t val);

  private:
    void tuneChannel(Si468xOperationMode opmode);

    friend class Si468xAction;
    friend class Si468xManagerBase;
    //friend class Si468xTransaction;
    friend class Si468xRadioManager;
    friend class Si468xFMManager;
    friend class Si468xDABManager;
    friend class Si4688FirmwareManager;
    friend class Si468xDabChannelScanner;
  };

  inline Si468xDriver & Si468xManagerBase::GetDriver()
  {
    return Si468xDriver::GetDriver(*this);
  }
}

namespace signals
{
  void Changed(const ::MusicBox::Drivers::SI468x::Si468xDriver&);
  void Changed(const ::MusicBox::Drivers::SI468x::Si468xDriver&, ::MusicBox::Drivers::SI468x::Si468xOperationMode);
  void Changed(const ::MusicBox::Drivers::SI468x::Si468xDriver&, const ::RDSManager&);
  void Changed(const ::MusicBox::Drivers::SI468x::Si468xDriver&, ::MusicBox::Drivers::SI468x::TunerStateType);
  void ServiceStarted(const ::MusicBox::Drivers::SI468x::Si468xDriver&);
}

#endif
