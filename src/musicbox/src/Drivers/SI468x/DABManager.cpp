/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Driver.hpp"
#include "Database/ServiceDatabase.hpp"
#include "System.hpp"

using namespace MusicBox::Drivers::SI468x;
using namespace MusicBox::Database;

const uint16_t Si468xDABManager::s_BootupProperties[] =
{
  /* For DAB VHFSW should be closed since we're using the "Appendix C
   * Applicatin circuit for EMI mitigation. See AN649, AN650. Since
   * this is the default, we can leave it as is
   * PROP_DAB_TUNE_FE_CFG,                          0x0001, */
  PROP_TUNE_FE_VARM,        static_cast<uint16_t>(-1541),
  PROP_TUNE_FE_VARB,        static_cast<uint16_t>(  394),
  PROP_DAB_VALID_RSSI_TIME,                          63,
  PROP_DAB_VALID_SYNC_TIME,                           0,
  PROP_DAB_VALID_DETECT_TIME,                         0,
  PROP_DAB_ACF_ENABLE,                           0x0000,
  PROP_DAB_CTRL_DAB_MUTE_SIGNAL_LEVEL_THRESHOLD,      0,
  PROB_DAB_ANOUNCEMENTS,                         0x0000,
  0xFFFF
};

void Si468xDABManager::handleDABBoot()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());

  if(s_BootupProperties[m_Offset] != 0xFFFF)
  {
    ta.SetProperty(s_BootupProperties[m_Offset], s_BootupProperties[m_Offset+1]);
    m_Offset += 2;
  }
  else
  {
    m_State = StateType::STATE_W_SET_CHANNELS;
    setChannelsList();
  }
}

/** Configure out channel list into the Si468x
 */
void
Si468xDABManager::setChannelsList()
{
  auto const & channels = Database::ServiceDatabase::instance().getDabChannelList();
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());
  auto s    = ta.GetStruct();
  int_fast8_t i;

  s.SetField32(0, CMD_DAB_SET_FREQ_LIST);

  for(i = 0; i < 48; ++i)
  {
    if (channels[i].Freq == 0)
      break;

    s.SetField32(i*4 + 4, channels[i].Freq);
  }

  s.SetField8(1,  i);
  ta.SendCommand(i*4 + 4);
}

void Si468xDABManager::handleAction()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());

  if(m_State < StateType::STATE_READY)
    return;

  if(!ta.IsReady())
    return;

  if( (RequestedAction == Si468xDriverCmd::Retune) ||
      (RequestedAction == Si468xDriverCmd::Tune))
  {
    auto & drv = Si468xDriver::GetDriver(*this);

    if( (m_State != StateType::STATE_TUNING) &&
        (drv.current_service_.DabChannel.isValid()) )
    {
      auto s    = ta.GetStruct();

      if ( (drv.current_service_.DabChannel != m_DabChannel) ||
           (RequestedAction                 == Si468xDriverCmd::Retune))
      {
        auto & db   = Database::ServiceDatabase::instance();

        m_State      = StateType::STATE_TUNING;
        m_DabChannel = drv.current_service_.DabChannel;

        AverageRssi.init(-128);
        TunedFrequency        = db.getDabChannelList().getDabChannelValue(m_DabChannel).Freq;
        ServiceCount          = -1;
        service_list_version_ = 0xFFFF;

        s.SetField16(0, CMD_DAB_TUNE_FREQ);
        s.SetField16(2, db.getDabChannelList().indexOf(m_DabChannel));
        s.SetField16(4, drv.AntCapVal);
        ta.SendCommand(6);
      }
      else
      {
        if(m_State > StateType::STATE_TUNED)
          m_State = StateType::STATE_TUNED;

        ta.SendSimpleCommand(CMD_DAB_DIGRAD_STATUS, 2);
      }

      RequestedAction = Si468xDriverCmd::None;
    }
  }
  else
  {
    RequestedAction = Si468xDriverCmd::None;
  }
}

void
Si468xDABManager::sendCommandGetEventStatus()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());
  auto s    = ta.GetStruct();

  s.SetField8(0, CMD_DAB_GET_EVENT_STATUS);
  s.SetField8(1,                      0x1);
  ta.SendCommand(2);

}

void Si468xDABManager::handleDABDigradStatusCommand()
{
  auto & drv = GetDriver();
  auto & ta  = ::sys::GetSi468xTransaction(drv);
  auto s     = ta.GetStruct();

  if((4+18) > ta.GetReadLen())
  {
    ta.ReadResponse(4+18);
    return;
  }

  if(m_State < StateType::STATE_TUNED)
    AverageRssi.init(Si468xRssiType{s.GetSigned8(6),1});
#if 0
  else
    AverageRssi.sample(Si468xRssiType{cmd.getSigned8(6),1});
#endif

  TunedFrequency = s.GetField32(12);
  FFTOffset      = s.GetField8(17);

  // drv.AntCapVal = cmd.getField8(18);

  m_Debug = s.GetField32(5);
  signals::Changed(drv);

  if(!ta.GetStatus().isSeeking())
  {
    bool valid = (s.GetField8(5) & 0x01) != 0;

    if(m_State == StateType::STATE_TUNING)
    {
      if (valid)
      {
        m_State = StateType::STATE_TUNED;
        changeTunerState(TunerStateType::Tuned);
      }
      else
      {
        m_State = StateType::STATE_READY;
        changeTunerState(TunerStateType::Failed);

        RequestedAction = Si468xDriverCmd::Retune;
      }
    }

    /* When sucessfully tuned into the ensemble, start playing */
    if(m_State == StateType::STATE_TUNED)
    {
      if(drv.current_service_.isDabService())
      {
        m_State = StateType::STATE_PLAY;

        s.SetField32(4, drv.current_service_.ServiceId);
        s.SetField32(8, drv.current_service_.CId);
        ta.SendSimpleCommand(CMD_START_DIGITAL_SERVICE, 12);
      }
    }

    /* if we still can send a command, either poll for dab
     * status or process with test rssi command */
    if(ta.IsReady())
    {
      if(getTunerState() == TunerStateType::Tuned)
        sendCommandGetEventStatus();
      else
        sendCommandTestGetRssi();
    }
  }
}

void Si468xDABManager::handleDABGetEventStatusCommand()
{
  auto & drv = GetDriver();
  auto & ta = ::sys::GetSi468xTransaction(drv);
  auto s    = ta.GetStruct();
  uint_least16_t service_list_version;

  if((4+4) > ta.GetReadLen())
  {
    ta.ReadResponse(4+4);
    return;
  }

  if( (s.GetField8(5) & 0x01) &&
      (s.GetField16(6) != service_list_version_))
  {
    drv.service_info_stable_ = false;
    service_list_changed_timer_.Start();

    ta.SendSimpleCommand(CMD_GET_DIGITAL_SERVICE_LIST, 2);
  }
  else
    sendCommandTestGetRssi();
}

void Si468xDABManager::handleTestGetRssi()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());
  auto s    = ta.GetStruct();

  if(6 > ta.GetReadLen())
  {
    ta.ReadResponse(6);
    return;
  }

  AverageRssi.sample(Si468xRssiType{s.GetSigned16(4), 256});
}

void Si468xDABManager::handleDABSetFreqListStatusCommand()
{
  //auto & drv = Si468xDriver::GetDriver(*this);
  //auto & cmd = drv.tSiCmd;
}

void Si468xDABManager::handleDABStartDigitalServiceCommand(bool success)
{
  auto & drv = GetDriver();
  if(success)
    ::signals::ServiceStarted(drv);
  else
    m_State = StateType::STATE_TUNED;

  if(getTunerState() == TunerStateType::Tuned)
    sendCommandGetEventStatus();
  else
    sendCommandTestGetRssi();
}

void Si468xDABManager::handleGetDigitalServiceListCommand()
{
  auto & drv = GetDriver();
  auto & ta  = ::sys::GetSi468xTransaction(drv);
  auto s    = ta.GetStruct();


  unsigned int size;
  unsigned int offset;
  unsigned int num_services;

  if(6 > ta.GetReadLen())
  {
    ta.ReadResponse(6);
    return;
  }

  size = s.GetField16(4);

  if( (8+size) > s.GetBufferSize())
    size = s.GetBufferSize() - 8;

  if(size == 0)
    return;

  if(((8 + size) > ta.GetReadLen()))
  {
    ta.ReadResponse(8 + size);
    return;
  }

  size    = 6 + s.GetField16(4);
  offset  = 4;

  service_list_version_ = s.GetField16(offset + 2);

  num_services = s.GetField8(offset + 4);
  offset += 8;

  auto program_services = 0;
  while(num_services--)
  {
    const char* service_name;
    unsigned int num_component;
    uint_fast16_t service_id;
    uint_fast8_t flags;

    if((offset + 24) > size)
      return;

    service_id    = s.GetField32(offset + 0);
    flags         = s.GetField8(offset+4);
    num_component = s.GetField8(offset+5) & 0x0F;
    service_name  = s.GetBlock<char[16]>(offset+8);
    offset += 24;

    while(num_component--)
    {
      auto & db = Database::ServiceDatabase::instance();

      bool     primary_component;
      uint16_t component_id;

      if((offset + 4) > size)
        return;

      if(0 == (flags & 0x01))
      { /* program service */
        component_id      = s.GetField16(offset);
        primary_component = (0 != (s.GetField8(offset+2) & 0x2));

        if(primary_component)
        {
          /* init service value current service */
          ServiceValue v {};
          ServiceName  tmp;

          v.DabChannel = m_DabChannel;
          v.ServiceId  = service_id;
          v.CId        = component_id;

          tmp.assignRawValue(service_name, 16);
          v.Name = tmp.Trim();

          if(v.Name.CountCharacters() > 0)
          {
            program_services++;

            auto handle = db.UpdateService(v);
            auto const * service = db.GetService(handle);

            if(service != nullptr)
            {
              if(drv.current_service_.isSameService(*service))
              {
                drv.current_service_ = *service;
                drv.current_service_handle_ = handle;
              }
            }
          }
        }
      }
      else
      { /* Data service */

      }

      offset += 4;
    }
  }

  ServiceCount = program_services;

  sendCommandTestGetRssi();
}

void Si468xDABManager::handleTuning()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());
  ta.SendSimpleCommand(CMD_DAB_DIGRAD_STATUS, 2);
}

void Si468xDABManager::sendCommandTestGetRssi()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());
  ta.SendSimpleCommand(CMD_TEST_GET_RSSI, 2);
}

void Si468xDABManager::handleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  auto & drv = Si468xDriver::GetDriver(*this);
  bool success = !status.isErrorBitSet();

  if(success)
    drv.startTimeout(MilliSecond<>(200));

  switch(m_State)
  {
  case StateType::STATE_W_SET_PROPERTIES:
    if (success)
      handleDABBoot();
    else
      drv.activateMode(Si468xOperationMode::DABRadio, true);
    break;
  case StateType::STATE_W_SET_CHANNELS:
    if (success)
    {
      m_State = StateType::STATE_READY;
      drv.pollAsync();
    }
    break;
  case StateType::STATE_TUNING:
    if((command == CMD_DAB_DIGRAD_STATUS) && success)
      handleDABDigradStatusCommand();
    else
      handleTuning();
    break;
  case StateType::STATE_READY:
  case StateType::STATE_TUNED:
  case StateType::STATE_PLAY:
    if(command == CMD_START_DIGITAL_SERVICE)
    {
      handleDABStartDigitalServiceCommand(success);
    }
    else if (success)
    {
      if(command == CMD_DAB_DIGRAD_STATUS)
        handleDABDigradStatusCommand();
      else if(command == CMD_GET_DIGITAL_SERVICE_LIST)
        handleGetDigitalServiceListCommand();
      else if(command == CMD_DAB_GET_EVENT_STATUS)
        handleDABGetEventStatusCommand();
      else if(command == CMD_TEST_GET_RSSI)
        handleTestGetRssi(); 
    }
    break;
  case StateType::STATE_ERROR:
    break;
  }
}

void Si468xDABManager::timeout()
{
  auto & drv = GetDriver();

  drv.startTimeout(MilliSecond<>(250));
  poll();
}

void Si468xDABManager::poll()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());
  auto & drv = Si468xDriver::GetDriver(*this);

  if( (m_State == StateType::STATE_TUNED) ||
      (m_State == StateType::STATE_PLAY) )
  {
    if (not drv.service_info_stable_) 
      drv.service_info_stable_ = service_list_changed_timer_.IsExpired(MilliSecond<>(5000));
  }

  if(!ta.IsReady())
    return;

  switch(m_State)
  {
  case StateType::STATE_W_SET_PROPERTIES:
    handleDABBoot();
    break;
  case StateType::STATE_W_SET_CHANNELS:
    break;
  case StateType::STATE_READY:
    handleAction();
    break;
  case StateType::STATE_TUNING:
  case StateType::STATE_TUNED:
  case StateType::STATE_PLAY:
    handleAction();

    if(ta.IsReady())
      ta.SendSimpleCommand(CMD_DAB_DIGRAD_STATUS, 2);
    break;
  case StateType::STATE_ERROR:
    handleAction();
  }
}
