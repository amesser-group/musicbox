/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Driver.hpp"
#include "System.hpp"

using namespace MusicBox::Drivers::SI468x;
using namespace MusicBox::Database;
using ::ecpp::Units::MilliSecond;

const uint16_t Si468xFMManager::s_BootupProperties[] =
{
  /* For FM VHFSW should be open since we're using the "Appendix C
   * Application circuit for EMI mitigation. See AN649, AN650. Since
   * this is the default, we can leave it as is
   * PROP_FM_TUNE_FE_CFG,                          0x0000, */
  PROP_FM_RDS_CONFIG,        0x00A1,
  0xFFFF
};

void Si468xFMManager::handleFMBoot()
{
  if(m_State == StateType::STATE_BOOTED)
  {
    m_Offset = 0;
    m_State = StateType::STATE_W_SET_PROPERTIES;
  }

  if(m_State == StateType::STATE_W_SET_PROPERTIES)
  {
    if(s_BootupProperties[m_Offset] != 0xFFFF)
    {
      auto & ta = ::sys::GetSi468xTransaction(GetDriver());

      ta.SetProperty(s_BootupProperties[m_Offset], s_BootupProperties[m_Offset+1]);
      m_Offset += 2;
    }
    else
    {
      m_State = StateType::STATE_READY;
    }
  }

  if(m_State == StateType::STATE_READY)
  {
    handleAction();
  }
}

void Si468xFMManager::handleAction()
{
  auto & drv = GetDriver();  
  auto & ta  = ::sys::GetSi468xTransaction(drv);
  auto  s    = ta.GetStruct();

  if(m_State < StateType::STATE_READY)
  {
    return;
  }

  if(!ta.IsReady())
    return;

  if( (RequestedAction == Si468xDriverCmd::SeekForward) ||
      (RequestedAction == Si468xDriverCmd::SeekBackWard))
  {
    if(m_State != StateType::STATE_SEEKING)
    {
      m_State = StateType::STATE_SEEKING;

      s.SetField16(0, CMD_FM_SEEK_START);

      if (RequestedAction == Si468xDriverCmd::SeekForward)
        s.SetField16(2, MSK_WRAP | MSK_SEEKUP);
      else
        s.SetField16(2, MSK_WRAP);

      s.SetField16(4, drv.AntCapVal);
      ta.SendCommand(6);

      RequestedAction = Si468xDriverCmd::None;
      resetState();
    }
  }
  else if( (RequestedAction == Si468xDriverCmd::Tune ) ||
           (RequestedAction == Si468xDriverCmd::Retune ))
  {
    if(m_State != StateType::STATE_SEEKING)
    {
      auto & v = drv.GetCurrentService();

      if(v.FMFreq != 0)
      {
        RequestedAction = Si468xDriverCmd::None;
        m_State = StateType::STATE_SEEKING;

        s.SetField16(0, CMD_FM_TUNE_FREQ);
        s.SetField16(2, v.FMFreq);
        s.SetField16(4, drv.AntCapVal);
        ta.SendCommand(6);

        resetState();
      }
    }
  }
}

void Si468xFMManager::handleRSQStatusCommand()
{
  auto & drv = GetDriver();
  auto & ta = ::sys::GetSi468xTransaction(drv);
  auto  s   = ta.GetStruct();


  if(!ta.IsReady())
    return;

  if(ta.GetStatus().isErrorBitSet())
    return;

  if(ta.GetLastCommand() != CMD_FM_RSQ_STATUS)
    return;

  if((4+16) > ta.GetReadLen())
  {
    ta.ReadResponse(4+16);
    return;
  }

  drv.startTimeout(MilliSecond<>(200));

  TunedFrequency = s.GetField16(6) * 10UL;
  // drv.AntCapVal = cmd.getField8(12);

  if(m_State >= StateType::STATE_RUN)
    AverageRssi.sample(Si468xRssiType({s.GetSigned8(9),1}));
  else
    AverageRssi.init(Si468xRssiType({s.GetSigned8(9),1}));

  if(ta.GetStatus().isSeeking())
  { /* seeking */
    changeTunerState(TunerStateType::Unknown);
  }
  else
  {
    if(m_State == StateType::STATE_SEEKING)
      m_State = StateType::STATE_RUN;

    if (s.GetField8(5) & 0x01)
    { /* tuned */

      if(getTunerState() != TunerStateType::Tuned)
      {
        changeTunerState(TunerStateType::Tuned);
        ::signals::ServiceStarted(drv);
      }

      s.SetField16(0, CMD_FM_RDS_STATUS);
      ta.SendCommand(2);
    }
    else
    { /* no signal */
      changeTunerState(TunerStateType::Failed);
    }
  }
}

void Si468xFMManager::handleRDSStatusCommand()
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());
  auto  s   = ta.GetStruct();  

  uint_fast8_t fifo_level;

  if(!ta.IsReady())
    return;

  if(ta.GetStatus().isErrorBitSet())
    return;

  if(ta.GetLastCommand() != CMD_FM_RDS_STATUS)
    return;

  if(20 > ta.GetReadLen())
  {
    ta.ReadResponse(20);
    return;
  }

  fifo_level = s.GetField8(10);

  if (fifo_level > 0)
  {
    RDSFrame tFrame = {0};

    uint_fast8_t BLE = s.GetField8(11);

    if((BLE & 0xC0) != 0xC0)
      tFrame.ValidMask |= tFrame.BLOCKA_VALID;
    if((BLE & 0x30) != 0x30)
      tFrame.ValidMask |= tFrame.BLOCKB_VALID;
    if((BLE & 0x0C) != 0x0C)
      tFrame.ValidMask |= tFrame.BLOCKC_VALID;
    if((BLE & 0x03) != 0x03)
      tFrame.ValidMask |= tFrame.BLOCKD_VALID;

    tFrame.BlockA = s.GetField16(12);
    tFrame.BlockB = s.GetField16(14);
    tFrame.BlockC = s.GetField16(16);
    tFrame.BlockD = s.GetField16(18);

    handleRDSFrame(tFrame);
  }

  if (fifo_level > 1)
  {
    s.SetField16(0, CMD_FM_RDS_STATUS);
    ta.SendCommand(2);
  }
}


void Si468xFMManager::handleSeek(uint_fast8_t command, const Si4688Status& status)
{
  auto & ta = ::sys::GetSi468xTransaction(GetDriver());

  if(m_State != StateType::STATE_SEEKING)
    return;

  if(ta.IsReady())
  {
    auto  s   = ta.GetStruct();  

    s.SetField16(0, CMD_FM_RSQ_STATUS);
    ta.SendCommand(2);
  }
}


void
Si468xFMManager::handleRDSFrame(const RDSFrame &Frame)
{
  auto & drv = GetDriver();
  auto & db  = Database::ServiceDatabase::instance();

  bool notify;

  notify = RdsManager.handleFrame(Frame);

  if(TunerStateType::Tuned == getTunerState())
  {
    const auto & state = RdsManager.getState();

    if (state.isProgramIdentifierValid())
    {
      ServiceValue    val = {};
      const ServiceValue* ptr;

      val.ServiceId   = state.program_identifier;
      val.FMFreq      = getTunedFreq() / 10;
      val.Name        = state.program_service_name;

      drv.current_service_handle_ = db.UpdateService(val);
      
      if ((ptr = db.GetService(drv.current_service_handle_)))
        drv.current_service_ = *ptr;
      else
        drv.current_service_ = val;
    }
  }

  if(notify)
    ::signals::Changed(drv, RdsManager);
}

void
Si468xFMManager::resetState()
{
  auto & drv = GetDriver();

  RdsManager.invalidate();

  drv.service_info_stable_ = false;
  drv.current_service_ = Database::ServiceValue();

  ::signals::Changed(drv, RdsManager);
}


void Si468xFMManager::handleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  bool success = !status.isErrorBitSet();

  switch(m_State)
  {
  case StateType::STATE_BOOTED:
    break;
  case StateType::STATE_W_SET_PROPERTIES:
    if(success)
      handleFMBoot();
    else
      m_State = StateType::STATE_ERROR;
    break;
  case StateType::STATE_READY:
    break;
  case StateType::STATE_SEEKING:

    if(command == CMD_FM_RSQ_STATUS)
      handleRSQStatusCommand();

    handleSeek(command,status);
    break;
  case StateType::STATE_RUN:
    if(command == CMD_FM_RSQ_STATUS)
    {
      handleRSQStatusCommand();
    }
    else if(command == CMD_FM_RDS_STATUS)
    {
      handleRDSStatusCommand();
    }
    break;
  case StateType::STATE_ERROR:
    break;
  }
}

void Si468xFMManager::poll()
{
  handleFMBoot();
}

void Si468xFMManager::timeout()
{
  auto & drv = GetDriver();

  handleAction();

  if(m_State == StateType::STATE_RUN)
  {
    auto & ta = ::sys::GetSi468xTransaction(drv);
    auto  s   = ta.GetStruct();  
    
    if(ta.IsReady())
    {
      s.SetField16(0, CMD_FM_RSQ_STATUS);
      ta.SendCommand(2);
    }
    else
    {
      drv.startTimeout(MilliSecond<>(25));
    }
  }
  else if (m_State >= StateType::STATE_READY)
  {
    drv.startTimeout(MilliSecond<>(200));
  }

}