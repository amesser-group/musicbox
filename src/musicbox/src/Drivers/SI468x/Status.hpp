/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DRIVERS_SI4688_STATUS_HPP_
#define MUSICBOX_DRIVERS_SI4688_STATUS_HPP_

#include "Devices/SI4688_Registers.hpp"
#include <cstring>

namespace MusicBox::Drivers::Si4688
{

  class Si4688Status
  {
  public:
    using RegDef = ::MusicBox::RegDef::Si4688 ;

    constexpr Si4688Status() {};
    constexpr Si4688Status(uint32_t init) : status_ {static_cast<uint8_t>((init >> 0) & 0xFF),
                                                     static_cast<uint8_t>((init >> 8) & 0xFF),
                                                     static_cast<uint8_t>((init >> 16) & 0xFF),
                                                     static_cast<uint8_t>((init >> 24) & 0xFF)} {};

    constexpr uint32_t getStatus32() const
    {
      return (status_[0] << 0UL) | (status_[1] << 8UL) | (status_[2] << 16UL) | (status_[3] << 24UL);
    }

    uint8_t operator [] (int index) const {return status_[index];}

    void update(const uint8_t* status) { ::std::memcpy(status_, status, 4); }

    bool isClearToSend() const {return 0 != (status_[0] & RegDef::MSK_STATUS0_CTS);}
    bool isErrorBitSet() const {return 0 != (status_[0] & RegDef::MSK_STATUS0_ERR);}
    bool isSeeking()     const {return 0 == (status_[0] & RegDef::MSK_STATUS0_STCINT);}

  private:    
    uint8_t status_[4] {};
  };
}

#endif