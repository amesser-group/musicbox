/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Actions/GetTime.hpp"
#include "System.hpp"

using namespace MusicBox::Drivers::SI468x::Actions;

void
Si468xGetTimeAction::HandleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  auto & drv = ::sys::GetSi468xDriver(*this);
  auto & ta  = ::sys::GetSi468xTransaction(drv);
  auto  s   = ta.GetStruct();

  if(done_)
    return;

  if(!(ta.IsReady()))
    return;

  if (command != Si468xDefs::CMD_DAB_GET_TIME)
  { /* issue command to get time */
    s.SetField8(1, 0); /* get local time */
    ta.SendSimpleCommand(Si468xDefs::CMD_DAB_GET_TIME, 2);
  }
  else if(status.isErrorBitSet())
  {
    done_ = true;
  }
  else if(11 > ta.GetReadLen())
  {
    ta.ReadResponse(11);
  }
  else
  {
    local_time_ = local_time_.from_duration({
      {s.GetField16(4), s.GetField8(6), s.GetField8(7)},
      {s.GetField8(8),  s.GetField8(9), s.GetField8(10)}
    });

    done_ = true;
  }
}