/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Actions/ScanDAB.hpp"
#include "System.hpp"

using ::MusicBox::Drivers::SI468x::Actions::Si468xScanDABAction;
using namespace MusicBox;

using Database::ServiceIterator;
using Database::ServiceValueIterator;
using Database::ServiceValue;

using ::ecpp::Units::MilliSecond;

void
Si468xScanDABAction::StartScan()
{
  auto & drv = ::sys::globals().si468x_driver_;

  /* dont write channels during scanning */

  sys::GetStorage(drv).setSupressSaves(true);

  PrepareDatabase();

  for(auto & entry : scan_history_ )
  {
    entry.channel_index_ = -1;
    entry.service_cnt_   = -1;
  }

  services_found_ = 0;

  ScanNextChannel();
}

void
Si468xScanDABAction::StopScan()
{
  auto & drv = ::sys::globals().si468x_driver_;

  scan_history_[0].channel_index_ = -1;
  Timeout.Stop();

  CleanupDatabase();

  drv.activateMode(Si468xOperationMode::Disabled, true);

  sys::GetStorage(drv).setSupressSaves(false);
}

void
Si468xScanDABAction::PrepareDatabase()
{
  auto & db   = ::MusicBox::Database::ServiceDatabase::instance();

  /* clear dab information for all channels */
  for(auto it = db.begin(); it != db.end(); ++it)
    db.ClearDABProperties(*it);
}

void
Si468xScanDABAction::CleanupDatabase()
{
  auto & db   = ::MusicBox::Database::ServiceDatabase::instance();

  ServiceValueIterator it = db.begin();

  while(it != db.end())
  {
    if(!(it->isDabService() || it->isFMService()))
    {
      auto h = it.GetHandle();

      /* advance iterator before removing the service */
      ++it;

      db.removeService(h);
    }
    else
    {
      ++it;
    }
  }
}

void
Si468xScanDABAction::ScanNextChannel()
{
  auto & l   = MusicBox::Database::ServiceDatabase::instance().getDabChannelList(); 
  auto idx = scan_history_[0].channel_index_ + 1;

  if((idx >= 0) && idx < l.MaxTransponders)
  {
    auto & drv = ::sys::globals().si468x_driver_;
    auto & chan = l[idx];

    if(chan.Handle.isValid() && chan.Freq > 0)
    {
      if(scan_history_[0].service_cnt_ > 0)
      {
        for(auto i = ::ecpp::ElementCount(scan_history_) - 1; i > 0; --i) 
          scan_history_[i] = scan_history_[i-1];
      }

      scan_history_[0].channel_index_ = idx;
      scan_history_[0].service_cnt_   = -1;

      retry_ = 10;

      drv.tuneChannel(chan.Handle);
      Timeout.Start(MilliSecond<>(1000));
    }
    else
    {
      StopScan();
    }
  }
  else
  { /* scan finished */
    StopScan();
  }
}

ECPP_JOB_DEF(Si468xScanDABAction, Timeout)
{
  auto dab   = GetDABManager();

  if(scan_history_[0].channel_index_ < 0)
    return;

  if(dab != nullptr)
  {
    auto & drv = ::sys::globals().si468x_driver_;

    if( (dab->getTunerState() == TunerStateType::Tuned) &&
        (dab->getServiceCount() > 0) && drv.service_info_stable())
    { /* channel tuned and ensemble info scanned */
      scan_history_[0].service_cnt_ = dab->getServiceCount();
      scan_history_[0].rssi_        = static_cast<int_least16_t>(dab->getRssi().raw().value);

      services_found_ += scan_history_[0].service_cnt_;

      ScanNextChannel();
    }
    else if(retry_ > 0)
    {
      --retry_;
      Timeout.Start(MilliSecond<>(1000));
    }
    else
    {
      ScanNextChannel();
    }
  }
  else if(retry_ > 0)
  { /* dab mnode not yet booted */
    --retry_;
    Timeout.Start(MilliSecond<>(1000));
  }
  else
  {
    StopScan();
  }
}