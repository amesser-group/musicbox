/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_SI468XDABCHANNELSCANNER_H_
#define MUSICBOX_SI468XDABCHANNELSCANNER_H_

#include "Drivers/SI468x/Action.hpp"

namespace MusicBox::Drivers::SI468x::Actions
{
  class Si468xScanDABAction : public Si468xAction
  {
  public:
    struct ScanHistory{
    int_least8_t  channel_index_ {-1};
    int_least8_t  service_cnt_   {-1};
    int_least16_t  rssi_         {0};
    };

    constexpr Si468xScanDABAction() : Si468xAction() {}

    void StartScan();
    void StopScan();

    constexpr bool IsScanning() const { return scan_history_[0].channel_index_ >= 0; }

    constexpr const auto & GetHistory() const {return scan_history_; }
    constexpr auto GetServicesFoundCnt() const { return services_found_;}

  private:
    void ScanNextChannel();

    void PrepareDatabase();
    void CleanupDatabase();

    ECPP_JOB_TIMER_DECL(Timeout);
    
    ScanHistory scan_history_[3];

    int_least8_t  retry_ {0};
    uint_least8_t services_found_{0};
  };

}

#endif // MUSICBOX_SI468XDABCHANNELSCANNER_H_
