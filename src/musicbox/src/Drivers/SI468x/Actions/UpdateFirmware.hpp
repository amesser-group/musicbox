/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUSICBOX_DRIVERS_SI468X_UPDATEFLASHMANAGER_HPP_
#define MUSICBOX_DRIVERS_SI468X_UPDATEFLASHMANAGER_HPP_

#include "ecpp/Math/Fraction.hpp"
#include "Drivers/SI468x/Common.hpp"
#include "Drivers/SI468x/Action.hpp"

#include "FileSystem.hpp"

namespace MusicBox::Drivers::SI468x::Actions
{
  class Si468xUpdateFirmwareAction : public Si468xAction, private RegDef::Si4688
  {
  public:
    typedef ::ecpp::Math::Fraction<uint32_t> ProgressType;

    Si468xUpdateFirmwareAction(ElmChanFile & file, uint32_t offset);
    ~Si468xUpdateFirmwareAction();

    virtual void HandleCommandFinished(uint_fast8_t command, const Si4688Status& status) override;

    ProgressType GetProgress() const { return {File.tell(), File.getSize()}; }

  private:
    using Si468xDefs = RegDef::Si4688;

    ElmChanFile                       File;
    uint_least32_t                    FlashOffset;
    Si468xOperationMode               opmode_backup_;
    SI4688_SUBCMD_FLASH_PASS_THROUGH  last_subcmd_  { SI4688_SUBCMD_FLASH_PASS_THROUGH::NONE };

    void                              WriteNextBlock();
  };

}

#endif