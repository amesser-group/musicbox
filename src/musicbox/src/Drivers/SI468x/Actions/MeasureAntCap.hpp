/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SI468X_ANTCAPMEASURER_HPP_
#define SI468X_ANTCAPMEASURER_HPP_

#include "Drivers/SI468x/Common.hpp"
#include "Drivers/SI468x/Action.hpp"
#include "Database/ServiceIterator.hpp"
#include "FileSystem.hpp"
#include "Platform.hpp"

namespace MusicBox::Drivers::SI468x::Actions
{
  using ::ecpp::Units::MilliSecond;

  class Si468xMeasureAntCapAction : public Si468xAction
  {
  public:
    constexpr Si468xMeasureAntCapAction() : Si468xAction() {}

    void StartMeasurement();
    void StopMeasurement();

    bool IsMeasuring() const { return File.isOpen(); }

  private:
    void NextService();
    void TuneNextService();

    static constexpr MilliSecond<>       SettlePeriod{5000};

    Database::ServiceIterator            ServiceIt;
    
    ECPP_JOB_TIMER_DECL(Timeout);

    ElmChanFile                  File;
    uint_least8_t                AntCapVal {0};
    Si468xOperationMode          Mode      {Si468xOperationMode::Disabled};
  };
}

#endif