/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DRIVERS_SI468X_ACTIONS_GETTIME_HPP_
#define MUSICBOX_DRIVERS_SI468X_ACTIONS_GETTIME_HPP_

#include "Drivers/SI468x/Action.hpp"
#include "ecpp_sys/Chrono.hpp"

namespace MusicBox::Drivers::SI468x::Actions
{
  class Si468xGetTimeAction : public Si468xAction
  {
  public:
    constexpr    Si468xGetTimeAction() : Si468xAction() {}
    virtual void HandleCommandFinished(uint_fast8_t command, const Si4688Status& status) override;

    bool                    IsFinished()  const { return done_; }
    constexpr const auto &  GetTime()     const { return local_time_; }

  protected:
    using Si468xDefs = RegDef::Si4688;

    ::ecpp_sys::Chrono::LocalClock::Timepoint local_time_;
    volatile uint_least8_t                    done_     {false};
  };

}

#endif