/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Actions/MeasureAntCap.hpp"
#include "System.hpp"

using namespace MusicBox::Database;
using namespace MusicBox::Drivers::SI468x::Actions;

void
Si468xMeasureAntCapAction::StartMeasurement()
{
  auto & drv    = ::sys::GetSi468xDriver(*this);
  auto & db     = ::MusicBox::Database::ServiceDatabase::instance();
  auto & volmgr = ::sys::GetVolumeManager(drv);

  volmgr.mountVolume();
  File = ElmChanFile("/antcap.dat", FA_WRITE | FA_CREATE_ALWAYS);
  volmgr.unmountVolume();

  if(File.isOpen())
  {
    ServiceIt = db.begin();
    AntCapVal = 1;

    auto const * service = db.GetService(*ServiceIt);

    if(service != nullptr)
    {
      sys::GetStorage(drv).setSupressSaves(true);

      if(service->isDabService())
        Mode = Si468xOperationMode::DABRadio;
      else
        Mode = Si468xOperationMode::FMRadio;

      TuneNextService();
    }
    else
    {
      File.close();
    }
  }
  else
  {
    ServiceIt = ServiceIterator();
  }
}

void
Si468xMeasureAntCapAction::StopMeasurement()
{
  auto & drv    = ::sys::GetSi468xDriver(*this);

  File.close();
  sys::GetStorage(drv).setSupressSaves(true);
  drv.setAntCap(0);
}


void
Si468xMeasureAntCapAction::NextService()
{
  auto & db  = ::MusicBox::Database::ServiceDatabase::instance();
  auto const * service = db.GetService(*ServiceIt);

  if(Mode == Si468xOperationMode::DABRadio)
  {
    if(service != nullptr)
    {
      if(service->isFMService())
      {
        Mode = Si468xOperationMode::FMRadio;
        return;
      }
    }
  }

  /* check if current service has fm mode
   * and we did not yet scanned it */
  ServiceValueIterator it = ServiceIt;
  while(it != db.end())
  {
    ServiceValueIterator jt = it;
    ++it;

    /* check if we already had this dab channel scanned
     * = Same frequency */
    if (it->isDabService())
    {
      for(;jt != db.rend(); --jt)
      {
        if(jt->DabChannel == it->DabChannel)
          break;
      }

      if(jt == db.rend())
      {
        Mode = Si468xOperationMode::DABRadio;
        break;
      }
    }

    if (it->isFMService())
    {
      Mode = Si468xOperationMode::FMRadio;
      break;
    }
  }

  ServiceIt = it;
}

void
Si468xMeasureAntCapAction::TuneNextService()
{
  auto & drv    = ::sys::GetSi468xDriver(*this);
  char linebuf[60] = {};
  int l;

  sniprintf(linebuf, sizeof(linebuf), "# Service %u %u\n",
            (unsigned)ServiceIt.GetHandle().GetRaw(), (unsigned)Mode);

  l = strlen(linebuf);
  if(l != File.write(linebuf, l))
  {
    StopMeasurement();
    return;
  }

  drv.tuneChannel(ServiceIt, Mode);
  drv.setAntCap(AntCapVal);

  Timeout.Start(SettlePeriod);
}


ECPP_JOB_DEF(Si468xMeasureAntCapAction, Timeout)
{
  auto & drv    = ::sys::GetSi468xDriver(*this);

  if(!File.isOpen())
    return;

  /* save last value */
  if( (drv.getOpMode() == Si468xOperationMode::DABRadio) ||
      (drv.getOpMode() == Si468xOperationMode::FMRadio) )
  {
    auto const & r = drv.getRadioManager();
    char linebuf[60] = {};
    int l;

    sniprintf(linebuf, sizeof(linebuf), "%lu;%u;%ld\n",
              r.getTunedFreq(), drv.getAntCapValue(),
              ::ecpp::Math::FixedPoint<int32_t, 1000>(r.getRssi()).raw().value);

    l = strlen(linebuf);
    if(l != File.write(linebuf, l))
    {
      StopMeasurement();
      return;
    }
  }

  if(AntCapVal < 128)
  {
    AntCapVal++;
    drv.setAntCap(AntCapVal);
    Timeout.Start(SettlePeriod);
  }
  else
  {
    auto & db  = ::MusicBox::Database::ServiceDatabase::instance();
    const char sep[] = "\n\n";

    if(2 !=  File.write(sep, 2))
    {
      StopMeasurement();
    }
    else
    {
      AntCapVal = 1;
      NextService();

      if(ServiceIt == db.end())
        StopMeasurement();
      else
        TuneNextService();
    }
  }
}





