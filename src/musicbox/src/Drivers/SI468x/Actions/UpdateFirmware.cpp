/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/SI468x/Actions/UpdateFirmware.hpp"
#include "System.hpp"

using namespace MusicBox::Drivers::Si4688;
using namespace MusicBox;

using MusicBox::Drivers::SI468x::Actions::Si468xUpdateFirmwareAction;

Si468xUpdateFirmwareAction::Si468xUpdateFirmwareAction(ElmChanFile & file, uint32_t offset) : File {file}, FlashOffset {offset}
{
  auto & drv = ::sys::GetSi468xDriver(*this);

  opmode_backup_ = drv.getOpMode();
  drv.activateMode(Si468xOperationMode::UpdateFlash);
}

Si468xUpdateFirmwareAction::~Si468xUpdateFirmwareAction()
{
  auto & drv = ::sys::GetSi468xDriver(*this);
  drv.activateMode(opmode_backup_);
}


void 
Si468xUpdateFirmwareAction::HandleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  auto & drv = ::sys::GetSi468xDriver(*this);

  if(!File.isOpen())
    return;

  if(!GetManager().ModeReached())
    return;

  if (0 == File.getSize())
  { /* nothing to flash, abort */
    drv.activateMode(opmode_backup_);
  }
  else if(status.isErrorBitSet())
  {
    /* error during flashing, abort */
    drv.activateMode(Si468xOperationMode::Disabled);
  }
  else
  { 
    if (last_subcmd_ == SI4688_SUBCMD_FLASH_PASS_THROUGH::WRITE_BLOCK)
      FlashOffset += 512;

    WriteNextBlock();
  }
}

void 
Si468xUpdateFirmwareAction::WriteNextBlock()
{
  auto & drv = ::sys::GetSi468xDriver(*this);
  auto & ta  = ::sys::GetSi468xTransaction(drv);
  auto  s   = ta.GetStruct();

  if (!(ta.IsReady()))
    return;

  if( (0 == (FlashOffset % 4096)) &&
      (SI4688_SUBCMD_FLASH_PASS_THROUGH::ERASE_SECTOR != last_subcmd_))
  { /* need to erase sector first */

    s.SetField8(0, CMD_FLASH_PASS_THROUGH);
    s.SetField8(1, SI4688_SUBCMD_FLASH_PASS_THROUGH::ERASE_SECTOR);
    s.SetField8(2, 0xC0);
    s.SetField8(3, 0xDE);
    s.SetField32(4, FlashOffset);

    last_subcmd_ = SI4688_SUBCMD_FLASH_PASS_THROUGH::ERASE_SECTOR;
    ta.SendCommand(8);
  }
  else
  { /* check for next flash sector to write */
    auto & buf = s.GetBlock<uint8_t[512]>(16);
    long count;

    count = File.read(buf, sizeof(buf));

    if(count <= 0)
    {
      File.close();
      drv.activateMode(opmode_backup_);
    }
    else
    {
      if(((unsigned long)count) < sizeof(buf))
      {
        memset(&(buf[count]), 0x00, sizeof(buf) - count);
        count = sizeof(buf);
      }

      s.SetField8(0, CMD_FLASH_PASS_THROUGH);
      s.SetField8(1, SI4688_SUBCMD_FLASH_PASS_THROUGH::WRITE_BLOCK);
      s.SetField8(2, 0x0C);
      s.SetField8(3, 0xED);
      s.SetField32(4, 0);
      s.SetField32(8, FlashOffset);
      s.SetField32(12,count);

      last_subcmd_ = SI4688_SUBCMD_FLASH_PASS_THROUGH::WRITE_BLOCK;
      ta.SendCommand(16 + count);
    }

  }
}

