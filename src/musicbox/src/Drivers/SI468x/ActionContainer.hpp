/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DRIVERS_SI468X_ACTIONS_CONTAINER_HPP_
#define MUSICBOX_DRIVERS_SI468X_ACTIONS_CONTAINER_HPP_

#include "Drivers/SI468x/Action.hpp"
#include <variant>

namespace MusicBox::Drivers::SI468x::Actions
{
  template<typename ...Actions>
  class Si468xActionContainer : protected ::std::variant<::std::monostate, Actions...>
  {
  protected:
    using Variant = ::std::variant<::std::monostate, Actions...>;

    template<typename Action>
    class Wrapper;

    class VirtualWrapper;    

  public:
    template<typename Action_>
    Wrapper<Action_> Action() { return Wrapper<Action_>(*this); }

    VirtualWrapper   VirtualAction() { return VirtualWrapper(*this); }
  };

  template<typename ...Actions>
  template<typename Action>
  class Si468xActionContainer<Actions...>::Wrapper
  {
  public:
    constexpr Wrapper(Si468xActionContainer &container) : container_{container} {}

    template<typename ...Args>  
    Action* Aquire(Args... args);

    Action* Get();
    void    Release();
  protected:
    Si468xActionContainer & container_;
  };

  template<typename ...Actions>
  template<typename Action>
  template<typename ...Args>
  Action* Si468xActionContainer<Actions...>::Wrapper<Action>::Aquire(Args... args)
  {
    if (::std::holds_alternative<::std::monostate>(container_))
      return &(container_.template emplace<Action>(args...));
    else
      return nullptr;
  }


  template<typename ...Actions>
  template<typename Action>
  Action* Si468xActionContainer<Actions...>::Wrapper<Action>::Get()
  {
    return ::std::get_if<Action>(&(container_));
  }

  template<typename ...Actions>
  template<typename Action>
  void Si468xActionContainer<Actions...>::Wrapper<Action>::Release()
  {
    if (::std::holds_alternative<Action>(container_))
      container_.template emplace<::std::monostate>();
  }

  template<typename ...Actions>
  class Si468xActionContainer<Actions...>::VirtualWrapper
  {
  public:
    constexpr VirtualWrapper(Si468xActionContainer &container) : container_{container} {}

    Si468xAction* Get();
    void    Release();
  protected:
    Si468xActionContainer & container_;
  };

  template<typename ...Actions>
  Si468xAction* Si468xActionContainer<Actions...>::VirtualWrapper::Get()
  {
    Variant & var = container_;

    return std::visit([](auto &&arg) -> Si468xAction* 
    { 
      using T = std::decay_t<decltype(arg)>;

      if constexpr (std::is_same_v<T, std::monostate>)
        return nullptr;
      else
        return &arg;
    }, var);
  }

};
#endif