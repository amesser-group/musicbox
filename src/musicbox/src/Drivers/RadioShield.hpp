/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DRIVERS_EEPROM_HPP_
#define MUSICBOX_DRIVERS_EEPROM_HPP_

#include "ecpp/BinaryStruct.hpp"
namespace MusicBox::Drivers::RadioShield
{
  static constexpr uint_least8_t kI2CAddr24CW1280   { 0x50 };

  class EepromStorage
  {
  public:
    size_t                            Read(size_t offset, size_t len);
    size_t                            Write(size_t offset, size_t len);
    constexpr ::ecpp::BinaryStruct    GetStruct() { return { buffer_, sizeof(buffer_)}; }
  protected:
    static constexpr uint_least16_t kSizeEeprom         = 128 / 8 * 1024;
    static constexpr uint_least16_t kReservedBytesBegin = 1024;
    uint8_t                         buffer_[128];
  };
}

#endif
