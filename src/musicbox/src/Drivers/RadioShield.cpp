/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Drivers/RadioShield.hpp"
#include "System.hpp"

#ifdef CONFIG_IDF_TARGET
#include "esp_log.h"
#else
#define ESP_LOGE(...)
#define ESP_LOGI(...)
#endif

using namespace MusicBox::Drivers::RadioShield;

static const char* TAG = "EepromStorage";

size_t
EepromStorage::Read(size_t offset, size_t len)
{
  auto & trb = ::sys::GetTwiRequestBlock(*this);

  if(len > trb.GetBufferSize())
    len = trb.GetBufferSize();

  if( (offset + len) > (kSizeEeprom - kReservedBytesBegin))
    len = (kSizeEeprom - kReservedBytesBegin - offset);

  if(len == 0)
    return 0;

  offset += kReservedBytesBegin;

  trb.SetUp(kI2CAddr24CW1280);
  trb.GetStruct().SetField8(0, (offset >> 8) & 0xFF);
  trb.GetStruct().SetField8(1, (offset >> 0) & 0xFF);
  
  if(trb.Execute(2,len) == trb.Status::OK)
  {
    /* this copy-around is awkward, but currently the simplest solution
     * Need to rework the twi handling */
    memcpy(buffer_, trb.GetStruct().GetBlock<uint8_t[sizeof(buffer_)]>(0), sizeof(buffer_));

    ESP_LOGI(TAG, "Read %d byte from offset %d, starting with %X:%X:%X:%X", trb.GetReadLen(), offset, 
      GetStruct().GetField8(0), GetStruct().GetField8(1), GetStruct().GetField8(2), GetStruct().GetField8(3));
    return trb.GetReadLen();
  }
  else
  {
    ESP_LOGE(TAG, "Failed to Read %d byte from offset %d", trb.GetReadLen(), offset);
    return 0;
  }
}

size_t
EepromStorage::Write(size_t offset, size_t len)
{
  auto & trb = ::sys::GetTwiRequestBlock(*this);
  size_t len_written;

  if(len > sizeof(buffer_))
    return 0;

  if( (offset + len) > (kSizeEeprom - kReservedBytesBegin))
    len = (kSizeEeprom - kReservedBytesBegin - offset);

  if(len == 0)
    return 0;

  offset += kReservedBytesBegin;

  trb.SetUp(kI2CAddr24CW1280);
  trb.GetStruct().SetField8(0, (offset >> 8) & 0xFF);
  trb.GetStruct().SetField8(1, (offset >> 0) & 0xFF);

  if(trb.Execute(2,len) != trb.Status::OK)
    return 0;

  if( memcmp(buffer_, trb.GetStruct().GetBlock<uint8_t[sizeof(buffer_)]>(0), len) == 0)
  {
    ESP_LOGI(TAG, "Skipping write of %d byte equal data at offset %d", len, offset);
    return len;
  }

  for(len_written = 0; len_written < len;)
  {
    decltype(trb.Execute(0,0)) status;

    /* we must not write accross page boundaries */
    size_t start_addr  = (offset + len_written);
    size_t page_offset = (offset + len_written) % 32;
    size_t len_write   = ::ecpp::min( len - len_written, (size_t)(32 - page_offset));

    trb.SetUp(kI2CAddr24CW1280);
    trb.GetStruct().SetField8(0, (start_addr >> 8) & 0xFF);
    trb.GetStruct().SetField8(1, (start_addr >> 0) & 0xFF);
    trb.GetStruct().Pad(2, &(buffer_[len_written]), len_write); 
    trb.Execute(2 + len_write, 0);

    unsigned int repeat = 0;
    while ((status = trb.Execute(2 + len_write, 0)) != trb.Status::OK)
    {
      if( ++repeat > 1000)
        break;

      vTaskDelay((2 + portTICK_PERIOD_MS - 1) / portTICK_PERIOD_MS);
    }
        
    if(status != trb.Status::OK)
      break;

    len_written += len_write;
  }

  ESP_LOGI(TAG, "Wrote %d of %d byte at offset %d, starting with %X:%X:%X:%X", len_written, len, offset, 
    GetStruct().GetField8(0), GetStruct().GetField8(1), GetStruct().GetField8(2), GetStruct().GetField8(3));

  return len_written;
}