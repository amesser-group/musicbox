/*
 *  Copyright 2017-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DRIVERS_MAX98089_HPP_
#define MUSICBOX_DRIVERS_MAX98089_HPP_

#include "Devices/MAX98089_Registers.hpp"

#include "ecpp/Execution/Job.hpp"
#include "ecpp/Container.hpp"
#include "ecpp/Units/Time.hpp"

#include <cstdlib>
#include <array>

namespace MusicBox::Drivers::MAX98089
{
  using Max98089Defs = MusicBox::RegDef::Max98089;

  using ::std::size_t;

  struct Max98089RegisterSequence
  {
    const uint8_t *sequence_;

    uint_least8_t  len_;

    template<size_t N> constexpr Max98089RegisterSequence(const uint8_t (&arr)[N]) : sequence_(arr), len_(N) {}
  };

  struct Max98089Configuration
  {
    const Max98089RegisterSequence * const  sequences_;
    uint_least8_t                           sequences_count_;

    template<size_t N>
    constexpr Max98089Configuration(const Max98089RegisterSequence (&sequences)[N]) : sequences_(sequences), sequences_count_(N) {}
  };


  struct Max98089CommonSequences 
  {
    static constexpr ::std::array<uint8_t,2> kShutdown { Max98089Defs::REG_SYSTEM_SHUTDOWN, 0x00};
  };


  template<unsigned long MCLK>
  struct Max98089MCLKTrait {};

  template<>
  struct Max98089MCLKTrait<1> 
  {
    /* PCLK = MCLK */
    static constexpr ::std::array<uint8_t, 2> kMasterClockControl {Max98089Defs::REG_MASTER_CLOCK, 0x10};
    static constexpr unsigned int kMasterClockDivider = 1;
  };

  template<>
  struct Max98089MCLKTrait<2> 
  {
    /* PCLK = MCLK / 2 */
    static constexpr ::std::array<uint8_t, 2> kMasterClockControl {Max98089Defs::REG_MASTER_CLOCK, 0x20};
    static constexpr unsigned int kMasterClockDivider = 2;
  };

  template<> struct Max98089MCLKTrait<3> : public Max98089MCLKTrait<2> {};

  template<unsigned long MasterClockFreq>
  struct Max98089RegisterSequences : public Max98089MCLKTrait<MasterClockFreq / 10000000>
  {
    using Max98089MCLKTrait<MasterClockFreq / 10000000>::kMasterClockControl;
    using Max98089MCLKTrait<MasterClockFreq / 10000000>::kMasterClockDivider;

    static constexpr auto kPCLKFrequency = MasterClockFreq / kMasterClockDivider;
  };

#if 0
  struct Max98089SampleRate
  {
    const uint_least16_t sample_rate;
    const uint_least8_t  reg_sr_value;
  };

  static constexpr Max98089SampleRate SampleRate_48kHz =
  {
    48000,
    0x80
  };

  struct I2SMode
  {
  };

  template<unsigned int PCLKFreq>
  class Max98089RegisterSequences
  {
  private:
    static constexpr uint16_t CalcLRCLKDivider(Max98089SampleRate rate)
    {
      return 65536ULL * 96 * rate.sample_rate / PCLKFreq;
    }
  public:
    static constexpr ::std::array<uint8_t, 6> GetDAIRegisters(I2SMode mode, Max98089SampleRate rate)
    {
      return {
        rate.reg_sr_value,
        (CalcLRCLKDivider(rate) >> 8) & 0x7F,
         CalcLRCLKDivider(rate) & 0xFE,
         0x90, /* I2S: Master, Second rising edge */
         0x02  /* I2S: BCLK = 48*LRCLK  */  
      };
    }
  };
#endif

  template<class... Profiles>
  class Max98089Manager;

  class Max98089ManagerState : public ::MusicBox::RegDef::Max98089
  {
  protected:
    virtual void handlePendingActions();
    virtual void handleTwiTransferDone();
    virtual void handleTimeout();
  };

  class Max98089ManagerStateWriteRegs : public Max98089ManagerState
  {
  protected:
    void writeRegister(uint8_t reg, uint8_t val);
  private:
    uint_least8_t data[2] {};
  };

  class Max98089ManagerStatePowerup : public Max98089ManagerState
  {
  protected:
    virtual void handlePendingActions();
  };

  class Max98089ManagerStateStartup : public Max98089ManagerState
  {
  protected:
    virtual void handlePendingActions();
    virtual void handleTimeout();
  };


  class Max98089ManagerStateSetup : public Max98089ManagerStateWriteRegs
  {
  protected:
    virtual void handlePendingActions()  override;
    virtual void handleTwiTransferDone() override;

    uint_least8_t seq_index_  {0};
    uint_least8_t seq_offset_ {0};
  };

  class Max98089ManagerStateRun : public Max98089ManagerState
  {
  public:
    Max98089ManagerStateRun(bool shutdown);

  protected:
    virtual void handlePendingActions() override;
  };

  class Max98089ManagerStateSingleTwiTransfer : public Max98089ManagerState
  {
  protected:
    virtual void handleTwiTransferDone() override;
  };

  class Max98089ManagerStateSetVol : public Max98089ManagerStateSingleTwiTransfer
  {

  protected:
    virtual void handlePendingActions()  override;
    uint8_t      buffer[5];
  };

  #if 0

  class Max98089ManagerStateWriteDataContainer : public Max98089ManagerState
  {
  public:
    constexpr Max98089ManagerStateWriteDataContainer(const uint8_t *data, uint16_t length) : Max98089ManagerState(), data {data}, length {length} {}
  protected:
    const uint8_t * const data;
    const uint_least16_t  length;
  };

  class Max98089ManagerStateWriteDataStart : public Max98089ManagerStateWriteDataContainer
  {
  public:
    constexpr Max98089ManagerStateWriteDataStart(const uint8_t *data, uint16_t length) : Max98089ManagerStateWriteDataContainer(data,length) {}
  protected:
    virtual void handlePendingActions()  override;
    virtual void handleTwiTransferDone() override;
  };

  class Max98089ManagerStateWriteData : public Max98089ManagerStateWriteDataContainer
  {
  public:
    constexpr Max98089ManagerStateWriteData(const Max98089ManagerStateWriteDataStart &init) : Max98089ManagerStateWriteDataContainer(init) {}

  protected:
    virtual void handlePendingActions()  override;
    virtual void handleTwiTransferDone() override;

    uint8_t               buffer[2] {};
    uint_least16_t        offset {0};
  };

  #endif

  class Max98089ManagerStateActivate : public Max98089ManagerState
  {
  protected:
    virtual void handlePendingActions()  override;
    virtual void handleTwiTransferDone() override;
  };

  class Max98089ManagerStateEnterShutdown : public Max98089ManagerState
  {
  protected:
    virtual void handlePendingActions()  override;
    virtual void handleTwiTransferDone() override;
  };

  class Max98089Driver
  {
  public:
    /** Volume in units of CentiBel */
    struct Volume : protected ::ecpp::Units::DecimalScale<-1>
    {
    public:
      static Volume FromLevel(uint_fast8_t level);
    
      int_least16_t volume_;
    protected:
      constexpr Volume(int_least16_t volume) : volume_(volume) {};
    };

  protected:
    template<size_t values_cnt>
    bool WriteRegisters(uint8_t start_register, const ::std::array<uint8_t, values_cnt> & values)
    {
      return WriteRegisters(start_register,values.data(), values_cnt);
    }

    template<size_t sequence_len>
    bool WriteRegisters(const ::std::array<uint8_t, sequence_len> & sequence)
    {
      return WriteRegisters(sequence.front(),sequence.data() + 1, sequence_len - 1);
    }

    bool WriteRegisters(uint8_t start_register, const uint8_t* values, size_t cnt);

    /** if the codec is currently active */
    uint_least8_t active_;
  };

  class Max98089PowerdownProfile
  {
  public:
    void TWITransferFinished(Max98089Driver& inst) {};
    void SetVolume(Max98089Driver& inst) {};
  };

  class Max98089ShutdownProfile
  {
  public:
    void TWITransferFinished(Max98089Driver& inst);
    void SetVolume(Max98089Driver& inst) {};
  protected:
    uint_least8_t register_written_ = false;
  };

  class Max98089RadioAmpProfile
  {
  private:
    uint8_t state_ = {0};
  public:
    void TWITransferFinished(Max98089Driver& inst);
    void SetVolume(Max98089Driver& inst);
  };

  template<class... Profiles>
  class Max98089Manager
  {
  public:
    using ShutdownProfile = Max98089ShutdownProfile; 
    typedef ::std::variant<Max98089PowerdownProfile, ShutdownProfile, Profiles...> ProfilesContainer;

    template<typename _Profile, typename... _Args>
    void ActivateProfile(_Args&&... __args)
    {
      profiles_.template emplace<_Profile>(::std::forward<_Args>(__args)...);
    }
  private:
    ProfilesContainer profiles_;

  public:
    void SetMute(bool mute);
    bool Shutdown();
    void initDevice();


    void          setVolumeLevel(uint_fast8_t level);


    void HandleTWITransferFinished();
    void handlePending();

  private:
    ECPP_JOB_DECL(HandlePendingActions);


    friend class Max98089Sequencer;
  };
};

namespace signals
{
  template<class... Profiles>
  void Changed(const ::MusicBox::Drivers::MAX98089::Max98089Manager<Profiles...>&);
}

#endif /* MUSICBOX_DRIVERS_MAX98089_HPP_ */
