/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "MusicBox.hpp"
#include "System.hpp"
#include "ecpp/Chrono/Timezones.hpp"

using namespace MusicBox;
using namespace ecpp::Units;

using MusicBox::Drivers::SI468x::Si468xOperationMode;
using MusicBox::Drivers::SI468x::Actions::Si468xGetTimeAction;

void
MusicBoxApp::Initialize()
{
}

void
MusicBoxApp::Start()
{
  /* try to load database from permanent memory, if it fails
  * init with some reasonable default values */
  storage_manager_.LoadDatabase();

  ::ecpp_sys::Chrono::LocalClock::SetTimezone(::ecpp::Chrono::Timezones::Europe::Berlin());

  si468x_driver_.start();

  user_interface_.Start();

  ChangeOperatingState(OperatingState::kPlayRadio);

  Poll.Enqueue();
}

ECPP_JOB_DEF(MusicBoxApp, Poll)
{
  { /* check if we need to adjust the time */
    auto a = si468x_actions_.Action<Si468xGetTimeAction>();
    auto p = a.Get();

    if((p == nullptr) && last_clock_update_timer_.IsExpired())
    {
      if (si468x_driver_.getOpMode() == Si468xOperationMode::DABRadio)
        p = a.Aquire();
    }

    if( (p != nullptr) && (p->IsFinished()))
    {
      const auto & time = p->GetTime();

      if(time.valid())
      {
        ::ecpp_sys::Chrono::SystemClock::SetTime(time);
        last_clock_update_timer_.Start(Second<>(3600));
      }
      else
      {
        last_clock_update_timer_.Start(Second<>(10));
      }

      a.Release();
    }
  }

  wakealarms_.CheckAlarms();

  Poll.Start(MilliSecond<>(1000));
}

ECPP_JOB_DEF(MusicBoxApp, Timeout)
{
  if (operating_state_ != OperatingState::kStandBy)
    GoToStandBy();

  ::signals::MusicBox::UserTimeout(*this);
}

void 
MusicBoxApp::ChangeOperatingState(OperatingState operating_state)
{
  operating_state_ = operating_state;
  user_interface_.HandleChange(operating_state);
}

void 
MusicBoxApp::GoToStandBy()
{
  if (operating_state_ == OperatingState::kStandBy)
    return;

  ::sys::bsp().ShutdownRadio();
  si468x_driver_.activateMode(Si468xOperationMode::Disabled);
  ChangeOperatingState(OperatingState::kStandBy);
}

void
MusicBoxApp::PlayLastStation(const Seconds timeout)
{
  if (operating_state_ != OperatingState::kPlayRadio)
  {
    si468x_driver_.tuneChannel(si468x_driver_.GetCurrentService());
    ChangeOperatingState(OperatingState::kPlayRadio);
  }

  Timeout.Start(timeout);      
}

void 
MusicBoxApp::StartTimeout(const Seconds timeout) 
{
  Timeout.Start(timeout);
}
