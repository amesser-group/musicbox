/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_HPP
#define MUSICBOX_HPP

#include "ecpp/System/TickTimer.hpp"
#include "ecpp/Units/Time.hpp"
#include "ecpp_sys/Chrono.hpp"

#include "Drivers/MAX98089.hpp"
#include "Drivers/SI468x/Driver.hpp"
#include "UserInterface/UserInterface.hpp"
#include "Storage/Manager.hpp"

#include "Alarm.hpp"
#include "State.hpp"

#include "config/MusicBox.hpp"

namespace MusicBox
{
  using Seconds = ::ecpp::Units::Second<unsigned int>;

  class MusicBoxApp
  {
  public:
    void Initialize();
    void Start();

    constexpr OperatingState operating_state() const { return operating_state_ ;}

    Drivers::SI468x::Si468xDriver     si468x_driver_;
    Storage::StorageManager           storage_manager_;
    UserInterface::UserInterface      user_interface_;
    ::config::MusicBox::Si468xActions si468x_actions_;

    ::config::MusicBox::WakeAlarmList wakealarms_;

    OperatingState                    operating_state_   {OperatingState::kStandBy};

    void GoToStandBy();
    void PlayLastStation(Seconds timeout = Seconds(60*60*6));
    void StartTimeout(Seconds timeout);
  protected:
    void      ChangeOperatingState(OperatingState mode);
    constexpr MusicBoxApp & Get(Drivers::SI468x::Si468xDriver &drv) { return container_of(MusicBoxApp, si468x_driver_, drv); }

    ::ecpp::System::DeadlineTimer   last_clock_update_timer_;

    ECPP_JOB_TIMER_DECL(Poll);
    ECPP_JOB_TIMER_DECL(Timeout); 
  };
}

namespace signals
{
  namespace MusicBox
  {
    void UserTimeout(::MusicBox::MusicBoxApp& app);
  }
}
#endif