/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "System.hpp"

using ::MusicBox::Drivers::SI468x::Si4688Status;
using ::MusicBox::Drivers::SI468x::Si468xDriver;
using ::MusicBox::Drivers::SI468x::Si468xOperationMode;
using ::MusicBox::Drivers::SI468x::TunerStateType;

using namespace ::MusicBox;

void
::signals::ServiceStarted(const Si468xDriver&)
{
  ::sys::globals().user_interface_.DispatchEventValue(UserInterface::events::StatusChange());
  ::sys::globals().storage_manager_.saveStoredState();

  ::sys::bsp().audio_codec_.SetMute(false);
}

void
::signals::Changed(const Si468xDriver&)
{
  ::sys::globals().user_interface_.DispatchEventValue(UserInterface::events::StatusChange());
}

void 
signals::Changed(const Si468xDriver&, Si468xOperationMode mode)
{
  UserInterface::events::OperationModeChanged ev = {mode};
  
  ::sys::globals().user_interface_.DispatchEventValue(ev);

  if ( (mode == Si468xOperationMode::FMRadio)  ||
       (mode == Si468xOperationMode::DABRadio))
  {
    ::sys::bsp().audio_codec_.PrepareRadioAudio();
  }
  else
  {
    ::sys::bsp().audio_codec_.ShutdownAudio();
  }
}

void
::signals::Changed(const Si468xDriver&, TunerStateType)
{
  ::sys::globals().user_interface_.DispatchEventValue(UserInterface::events::StatusChange());
}

void
::signals::Changed(const Si468xDriver&, const RDSManager &)
{
  ::sys::globals().user_interface_.DispatchEventValue(UserInterface::events::StatusChange());
}

void
::signals::Changed(const Database::ServiceDatabase&, Database::ServiceHandle handle)
{
  ::sys::globals().user_interface_.DispatchEventValue(UserInterface::events::DatabaseChanged(handle));
  ::sys::globals().storage_manager_.databaseChanged();
}

void
::signals::Raise(const Alarm& Alarm)
{
  sys::globals().Wake();
}