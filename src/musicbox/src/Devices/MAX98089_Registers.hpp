/*
 *  Copyright 2017-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DRIVERS_MAX98089_REGISTERS_HPP_
#define MUSICBOX_DRIVERS_MAX98089_REGISTERS_HPP_

#include <cstdint>

#define EQ_PARAM(val) ((val) >> 8) & 0xFF, (val) &0xFF

namespace MusicBox::RegDef
{
  class Max98089
  {
  public:
    enum Registers : ::std::uint8_t
    {
      REG_BATTERY_VOLTAGE                          = 0x03,
      REG_MASTER_CLOCK                             = 0x10,
      REG_DAI1_CLOCK_MODE                          = 0x11,
      REG_DAI1_ANY_CLOCK_CONTROL1                  = 0x12,
      REG_DAI1_ANY_CLOCK_CONTROL2                  = 0x13,
      REG_DAI1_FORMAT                              = 0x14,
      REG_DAI1_CLOCK                               = 0x15,
      REG_DAI1_IO_CONFIGURATION                    = 0x16,
      REG_DAI1_TIME_DIVISION_MULTIPLEX             = 0x17,
      REG_DAI1_FILTERS                             = 0x18,
      REG_DAI2_CLOCK_MODE                          = 0x19,
      REG_DAC_MIXER                                = 0x22,
      REG_LEFT_ADC_MIXER                           = 0x23,
      REG_LEFT_RECEIVER_AMPLIFIER_MIXER            = 0x28,
      REG_RIGHT_RECEIVER_AMPLIFIER_MIXER           = 0x29,
      REG_RECEIVER_AMPLIFIER_MIXER_CONTROL         = 0x2A,
      REG_LEFT_SPEAKER_OUTPUT_MIXER_REGISTER       = 0x2B,
      REG_RIGHT_SPEAKER_OUTPUT_MIXER_REGISTER      = 0x2C,
      REG_DAI1_PLAYBACK_LEVEL_CONTROL              = 0x2F,

      REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL   = 0x3B,
      REG_RIGHT_RECEIVER_AMPLIFIER_VOLUME_CONTROL  = 0x3C,
      REG_LEFT_SPEAKER_OUTPUT_LEVEL_REGISTER       = 0x3D,
      REG_RIGHT_SPEAKER_OUTPUT_LEVEL_REGISTER      = 0x3E,

      REG_LEVEL_CONTROL                            = 0x49,
      REG_OUTPUT_ENABLE                            = 0x4D,
      REG_POWER_MANAGEMENT_TOPLEVEL_BIAS_CONTROL   = 0x4E,
      REG_DAC_LOW_POWER_MODE2                      = 0x50,
      REG_SYSTEM_SHUTDOWN                          = 0x51,

      REG_EQ_DAI1_BAND1                            = 0x52,
      REG_EQ_DAI1_BAND2                            = 0x5C,
      REG_EQ_DAI1_BAND3                            = 0x66,
      REG_EQ_DAI1_BAND4                            = 0x70,
      REG_EQ_DAI1_BAND5                            = 0x7A,

      REG_EQ_DAI2_BAND1                            = 0x84,
      REG_EQ_DAI2_BAND2                            = 0x8E,
      REG_EQ_DAI2_BAND3                            = 0x98,
      REG_EQ_DAI2_BAND4                            = 0xA2,
      REG_EQ_DAI2_BAND5                            = 0xAC,
    };

    static constexpr uint8_t kTwiAddr              = 0x10;
    static constexpr uint8_t kRegMask_Volume_Mute  = 0x80;
  };
}

#endif /* MUSICBOX_DRIVERS_MAX98089_HPP_ */
