/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SnoozeController.hpp"
#include "System.hpp"

using namespace MusicBox;
using ::ecpp::Units::Second;

void
SnoozeController::HandleUserTimeout()
{
  process_event(Derived::Timeout());
}

void
SnoozeController::HandleSnoozeKeyPress()
{
  process_event(Derived::SnoozeKey());
}

void
SnoozeController::HandleUserShutdown()
{
  process_event(Derived::UserShutdown());
}

void
SnoozeController::HandleAlarm()
{
  process_event(Derived::Alarm());
}

void
SnoozeController_::PlayOneHour(const SnoozeController_::SnoozeKey &)
{
  auto & app  = sys::GetApp(*this);
  app.PlayLastStation(Second<>(60*60));
}

void 
SnoozeController_::MuteAlarm(const SnoozeController_::SnoozeKey &)
{
  auto & app  = sys::GetApp(*this);

  app.GoToStandBy();
  app.StartTimeout(Second<>(5*60));
}

void 
SnoozeController_::RestartAlarm(const SnoozeController_::Timeout &)
{
  auto & app  = sys::GetApp(*this);
  app.PlayLastStation(Second<>(60*60));
}

void
SnoozeController_::RadioOff(const SnoozeController_::Timeout&)
{
  auto & app  = sys::GetApp(*this);
  app.GoToStandBy();
}

void
SnoozeController_::RadioOff(const SnoozeController_::SnoozeKey&)
{
  auto & app  = sys::GetApp(*this);
  app.GoToStandBy();
}
