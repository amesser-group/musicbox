/*
 *  Copyright 2018-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DATABASE_SERVICEVALUEITERATOR_HPP_
#define MUSICBOX_DATABASE_SERVICEVALUEITERATOR_HPP_

#include "Database/ServiceIterator.hpp"

namespace MusicBox::Database
{
  class ServiceValueIterator : public ServiceIterator
  {
  public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type        = const ServiceValue;
    using difference_type   = intptr_t;
    using pointer           = const ServiceValue *;
    using reference         = const ServiceValue&;
    
    ServiceValueIterator(const ServiceHandle & init);
    ServiceValueIterator(const ServiceIterator& init) : ServiceValueIterator(init.GetHandle()) {}

    constexpr const ServiceValue& operator*()  const {return value_;}
    constexpr const ServiceValue* operator->() const {return &value_;}

    const ServiceValueIterator & operator= (const ServiceIterator& it)      { ServiceIterator::operator=(it); LoadValue(); return *this; }
    const ServiceValueIterator & operator= (const ServiceValueIterator& it) { ServiceIterator::operator=(it); value_ = it.value_; return *this; }

    using ServiceIterator::operator==;
    using ServiceIterator::operator!=;

    const ServiceValueIterator & operator++() { ServiceIterator::operator++(); LoadValue(); return *this; }
    const ServiceValueIterator & operator--() { ServiceIterator::operator--(); LoadValue(); return *this; }

  private:
    void LoadValue();

    ServiceValue         value_;
  };
}

#endif