#include "Service.hpp"

using namespace ::MusicBox::Database;

bool
ServiceValue::isSameService(const ServiceValue& rhs) const
{
  /* exakt match by service id */
  if ( (ServiceId != 0) && (ServiceId == rhs.ServiceId))
    return true;

  if ( (FMFreq != 0) && (FMFreq == rhs.FMFreq) )
    return true;

  return false;
}

void
ServiceValue::ClearDABParameters()
{
  DabChannel = {};
  CId       = 0;
}

bool ServiceValue::update(const ServiceValue & rhs)
{
  bool updated = false;

  if( rhs.ServiceId == 0)
    return false;

  if( rhs.ServiceId != ServiceId )
  {
    updated = true;
    ServiceId = rhs.ServiceId;
  }

  if( rhs.isFMService() && (rhs.FMFreq != FMFreq))
  {
    updated = true;
    FMFreq = rhs.FMFreq;
  }

  if( rhs.isDabService() )
  {
    if((rhs.DabChannel != DabChannel) ||
        (rhs.CId       != CId) ||
        (rhs.Name      != Name))
      updated = true;

    DabChannel = rhs.DabChannel;
    CId        = rhs.CId;
    Name       = rhs.Name;
  }

  /* Update name from FM service only if this is
    * a non-dab service */
  if(!isDabService() && rhs.isFMService())
    Name = rhs.Name;

  return updated;
}
