/*
 *  Copyright 2018-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DATABASE_SERVICEHANDLE_HPP_
#define MUSICBOX_DATABASE_SERVICEHANDLE_HPP_

#include <cstdint>

namespace MusicBox::Database
{
  class ServiceDatabase;
  class ServiceIterator;
  class ServiceValueIterator;

  class ServiceHandle
  {
  public:
    constexpr ServiceHandle() : val_{0} {}
    constexpr ServiceHandle(uint_fast8_t init) : val_{static_cast<uint_least8_t>(init)} {}
    constexpr ServiceHandle(const ServiceHandle& init) = default;

    constexpr bool operator> (const ServiceHandle& rhs ) const { return val_ > rhs.val_; }
    constexpr bool operator< (const ServiceHandle& rhs ) const { return val_ < rhs.val_; }

    constexpr bool operator== (const ServiceHandle& rhs ) const { return val_ == rhs.val_; }
    constexpr bool operator!= (const ServiceHandle& rhs ) const { return val_ != rhs.val_; }

    constexpr auto GetRaw() const { return val_; }
  protected:
    uint_least8_t  val_;

    friend class ServiceDatabase;
    friend class ServiceIterator;
    friend class ServiceValueIterator;
  };
}
#endif