/*
 *  Copyright 2018-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DATABASE_CHANNELDATABASE_HPP_
#define MUSICBOX_DATABASE_CHANNELDATABASE_HPP_

#include "Database/ServiceHandle.hpp"
#include "Database/Service.hpp"
#include "ecpp/Datatypes.hpp"
#include "ecpp_config/Foundation.hpp"

#include <cstdint>
#include <iterator>
#include <array>

namespace MusicBox::Database
{
  class StorageManager;

  class ServiceIterator;

  class ServiceValueIterator;

  class ServiceDatabase : protected ecpp_config::Foundation<ServiceDatabase>
  {
  public:
    static constexpr uint_least8_t kMaxServices = 96;

    static constexpr ServiceHandle kHandleREnd  = 0;
    static constexpr ServiceHandle kHandleEnd   = kMaxServices + 1;

    ServiceHandle GetNextServiceHandle(ServiceHandle handle) const;
    ServiceHandle GetPrevServiceHandle(ServiceHandle handle) const;

    static constexpr bool IsValidServiceHandle(ServiceHandle handle) {return (handle > kHandleREnd) && (handle < kHandleEnd);}

    const ServiceValue * GetService(ServiceHandle handle) const;
  private:

    struct ServiceContainer
    {
      ServiceHandle    _handleNext {0};
      ServiceHandle    _handlePrev {0};
      ServiceValue     _value      {};
    };

    constexpr ServiceContainer * GetServiceContainer(ServiceHandle handle)
    {
      return IsValidServiceHandle(handle) ? &(services_[handle.val_ - 1]) : nullptr;
    }

    constexpr const ServiceContainer * GetServiceContainer(ServiceHandle handle) const 
    {
      return IsValidServiceHandle(handle) ? &(services_[handle.val_ - 1]) : nullptr;
    }

    /* link a service in service list */
    void LinkService(ServiceHandle handle);
    void UnlinkService(ServiceHandle handle);

    ServiceDatabase();
  public:
    static constexpr ServiceDatabase & instance() { return instance_; }

    ServiceIterator                      begin() const;
    constexpr ::ecpp::EndIterationTag    end()   const {return {};}
    constexpr ::ecpp::EndIterationTag    rend()  const {return {};}


    void             getTransponder(uint_fast16_t channel, DabChannelValue &arg) const;

    DabChannelList                   & getDabChannelList() { return ChannelList; }

    ServiceHandle UpdateService(const ServiceValue &value, ServiceHandle handle = kHandleEnd);
    void          ClearDABProperties(ServiceHandle handle);
    void          removeService(ServiceHandle handle) { UnlinkService(handle); }
  private:
    DabChannelList      ChannelList;

    ServiceHandle       handle_begin_   {kHandleEnd};
    ServiceHandle       handle_rbegin_  {kHandleREnd};
    ServiceContainer    services_[kMaxServices];

    static ServiceDatabase            instance_;

    friend class ServiceIterator;
    friend class ServiceValueIterator;
    friend class StorageManager;
  };

}

namespace signals
{
  void Changed(const ::MusicBox::Database::ServiceDatabase&, ::MusicBox::Database::ServiceHandle);
}

#endif
