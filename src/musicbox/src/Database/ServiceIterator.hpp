/*
 *  Copyright 2018-2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DATABASE_SERVICEITERATOR_HPP_
#define MUSICBOX_DATABASE_SERVICEITERATOR_HPP_

#include "Database/ServiceHandle.hpp"
#include "Database/ServiceDatabase.hpp"
#include "ecpp/Iterator.hpp"


namespace MusicBox::Database
{
  class ServiceIterator
  {
  public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type        = const ServiceHandle;
    using difference_type   = intptr_t;
    using pointer           = const ServiceHandle *;
    using reference         = const ServiceHandle&;

    constexpr ServiceIterator() : handle_{ServiceDatabase::kHandleREnd} {}
    constexpr ServiceIterator(ServiceHandle handle) : handle_{handle}    {}
    constexpr ServiceIterator(const ServiceIterator & init) : handle_{init.handle_}    {}

    constexpr ServiceHandle GetHandle() const { return handle_; }

    constexpr bool operator==(const ServiceIterator& other) const { return handle_ == other.handle_;}
    constexpr bool operator!=(const ServiceIterator& other) const { return handle_ != other.handle_;}

    const ServiceIterator & operator++()    { handle_ = ServiceDatabase::instance().GetNextServiceHandle(handle_); return *this;}
    const ServiceIterator & operator--()    { handle_ = ServiceDatabase::instance().GetPrevServiceHandle(handle_); return *this;}

    constexpr bool operator!=(const ::ecpp::EndIterationTag& end) const 
    { 
      return ServiceDatabase::instance().IsValidServiceHandle(handle_);
    }

    constexpr bool operator==(const ::ecpp::EndIterationTag& end) const 
    { 
      return !ServiceDatabase::instance().IsValidServiceHandle(handle_);
    }

    constexpr ServiceHandle operator*() const {return GetHandle(); }
  protected:
    ServiceHandle handle_;
  };
}

#endif