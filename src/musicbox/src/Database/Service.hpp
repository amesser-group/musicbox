/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DATABASE_SERVICE_HPP_
#define MUSICBOX_DATABASE_SERVICE_HPP_

#include "ecpp/ArrayString.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

#include "Database/DabTransponder.hpp"

namespace MusicBox::Database
{
  typedef ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8, 16> ServiceName;

  class ServiceValue
  {
  public:
    constexpr ServiceValue() {}
    constexpr ServiceValue(const DabChannelHandle &chan) : DabChannel{chan} {}

    typedef uint_least16_t FMFrequency_10kHz;

    constexpr bool isDabService() const  { return DabChannel.isValid() && (ServiceId != 0);}
    constexpr bool isFMService()  const  { return (FMFreq > 0);}
    constexpr bool isUnused()     const  { return (0 == ServiceId); }

    void ClearDABParameters();
    
    bool update(const ServiceValue & rhs);

    /* const ServiceValue & operator= (const ServiceValue & rhs); */

    bool isSameService(const ServiceValue & rhs) const;

    uint_least16_t     ServiceId {0};

    FMFrequency_10kHz  FMFreq    {0};

    DabChannelHandle   DabChannel {};

    /** Si46xx specific component id */
    uint_least16_t CId            {0};

    ServiceName     Name         {};

    uint_least8_t  bPreset       {0};
  };

}

#endif