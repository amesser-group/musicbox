/*
 *  Copyright 2018-2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Database/ServiceDatabase.hpp"
#include "Database/ServiceValueIterator.hpp"

using namespace MusicBox::Database;
using ::ecpp::ElementCount;

/* DAB                              FM


  * -------------------------------+--------------
  * Ensemble                         Channel
  * - DAB Service <-> Radio Sender   - FM Service
  *   - Component "Audio Stream"
  *   - Component "TMC Stream"
  * - DAB Service n...
  *
  *
  *   */

ServiceDatabase ServiceDatabase::instance_;

/* For some reason, declaring a default constructor crashes gcc */
ServiceDatabase::ServiceDatabase() {}


ServiceHandle
ServiceDatabase::GetNextServiceHandle(ServiceHandle handle) const
{
  if(!IsValidServiceHandle(handle))
    return handle;

  return GetServiceContainer(handle)->_handleNext;
}

ServiceHandle
ServiceDatabase::GetPrevServiceHandle(ServiceHandle handle) const
{
  if(!IsValidServiceHandle(handle))
    return handle;

  return GetServiceContainer(handle)->_handlePrev;
}

const ServiceValue *
ServiceDatabase::GetService(ServiceHandle handle) const
{
  if(!IsValidServiceHandle(handle))
    return nullptr;

  return &(GetServiceContainer(handle)->_value);
}

ServiceIterator ServiceDatabase::begin() const
{
  return ServiceIterator(handle_begin_);
}

/** Update service information within the database.
 *
 * @return handle of updated service
 */
ServiceHandle
ServiceDatabase::UpdateService(const ServiceValue &value, ServiceHandle handle)
{
  if(!(value.isDabService() || value.isFMService()))
    return kHandleEnd;


  /* find matching service if not provided */
  if (handle == kHandleEnd)
  {
    ServiceValueIterator it = begin();

    for(; it != end(); ++it)
    {
      if (it->isSameService(value))
        break;
    }

    if(it != end())
    { /* found a matching service */
      handle = it.GetHandle();
    }
    else
    { /* found no matching service, locate first unused
        * service entry */
      uint_fast8_t i;

      for(i = 0; i < kMaxServices; ++i)
      {
        if(services_[i]._value.isUnused())
          break;
      }
      handle = ServiceHandle(i+1);

    }
  }

  auto * container = GetServiceContainer(handle);
  /* when existing or empty entry found update it if required
    * and link service if needed */
  if(container)
  {
    log_.Info("Updating service %d with id %d as \"%.16s\" DAB %s/%d FM %dkHz\n", handle.GetRaw(), 
        value.ServiceId, value.Name.data(), 
        value.DabChannel.ToString().data(), value.CId,
        value.FMFreq*10 );
        
    if (container->_value.update(value))
    {
      LinkService(handle);
      signals::Changed(*this, handle);
    }
  }
  else
  {
    log_.Error("Out of service records\n");
  }

  return handle;
}

void
ServiceDatabase::ClearDABProperties(ServiceHandle handle)
{
  ServiceContainer *container = GetServiceContainer(handle);

  if(container)
    container->_value.ClearDABParameters();
}

void
ServiceDatabase::LinkService(ServiceHandle handle)
{
  ServiceContainer *container = GetServiceContainer(handle);

  if(container == nullptr)
    return;

  /* check if already linked */
  if (  (container->_handleNext != kHandleREnd) ||
        (container->_handlePrev != kHandleREnd))
    return;

    /* insert at start of list */
  container->_handleNext = handle_begin_;
  container->_handlePrev = kHandleREnd;

  /* update successor */
  if(handle_begin_ == kHandleEnd)
    handle_rbegin_ = handle; /* set reverse begin */
  else
    GetServiceContainer(handle_begin_)->_handlePrev = handle; /* reverse-link current list-head*/

  /* update precedessor */
  /* set new list start point */
  handle_begin_ = handle;
}

void
ServiceDatabase::UnlinkService(ServiceHandle handle)
{
  ServiceContainer *container = GetServiceContainer(handle);

  if(container == nullptr)
    return;

  /* check if already unlinked */
  if ( (container->_handleNext == kHandleREnd) &&
       (container->_handlePrev == kHandleREnd))
    return;

  /* update precedessor */
  if(container->_handlePrev == kHandleREnd)
    handle_begin_ = container->_handleNext;
  else
    GetServiceContainer(container->_handlePrev)->_handleNext = container->_handleNext;

  /* update successor */
  if(container->_handleNext == kHandleEnd)
    handle_rbegin_ = container->_handlePrev;
  else
    GetServiceContainer(container->_handleNext)->_handlePrev = container->_handlePrev;

  /* empty container (implicitly marks it as unused) */
  *container = {};
}
