/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUSICBOX_DATABASE_DABCHANNELS_HPP_
#define MUSICBOX_DATABASE_DABCHANNELS_HPP_

#include <cstdint>
#include "ecpp/ArrayString.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

namespace MusicBox::Database
{
  using ::std::uint_least8_t;

  class DabChannelHandle
  {
  public:
    constexpr DabChannelHandle() {}

    template<int S>
    constexpr DabChannelHandle(const char (&name)[S]) : Val {compress(name)} {}

    constexpr bool operator== (const DabChannelHandle &rhs) const { return Val == rhs.Val; }
    constexpr bool operator!= (const DabChannelHandle &rhs) const { return Val != rhs.Val; }

    constexpr bool isValid() const { return Val != 0; }

    constexpr uint_least8_t getRaw() const { return Val; }
    void                    setRaw(uint8_t val) { Val = val; }

    ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8,4> ToString() const
    {
      ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8,4> buf;
      buf.IPrintF("%d%c", Val >> 3, 'A' + (Val & 0x7) );
      return buf;
    }
    
  private:
    static constexpr uint_least8_t compress(const char (&name)[3])
    {
      return (((name[0] - '0') & 0x1F) << 3) | ((name[1] - 'A') & 0x7);
    }

    static constexpr uint_least8_t compress(const char (&name)[4])
    {
      return ((( (name[0] - '0') * 10 + name[1] - '0') & 0x1F) << 3) | ((name[2] - 'A') & 0x7);
    }

    uint_least8_t Val    {0};
  };

  class DabChannelFreq
  {
  public:
    constexpr DabChannelFreq() {}
    constexpr DabChannelFreq(uint32_t freq) : Val { static_cast<uint_least8_t>(freq & 0xFF), 
                                                    static_cast<uint_least8_t>((freq >> 8) & 0xFF),
                                                    static_cast<uint_least8_t>((freq >> 16) & 0xFF)} {}

    constexpr operator uint32_t() const { return Val[0] | (Val[1] << 8UL) | (Val[2] << 16UL); }

  private:
    uint_least8_t Val[3] {};
  };

  class DabChannelValue
  {
  public:
    constexpr DabChannelValue() {}

    template<int S>
    constexpr DabChannelValue(const char (&name)[S], uint32_t freq) : Handle { name }, Freq {freq} {}
    
    DabChannelHandle Handle {};
    DabChannelFreq   Freq   {};
  };

  class DabChannelList
  {
  public:
    constexpr DabChannelList() {}

    static constexpr int_least8_t MaxTransponders = 40;

    constexpr const DabChannelValue & operator[] (int i) const { return Channels[i];}
    constexpr       DabChannelValue & operator[] (int i) { return Channels[i];}

    const DabChannelValue & getDabChannelValue(DabChannelHandle handle) const;
    int_fast8_t             indexOf(DabChannelHandle handle) const;
  private:
    DabChannelValue Channels[MaxTransponders + 1] {
      {  "5A", 174928 },
      {  "5B", 176640 },
      {  "5C", 178352 },
      {  "5D", 180064 },
      {  "6A", 181936 },
      {  "6B", 183648 },
      {  "6C", 185360 },
      {  "6D", 187072 },
      {  "7A", 188928 },
      {  "7B", 190640 },
      {  "7C", 192352 },
      {  "7D", 194064 },
      {  "8A", 195936 },
      {  "8B", 197648 },
      {  "8C", 199360 },
      {  "8D", 201072 },
      {  "9A", 202928 },
      {  "9B", 204640 },
      {  "9C", 206352 },
      {  "9D", 208064 },
      { "10A", 209936 },
      { "10B", 211648 },
      { "10C", 213360 },
      { "10D", 215072 },
      { "11A", 216928 },
      { "11B", 218640 },
      { "11C", 220352 },
      { "11D", 222064 },
      { "12A", 223936 },
      { "12B", 225648 },
      { "12C", 227360 },
      { "12D", 229072 },
      { "13A", 230784 },
      { "13B", 232496 },
      { "13C", 234208 },
      { "13D", 235776 },
      { "13E", 237488 },
      { "13F", 239200 },
    };
  };
}
#endif