/*
 * fm.cpp
 *
 *  Created on: 03.11.2016
 *      Author: andi
 */

#include "app.hpp"



void menu_fm_enter()
{
  menu_arg() = 0;

  lcd_loc(0,0);
  lcd_write("-- FM --",8);
}

void menu_fm_encoder_changed(int16_t)
{
  char buf[16] = {0};

  lcd_loc(5,0);
  lcd_write(buf, 16);
}

void menu_fm_encoder_pressed(bool down)
{
  if(down)
  {
    leave_menu();
  }
}

const struct menu menu_fm_node =
{
  menu_fm_encoder_changed,
  menu_fm_encoder_pressed,
  menu_fm_enter,
  0,
  0,
  0,
  0,
};
