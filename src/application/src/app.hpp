/*
 * app.hpp
 *
 *  Created on: 03.11.2016
 *      Author: andi
 */

#ifndef FIRMWARE_APP_SRC_APP_HPP_
#define FIRMWARE_APP_SRC_APP_HPP_

#include <cstdint>
#include <cstring>
#include <cstdio>

struct menu
{
  void (*encoder_changed)(int16_t delta);
  void (*encoder_pressed)(bool down);
  void (*enter)();
  void (*resume)();
  void (*leave)();
  void (*update)();
  void  *arg;
};

extern const struct menu menu_fm_node;


uint32_t   & menu_arg();
void*        handler_arg();

void leave_menu();


void lcd_init();
void lcd_loc(unsigned int col, unsigned int row);
void lcd_write(const char* text);
void lcd_write(const char* text, size_t textlen);


#endif /* FIRMWARE_APP_SRC_APP_HPP_ */
