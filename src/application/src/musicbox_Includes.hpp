/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_INCLUDES_HPP_
#define MUSICBOX_INCLUDES_HPP_

#include <ecpp/Container.hpp>
#include <app.hpp>
#include <worker.hpp>
#include <rds.hpp>


#include "database.hpp"

#include "ui_model.hpp"
#include "FileSystem.hpp"

#include "updater/UpdaterRunner.hpp"
#include "Config/Execution.hpp"

using namespace ECPP::Worker;
using namespace MusicBox;








#endif /* FIRMWARE_APP_SRC_GLOBALS_HPP_ */
