/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "FileSystem.hpp"
#include "System.hpp"
#include "diskio.h"

using namespace MusicBox;

ElmChanFile::ElmChanFile(const char* path, FileModeType mode)
{
  auto result = f_open(&File, path, mode);

  /* on success increase refcounter in volume manager */
  if(FR_OK == result)
  {
    auto vptr = ElmChanVolumeManager::getVolumeManager(File.obj);
    vptr->mountVolume();
  }
  else
  {
    /* ensure that not associated */
    File.obj.fs = nullptr;
  }
}

void
ElmChanFile::close()
{
  auto vptr = ElmChanVolumeManager::getVolumeManager(File.obj);

  f_close(&File);

  if(nullptr != vptr)
    vptr->unmountVolume();
}

ElmChanDirectory::ElmChanDirectory(const char* path)
{
  auto result = f_opendir(&Dir, path);

  /* on success increase refcounter in volume manager */
  if(FR_OK == result)
  {
    auto vptr = ElmChanVolumeManager::getVolumeManager(Dir.obj);
    vptr->mountVolume();
  }
  else
  {
    /* ensure that not associated */
    Dir.obj.fs = nullptr;
  }
}


void
ElmChanDirectory::close()
{
  auto vptr = ElmChanVolumeManager::getVolumeManager(Dir.obj);

  f_closedir(&Dir);

  if(nullptr != vptr)
    vptr->unmountVolume();
}


void
ElmChanVolumeManager::mountVolume()
{
  if( 0 == RefCount)
    MountResult = f_mount(&FatFS, "", 1);

  RefCount++;
}

void
ElmChanVolumeManager::unmountVolume()
{
  if(RefCount == 0)
    return;

  if(1 == RefCount)
    f_unmount("");

  RefCount--;
}

/** @defgroup musixbox_fs_glue ElmChan FatFS glue
 *
 * @{ */

/** get disk status */
DSTATUS
disk_status (BYTE pdrv)
{
  auto & media = ::sys::globals().sdcard_driver_;

  switch (media.getCardState().CardState)
  {
  case SDCardState::Ready:
    return 0; /* STA_PROTECT; */
  default:
    return STA_NOINIT;
  }
}

DSTATUS
disk_initialize (BYTE pdrv)
{
  return disk_status(pdrv);
}

/** Read sectors from filesystem */
DRESULT
disk_read (BYTE pdrv, BYTE* buff, LBA_t sector, UINT count)
{
  auto & media = ::sys::globals().sdcard_driver_;

  for(;count > 0; --count, buff += 512)
  {
    if(!media.readBlock(sector, buff))
      return RES_ERROR;
  }

  return RES_OK;
}

/** Write sectors to disk */
DRESULT
disk_write (BYTE pdrv, const BYTE* buff, LBA_t sector, UINT count)
{
  auto & media = ::sys::globals().sdcard_driver_;

  if(!media.writeBlock(sector, buff, count))
    return RES_ERROR;

  return RES_OK;
}

/** perform ioctl on disk */
DRESULT
disk_ioctl (BYTE pdrv, BYTE cmd, void* buff)
{
  auto & media = ::sys::globals().sdcard_driver_;

  switch(cmd)
  {
  case GET_SECTOR_COUNT:
    {
      LBA_t* plba = reinterpret_cast<LBA_t*>(buff);
      *plba = media.getCardState().BlockCount;
      return RES_OK;
    }
  case GET_SECTOR_SIZE:
    {
      WORD* pw = reinterpret_cast<WORD*>(buff);
      *pw = 512;
      return RES_OK;
    }
  case GET_BLOCK_SIZE:
    {
      DWORD* pdw = reinterpret_cast<DWORD*>(buff);
      *pdw = 1;
      return RES_OK;
    }
  case  CTRL_SYNC:
    return RES_OK;
  case CTRL_TRIM:
    return RES_OK;
  default:
    return RES_PARERR;
  }
}

DWORD
get_fattime (void)
{
  return 0;
}

/** @} */