/*
 * ui.cpp

 *
 *  Created on: 30.03.2017
 *      Author: andi
 */
#include "app.hpp"
#include <cstdarg>
#include "ui_4x20_rot/ui_view_controller.hpp"


DisplayManager DisplayManager::instance;


void Screen::drawScreen()
{

}

void Screen::handleRotaryEncoder(int_fast16_t pos, uint_fast8_t button_state)
{

}

void Screen::refresh()
{
  DisplayManager & dm = DisplayManager::instance;

  if(popup || this == dm.getDisplayedScreen())
  {
    dm.redraw_screen();
  }
}


void DisplayManager::redraw_screen_job(Job &job)
{
  BoardState & state = board_get_state();
  DisplayManager & dm = container_of(DisplayManager, draw_job, job);
  Screen *screen, *display_screen;

  display_screen = dm.displayed_screen;

  if((NULL != display_screen) &&
     display_screen->popup )
  {
    if(static_cast<uint16_t>(state.time - dm.screen_display_tick) > 1500)
    {
      display_screen->popup = 0;
      display_screen = NULL;
    }
  }

  if(display_screen == NULL)
  {
    display_screen = dm.top_screen;
    for(screen = display_screen->next_screen; screen; screen = screen->next_screen)
    {
      if(screen->popup && !display_screen->popup)
      {
        display_screen = screen;
      }
      else if(!screen->popup && display_screen->popup)
      {
        ;
      }
      else if(screen->layer < display_screen->layer)
      {
        display_screen = screen;
      }
    }
  }

  if(display_screen != dm.displayed_screen)
  {
    dm.screen_display_tick = static_cast<uint16_t>(state.time);
    dm.displayed_screen = display_screen;
  }

  if(display_screen)
  {
    display_screen->drawScreen();
  }
}

void DisplayManager::handle_input_job(Job &job)
{
  DisplayManager & dm = container_of(DisplayManager, input_job, job);
  int16_t encoder_delta;

  encoder_delta = dm.encoder_pos - dm.encoder_last_pos;
  dm.encoder_last_pos =  dm.encoder_pos;

  if(dm.displayed_screen)
  {
    dm.displayed_screen->handleRotaryEncoder(encoder_delta, dm.encoder_button_state);
  }

}


void DisplayManager::redraw_screen()
{
  DisplayManager & dm = DisplayManager::instance;
  Job &job = dm.draw_job;

  job.set_handler(dm.redraw_screen_job);
  enqueueJob(&job);
}

void DisplayManager::popup_screen()
{
  DisplayManager & dm = DisplayManager::instance;

  dm.displayed_screen = NULL;
  dm.redraw_screen();
}

void DisplayManager::register_screen(Screen & screen)
{
  DisplayManager & dm = DisplayManager::instance;

  screen.next_screen = dm.top_screen;
  dm.top_screen = &screen;
}

void DisplayManager::handle_input(uint_fast16_t EncoderPos, bool ButtonState)
{
  DisplayManager & dm = DisplayManager::instance;
  Job &job = dm.input_job;

  if((EncoderPos != dm.encoder_pos) || ButtonState || dm.encoder_button_state)
  {
    dm.encoder_pos = EncoderPos;

    if(ButtonState)
    {
      dm.encoder_button_state = min(2, dm.encoder_button_state + 1);
    }
    else
    {
      dm.encoder_button_state = 0;
    }

    job.set_handler(dm.handle_input_job);
    enqueueJob(&job);
  }
}
LogScreen LogScreen::instance;

void
LogScreen::drawScreen()
{
  uint8_t cnt;

  for(cnt = 0; cnt < 4; ++cnt)
  {
    uint8_t row = (pos + 16 - 4 + cnt) % 16;

    lcd_loc(0, cnt);
    lcd_write(buffer[row], sizeof(buffer[row]));
  }
}

void LogScreen::log_info(const char* fmt, ...)
{
  LogScreen &log_screen = instance;
  va_list ap;
  int cx;

  va_start(ap,fmt);
  cx = vsniprintf(log_screen.buffer[log_screen.pos % 16], sizeof(log_screen.buffer[log_screen.pos % 16]),
      fmt, ap);
  va_end(ap);

  for(;(cx >= 0) && (cx < (int)(sizeof(log_screen.buffer[0]) / sizeof(log_screen.buffer[0][0]))); ++cx)
  {
    log_screen.buffer[log_screen.pos % 16][cx] = ' ';
  }

  log_screen.pos++;
  //log_screen.setPopUp();

  DisplayManager::popup_screen();
}
