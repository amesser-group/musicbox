/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FILESYSTEM_HPP_
#define FILESYSTEM_HPP_

#include "ff.h"
#include "ecpp/Container.hpp"

namespace MusicBox
{
  class ElmChanFs
  {
  public:
    typedef FRESULT          ResultType;
    typedef FILINFO          DirEntType;
    typedef BYTE             FileModeType;
    typedef FSIZE_t          FileSizeType;
    typedef DWORD            ClusterNumberType;
    typedef LBA_t            SectorNumberType;
    typedef FFOBJID          ObjectIdType;
  };

  /** Directory Handler */
  class ElmChanDirectory : public ElmChanFs
  {
  public:
    constexpr ElmChanDirectory() : ElmChanFs{} {}

    ElmChanDirectory(const char* path);
    ElmChanDirectory(const ElmChanDirectory & init) = delete;

    ElmChanDirectory(ElmChanDirectory && init)
    {
      Dir             = init.Dir;
      init.Dir.obj.fs = nullptr;
    }

    ~ElmChanDirectory()
    {
      close();
    }

    ElmChanDirectory & operator = (ElmChanDirectory && rhs)
    {
      close();

      Dir             = rhs.Dir;
      rhs.Dir.obj.fs = nullptr;
      return *this;
    }

    void close();

    void rewind() {
      f_readdir(&Dir, nullptr);
    }

    bool read(DirEntType &dirent) {
      if (FR_OK != f_readdir(&Dir, &dirent))
        return false;

      return dirent.fname[0] != '\0';
    }

  private:
    DIR           Dir {};

    friend class ElmChanVolumeManager;
  };

  /** Directory Handler */
  class ElmChanFile : public ElmChanFs
  {
  public:
    constexpr ElmChanFile() : ElmChanFs{} {}

    ElmChanFile(const char* path, FileModeType mode = FA_READ);
    ElmChanFile(const ElmChanFile & init) = delete;

    ElmChanFile(ElmChanFile & init)
    {
      File   = init.File;
      init.File.obj.fs = nullptr;
    }

    ElmChanFile(ElmChanFile && init)
    {
      File   = init.File;
      init.File.obj.fs = nullptr;
    }

    ~ElmChanFile()
    {
      close();
    }

    ElmChanFile & operator = (ElmChanFile && rhs)
    {
      close();

      File   = rhs.File;
      rhs.File.obj.fs = nullptr;
      return *this;
    }

    constexpr bool isOpen() const {
      return nullptr != File.obj.fs;
    }

    constexpr FileSizeType getSize() const {
      return isOpen() ? f_size(&File) : 0;
    }

    constexpr FileSizeType tell() const {
      return isOpen() ? f_tell(&File) : 0;
    }

    void close();

    /* Seek file pointer to given position
     *
     * @return Non-zero cluster number on success, zero otherwise */
    ClusterNumberType seek(FileSizeType offset)
    {
      if (FR_OK != f_lseek(&File, offset))
        return 0;

      if(offset == 0) /* workaround different behavior of f_lseek incase of offset 0 */
        return File.obj.sclust;
      else
        return File.clust;
    }

    void rewind() {
      f_lseek(&File, 0);
    }

    long read(void *buf, unsigned int buflen)
    {
      UINT count;

      if(FR_OK != f_read(&File, buf, buflen, &count))
        return -1;

      return count;
    }

    long write(const void* buf, unsigned int buflen)
    {
      UINT count;

      if(FR_OK != f_write(&File, buf, buflen, &count))
        return -1;

      return count;
    }

  private:
    FIL           File   {};

    friend class ElmChanVolumeManager;
  };


  /** Manager for Volumes */
  class ElmChanVolumeManager : public ElmChanFs
  {
  public:
    typedef ElmChanDirectory DirectoryType;
    typedef ElmChanFile      FileType;

    constexpr ElmChanVolumeManager() {}

    void      mountVolume();
    void      unmountVolume();

    constexpr bool isMounted() const { return FatFS.fs_type != 0;};

    DirectoryType openDir(const char* path)
    {
      return DirectoryType(path);
    }

    FileType openFile(const char* path, FileModeType mode = FA_READ)
    {
      return FileType(path, mode);
    }

    constexpr SectorNumberType getBaseSector() const
    {
      return FatFS.database;
    }

    constexpr unsigned int getSectorSize() const
    {
#if FF_MAX_SS == FF_MIN_SS
      return FF_MAX_SS;
#else
      return FatFS.ssize;
#endif
    }
    constexpr unsigned short getSectorsPerCluster() const
    {
      return FatFS.csize;
    }

  protected:
    static ElmChanVolumeManager * getVolumeManager(ObjectIdType & objid)
    {
      if(nullptr == objid.fs)
        return nullptr;

      return & container_of(ElmChanVolumeManager, FatFS, *(objid.fs));
    }

    /** fatfs structure */
    FATFS          FatFS       {};
    ResultType     MountResult {FR_OK};
    unsigned int   RefCount    {0};

    friend class ElmChanFile;
    friend class ElmChanDirectory;
  };

}

#endif /* FILESYSTEM_HPP_ */