
#ifndef MUSICBOX_DRAWCONTEXT_H_
#define MUSICBOX_DRAWCONTEXT_H_

#include "ecpp/Ui/Widget/Text/DrawContext.hpp"
#include "ecpp/Datatypes.hpp"
#include "Platform.hpp"
#include <cstring>
#include <ecpp/Container.hpp>
#include "Bsp.hpp"
#include "Database/ServiceDatabase.hpp"
#include "Drivers/SI468x/Driver.hpp"

namespace MusicBox
{
  using ::ecpp::Units::MilliSecond;

  class Lcd4x20DrawContext;


  class Lcd4x20DrawContext : public ecpp::DrawContext
  {
  public:
    typedef decltype(sys::Bsp::instance().display_)                 Display;
    typedef decltype(sys::Bsp::instance().display_.CreatePainter()) Painter;
    typedef uint_least8_t IndexType;

    /* typedef ::ecpp::FieldContext<IndexType> FieldContext; */

    constexpr Lcd4x20DrawContext() : DrawContext{} {}

    void start();
    void poll()  { Poll.Enqueue(); }

    template<typename T>
    void scheduleRedraw(const T &timeout) { RedrawTimeout = timeout; }

  private:
    void handleInteractionTimeout();

    ECPP_JOB_TIMER_DECL(Poll);

    MilliSecond<>  RedrawTimeout {0};
  };

};

#endif
