/*
 * ui.cpp
 *
 *  Created on: 14.09.2017
 *      Author: andi
 */
#include <functional>
#include "globals.hpp"
#include "bsp.h"


const StaticMenuItem Lcd4x20Rot::s_EndMarker    = {"-----------", nullptr};


void Lcd4x20Rot::formatSummary(char *buf, uint_fast8_t BufferLen)
{

  switch(m_OpMode)
  {
  case OpModeType::MODE_PLAY_FM:
    {
      const auto & SiStatusMgr = Globals::getInstance().SiHandler.getStatusManager();
      const char *pState;
      unsigned long ulVal;

      switch(SiStatusMgr.getTunerState())
      {
      case SiStatusMgr.TunerStateType::STATE_UNKNOWN:
      default:
        pState = "UNK";
        break;
      case SiStatusMgr.TunerStateType::STATE_TUNED:
        pState = "TUN";
        break;
      case SiStatusMgr.TunerStateType::STATE_FAILED:
        pState = "ERR";
        break;
      }
      ulVal = SiStatusMgr.getTunedFrequency();

      sniprintf(buf, BufferLen, "FM: %3lu.%1lu MHz %s   ",
          ulVal / 1000U, (ulVal % 1000U + 50U) / 100U, pState);
      break;
    }
  case OpModeType::MODE_PLAY_DAB:
    {
      const auto & dabmgr = Globals::getInstance().SiHandler.getDABManager();
      const char *pState;
      unsigned long ulVal;

      switch(dabmgr.getTunerState())
      {
      case dabmgr.TunerStateType::STATE_UNKNOWN:
      default:
        pState = "UNK";
        break;
      case dabmgr.TunerStateType::STATE_TUNED:
        pState = "TUN";
        break;
      case dabmgr.TunerStateType::STATE_FAILED:
        pState = "ERR";
        break;
      }

      //ulVal = dabmgr.getTunedFrequency();
      ulVal = 0;

      sniprintf(buf, BufferLen, "DAB: %3lu.%1lu MHz %s  ",
          ulVal / 1000U, (ulVal % 1000U + 50U) / 100U, pState);
      break;
    }
  case OpModeType::MODE_INIT:
    {
      sniprintf(buf, BufferLen, "INIT:               ");
      break;
    }
  }
}

#if 0

void Lcd4x20Rot::drawFMScreen()
{
  const auto & RdsManager  = Globals::getInstance().RdsManager;
  char buf[21];

  lcd_loc(0,0);

  formatSummary(buf, sizeof(buf));

  lcd_write(buf, 20);

  lcd_loc(0,1);

  sniprintf(buf, sizeof(buf), "      %8.8s      ",
      RdsManager.getStationName());

  if(m_State == StateType::STATE_FM_TUNING)
  {
    buf[0]  = '<';
    buf[19] = '>';
  }
  lcd_write(buf, 20);

  lcd_loc(0,2);
  if(m_State == StateType::STATE_FM_VOL)
  {
    const auto & Max98089Manager  = Globals::getInstance().MaxHandler;

    int_fast16_t dB = Max98089Manager.getVolume_cB();

    sniprintf(buf, sizeof(buf), "-    %4d  dB     +", dB);

    buf[9] = buf[8];
    buf[8] = '.';
  }
  else
  {
    memset(buf,' ', 20);
  }

  lcd_write(buf, 20);

  memset(buf,' ', 20);

  lcd_loc(0,3);
  lcd_write(buf, 20);
}

void Lcd4x20Rot::drawDABScreen()
{
  const auto & dabmgr = Globals::getInstance().SiHandler.getDABManager();
  const ServiceValue &s = dabmgr.GetCurrentService();
  char buf[21];

  lcd_loc(0,0);

  formatSummary(buf, sizeof(buf));

  lcd_write(buf, 20);

  memset(buf,' ', 20);
  lcd_loc(0,1);

    memcpy(&buf[2], s.Name, 16);

  if(m_State == StateType::STATE_DAB_TUNING)
  {
    buf[0]  = '<';
    buf[19] = '>';
  }

  lcd_write(buf, 20);

  lcd_loc(0,2);
  if(m_State == StateType::STATE_DAB_VOL)
  {
    const auto & Max98089Manager  = Globals::getInstance().MaxHandler;

    int_fast16_t dB = Max98089Manager.getVolume_cB();

    sniprintf(buf, sizeof(buf), "-    %4d  dB     +", dB);

    buf[9] = buf[8];
    buf[8] = '.';
  }
  else
  {
    memset(buf,' ', 20);
  }
  lcd_write(buf, 20);

  sniprintf(buf, sizeof(buf), "DebugVal: 0x%08lX", dabmgr.getDebugVal());

  lcd_loc(0,3);
  lcd_write(buf, 20);
}

void Lcd4x20Rot::drawMenuScreen()
{
  char buf[21];

  lcd_loc(0,0);

  formatSummary(buf,sizeof(buf));

  lcd_write(buf, 20);

  lcd_loc(0,1);

  memcpy(buf, " FM Modus           ", sizeof(buf));

  if(m_MenuPos == MenuItemType::MENU_ITEM_SELECT_FM)
  {
    buf[0]  = '[';
    buf[19] = ']';
  }

  lcd_write(buf, 20);
  lcd_loc(0,2);

  memcpy(buf, " DAB Modus          ", sizeof(buf));

  if(m_MenuPos == MenuItemType::MENU_ITEM_SELECT_DAB)
  {
    buf[0]  = '[';
    buf[19] = ']';
  }

  lcd_write(buf, 20);

  lcd_loc(0,3);
  memset(buf,' ', 20);
  lcd_write(buf, 20);
}

void Lcd4x20Rot::handleFMTuningInput(int_fast16_t pos, unsigned int pressed)
{

  auto & mgr = Globals::getInstance().SiHandler;

  if(pressed == BUTTON_DOWN)
  {
    m_State   = StateType::STATE_MENU;
    m_MenuPos = MenuItemType::MENU_ITEM_SELECT_FM;

    refresh();
  }
  else
  {
    if (pos > 0)
      mgr.seek(true);
    else if (pos < 0)
      mgr.seek(false);

    if(pos)
    {
      m_StateChangedTime = board_get_state().time;
    }
    else if((board_get_state().time - m_StateChangedTime) > 50)
    {
      m_State = StateType::STATE_FM_IDLE;
      refresh();
    }
  }
}

void Lcd4x20Rot::handleFMIdleInput(int_fast16_t pos, unsigned int pressed)
{
  //auto & SiMgr = Globals::getInstance().SiHandler;

  if(pressed == BUTTON_PRESSED)
  {
    m_StateChangedTime = board_get_state().time;
    m_State = StateType::STATE_FM_TUNING;
    refresh();
  }
  else if(pos)
  { /* volume change */
    auto & Max98089Handler = Globals::getInstance().MaxHandler;

    m_StateChangedTime = board_get_state().time;
    m_State            = StateType::STATE_FM_VOL;

    Max98089Handler.changeVolumeLevel(pos);
    refresh();
  }
  else if((board_get_state().time - m_StateChangedTime) > 50)
  {
    if(m_State != StateType::STATE_FM_IDLE)
    {
      m_State = StateType::STATE_FM_IDLE;
      refresh();
    }
  }
}

void Lcd4x20Rot::handleDABIdleInput(int_fast16_t pos, unsigned int pressed)
{
  //auto & SiMgr = Globals::getInstance().SiHandler;

  if(pressed == BUTTON_DOWN)
  {
    m_StateChangedTime = board_get_state().time;
    m_State = StateType::STATE_DAB_TUNING;
    refresh();
  }
  else if(pos)
  { /* volume change */
    auto & Max98089Handler = Globals::getInstance().MaxHandler;

    m_StateChangedTime = board_get_state().time;
    m_State            = StateType::STATE_DAB_VOL;

    Max98089Handler.changeVolumeLevel(pos);
    refresh();
  }
  else if((board_get_state().time - m_StateChangedTime) > 50)
  {
    if(m_State != StateType::STATE_DAB_IDLE)
    {
      m_State = StateType::STATE_DAB_IDLE;
      refresh();
    }
  }
}

void Lcd4x20Rot::handleDABTuningInput(int_fast16_t pos, unsigned int pressed)
{


  if(pressed == BUTTON_DOWN)
  {
    m_State   = StateType::STATE_MENU;
    m_MenuPos = MenuItemType::MENU_ITEM_SELECT_DAB;

    refresh();
  }
  else
  {
    auto & mgr = Globals::getInstance().SiHandler;

    if (pos > 0)
      mgr.seek(true);

    if(pos)
    {
      m_StateChangedTime = board_get_state().time;
    }
    else if((board_get_state().time - m_StateChangedTime) > 50)
    {
      m_State = StateType::STATE_DAB_IDLE;
      refresh();
    }
  }
}


void Lcd4x20Rot::handleMenuInput(int_fast16_t pos, unsigned int pressed)
{
  //auto & SiMgr = Globals::getInstance().SiHandler;

  if(pressed == BUTTON_DOWN)
  {
    auto & SiMgr = Globals::getInstance().SiHandler;

    switch(m_MenuPos)
    {
    case MenuItemType::MENU_ITEM_SELECT_FM:
      SiMgr.activateMode(Si4688DeviceManager::OpModeType::OPMODE_FM);
      break;
    case MenuItemType::MENU_ITEM_SELECT_DAB:
      SiMgr.activateMode(Si4688DeviceManager::OpModeType::OPMODE_DAB);
      break;
    default:
      break;
    }

    refresh();

    m_StateChangedTime = board_get_state().time;
  }
  else if(pos)
  {
    if(pos > 0)
    {
      if (((unsigned int)m_MenuPos + pos) >= (unsigned int)MenuItemType::MENU_ITEM_LAST)
      {
        m_MenuPos = MenuItemType::MENU_ITEM_LAST;
      }
      else
      {
        m_MenuPos = static_cast<MenuItemType>(static_cast<int>(m_MenuPos) + pos);
      }
    }
    else if(pos < 0)
    {
      if ((-pos) >= static_cast<int>(m_MenuPos))
      {
        m_MenuPos = MenuItemType::MENU_ITEM_FIRST;
      }
      else
      {
        m_MenuPos = static_cast<MenuItemType>(static_cast<int>(m_MenuPos) + pos);
      }
    }

    m_StateChangedTime = board_get_state().time;
    refresh();
  }
  else if((board_get_state().time - m_StateChangedTime) > 50)
  {
    switch(m_OpMode)
    {
    case OpModeType::MODE_PLAY_FM:
      m_State = StateType::STATE_FM_IDLE;
      refresh();
      break;
    case OpModeType::MODE_PLAY_DAB:
      m_State = StateType::STATE_DAB_IDLE;
      refresh();
      break;
    }
  }
}

#endif

void Lcd4x20Rot::handleSi4688OpModeChange(Si4688DeviceManager::OpModeType mode)
{
  switch(mode)
  {
  case Si4688DeviceManager::OpModeType::OPMODE_FM:
    m_OpMode = OpModeType::MODE_PLAY_FM;
    break;
  case Si4688DeviceManager::OpModeType::OPMODE_DAB:
    m_OpMode = OpModeType::MODE_PLAY_DAB;
    break;
  default:
    m_OpMode = OpModeType::MODE_INIT;
    m_State = StateType::STATE_USER;
    break;
  }

  refresh();
}

void Lcd4x20Rot::refresh_job(Job& job)
{
  Lcd4x20Rot & self = container_of(Lcd4x20Rot, m_Job, job);

  if(self.m_State == StateType::STATE_IDLE)
  {
    self.buildIdleScreen();
  }
  else
  {
    BuildFn handler = self.m_buildScreen;

    if(nullptr != handler)
      std::invoke(handler, Globals::getInstance().UiModel);
  }
}

void Lcd4x20Rot::handlePeriodic_job(Job& job)
{
  Lcd4x20Rot & self = container_of(Lcd4x20Rot, m_InputJob, job);
  int16_t encoder_change = board_get_encoder_change();
  unsigned int pressed = board_get_encoder_button();

  if(pressed == BUTTON_DOWN)
  {
    ActionFn handler;

    if (self.m_State != StateType::STATE_USER)
    {
      self.m_State = StateType::STATE_USER;
      handler = self.m_buildScreen;
    }
    else
    {
      handler = self.m_handleAction;
    }

    if(nullptr != handler)
      std::invoke(handler, Globals::getInstance().UiModel);

    self.m_StateChangedTime = board_get_state().time;
  }
  else if(encoder_change)
  {

    if(self.m_State == StateType::STATE_IDLE)
    {
      //self.scrollIdleMenu(encoder_change);
    }
    else
    {
      ScrollFn handler = self.m_handleScroll;

      if (nullptr != handler)
        std::invoke(handler, Globals::getInstance().UiModel, encoder_change);
    }

    self.m_StateChangedTime = board_get_state().time;
  }
  else
  {
    if((board_get_state().time - self.m_StateChangedTime) > 100)
    {
      self.m_StateChangedTime = board_get_state().time;

      self.m_State = StateType::STATE_IDLE;
    }
  }

  /* update volume from potentiometer */
  Globals::getInstance().MaxHandler.setVolumeLevel(board_get_volume() / 0x20);

  self.refresh();
  self.m_PeriodicTimer.continueTimeoutMs(200, job);
}


void Lcd4x20Rot::clearScreen()
{
  memset(m_DisplayBuffer, 0x00, sizeof(m_DisplayBuffer));
}

void Lcd4x20Rot::writeLine(int line, const char* text)
{
  if(line >= 4)
    return;

  strncpy(reinterpret_cast<char*>(m_DisplayBuffer[line]),
      text, sizeof(m_DisplayBuffer[line]));
}

void Lcd4x20Rot::writeLineCentered(int line, const char* text)
{
  int len,pos;

  if(line >= 4)
    return;

  len = strnlen(text, sizeof(m_DisplayBuffer[line]));

  if(len < sizeof(m_DisplayBuffer[line]))
    pos = (sizeof(m_DisplayBuffer[line]) - len) / 2;
  else
    pos = 0;

  memset(&(m_DisplayBuffer[line][0]), 0x00, pos);

  strncpy(reinterpret_cast<char*>(&m_DisplayBuffer[line][pos]),
      text, sizeof(m_DisplayBuffer[line]) - pos);
}

void Lcd4x20Rot::flush()
{
  int i;

  for(i = 0; i < 4; ++i)
  {
    lcd_loc(0,i);
    lcd_write((const char*)m_DisplayBuffer[i], 20);
  }
}

void Lcd4x20Rot::init()
{
  m_Job.set_handler(refresh_job);
  m_InputJob.set_handler(handlePeriodic_job);

  m_PeriodicTimer.startTimeoutMs(200, m_InputJob);

  m_buildScreen  = Globals::getInstance().UiModel.s_MainMenu.getBuild(-1);
  m_handleScroll = Globals::getInstance().UiModel.s_MainMenu.getScroll(-1);

  enqueueJob(&m_Job);
}


void Lcd4x20Rot::refresh()
{
  enqueueJob(&m_Job);
}

void Lcd4x20Rot::scrollSaturated(int delta)
{
  auto max = m_EntryCount;

  if(delta > 0)
  {
    if( (delta + selected_pos) >= max)
      selected_pos = max - 1;
    else
      selected_pos += delta;
  }

  if(delta < 0)
  {
    if( (delta + selected_pos) < 0)
      selected_pos = 0;
    else
      selected_pos += delta;
  }

  if(selected_pos <= 1)
    display_pos = 0;
  else
    display_pos = selected_pos - 1;

  refresh();
}


void Lcd4x20Rot::scrollIdleMenu(int delta)
{
  Globals::getInstance().MaxHandler.changeVolumeLevel(delta);
  refresh();
}

void Lcd4x20Rot::show(const SubMenuItem &menu)
{
  if(m_buildScreen == menu.getBuild(-1))
    return;

  m_buildScreen  = menu.getBuild(-1);
  m_handleScroll = menu.getScroll(-1);
  m_handleAction = nullptr;

  display_pos    = 0;
  selected_pos   = 0;
}

