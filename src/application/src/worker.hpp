/*
 * worker.hpp
 *
 *  Created on: 28.06.2017
 *      Author: andi
 */

#ifndef WORKER_HPP_
#define WORKER_HPP_

#include "ecpp/Execution/JobQueue.hpp"
#include "ecpp/Units/Time.hpp"
#include "Platform.hpp"
#include "Config/Execution.hpp"

namespace ECPP
{
  namespace Worker
  {
    using ::ecpp::Execution::Job;

    class TimerQueue;

  };
};





#endif /* FIRMWARE_APP_SRC_WORKER_HPP_ */
