/*
 * unused.cpp
 *
 *  Created on: 27.04.2017
 *      Author: andi
 */

#define MAX_MENU_DEPTH 10

struct menu_state
{
  const struct menu* handler[MAX_MENU_DEPTH];

  uint32_t           state[MAX_MENU_DEPTH];

  uint_least8_t      depth;

  uint_least8_t      encoder_button;

  uint_least16_t     encoder_pos;
};

static struct menu_state menu_state;

void enter_menu(const struct menu* menu)
{
  if(menu_state.depth < MAX_MENU_DEPTH)
  {
    menu_state.handler[menu_state.depth++] = menu;

    if(menu->enter)
    {
      menu->enter();
    }
  }
}

void leave_menu()
{
  if(menu_state.depth > 1)
  {
    const struct menu *menu_cur, *menu_prev;

    menu_cur = menu_state.handler[menu_state.depth - 1];
    menu_state.depth--;
    menu_prev = menu_state.handler[menu_state.depth - 1];

    if(menu_cur->leave)
    {
      menu_cur->leave();
    }

    if(menu_prev->resume)
    {
      menu_prev->resume();
    }
  }
}

void update_menu()
{
  uint_fast16_t  pos;
  int_fast16_t  delta;
  bool button;


  button = board_get_encoder_button();
  pos    = board_get_encoder_pos();

  delta = pos - menu_state.encoder_pos;
  menu_state.encoder_pos = pos;

  if(menu_state.depth >= 1)
  {
    const struct menu* menu = menu_state.handler[menu_state.depth - 1];

    if(delta && menu->encoder_changed)
    {
      menu->encoder_changed(delta);
    }

    if(menu_state.encoder_button != button && menu->encoder_pressed)
    {
      menu->encoder_pressed(button);
    }

    if(menu->update)
    {
      menu->update();
    }
  }

  menu_state.encoder_button = button;
}


uint32_t &
menu_arg()
{
  return menu_state.state[menu_state.depth-1];
}

void*
handler_arg()
{
  return menu_state.handler[menu_state.depth-1]->arg;
}

struct submenu_entry
{
  const char        *name;
  const struct menu *menu;
};

struct submenu
{
  const char            *title;
  uint_least8_t          numentries;
  struct submenu_entry  *entries;
};

struct submenu_entry root_menu_entries[] =
{
  {"FM", &menu_fm_node},
  {"FM1", &menu_fm_node},
  {"FM2", &menu_fm_node},
  {"FM3", &menu_fm_node},
  {"FM4", &menu_fm_node},
};



static const struct submenu root_menu =
{
  "Hauptmenü",
  sizeof(root_menu_entries) / sizeof(root_menu_entries[0]),
  root_menu_entries,
};

void submenu_update()
{
  const struct submenu *m = static_cast<struct submenu *>(handler_arg());
  unsigned int row;

  lcd_loc(0,0);

  lcd_write("-- ");
  lcd_write(m->title);
  lcd_write(" --");

  for(row = 0; row < 3; ++row)
  {
    unsigned int index = menu_arg() + row;
    unsigned int len = 0;

    lcd_loc(0,row+1);

    if(index < m->numentries)
    {
      lcd_write(m->entries[index].name);
      len = strlen(m->entries[index].name);
    }

    if(len < 20)
    {
      lcd_write("                    " + len);
    }
  }
}

void submenu_enter()
{
  const struct submenu *m = static_cast<struct submenu *>(handler_arg());
  menu_arg() = 0;

  submenu_update();
}

void submenu_resume()
{
  submenu_update();
}


void submenu_encoder_changed(int16_t iDelta)
{
  auto & arg = menu_arg();

  if(iDelta < 0)
  {
    if (arg > (uint16_t)(-iDelta))
    {
      arg += iDelta;
    }
    else
    {
      arg = 0;
    }
  }
  else if (iDelta > 0)
  {
    const struct submenu *m = static_cast<struct submenu *>(handler_arg());

    if((arg + iDelta) < m->numentries)
    {
      arg += iDelta;
    }
    else
    {
      arg = m->numentries - 1;
    }
  }

  submenu_update();
}

void submenu_encoder_pressed(bool down)
{
  if(down)
  {
    const struct submenu *m = static_cast<struct submenu *>(handler_arg());
    auto arg = menu_arg();

    if(arg < m->numentries)
    {
      enter_menu(m->entries[arg].menu);
    }
  }
}

void menu_root_enter()
{
  lcd_loc(0,0);
  lcd_write("Enc: XXXXX But: XX");
}

void menu_root_encoder_changed(int16_t)
{
  char buf[16];
  sniprintf(buf,sizeof(buf),"%5d", menu_state.encoder_pos);

  lcd_loc(5,0);
  lcd_write(buf);
}

static char button_cnt = 0;

void menu_root_encoder_pressed(bool down)
{
  char buf[16];

  if (down)
  {
    button_cnt |= 0x1;
  }
  else
  {
    button_cnt += 0x1;
  }

  sniprintf(buf,sizeof(buf),"%2d", button_cnt);
  lcd_loc(16,0);
  lcd_write(buf);

}

static const struct menu menu_root_node =
{
  submenu_encoder_changed,
  submenu_encoder_pressed,
  submenu_enter,
  submenu_resume,
  0,
  0,
  const_cast<void*>(static_cast<const void*>(&root_menu)),
};
