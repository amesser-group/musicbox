/*
 * si46xx_ui.cpp
 *
 *  Created on: 27.04.2017
 *      Author: andi
 */
#include "globals.hpp"
#include "app.hpp"


void Si46xxScreen::drawScreen()
{
  const auto & SiStatusMgr = Globals::getInstance().SiHandler.getStatusManager();
  const auto & RdsManager  = Globals::getInstance().RdsManager;
  unsigned long ulVal;
  const char *pState;
  char buf[20];

  lcd_loc(0,0);

  ulVal = SiStatusMgr.getTunedFrequency();

  switch(SiStatusMgr.getTunerState())
  {
  case SiStatusMgr.TunerStateType::STATE_UNKNOWN:
    pState = "UNK";
    break;
  case SiStatusMgr.TunerStateType::STATE_TUNED:
    pState = "TUN";
    break;
  case SiStatusMgr.TunerStateType::STATE_FAILED:
    pState = "ERR";
    break;
  }

  sniprintf(buf, sizeof(buf), "FM: %3lu.%1lu MHz %s   ",
      ulVal / 1000U, (ulVal % 1000U + 50U) / 100U, pState);

  lcd_write(buf, 20);

  lcd_loc(0,1);

  lcd_write(RdsManager.getStationName(), 8);
}




