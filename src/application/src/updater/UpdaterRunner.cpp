/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "updater/UpdaterRunner.hpp"
#include "System.hpp"
#include <alloca.h>

using namespace MusicBox;

extern "C" uint8_t _ebss;

bool UpdaterRunner::run()
{
  static constexpr uintptr_t align = (1 << 4);

  { /* load the updater from sd card into stack memory of current function */
    auto fs = sys::GetVolumeManager(*this).openFile("/updater.bin");
    auto  s = fs.getSize();
    uint8_t* buf;

    if ( s < 2)
      return false;

    buf = reinterpret_cast<uint8_t*>(alloca(s + align));

    /* adjust alignment */
    while( reinterpret_cast<uintptr_t>(buf) % (align))
      buf++;

    /* out of memory */
    if (buf < &_ebss)
      return false;

    fs.read(buf, s);

    Img = reinterpret_cast<UpdaterImg*>(buf);
  }

  if(!Img->relocate())
    return false;

  {
    UpdaterParameter prm;

    if(!prm.load())
      return false;

    Img->main(prm);
  }

  return true;
}