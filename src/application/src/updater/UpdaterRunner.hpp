#ifndef  UPDATER_HPP_
#define  UPDATER_HPP_

#include "Updater.hpp"

namespace MusicBox
{
  class UpdaterRunner
  {
  public:
    bool run();
  private:
    UpdaterImg *Img = nullptr;
  };
}


#endif