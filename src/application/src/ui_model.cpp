/*
 * ui_model.cpp
 *
 *  Created on: 01.04.2018
 *      Author: andi
 */
#include "globals.hpp"



class ChannelMenuItem : public DynamicMenuItem
{
private:
  const Database::ConstIteratorType & m_It;
  char  m_Title[18];
public:
  ChannelMenuItem(const Database::ConstIteratorType &it) : m_It(it)
  {
    int i;
    strncpy(m_Title, reinterpret_cast<const char*>(m_It->Name), 16);

    if(m_It->isDabService())
      m_Title[16] = 'D';
    else
      m_Title[16] = ' ';

    if(m_It->isFMService())
      m_Title[17] = 'F';
    else
      m_Title[17] = ' ';

    for(i=0; i < sizeof(m_Title); ++i)
      if(m_Title[i] == '\0')
        m_Title[i] = ' ';
  }

  constexpr const char * getTitle(int pos)  const { return m_Title;}
  constexpr ActionFn     getAction(int pos) const { return &UserInterface::selectChannel;}

  constexpr unsigned int getHandle() const {return m_It.getHandle();}
};

const SubMenuItem    UserInterface::s_MainMenu        = {"Main menu",    &UserInterface::buildMainMenu,        &UserInterface::scrollMainMenu,        };
const SubMenuItem    UserInterface::s_PresetMenu      = {"Presets",      &UserInterface::buildPresetMenu,      &UserInterface::scrollPresetMenu,      };
const SubMenuItem    UserInterface::s_ChannelsMenu    = {"Channels",     &UserInterface::buildChannelsMenu,    &UserInterface::scrollChannelsMenu,    };
const SubMenuItem    UserInterface::s_FMMenu          = {"FM Radio",     &UserInterface::buildFMMenu,          &UserInterface::scrollFMMenu,          };
const SubMenuItem    UserInterface::s_FMChannelsMenu  = {"FM Channels",  &UserInterface::buildFMChannelsMenu,  &UserInterface::scrollFMChannelsMenu,  };
const WidgetMenuItem UserInterface::s_FMTune          = {"FM Tune",      &UserInterface::enterFMTune, };
const SubMenuItem    UserInterface::s_DABMenu         = {"DAB Radio",    &UserInterface::buildDABMenu,         &UserInterface::scrollDABMenu,         };
const SubMenuItem    UserInterface::s_DABChannelsMenu = {"DAB Channels", &UserInterface::buildDABChannelsMenu, &UserInterface::scrollDABChannelsMenu, };
const SubMenuItem    UserInterface::s_SettingsMenu    = {"Settings",     &UserInterface::buildSettingsMenu,    &UserInterface::scrollSettingsMenu,    };

void UserInterface::buildMainMenu()
{
  MenuState state(Globals::getInstance().Ui, s_MainMenu);

  state.putItem(s_PresetMenu);
  state.putItem(s_ChannelsMenu);
  state.putItem(s_FMMenu);
  state.putItem(s_DABMenu);
  state.putItem(s_SettingsMenu);
}

void UserInterface::scrollMainMenu(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollSaturated(delta);
}

void UserInterface::buildPresetMenu()
{
  MenuState state(Globals::getInstance().Ui, s_PresetMenu);

  auto & g = Globals::getInstance();
  auto it = g.Db.beginService();

  while(it != g.Db.endService())
  {
    if(it->bPreset)
      state.putItem(ChannelMenuItem(it));
    ++it;
  }

  state.putItem(s_MainMenu);
}

void UserInterface::scrollPresetMenu(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollSaturated(delta);
}


void UserInterface::buildChannelsMenu()
{
  MenuState state(Globals::getInstance().Ui, s_ChannelsMenu);

  auto & g = Globals::getInstance();
  auto it = g.Db.beginService();

  while(it != g.Db.endService())
  {
    if( (it->isDabService() ||
        (it->isFMService())))
      state.putItem(ChannelMenuItem(it));
    ++it;
  }
  state.putItem(s_MainMenu);
}

void UserInterface::scrollChannelsMenu(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollSaturated(delta);
}

void UserInterface::buildFMMenu()
{
  MenuState state(Globals::getInstance().Ui, s_FMMenu);

  state.putItem(s_FMChannelsMenu);
  state.putItem(s_FMTune);
  state.putItem(s_MainMenu);
}

void UserInterface::scrollFMMenu(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollSaturated(delta);
}

void UserInterface::buildFMChannelsMenu()
{
  MenuState state(Globals::getInstance().Ui, s_FMChannelsMenu);

  auto & g = Globals::getInstance();
  auto it = g.Db.beginService();

  while(it != g.Db.endService())
  {
    if (it->isFMService())
      state.putItem(ChannelMenuItem(it));
    ++it;
  }
  state.putItem(s_FMMenu);
}

void UserInterface::scrollFMChannelsMenu(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollSaturated(delta);
}

void UserInterface::enterFMTune()
{
  auto & mgr = Globals::getInstance().SiHandler;

  mgr.activateMode(Si4688DeviceManager::OpModeType::OPMODE_FM);

  WidgetEnter enter(Globals::getInstance().Ui, &UserInterface::buildFMTune,
                                               &UserInterface::scrollFMTune,
                                               &UserInterface::actionFMTune );
}

void UserInterface::buildFMTune()
{
  WidgetState state(Globals::getInstance().Ui, s_FMTune );
  auto & mgr = Globals::getInstance().SiHandler;
  auto & fm  = mgr.getStatusManager();
  auto & rds  = mgr.getRDSManager();
  unsigned int freq;
  char buf[15];

  freq = fm.getTunedFrequency();

  snprintf(buf, sizeof(buf), "%3d.%02d MHz", freq/1000, freq%1000/10);
  state.putStringCentered(1, buf);

  if (fm.TunerStateType::STATE_TUNED == fm.getTunerState())
  {
    const auto & rds_state = rds.getState();

    if(rds_state.ValidFlags & rds_state.RDS_PS_NAME_VALID)
      state.putStringCentered(2, rds_state.ProgramServiceName);
    else
      state.putStringCentered(2, "<Unkown>");
  }
  else
  {
    state.putStringCentered(2, "<No Channel>");
  }
}


void UserInterface::scrollFMTune(int delta)
{
  auto & mgr = Globals::getInstance().SiHandler;

  if(delta > 0)
    mgr.seek(true);
  else
    mgr.seek(false);
}

void UserInterface::actionFMTune()
{
  auto & ui = Globals::getInstance().Ui;
  ui.show(s_FMMenu);
}

void UserInterface::buildDABMenu()
{
  MenuState state(Globals::getInstance().Ui, s_DABMenu);

  state.putItem(s_DABChannelsMenu);
  state.putItem(s_MainMenu);
}



void UserInterface::scrollDABMenu(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollSaturated(delta);
}

void UserInterface::buildDABChannelsMenu()
{
  MenuState state(Globals::getInstance().Ui, s_DABChannelsMenu);

  auto & g = Globals::getInstance();
  auto it = g.Db.beginService();

  while(it != g.Db.endService())
  {
    if (it->isDabService())
      state.putItem(ChannelMenuItem(it)); /* regular channel */
    else if ((it->ServiceId == 0) && (it->DabChannel != 0))
      state.putItem(ChannelMenuItem(it)); /* empty transponder */
    ++it;
  }
  state.putItem(s_DABMenu);
}

void UserInterface::scrollDABChannelsMenu(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollSaturated(delta);
}

void UserInterface::buildSettingsMenu()
{
  MenuState state(Globals::getInstance().Ui, s_SettingsMenu);
  state.putItem(s_MainMenu);
}

void UserInterface::scrollSettingsMenu(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollSaturated(delta);
}

void UserInterface::selectChannel()
{
  auto & si = Globals::getInstance().SiHandler;
  auto & ui = Globals::getInstance().Ui;

  si.tuneChannel(ui.getSelectedEntryHandle());
}

void UserInterface::buildIdleScreen()
{
  Globals::getInstance().Ui.buildIdleScreen();

}

void UserInterface::scrollIdleScreen(int delta)
{
  auto & ui = Globals::getInstance().Ui;
  ui.scrollIdleMenu(delta);
}
