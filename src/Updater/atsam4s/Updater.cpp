#include <cstring>
#include "Updater.hpp"
#include "Hsmci/HsmciHal.hpp"
#include "ecpp/Drivers/Atmel/ATSAM4S/EEFC.hpp"

#include "sam4s.h"
#include "sd_mmc_protocol.h"

using namespace std;
using namespace MusicBox;

using ::ecpp::Drivers::Atmel::ATSAM4S::EEFC;

bool
readBlock(const UpdaterParameter &prm, uint32_t sector, void* buf)
{
  HsmciHal hal;
  HsmciState state;

  /* select card */
  hal.startCommand(SDMMC_CMD7_SELECT_CARD_CMD, prm.RCA);

  while(HsmciState::CommandPending == (state = hal.getHsmciState()));

  if (HsmciState::CommandFailed != state)
  {
    /* send read command  */
    if (prm.HighCapacity)
      hal.startDMACommand(SDMMC_CMD17_READ_SINGLE_BLOCK, sector, buf, 1);
    else
      hal.startDMACommand(SDMMC_CMD17_READ_SINGLE_BLOCK, sector * 512, buf, 1);

    do {
      state = hal.getHsmciState();
    } while((HsmciState::CommandPending == state) || (HsmciState::TransferPending == state));
  }

  /* deselect card, dont expect response */
  hal.startCommand(7 | SDMMC_CMD_NO_RESP, 0);

  while(HsmciState::CommandPending == hal.getHsmciState());

  return HsmciState::Success == state;
}


bool
loadPage(const UpdaterParameter &prm, unsigned int flash_page, uint32_t (&buf)[128])
{
  unsigned int cluster_idx, cluster_sector, sd_sector;

  /* get cluster index and sector within cluster */
  cluster_idx     = flash_page / prm.SectorsPerCluster;
  cluster_sector  = flash_page % prm.SectorsPerCluster;

  if(prm.Clusters[cluster_idx] < 2)
    return false;

  /* get sector to load from disk */
  sd_sector = prm.BaseSector + (prm.Clusters[cluster_idx] - 2) * prm.SectorsPerCluster + cluster_sector;

  return readBlock(prm, sd_sector, buf);
}


bool
verifyPage(const UpdaterParameter &prm, unsigned int flash_page, uint32_t (&buf)[128])
{
  const uint32_t *page_ptr =reinterpret_cast<uint32_t*>(0x00400000 + flash_page * 512);

  if(!loadPage(prm, flash_page, buf))
    return false;

  return 0 == memcmp(page_ptr,buf, 512);
}

void
doUpdate(const UpdaterParameter &prm)
{
  auto & eefc = EEFC::getInstance();
  unsigned int flash_page;
  uint32_t buf[128];

  __disable_irq();
  for(flash_page = 0; flash_page < prm.NumSectors;)
  {
    unsigned int i, imax;


    /* number of sectors/pages to write */
    imax = (eefc.SectorSize / 512);

    if ( (prm.NumSectors - flash_page) < imax)
      imax = (prm.NumSectors - flash_page);

    /* check if write needed */
    for(i = 0; i < imax; ++i)
    {
      if (!verifyPage(prm, flash_page + i, buf))
        break;
    }

    if (i != imax)
    { /* write needed */

      /* erase sector (8 pages = 4096 byte) */
      eefc.eraseSectors(8, reinterpret_cast<uint32_t*>(0x00400000 + 512 * flash_page));

      /* write up to 8 pages */
      for(i = 0; i < imax; ++i)
      {
        if(!loadPage(prm, flash_page + i, buf))
          break;

        eefc.writePages(1, reinterpret_cast<uint32_t*>(0x00400000 + 512 * (flash_page + i)), buf);
      }
    }

    flash_page += i;
  }

  /* after flashing perofrm software reset */
  RSTC->RSTC_CR = RSTC_CR_KEY_PASSWD |
                  RSTC_CR_PROCRST |
                  RSTC_CR_PERRST |
                  RSTC_CR_EXTRST ;
}

struct UpdaterImg UpdaterHeader __attribute__((section(".updater_header"),visibility("default"))) =
{
  doUpdate
};


