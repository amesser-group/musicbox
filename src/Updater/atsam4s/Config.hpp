#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#include "ecpp/Peripherals/Display/NHD/NHD_0420DZW.hpp"

namespace Target::Config
{
  using DisplayDriver = ecpp::Peripherals::Display::NHD::NHD0420DZW_4Bit;
};

#endif