/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the MusicBox project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "Updater.hpp"
#include "System.hpp"

using namespace MusicBox;

bool
UpdaterParameter::load()
{
  auto & v = sys::GetVolumeManager(*this);
  auto f = v.openFile("/firmware.bin");

  { /* load some card params */
    auto & cs =  sys::globals().sdcard_driver_.getCardState();

    HighCapacity = cs.HighCapacity;
    RCA          = cs.RCA;
  }

  /* For simplification, support only sector size 512 */
  if (v.getSectorSize() != 512)
    return false;

  if(f.getSize() >0)
  {
    decltype(f)::FileSizeType offset;
    unsigned int i;

    NumSectors        = (f.getSize() + v.getSectorSize() - 1) / v.getSectorSize();
    SectorsPerCluster = v.getSectorsPerCluster();
    BaseSector        = v.getBaseSector();

    for(i=0, offset = 0; i < MaxClusters; ++i)
    {
      if ( (i * SectorsPerCluster) >= NumSectors)
        break;

      Clusters[i] = f.seek(offset);

      if(0 == Clusters[i])
        return false;

      offset += SectorsPerCluster * v.getSectorSize();
    }

    if(offset < f.getSize())
      return false;

    return true;
  }
  else
  {
    return false;
  }
}