/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "System.hpp"
#include "esp_sleep.h"
#include "ecpp/Drivers/ESP32/PowerManager.hpp"

#include "soc/pcnt_periph.h"
#include "hal/pcnt_types.h"

using namespace sys;

using Max98089Defs = MusicBox::RegDef::Max98089;
using Si468xDefs   = MusicBox::RegDef::Si4688;
using ecpp::Units::Microsecond;

#ifdef CONFIG_IDF_TARGET
#include "esp_log.h"
static const char* TAG = "Bsp";
#else
#define ESP_LOGE(...)
#define ESP_LOGI(...)
#endif

Bsp Bsp::instance_;

/* Reference Clock & Audio interface setup */
static const uint8_t max98089_clockconfig[] =
{
  Max98089Defs::REG_MASTER_CLOCK,
  0x10, /* PCLK = MCLK = 12.288 MHz */

  /* Start Config for Si4688 IF */
  /*DAI1 Clock Control */
  0x80,                                  /* 48 kHz Samplerate for ALC */
  (0x6000 >> 8) & 0x7F, 0x6000  & 0xFE,  /* Setup for 48 khz samplerate from 12.288 MHz PCLK and DHF=0 */ 
  /*DAI1 Configuration */
  0x90, /* I2S: Master, Second rising edge */
  0x01, /* I2S: BCLK = 64*LRCLK = 3.072 MHz-> suitable for 24bit output of si468x */// todo
  0x81, /* DAI1: Use Port S2, Playback enable */
  0x00,
  0x80, /* Set mode 1 for music mode */

#if 0
  /* Start Config for Microcontroller IF */
  /* DAI2 Clock Control */
  0x70,                                 /* 44.1 kHz Samplerate for ALC */
  (0x5833 >> 8) & 0x7F, 0x5833  & 0xFE,  /* Setup for 44.1 khz samplerate from 12.288 MHz PCLK and DHF=0 */
  /*DAI2 Configuration */
  0x10, /* I2S: Slave, Second rising edge */
  0x02, /* I2S: BCLK, not relevant in slave mode */
  0x41, /* DAI2: Use Port S1, Playback enable */
  0x00,
  0x00,
#endif
};


/* Mixers configuration */
static const uint8_t max98089_mixer[]  =
{
  Max98089Defs::REG_DAC_MIXER, 
#if 0
  0xA5, /* Connect DAC with DAI1 & DAI2*/
#else
  0x84, /* Connect DAC with DAI1*/
#endif
  0x00, /* Left ADC  unconnected */
  0x00, /* Right ADC unconnected */
  0x01, /* Connect Headphone Left  with Left  DAC */
  0x01, /* Connect Headphone Right with Right DAC */
  0x00, /* Bypass Headphone Output Mixer, Connect with DAC directly, Gain = 0db */
  0x00, /* Disconnect Left Lineout */
  0x00, /* Diconnect Right Lineout */
  0x80, /* Stereo Linout, Gain 0dB */
  0x01, /* Reg 0x2b Connect Left Speaker to Left DAC */
  0x01, /* Reg 0x2c Connect Right Speaker to Right DAC */
  0x00, /* Speaker Gain 0dB */  
};


/* Power management */
static const uint8_t max98089_powermgmt[]  =
{
  Max98089Defs::REG_OUTPUT_ENABLE,
  0xF3, /* Enable Headphones, Speaker, DAC */
  0xF0, /* Enable Bandgap, Regulator, Common Mode & Bias */
  0x00, /* Disable low power mode */
  0x0F, /* DAI1 + DAI2 Input Dither, DAI1 + DAI2 Clock Gen Enable */
};


const Max98089RegisterSequence Bsp::kMax98089InitRegisters[] =
{
  {max98089_clockconfig}, {max98089_mixer}, {max98089_powermgmt}
};


const i2c_config_t Bsp::kI2CConfig = 
{
  .mode             =   I2C_MODE_MASTER,
  .sda_io_num       =   kGpioI2C_SDA,
  .scl_io_num       =   kGpioI2C_SCL,
  .sda_pullup_en    =   GPIO_PULLUP_DISABLE,
  .scl_pullup_en    =   GPIO_PULLUP_DISABLE,
  .master           = { .clk_speed = 100000, },
  .clk_flags        = 0,
};

const spi_bus_config_t Bsp::kSpiMasterConfig = 
{
  .mosi_io_num      = kGpioSPI_MOSI,
  .miso_io_num      = -1,
  .sclk_io_num      = kGpioSPI_SCLK,
  .quadwp_io_num    = -1,
  .quadhd_io_num    = -1,
  .max_transfer_sz  = 2048,
  .flags            = SPICOMMON_BUSFLAG_MASTER |
                      SPICOMMON_BUSFLAG_SCLK |
                      SPICOMMON_BUSFLAG_MOSI |
                      SPICOMMON_BUSFLAG_IOMUX_PINS,
  .intr_flags       = 0,
};

const spi_device_interface_config_t Bsp::Display::kSpiConfig = 
{
  .command_bits     = 0,
  .address_bits     = 0,
  .dummy_bits       = 0,
  .mode             = 3,
  .duty_cycle_pos   = 128,
  .cs_ena_pretrans  = 0,
  .cs_ena_posttrans = 0,
  .clock_speed_hz   = 200000,
  .input_delay_ns   = 0,
  .spics_io_num     = kGpioDisplay_CS,
  .flags            = 0,
  .queue_size       = 1,
  .pre_cb           = ::ecpp::Drivers::ESP32::SpiDevice::PreTransferCB,
  .post_cb          = ::ecpp::Drivers::ESP32::SpiDevice::PostTransferCB,
};

const timer_config_t Bsp::kDefaultTimerConfig =
{
  .alarm_en    = TIMER_ALARM_EN,
  .counter_en  = TIMER_PAUSE,
  .intr_type   = TIMER_INTR_LEVEL,
  .counter_dir = TIMER_COUNT_UP,
  .auto_reload = TIMER_AUTORELOAD_EN,
  .divider     = 65536,
  .clk_src     = TIMER_SRC_CLK_APB,
};


const ledc_channel_config_t Bsp::kBacklightLedChannelConfig =
{
  .gpio_num   = Bsp::kGpioBacklight,
  .speed_mode = LEDC_LOW_SPEED_MODE,
  .channel    = LEDC_CHANNEL_0,
  .intr_type  = LEDC_INTR_DISABLE,
  .timer_sel  = LEDC_TIMER_0,
  .duty       = 0x3FFF / 8,
  .hpoint     = 0,
};

const ledc_timer_config_t Bsp::kBacklightLedTimerConfig =
{
  .speed_mode      = LEDC_LOW_SPEED_MODE,
  .duty_resolution = LEDC_TIMER_14_BIT,
  .timer_num       = LEDC_TIMER_0,
  .freq_hz         = 100,
  .clk_cfg         = LEDC_USE_RTC8M_CLK,
};

const pcnt_config_t Bsp::kVolumeEncoderConfig = 
{
  .pulse_gpio_num = kGpioEncoder_A,
  .ctrl_gpio_num  = kGpioEncoder_B,
  .lctrl_mode     = PCNT_MODE_REVERSE,
  .hctrl_mode     = PCNT_MODE_KEEP,
  .pos_mode       = PCNT_COUNT_DEC,
  .neg_mode       = PCNT_COUNT_INC,
  .counter_h_lim  = 4,
  .counter_l_lim  = -4,
  .unit           = PCNT_UNIT_0,
  .channel        = PCNT_CHANNEL_0,
};

const uint16_t Bsp::kSI468xProperties[] =
{
  Si468xDefs::PROP_DIGITAL_IO_OUTPUT_SELECT,      0x0000, /* I2S Slave */
  Si468xDefs::PROP_PIN_CONFIG_ENABLE,             0x0002, /* Enable I2S, disable internal DAC */
  0xFFFF
};

void
Bsp::Initialize()
{
  peripheral_task_ = xTaskGetCurrentTaskHandle();

  twi_driver_.Initialize(0, kI2CConfig);

  spi_master_.Initialize(kSpiMasterConfig);

  gpio_set_direction(kGpioDisplay_Reset, GPIO_MODE_OUTPUT);
  gpio_set_direction(kGpioDisplay_A0,    GPIO_MODE_OUTPUT);

  gpio_isr_register(GpioIsrHandler, this, 0, nullptr);

  for( gpio_num_t g : kKeyGpios)
  {
    gpio_set_direction(g, GPIO_MODE_INPUT);
    gpio_hold_en(g);
    gpio_set_intr_type(g, GPIO_INTR_NEGEDGE);
    gpio_wakeup_enable(g, GPIO_INTR_LOW_LEVEL);
  }

  { /* configure clock timer to provide a 1 Hz Signal */
    clock_timer_.Init(kDefaultTimerConfig);

    timer_isr_callback_add(clock_timer_.kTimerGroup, clock_timer_.kTimerIdx,
                            ClockTimerIsrHandler, this, 0);

    timer_enable_intr(clock_timer_.kTimerGroup, clock_timer_.kTimerIdx);

    clock_timer_.SetMode(2, false);
    clock_timer_.SetMaxCount(::ecpp::Drivers::ESP32::PowerManager::GetABPFreq().ToHerz() / 2 - 1);
    clock_timer_.Start();  
  }

  ledc_timer_config(&kBacklightLedTimerConfig);
  ledc_channel_config(&kBacklightLedChannelConfig);

  {
    auto cfg = kVolumeEncoderConfig;
    pcnt_unit_config(&cfg);

    // Configure channel 1
    cfg.pulse_gpio_num  = kVolumeEncoderConfig.ctrl_gpio_num;
    cfg.ctrl_gpio_num   = kVolumeEncoderConfig.pulse_gpio_num;
    cfg.channel         = PCNT_CHANNEL_1;
    cfg.pos_mode        = PCNT_COUNT_INC;
    cfg.neg_mode        = PCNT_COUNT_DEC;

    pcnt_unit_config(&cfg);

    // PCNT pause and reset value
    pcnt_counter_pause(kVolumeEncoderConfig.unit);
    pcnt_counter_clear(kVolumeEncoderConfig.unit);

    // register interrupt handler
    pcnt_isr_register(PCntIsrHandler, this, 0, nullptr);
    pcnt_intr_enable(kVolumeEncoderConfig.unit);

    pcnt_event_enable(kVolumeEncoderConfig.unit, PCNT_EVT_H_LIM);
    pcnt_event_enable(kVolumeEncoderConfig.unit, PCNT_EVT_L_LIM);

    pcnt_counter_resume(kVolumeEncoderConfig.unit);
  }

  InitializePCAL6408();

  display_.Initialize(spi_master_);
}

void
Bsp::Start()
{
  Notify(UserInputEvent);
}

void
Bsp::Notify(PeripheralEvent event)
{
  xTaskNotify(peripheral_task_, 0x1 << event, eSetBits);
}

void Bsp::GpioIsrHandler(void *arg)
{
  static_cast<Bsp*>(arg)->GpioIsr();
}

void Bsp::GpioIsr()
{
  for( gpio_num_t g : kKeyGpios)
    gpio_intr_disable(g);

  keys_last_change_tmr_.Start();
  keys_bouncing_ = true;

  Notify(UserInputEvent);
}

bool Bsp::ClockTimerIsrHandler(void *arg)
{
  static_cast<Bsp*>(arg)->ClockTimerIsr();
  return false;
}

void Bsp::ClockTimerIsr()
{
  clock_.Tick();
}

void Bsp::PCntIsr()
{
  uint32_t status;

  /* clear the interrupt status */
  PCNT.int_clr.val = PCNT.int_st.val;

  pcnt_get_event_status(kVolumeEncoderConfig.unit, &status);

  if (status & PCNT_EVT_H_LIM)
    rotary_real_ += 1;
  else if (status & PCNT_EVT_L_LIM)
    rotary_real_ -= 1;

  Notify(UserInputEvent);
}


void Bsp::PCntIsrHandler(void *arg)
{
  static_cast<Bsp*>(arg)->PCntIsr();
}


void
Bsp::InitializePCAL6408()
{
  auto & trb = pcal6408_trb_;

  /* enable pull ups as required */
  trb.SetUp(kI2CAddrPCAL6408);
  trb.GetStruct().SetField8(0, 0x43);
  trb.GetStruct().SetField8(1, 0x7C);
  trb.Execute(2,0);

  /* pre-configure output pin states */
  trb.SetUp(kI2CAddrPCAL6408);
  trb.GetStruct().SetField8(0, 0x01);
  trb.GetStruct().SetField8(1, 0b00011011);
  trb.Execute(2,0);

  /* configure output drivers. This will disconnect & reset si4688 & disable the max98089 clock oscillator */
  pcal6408_oe_cur_ = 0;
  UpdatePCAL6408(kPCAL6408_PinMsk_MAX98089_CLKEN, 0);

#if 0
  /* read input register */
  buf[0] = 0;
  if (i2c_write_read(i2caddr_pcal6408, buf, 1, 1) != 1)
    return false;
    
  /* Serial.write("PCAL6408 Status: "); serial_dump_hex(buf,2); Serial.write('\n'); */

  /* Check if all lines are in expected state,
   * ignore sd-card detect pin */
  if( (buf[0] | 0x20) != 0xBC)
    return false;

  return true;    
#endif
}

void
Bsp::EnableClock(const Max98089Manager&)
{
  UpdatePCAL6408(0, kPCAL6408_PinMsk_MAX98089_CLKEN);
}

bool
Bsp::AssertReset(const Si468xDriver&)
{
  auto & g = ::sys::globals();

  if (max98089_trb_.IsActive())
    return false;

  if (!g.max98089_driver_.isShutdown())
  {
    g.max98089_driver_.setShutdown(true);
    return false;
  }

  UpdatePCAL6408(0, kPCAL6408_PinMsk_SI4688_RESET | kPCAL6408_PinMsk_SI4688_I2CEN);
  return true;
}

void
Bsp::DeassertReset(const Si468xDriver&)
{
  UpdatePCAL6408(kPCAL6408_PinMsk_SI4688_RESET, 0);
}

void
Bsp::ConnectI2C(const Si468xDriver& drv)
{
  if (IsInReset(drv))
    return;

  UpdatePCAL6408(kPCAL6408_PinMsk_SI4688_RESET | kPCAL6408_PinMsk_SI4688_I2CEN, 0);
}

void
Bsp::UpdatePCAL6408(uint_least8_t oe_set_mask, uint_least8_t oe_clear_mask)
{
  auto & trb = pcal6408_trb_;
  uint_least8_t mask;

  mask = (pcal6408_oe_cur_ & ~oe_clear_mask) | oe_set_mask;
  pcal6408_oe_cur_ = mask;

  trb.SetUp(kI2CAddrPCAL6408);
  trb.GetStruct().SetField8(0, 0x03);
  trb.GetStruct().SetField8(1, ~mask);
  trb.Execute(2,0);

  PCAL6408Changed.Start(MilliSecond<>(10));
}

ECPP_JOB_DEF(Bsp, PCAL6408Changed)
{
  //auto changes = pcal6408_oe_cur_ ^ pcal6408_oe_old_;
  pcal6408_oe_old_ = pcal6408_oe_cur_;

  /* clock of MAX98089 is enabled */
  if(IsClockEnabled(sys::globals().max98089_driver_))
    globals().max98089_driver_.handlePending();
}

void 
Bsp::Display::Initialize(::ecpp::Drivers::ESP32::SpiMaster& master)
{
  int_fast8_t page;

  ::ecpp::Drivers::ESP32::SpiDevice::Initialize(master, kSpiConfig);

  gpio_set_level(kGpioDisplay_Reset, 0);
  ets_delay_us(5);
  gpio_set_level(kGpioDisplay_Reset, 1);
  ets_delay_us(5);

  for(page = 0; page < kDisplayPages; ++page)
  {
    buffer_[page][0] = 0b10110000 | page;
    buffer_[page][1] = 0b00010000;
    buffer_[page][2] = 0b00000000;
  }

  srb_.SetUp(&(buffer_[0][kDisplayDataOffset]), true);

  buffer_[0][kDisplayDataOffset + 0] = 0b10100010; /* Bias 1/9 */
  buffer_[0][kDisplayDataOffset + 1] = 0b10100000; /* SEG Direction MX=0 */
  buffer_[0][kDisplayDataOffset + 2] = 0b11001000; /* COM Direction MY=1 (Mirrored) */
  buffer_[0][kDisplayDataOffset + 3] = 0b00100100; /* RR=5.0 */
  buffer_[0][kDisplayDataOffset + 4] = 0b10000001; /* EV=32 */
  buffer_[0][kDisplayDataOffset + 5] = 32;
  buffer_[0][kDisplayDataOffset + 6] = 0b00101111; /* Powercontro, VB, VR, VF = on */
  buffer_[0][kDisplayDataOffset + 7] = 0b10101111; /* Display on */

  srb_.Submit(*this, 8, 0);
}

void
Bsp::Poll()
{
  uint32_t event_mask;

  event_mask = ulTaskNotifyTake(pdTRUE, 0);

  ProcessEvents(event_mask);
}


void
Bsp::Poll(SystemTick wait_till)
{
  auto current_ticks = GetSystemTick();
  uint32_t event_mask;


  if(current_ticks < wait_till)
  {
    auto ticks_sleep(wait_till - current_ticks);
    
    /* esp_sleep_enable_timer_wakeup(ticks_sleep.GetRaw() * 10000);
    esp_light_sleep_start(); */
    event_mask = ulTaskNotifyTake(pdTRUE, (wait_till - current_ticks).GetRaw());
  }
  else
  {
    event_mask = ulTaskNotifyTake(pdTRUE, 0);
  }

  ProcessEvents(event_mask);
}


void
Bsp::ProcessEvents(uint32_t event_mask)
{
  for(PeripheralEvent e: {DisplayTransferDoneEvent, UserInputEvent} )
  {
    const uint32_t mask = (0x1 << e);

    if (event_mask == 0)
      break;

    if (event_mask & mask)
    {
      event_mask &= ~mask;

      switch(e)
      {
      case DisplayTransferDoneEvent: 
        display_.TransferComplete.Enqueue(); 
        break;
      case UserInputEvent:
        ::sys::globals().user_interface_.HandleUserInput(); 
        break;
      }
    }    
  }
}

::sys::SystemTick::DeltaType
Bsp::HandleUserInput(Application &app)
{
  auto keys_bouncing = keys_bouncing_;

  if(keys_bouncing)
  {
    if (keys_last_change_tmr_.elapsed() > MilliSecond<>(20))
    {
      keys_bouncing = keys_bouncing_ = false;

      for( gpio_num_t g : kKeyGpios)
        gpio_intr_enable(g);
    }
  }

  /* This could be improved a lot but lets leave it alone now as it is
   * In future we could support different volume levels in different situations
   * if we have an encoder */
  if( (static_cast<int8_t>(rotary_real_) - static_cast<int8_t>(rotary_reported_)) > 0)
  {
    rotary_reported_+= 1;

    if(sys::globals().operating_state() != MusicBox::OperatingState::kStandBy)
      sys::bsp().ChangeVolume(+1);

    app.HandleKey(events::KeyEvent::kVolumePlus);
  }
  else if( (static_cast<int8_t>(rotary_real_) - static_cast<int8_t>(rotary_reported_)) < 0)
  {
    rotary_reported_ -= 1;

    if(sys::globals().operating_state() != MusicBox::OperatingState::kStandBy)
      sys::bsp().ChangeVolume(-1);

    app.HandleKey(events::KeyEvent::kVolumeMinus);
  }

  for(auto it = kKeyGpios.begin(); it < kKeyGpios.end(); ++it)
  {
    uint_least8_t m = 0x1 << (it - kKeyGpios.begin());
    bool pressed = gpio_get_level(*it) == 0;

    if(!pressed && !keys_bouncing)
    {
      keys_state_ &= ~m;
    }
    else if(pressed && ((keys_state_ & m) == 0))
    {
      keys_state_ |= m;

      switch(*it)
      {
        case kGpioKey_A: app.HandleKey(events::KeyEvent::kPrev);    break;
        case kGpioKey_B: app.HandleKey(events::KeyEvent::kNext);    break;
        case kGpioKey_C: app.HandleKey(events::KeyEvent::kConfirm); break;
        case kGpioKey_D: app.HandleKey(events::KeyEvent::kBack);    break;
        case kGpioKey_E: app.HandleKey(events::KeyEvent::kSnooze);  break;
        default: break;
      }
    }
  }

  if ( (keys_state_ != 0) || keys_bouncing)
    return MilliSecond<>(20);
  else
    return MilliSecond<>(0);
}

void
Bsp::Display::RequestBlock::PreTransfer()
{
  gpio_set_level(kGpioDisplay_A0, rs_);
}

void
Bsp::Display::Update()
{
  transfer_stage_ = 0;
  TransferComplete.Enqueue();
}


ECPP_JOB_DEF(Bsp::Display, TransferComplete)
{
  if(!srb_.WaitFinished(*this, MilliSecond<>(0)))
    return;

  if(transfer_stage_ < (kDisplayPages * 2))
  {
    if((transfer_stage_ % 2) == 0)
    { /* locate cursor */
      srb_.SetUp(&buffer_[transfer_stage_/2][0], true);
      srb_.Submit(*this, kDisplayDataOffset, 0);
    }
    else
    { /* send data */
      srb_.SetUp(&buffer_[transfer_stage_/2][kDisplayDataOffset], false);
      srb_.Submit(*this, kDisplayColumns, 0);
    }

    transfer_stage_++;
  }
  else
  {
    transfer_stage_++;
  }
}
