/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ECPPCONFIG_FOUNDATION_HPP_
#define ECPPCONFIG_FOUNDATION_HPP_

#include "ecpp_sys/Log.hpp"

//static auto log = ;

namespace MusicBox::Database
{
  class ServiceDatabase;
}

namespace ecpp_config
{
  template<typename Class>
  class Foundation {};

  template<>
  class Foundation<::MusicBox::Database::ServiceDatabase>
  {
  protected:
    static constexpr auto log_ = ecpp_sys::GetLogger("ServiceDatabase");
  };

}

#endif