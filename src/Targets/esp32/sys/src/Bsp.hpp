/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BSP_HPP_
#define BSP_HPP_

#include <array>

#include "sys/application.hpp"
#include "Board.hpp"
#include "Platform.hpp"

#include "ecpp/DateTime/Clock.hpp"
#include "ecpp/DateTime/DateTime.hpp"

#include "ecpp/Execution/Job.hpp"
#include "ecpp/Drivers/ESP32/Spi.hpp"
#include "ecpp/Drivers/ESP32/Timer.hpp"
#include "ecpp/Drivers/ESP32/Twi.hpp"

#include "Drivers/MAX98089.hpp"
#include "Drivers/SI468x/Driver.hpp"
#include "Drivers/SI468x/Transaction.hpp"
#include "Drivers/RadioShield.hpp"

#include "Storage/PackedStorage.hpp"

#include "ecpp/Graphics/Monochrome/PagedBufferProxy.hpp"
#include "ecpp/Graphics/Painter.hpp"

#include "ecpp/SystemTick.hpp"

#include "driver/ledc.h"
#include "driver/pcnt.h"

namespace sys
{
  using ::ecpp::SystemTick;
  using ::ecpp::GetSystemTick;

  using ::MusicBox::Drivers::Max98089Manager;
  using ::MusicBox::Drivers::Max98089Configuration;
  using ::MusicBox::Drivers::Max98089RegisterSequence;

  using ::MusicBox::Drivers::SI468x::Si468xDriver;
  using ::MusicBox::Drivers::SI468x::Si468xTransaction;

  class Bsp : public Platform, protected BoardConfig
  {
  public:
    using DateTime        = ::ecpp::DateTime::DateTime;
    using Clock           = ::ecpp::DateTime::Clock<DateTime>;

    using StorageReader = ::MusicBox::Storage::PackedStorage::Reader;
    using StorageWriter = ::MusicBox::Storage::PackedStorage::Writer;

    struct UserInput;

    enum PeripheralEvent : int_fast8_t
    {
      DisplayTransferDoneEvent, 
      UserInputEvent, 
    };

    static constexpr uint32_t      kSi468x_FlashAddr_RomPatch    = 0x04000;
    static constexpr uint32_t      kSi468x_FlashAddr_FMFirmware  = 0x08000;
    static constexpr uint32_t      kSi468x_FlashAddr_DABFirmware = 0xa0000;

    template<typename Buffer>
    class TwiRequestBlock : public ::ecpp::Drivers::ESP32::TwiBufferRequestBlock<Buffer>
    {
    protected:
      using _Base = ::ecpp::Drivers::ESP32::TwiBufferRequestBlock<Buffer>;
    public:
      void Submit(uint_fast16_t write_len, uint_fast16_t read_len)
      {
        _Base::Submit( instance().twi_driver_, write_len, read_len );
      }

      auto Execute(uint_fast16_t write_len, uint_fast16_t read_len)
      {
        return _Base::Execute( instance().twi_driver_, write_len, read_len );
      }
    };

    class Si4688RequestBlock : public TwiRequestBlock<uint32_t[4 + 128]>
    {
    protected:
      void             SetUp()                    { TwiRequestBlock<uint32_t[4 + 128]>::SetUp(kI2CAddrSi468x); }
      constexpr Si468xDriver & GetDriver() const;
    private:
    };

    class Display;

    class Display : public ::ecpp::Drivers::ESP32::SpiDevice
    {
    protected:
      class RequestBlock : public ::ecpp::Drivers::ESP32::SpiRequestBlock
      {
      protected:
        uint8_t rs_ {0};

        virtual void PreTransfer() override;

        void SetUp(uint8_t* dma_buffer, bool command) 
        {
          ::ecpp::Drivers::ESP32::SpiRequestBlock::SetUp(dma_buffer);
          rs_ = command ? 0 : 1;
        }

        constexpr RequestBlock() : ::ecpp::Drivers::ESP32::SpiRequestBlock() {}

        friend class Bsp::Display;
      };

      static const      spi_device_interface_config_t kSpiConfig;
      static constexpr  int_fast8_t                   kDisplayPages      { 8 };
      static constexpr  int_fast8_t                   kDisplayColumns    { 128 };
      static constexpr  int_fast8_t                   kDisplayRows       { kDisplayPages * 8 };
      static constexpr  int_fast8_t                   kDisplayDataOffset { 3 };


      uint8_t       buffer_[kDisplayPages][kDisplayDataOffset + kDisplayColumns];
      RequestBlock  srb_            {};
      uint_least8_t transfer_stage_ {0};

    public:
      using BufferProxy = ::ecpp::Graphics::Monochrome::PagedBufferProxy<sizeof(buffer_[0])>;
      using Painter = ::ecpp::Graphics::Painter<Display::BufferProxy>;

      void Initialize(::ecpp::Drivers::ESP32::SpiMaster& master);
      void Update();

      constexpr Painter CreatePainter()
      { 
        return Painter(BufferProxy(&buffer_[0][kDisplayDataOffset], 0, 0, kDisplayColumns, kDisplayRows)); 
      }

      constexpr bool IsUpdatePending()
      {
        return (transfer_stage_ <= (kDisplayPages * 2));
      }

      ECPP_JOB_DECL(TransferComplete);
    };

    
    static constexpr Bsp & instance() { return instance_; }

    constexpr Max98089Configuration GetConfiguration(const Max98089Manager&) { return kMax98089InitRegisters; }

    TwiRequestBlock<uint32_t[1]>                   pcal6408_trb_;
    TwiRequestBlock<uint32_t[4]>                   max98089_trb_;
    TwiRequestBlock<uint32_t[32]>                  cw1280_trb_;

    Si468xTransaction<Si4688RequestBlock >         si468x_transaction_;

    ::ecpp::Drivers::ESP32::TwiMaster              twi_driver_;

    Display                                        display_;
    MusicBox::Drivers::RadioShield::EepromStorage  storage_provider_;

    TaskHandle_t                                   peripheral_task_;

    void             Initialize();
    void             Start();

    void             Notify(PeripheralEvent event);

    void             Poll();
    void             Poll(SystemTick wait_till);

    void             EnableClock(const Max98089Manager&);
    constexpr bool   IsClockEnabled(const Max98089Manager&) const { return ( (pcal6408_oe_cur_ | pcal6408_oe_old_) & kPCAL6408_PinMsk_MAX98089_CLKEN) == 0; }

    bool             AssertReset(const Si468xDriver&);
    void             DeassertReset(const Si468xDriver&);
    void             ConnectI2C(const Si468xDriver&);
    constexpr bool   IsInReset(const Si468xDriver&) const   { return ((pcal6408_oe_cur_ | pcal6408_oe_old_) & kPCAL6408_PinMsk_SI4688_RESET) == 0; }

    constexpr const uint16_t* GetConfiguration(const Si468xDriver&) {return kSI468xProperties; }

    ::sys::SystemTick::DeltaType  HandleUserInput(Application & app);

  protected:
    static Bsp instance_;

    static const i2c_config_t                  kI2CConfig;
    static const spi_bus_config_t              kSpiMasterConfig;
    static const timer_config_t                kDefaultTimerConfig;

    static const ledc_channel_config_t         kBacklightLedChannelConfig;
    static const ledc_timer_config_t           kBacklightLedTimerConfig;

    static const pcnt_config_t                 kVolumeEncoderConfig;

    static constexpr uint_least8_t kI2CAddrSi468x     { 0x64 };
    static constexpr uint_least8_t kI2CAddrPCAL6408   { 0x20 };
    
    static constexpr uint_least8_t kPCAL6408_PinMsk_SI4688_I2CEN   { 0x01 };
    static constexpr uint_least8_t kPCAL6408_PinMsk_SI4688_RESET   { 0x02 };
    static constexpr uint_least8_t kPCAL6408_PinMsk_MAX98089_CLKEN { 0x40 };

    static const          Max98089RegisterSequence kMax98089InitRegisters[3];
    static const uint16_t kSI468xProperties[];

    static constexpr ::std::array   kKeyGpios {kGpioKey_A, kGpioKey_B, kGpioKey_C, kGpioKey_D, kGpioKey_E };

    ::ecpp::SystemTickTimer         keys_last_change_tmr_ {};
    volatile uint_least8_t          keys_bouncing_     {true};
    uint_least8_t                   keys_state_        {0};

    uint_least8_t                  pcal6408_oe_cur_   { 0 };
    uint_least8_t                  pcal6408_oe_old_   { 0 };

    uint_least8_t                  rotary_real_     {0};
    uint_least8_t                  rotary_reported_ {0};

    Clock                          clock_;

    ::ecpp::Drivers::ESP32::Timer<TIMER_GROUP_0, TIMER_0> clock_timer_;

    ECPP_JOB_TIMER_DECL(PCAL6408Changed);

    void InitializePCAL6408();
    void UpdatePCAL6408(uint_least8_t oe_set_mask, uint_least8_t oe_clear_mask);

    void ProcessEvents(uint32_t event_mask);

    void        GpioIsr();
    void        ClockTimerIsr();
    void        PCntIsr();

    static void GpioIsrHandler(void*);
    static bool ClockTimerIsrHandler(void*);
    static void PCntIsrHandler(void*);
  };

  static constexpr auto & bsp(void) { return Bsp::instance(); }

  static constexpr auto & GetTwiRequestBlock(const Max98089Driver &driver) { return bsp().max98089_trb_; }
  static constexpr auto & GetTwiRequestBlock(const MusicBox::Drivers::RadioShield::EepromStorage& str) { return bsp().cw1280_trb_; }

  static inline const Max98089Configuration GetConfiguration(const Max98089Manager &mgr) { return bsp().GetConfiguration(mgr); }

}
#endif