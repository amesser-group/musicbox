/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_TARGET_ESP32_BOARD_HPP_
#define MUSICBOX_TARGET_ESP32_BOARD_HPP_

#include "ecpp/Drivers/ESP32/Spi.hpp"
#include "driver/gpio.h"

namespace sys
{
  class BoardConfig
  {
  protected:
#if CONFIG_IDF_TARGET_ESP32S2
    /* configuration for bastelino esp32-s2 board */
    static constexpr gpio_num_t  kGpioI2C_SDA         { GPIO_NUM_17 };
    static constexpr gpio_num_t  kGpioI2C_SCL         { GPIO_NUM_18 };

    static constexpr gpio_num_t  kGpioSPI_MOSI        { GPIO_NUM_11 };
    static constexpr gpio_num_t  kGpioSPI_SCLK        { GPIO_NUM_12 };

    static constexpr gpio_num_t  kGpioDisplay_CS      { GPIO_NUM_10 };
    static constexpr gpio_num_t  kGpioDisplay_Reset   { GPIO_NUM_14 };
    static constexpr gpio_num_t  kGpioDisplay_A0      { GPIO_NUM_9 };

    static constexpr gpio_num_t  kGpioKey_A           { GPIO_NUM_4  };
    static constexpr gpio_num_t  kGpioKey_B           { GPIO_NUM_6  };
    static constexpr gpio_num_t  kGpioKey_C           { GPIO_NUM_5 };
    static constexpr gpio_num_t  kGpioKey_D           { GPIO_NUM_3 };
    static constexpr gpio_num_t  kGpioKey_E           { GPIO_NUM_13 };

    static constexpr gpio_num_t  kGpioEncoder_A       { GPIO_NUM_2 };
    static constexpr gpio_num_t  kGpioEncoder_B       { GPIO_NUM_1 };

    static constexpr gpio_num_t  kGpioBacklight       { GPIO_NUM_7 };

    ::ecpp::Drivers::ESP32::SpiMaster     spi_master_ { SPI2_HOST };

#else
# error
    /* breadboard */
    static constexpr gpio_num_t  kGpioI2C_SDA         { GPIO_NUM_21 };
    static constexpr gpio_num_t  kGpioI2C_SCL         { GPIO_NUM_22 };

    static constexpr gpio_num_t  kGpioSPI_MOSI        { GPIO_NUM_23 };
    static constexpr gpio_num_t  kGpioSPI_SCLK        { GPIO_NUM_18 };

    static constexpr gpio_num_t  kGpioDisplay_CS      { GPIO_NUM_5 };
    static constexpr gpio_num_t  kGpioDisplay_Reset   { GPIO_NUM_16 };
    static constexpr gpio_num_t  kGpioDisplay_A0      { GPIO_NUM_19 };

    static constexpr gpio_num_t  kGpioKey_A           { GPIO_NUM_2  };
    static constexpr gpio_num_t  kGpioKey_B           { GPIO_NUM_4  };
    static constexpr gpio_num_t  kGpioKey_C           { GPIO_NUM_27 };
    static constexpr gpio_num_t  kGpioKey_D           { GPIO_NUM_32 };
    static constexpr gpio_num_t  kGpioKey_E           { GPIO_NUM_33 };

    ::ecpp::Drivers::ESP32::SpiMaster     spi_master_ { SPI3_HOST };
#endif    
  };
}
#endif
