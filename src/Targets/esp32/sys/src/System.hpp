/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SYSTEM_HPP_
#define SYSTEM_HPP_

#include "MusicBox.hpp"
#include "sys/application.hpp"
#include "SnoozeController.hpp"

#include "ecpp/Units/Time.hpp"
#include "Platform.hpp"
#include "Bsp.hpp"

namespace sys
{
  using   ::ecpp::Units::MilliSecond;

  class Globals : public MusicBox::MusicBoxApp, public Application
  {
  public:
    static constexpr Globals & instance() {return instance_;}

    JobQueue                               job_queue_;
    TimerQueue                             timer_queue_;

    MusicBox::SnoozeController             snoozecontroller_;
  protected:
    static Globals instance_;

    static constexpr Globals & GetGlobals(Application &app) {return static_cast<Globals &>(app);}
    
    friend class Application;
  };

  void Initialize();

  void Start();

  static constexpr auto & globals(void) { return Globals::instance(); }

  constexpr TimerQueue & GetTimerQueue(const Timer&) { return globals().timer_queue_; }


  void Enqueue(::ecpp::Execution::Job& job, ::ecpp::Execution::Job::Handler handler);

  template<typename OBJ>
  static inline void Enqueue(const OBJ& obj, ::ecpp::Execution::Job& job, ::ecpp::Execution::Job::Handler handler)
  {
    globals().job_queue_.Enqueue(job, handler);
  }

  template<typename Object>
  auto & GetSi468xDriver(const Object &)                              { return globals().si468x_driver_; }

  template<typename Object>
  auto & GetStorage(const Object &)                                   { return globals().storage_manager_; }

  constexpr Si468xDriver & Bsp::Si4688RequestBlock::GetDriver() const { return globals().si468x_driver_; }

  constexpr auto & GetSi468xTransaction(const Si468xDriver&)          { return bsp().si468x_transaction_; }

  constexpr auto & GetSi468xActions(const Si468xDriver&)              { return globals().si468x_actions_; }
  
  template<typename OBJ>
  constexpr auto & GetApp(const OBJ&) { return globals(); }
}
#endif