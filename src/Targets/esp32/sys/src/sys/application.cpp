/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "sys/application.hpp"
#include "System.hpp"

using namespace sys;

void
Application::HandleKey(events::KeyEvent key)
{
  auto & g = Globals::GetGlobals(*this);
  g.user_interface_.HandleKey(key);
}

void
Application::HandleKey(events::KeyEvent::Back key)
{
  auto & g = Globals::GetGlobals(*this);
  g.user_interface_.HandleKey(key);
}

void
Application::HandleKey(events::KeyEvent::Snooze key)
{
  auto & g = Globals::GetGlobals(*this);
  g.snoozecontroller_.HandleSnoozeKeyPress();
}

void 
Application::Wake()
{
  auto & g = Globals::GetGlobals(*this);

  g.PlayLastStation(MusicBox::Seconds(60*60));
  g.snoozecontroller_.HandleAlarm();
}
