/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/UserInterface.hpp"

namespace MusicBox::UserInterface::Model
{
  static const FilterableActionListItem kMainMenuItems[] =
  {
    {"Radio",        [] (const WidgetEnvironment::Context &ctx) { ctx.SpawnRadioMenu(); } },
#if 0
    {"SD Karte",     [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<SDCardInfoWidget>(); }
                     [] () { (::sys::globals().sdcard_driver_.getCardState().CardState != SDCardState::Ready) } },
    {"Dateien",      [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<FileSystemWidget>(); }
                     [] () { (::sys::globals().sdcard_driver_.getCardState().CardState != SDCardState::Ready) } },
#endif                    
    {"Wecker"       , [] (const WidgetEnvironment::Context &ctx) { ctx.SpawnAlarmClockMenu(); } },
    {"Einstellungen", [] (const WidgetEnvironment::Context &ctx) { ctx.SpawnSettingsMenu(); } },
    {"Abschalten",    UserInterface::EnterStandbyAction }
  };

  static const ActionListItem kRadioSettingsMenuItems[] =
  {
    {"DAB Kanalsuche", [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<DABChannelScanWidget>(); } },
    {"Zurück",          WidgetEnvironment::Context::Back },
  };

#if 0
  static const ActionListItem kMaintenanceSettingsMenuItems[] =
  {
    {"AntCap Messung",    [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<Si468xAutoAntCapTuningWidget>(); } },
    {"AndCap Einstellen", [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<Si468xManualAntCapTuningWidget>(); } },
//    {"Firmware Update",   [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<Si468xSelectFirmwareWidget>(); } },
  };
#endif
  static const ActionListItem kSettingsMenuItems[] =
  {
    {"Radio",    [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<ActionListWidget>("Radio Einstellungen", kRadioSettingsMenuItems); } },
    // {"Wartung",  [] (const WidgetEnvironment::Context &ctx) { ctx.PushWidget<ActionListWidget>("Wartung", kMaintenanceSettingsMenuItems); } } ,
    {"Zurück",          WidgetEnvironment::Context::Back },
  };

  void
  ContextTargetMixin::SpawnMainMenu() const
  {
    auto & ctx = static_cast<const WidgetEnvironment::Context&>(*this);
    ctx.PushWidget<FilterableActionListWidget>("Hauptmenü", kMainMenuItems);
  }

  void
  ContextTargetMixin::SpawnSettingsMenu() const
  {
    auto & ctx = static_cast<const WidgetEnvironment::Context&>(*this);
    ctx.PushWidget<ActionListWidget>("Einstellungen", kSettingsMenuItems);
  }
}