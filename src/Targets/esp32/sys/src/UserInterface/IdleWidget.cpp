/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Widgets.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;

using ::ecpp::ArrayString;
using ::MusicBox::Drivers::SI468x::Si468xOperationMode;

void
IdleWidget::Draw(WidgetEnvironment::Painter &painter) const
{  
  const auto & mgr = ::sys::globals().si468x_driver_;
  // const auto & fm = mgr.getStatusManager();
  const auto &  s = mgr.GetCurrentService();
  const auto mode = mgr.getMode();
  auto user_idle = ::sys::globals().user_interface_.IsUserIdle();

  auto row0 = painter.CreateField({ 0,0,  painter.GetWidth(), 16});
  auto row1 = painter.CreateField({ 0,16, painter.GetWidth(), 32});
  auto row2 = painter.CreateField({ 0,32, painter.GetWidth(), 48});
  auto row3 = painter.CreateField({ 0,48, painter.GetWidth(), 64});

  using Font = WidgetEnvironment::Font<decltype(*this)>;

  if(mode == Si468xOperationMode::FMRadio)
  {
    const auto & rds = mgr.getRDSState();
    const auto freq  = mgr.getRadioManager().getTunedFreq();

    ArrayString<ecpp::StringEncodings::Utf8, 16> rds_name = rds.program_service_name;

    row0.IPrintF<Font>(u8"FM %3u.%02u MHz %2d dBµV", static_cast<unsigned>(freq/1000), static_cast<unsigned>(freq%1000/10),
                          static_cast<int>(mgr.getRadioManager().getRssi()));

    if ( s.Name.CountCharacters() > 0)
    {
      row1.CenterText<Font>(s.Name);

      if (rds.isProgramServiceNameValid())
      {
        if (s.Name != rds_name.Trim())
          row2.CenterText<Font>(rds_name);
      }
    }
    else if (rds.isProgramServiceNameValid())
    {
      row1.CenterText<Font>(rds_name);
    }
    else
    {
      row1.CenterText<Font>("<Unbekannt>");
    }
  }
  else if(mode == Si468xOperationMode::DABRadio)
  {
    const auto freq  = mgr.getRadioManager().getTunedFreq();

    row0.IPrintF<Font>("DAB     %2d dBµV", static_cast<int>(mgr.getRadioManager().getRssi()));

    if (s.Name.CountCharacters() > 0)
      row1.CenterText<Font>(s.Name);
    else
      row1.CenterText<Font>("<Unbekannt>");

    row2.IPrintF<Font>("%3d.%03d MHz", freq/1000, freq%1000);
  }
  else
  {
    row1.CenterText<Font>("<Kein Kanal>");
  }

  if (user_idle)
  {
    ArrayString<ecpp::StringEncodings::Utf8, 3> buffer;
    auto t = ::ecpp_sys::Chrono::LocalClock::now().time();
    constexpr uint_least8_t clock_center = 64;

    buffer = ":";
    auto w = row3.GetStringRect<Font>(buffer).GetWidth() + 1;
    row3.CreateField({clock_center - w/2, 0 , clock_center - w/2 + w, row3.GetHeight()}).PutText<Font>(buffer);

    buffer.IPrintF("%02d", t.hours());
    auto wh = row3.GetStringRect<Font>(buffer).GetWidth();
    row3.CreateField({clock_center - wh - w/2, 0 , clock_center - w/2, row3.GetHeight()}).PutText<Font>(buffer);

    buffer.IPrintF("%02d", t.minutes());
    auto wm = row3.GetStringRect<Font>(buffer).GetWidth();
    row3.CreateField({clock_center - w/2 + w, 0 , clock_center - w/2 + w + wm, row3.GetHeight()}).PutText<Font>(buffer);
  }
  else
  {
    row3.Draw(*quick_actions_.GetSelectedItem(), true);
  }
}
