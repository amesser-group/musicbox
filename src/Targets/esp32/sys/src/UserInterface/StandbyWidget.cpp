/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Widgets.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;

using ::ecpp::ArrayString;
using ::MusicBox::Drivers::SI468x::Si468xOperationMode;

void
StandByWidget::Draw(WidgetEnvironment::Painter &painter) const
{  
  auto top    = painter.CreateField({ 0,0,  painter.GetWidth(), 16});
  auto middle = painter.CreateField({ 0,16, painter.GetWidth(), 48});
  auto bottom = painter.CreateField({ 0,48, painter.GetWidth(), 64});

  using FontSmall = Fonts::FontSmallFonts::BitstreamVeraSans14px;
  using FontLarge = Fonts::FontLargeFonts::BitstreamVeraSans28px;

  if (sys::globals().user_interface_.IsUserIdle())
  {
    ArrayString<ecpp::StringEncodings::Utf8, 14> buffer;
    auto t = ::ecpp_sys::Chrono::LocalClock::now().time();

    const uint_least8_t clock_center = painter.GetWidth() / 2;

    buffer.IPrintF("%02d:%02d", t.hours(), t.minutes());

    auto offset = middle.GetStringRect<FontLarge>(buffer.substring(0,2)).GetWidth() + 
                  middle.GetStringRect<FontLarge>(buffer.substring(2,1)).GetWidth() / 2;

    auto width  = middle.GetStringRect<FontLarge>(buffer).GetWidth();

    middle.CreateField({clock_center-offset, 0, clock_center-offset+width, middle.GetHeight()}).PutText<FontLarge>(buffer);

    {
      const Alarm::Setpoint sp = ::sys::globals().wakealarms_.FindNextAlarm();

      if(sp.IsEnabled())
      {
        uint_fast8_t offset = 16;
        for(uint8_t mask = (sp.mask & 0x7F); mask != 0; mask>>= 1, offset-=2);

        buffer.IPrintF("((%.2s, %02d:%02d))", kDayNames+offset, sp.hour, sp.minute);

        width = bottom.GetStringRect<FontSmall>(buffer).GetWidth();
        bottom.CreateField({bottom.GetWidth() - width, 0, bottom.GetWidth(), bottom.GetHeight()}).PutText<FontSmall>(buffer);
      }
    }
  }
  else
  {
    middle.DrawTextItem<WidgetEnvironment, FontLarge>((*actions_.GetSelectedItem()).name(), true);
  }
}
