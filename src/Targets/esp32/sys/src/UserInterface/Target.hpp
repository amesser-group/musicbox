/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_TARGET_HPP_
#define MUSICBOX_USERINTERFACE_TARGET_HPP_

#include "ecpp/UserInterface/Graphic/Painter.hpp"
#include "UserInterface/Graphic/Painter.hpp"
#include "UserInterface/events.hpp"
#include "UserInterface/Font/FontLarge.hpp"
#include "UserInterface/Font/FontSmall.hpp"
#include "Bsp.hpp"
#include <variant>

namespace MusicBox::UserInterface
{
  class WidgetEnvironment
  {
  public:
    typedef MusicBox::UserInterface::Graphic::Painter< ::ecpp::UserInterface::Graphic::Painter<::sys::Bsp::Display::Painter> >Painter;

    class Event;

    class Context;

    class Widget;

    template<typename T>
    using Font = Fonts::FontSmallFonts::NotoSansCondensedThin9pt;

    typedef ::std::variant<events::Refresh, 
                           events::KeyEvent, 
                           events::UserInteractionTimeout, 
                           events::StatusChange,
                           events::OperationModeChanged, 
                           events::DatabaseChanged> EventValue;

    constexpr Painter CreatePainter()
    {
      return { ::sys::bsp().display_.CreatePainter() };
    }
  };

  namespace Model
  {
    class ContextTargetMixin
    {
    public:
      void SpawnMainMenu() const;
      void SpawnSettingsMenu() const;
    };
  };

};

#endif 