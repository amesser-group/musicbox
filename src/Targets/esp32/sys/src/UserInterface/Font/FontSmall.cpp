
/* Auto generated C++ Font implementation */
#include "FontSmall.hpp"

using namespace MusicBox::UserInterface::Fonts;

const uint8_t FontSmallFonts::kTexture[] = {
  0x1,0x3,0x4,0x0,0x0,0x0,0x0,0x0,0x0,0xfb,0x0,0xff,0x0,0x40,0xe0,0xf0,
  0xf8,0xfc,0xfa,0xfb,0x3b,0x9a,0xc4,0xf8,0xf0,0xe0,0x40,0x0,0x38,0x64,0x44,0xff,
  0x44,0xc4,0x88,0x0,0x1,0x1,0xbe,0x40,0x40,0x0,0x40,0x40,0xbe,0x1,0x1,0x0,
  0x0,0x0,0xff,0x0,0xf0,0xc,0x2,0xf2,0x99,0x9,0x9,0x91,0xf9,0x2,0x84,0x78,
  0x0,0xf0,0x18,0xc,0x5,0x4,0x5,0xc,0x18,0xf0,0x0,0x0,0x0,0xe0,0x19,0x4,
  0x19,0xe0,0x0,0x0,0x0,0xfc,0x86,0x3,0x1,0x1,0x1,0x3,0x86,0xfc,0x0,0xfc,
  0x0,0x0,0x1,0x0,0x1,0x0,0xfc,0x0,0x0,0x80,0xf0,0x1e,0x3,0x0,0x20,0xde,
  0x1,0x1,0x0,0xf8,0x7,0x1,0x0,0x1,0xde,0x20,0x0,0x1,0x6,0xf8,0x0,0x0,
  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x20,0xa0,0x1f,0x0,0x3f,0x0,0x0,0x0,0x1,
  0x3,0x87,0x4f,0x53,0x52,0x8f,0xc7,0x3,0x1,0x80,0x40,0x40,0x42,0x84,0xc4,0x1f,
  0x4,0x4,0x43,0x0,0x50,0x10,0xf,0x0,0xc0,0x0,0x0,0x0,0xf,0x10,0x10,0x0,
  0x10,0x10,0x4f,0x0,0x40,0x3,0x4,0x4,0xc9,0x69,0x29,0x28,0x29,0xc5,0x0,0x0,
  0x0,0xc3,0x26,0x2c,0x28,0xc8,0x8,0xc,0x66,0x83,0x0,0x8,0x7,0x81,0x61,0x1,
  0x1,0x1,0x7,0x8,0x0,0x0,0xe1,0x3,0xe2,0x2,0x2,0x7,0x9,0x0,0xe0,0x3,
  0x4,0x8,0x8,0x8,0x8,0x84,0x63,0x40,0x8c,0x7,0x0,0x0,0x0,0x0,0xe0,0x23,
  0x4,0x8,0xa0,0x1,0x6,0xc8,0x20,0x8,0xe7,0x20,0x20,0x8,0x26,0x21,0xe0,0x0,
  0xfe,0x84,0x2,0x2,0x2,0x84,0x78,0x0,0xff,0x8,0x4,0x4,0x4,0xc,0xf8,0x0,
  0xf,0x10,0x20,0x20,0x20,0x10,0xff,0x0,0xf,0x90,0x20,0x20,0x20,0x90,0xff,0x0,
  0x78,0x84,0x2,0x2,0x2,0x84,0x78,0x0,0xff,0x84,0x2,0x2,0x2,0x84,0x78,0x0,
  0xfe,0x80,0x0,0x0,0x0,0x80,0xfe,0x0,0xff,0x0,0x80,0x8f,0x89,0x98,0x70,0x0,
  0x7,0x18,0x20,0x20,0xe0,0x18,0x7,0x0,0x80,0x81,0x46,0x38,0x6,0x1,0x0,0x0,
  0x3c,0x42,0x81,0x81,0x81,0x42,0xff,0x0,0xff,0x18,0x10,0x10,0x10,0x8,0x1f,0x10,
  0x0,0x4,0xc,0x1e,0x3f,0x7e,0xd7,0xda,0x7c,0x3f,0x1e,0xc,0x4,0x0,0xff,0x0,
  0x0,0x0,0xff,0x0,0x1f,0xe0,0x0,0x0,0xff,0x0,0x0,0x0,0x0,0x0,0xff,0x0,
  0xef,0x20,0x21,0x21,0x21,0x20,0x40,0xc0,0x83,0x0,0xc0,0x30,0x10,0x8,0xb,0x8,
  0x8,0x8,0x10,0x0,0x80,0x58,0x25,0x44,0x84,0x8,0x1,0x1,0x1,0x81,0x0,0x0,
  0x20,0x20,0xe1,0x3d,0x21,0xe0,0x3c,0x20,0x21,0x0,0xd,0x71,0x81,0x0,0x80,0x78,
  0x4,0x79,0x81,0x1,0x81,0x70,0xd,0x0,0xc0,0x30,0xa,0x8,0xa,0x30,0xc0,0x0,
  0xfe,0x80,0x80,0x40,0x20,0x10,0x0,0x20,0x90,0x92,0x90,0x92,0xe0,0x0,0xf8,0x2,
  0x0,0x2,0xf8,0x0,0x0,0xe2,0x18,0xe2,0x0,0x0,0x18,0x24,0xfe,0x84,0x80,0x0,
  0x10,0xfc,0x12,0x12,0x0,0xf6,0x0,0x78,0x84,0x84,0x84,0x78,0x80,0x60,0xb1,0x4d,
  0x40,0x41,0x80,0x0,0xfc,0xc,0x31,0xc0,0x1,0x1,0xc1,0x30,0xd,0xfd,0x1,0x0,
  0x7f,0x40,0x40,0x40,0x40,0x40,0x20,0x30,0x1f,0x0,0x3,0x4c,0x48,0x50,0x50,0x50,
  0xd1,0x51,0xf,0xc0,0x43,0x44,0x48,0x48,0xc8,0x89,0x6,0x4,0xea,0x69,0x80,0x1,
  0x1,0xf,0x1,0xe1,0xf,0x3,0xc1,0x21,0xa0,0xa0,0xa0,0x40,0x87,0x8,0x27,0x20,
  0x20,0x20,0xe7,0x28,0x27,0x20,0x20,0x0,0x21,0x66,0x88,0x8,0x8,0x6,0x81,0x60,
  0x2f,0x0,0x21,0x62,0x84,0x8,0x0,0x7,0x88,0x68,0x28,0x4,0x2f,0xc0,0x7,0x8,
  0x8,0x8,0x7,0xc0,0x2e,0x1,0xe1,0x21,0x2e,0x20,0x24,0x24,0x6f,0xc4,0x3,0xe0,
  0x0,0xf,0x0,0x0,0x0,0xf,0xe0,0x0,0x80,0xc0,0x6c,0x23,0x21,0x20,0x27,0x48,
  0x8,0x28,0x27,0x20,0x2f,0x20,0x20,0xa0,0x63,0x3,0x0,0x0,0x0,0xf,0xe0,0x0,
  0x0,0x3f,0x0,0x2,0x1,0x11,0x11,0x11,0x33,0xee,0x0,0x0,0x80,0x60,0x18,0x6,
  0x1,0x0,0x0,0xff,0x8,0x8,0x8,0x8,0x1c,0x77,0x80,0x0,0x7f,0x0,0x0,0x3,
  0xc,0x10,0x60,0x7f,0x0,0xf,0x30,0x4f,0x50,0x48,0x57,0x10,0xf,0x0,0x0,0x0,
  0x0,0x0,0x7f,0x0,0x0,0x0,0x0,0x0,0x40,0x60,0x18,0x5,0x2,0x5,0x18,0x60,
  0x40,0x0,0x0,0x0,0x1,0x2,0x7c,0x2,0x1,0x0,0x0,0x0,0x0,0x1,0x6,0x38,
  0x40,0x38,0x6,0x1,0x0,0x0,0x7f,0x42,0x42,0x42,0x42,0x42,0x67,0x3d,0x0,0x7f,
  0x2,0x2,0x2,0x2,0x2,0x2,0x7f,0x0,0x1f,0x30,0x60,0x40,0x40,0x40,0x40,0x20,
  0x0,0x60,0x50,0x48,0x44,0x42,0x41,0x40,0x40,0x0,0x1c,0x63,0x41,0x41,0x7f,0x0,
  0x0,0x23,0xf8,0x21,0x22,0x22,0x2,0xe2,0x19,0x9,0x8,0xf8,0x0,0x38,0xc6,0x82,
  0x82,0xfe,0x0,0x4,0x2,0xfe,0x0,0x0,0x0,0x4,0x2,0x2,0x82,0x66,0x3c,0x0,
  0x1e,0x33,0x21,0x21,0x21,0x92,0xfc,0x0,0xfc,0x2,0x1,0x1,0x1,0x2,0xfc,0x0,
  0xff,0x10,0x30,0x48,0x84,0x2,0x1,0x0,0x1f,0x9,0x9,0x9,0x9,0x11,0xe0,0x0,
  0xff,0x11,0x11,0x11,0x11,0x11,0x11,0x0,0xfc,0x26,0x13,0x11,0x11,0x31,0xe2,0x0,
  0x60,0x50,0x4c,0x42,0x41,0xff,0x40,0x0,0xe,0x13,0x11,0x21,0x21,0x21,0xc2,0x0,
  0xff,0x21,0x21,0x21,0x21,0x33,0x1e,0x0,0xee,0x2b,0x11,0x11,0x11,0x2b,0xee,0x0,
  0xff,0x0,0x0,0x0,0x0,0x0,0x0,0xff,0x11,0x11,0x11,0x11,0x11,0x0,0x7f,0x0,
  0x0,0xc0,0xf,0x10,0x10,0x90,0x40,0x40,0x3,0x2,0x42,0x5f,0x40,0x40,0x84,0x4,
  0x4,0xf3,0x10,0xf4,0x4,0x7,0x4,0x4,0xc0,0x74,0x46,0xf5,0x44,0x4,0x84,0x60,
  0x11,0x12,0x12,0x12,0x3,0xf9,0x0,0xc0,0x0,0xf9,0xa,0xa,0xfa,0x1,0xf8,0x40,
  0x43,0xc0,0x0,0xf8,0x0,0x1,0xfa,0x0,0x11,0xa,0x8a,0xca,0x72,0x1,0xf8,0xc8,
  0x3b,0x2,0x2,0x1a,0xe2,0x82,0x7a,0x0,0xf8,0x41,0x42,0x82,0x2,0x43,0xf9,0x8,
  0x0,0x38,0xc0,0x0,0xe0,0x7b,0x80,0x0,0xf1,0xa,0x2,0x82,0x82,0x83,0x81,0xf8,
  0x83,0x80,0x80,0x80,0x0,0xf0,0x8,0x98,0xe1,0xb3,0x4a,0x82,0x2,0xfb,0xc1,0x0,
  0x3,0x82,0x72,0x8a,0x2,0xc2,0x30,0xb,0x8,0x88,0x88,0x0,0x0,0x10,0xf9,0x0,
  0x1,0x7f,0x41,0x0,0x1e,0x65,0x42,0x42,0x3c,0x0,0x40,0x40,0x44,0x46,0x39,0x0,
  0xce,0x51,0x51,0x56,0xc,0x12,0x40,0x2,0x5f,0x2,0x1f,0xc2,0x0,0x0,0x3,0xcc,
  0x10,0x90,0x50,0x50,0x40,0x5f,0x3,0x1c,0x40,0x5f,0xc2,0x42,0x1,0xc0,0x1f,0x10,
  0x90,0x4f,0x0,0x1f,0xc2,0x42,0x43,0x80,0x0,0x40,0x9b,0x0,0x80,0x40,0x1f,0xd0,
  0x13,0xc,0x0,0xd0,0x1c,0xc3,0x0,0x0,0x1f,0xc0,0x0,0x5f,0x40,0xc0,0x5f,0x40,
  0x0,0xc0,0x27,0x2e,0x21,0xc0,0x3,0xe,0x1,0x80,0xe0,0x0,0x0,0xe0,0x20,0x2f,
  0x20,0xc0,0x0,0xc0,0x20,0x20,0x21,0xcc,0x3,0x7,0xe8,0x2f,0x20,0xf,0x0,0xe7,
  0x2c,0x23,0xe0,0xf,0x0,0x1,0x26,0x28,0x28,0xc8,0x8,0xe7,0x0,0xc0,0x2f,0x0,
  0x0,0xff,0x11,0x71,0x8e,0x0,0xff,0x11,0x11,0xe,0x0,0xc0,0x25,0x24,0xfd,0x0,
  0x7f,0x4,0x4,0x0,0x0,0x1c,0x63,0x41,0x41,0x3e,0x0,0x7f,0x3,0xc,0x10,0x7f,
  0x0,0x41,0x42,0x44,0x44,0x38,0x0,0x40,0x78,0x46,0x41,0x0,0x0,0x0,0x3,0x7e,
  0x1,0x0,0x0,0x1f,0x60,0x40,0x40,0x3f,0x0,0x60,0x1b,0xe,0x31,0x40,0x0,0x1,
  0x1e,0x60,0x1c,0x3,0x0,0x7f,0x4,0x4,0x4,0x7f,0x0,0x0,0x0,0x7f,0x0,0x0,
  0x0,0x18,0x25,0x22,0xa3,0x1c,0x80,0x8c,0x8b,0x8,0x3f,0x88,0x80,0xbf,0xa0,0x20,
  0xa0,0x1f,0x80,0x3,0x24,0xa4,0xb4,0x8f,0x80,0x20,0xa1,0x21,0xa1,0x9e,0x80,0x3f,
  0x22,0xa2,0xa5,0x98,0x0,0x20,0xb0,0x28,0x26,0x21,0x80,0x3f,0x3,0xc,0xb0,0x0,
  0x70,0x8d,0x4,0x4,0x1,0x0,0x8d,0x0,0x0,0x84,0x0,0xfc,0x9,0x5,0x5,0x0,
  0x3c,0x42,0x81,0x81,0x81,0x42,0x0,0xc1,0xa1,0x91,0x89,0x85,0x83,0x0,0x4e,0x89,
  0x89,0x91,0x91,0x72,0x0,0x3c,0x4a,0x89,0x89,0x89,0x8b,0x4e,0x0,0x81,0x42,0x24,
  0x18,0x24,0x42,0x81,0x0,0x3,0x1c,0x60,0x80,0x60,0x1c,0x3,0x0,0x18,0x18,0x3c,
  0x24,0x24,0x42,0x42,0x42,0x81,0x0,0x81,0x42,0x42,0x42,0x24,0x24,0x3c,0x18,0x18,
  0x0,0xc0,0x38,0x6,0x1,0x0,0x0,0xb0,0xc,0x3,0x0,0xff,0x88,0x88,0x80,0x0,
  0xfe,0x80,0x80,0xfe,0x0,0x80,0x70,0xe,0x1,0x0,0x7f,0x1,0x0,0x0,0x0,0x7f,
  0x1,0x0,0x0,0x0,0x7f,0x0,0x3,0x1c,0x60,0x1c,0x3,0x1c,0x60,0x1c,0x3,0x0,
  0x48,0x49,0x49,0x49,0x48,0x4a,0x49,0x48,0x4a,0x1,0x10,0x53,0x38,0x50,0x0,0x0,
  0xc0,0x0,0xd0,0x18,0xc,0xc6,0x2,0xc6,0xc,0x18,0x90,0x0,0xa2,0x14,0x2a,0x94,
  0x88,0x0,0x8,0x94,0xaa,0x94,0xa2,0x0,0x20,0x18,0x86,0xc,0x30,0x80,0x88,0x18,
  0x94,0x24,0x22,0x0,0x8,0x8,0x7e,0x8,0x8,0x0,0x42,0x22,0x14,0x14,0x8,0x0,
  0x66,0x3c,0x18,0x66,0x3c,0x18,0x0,0x18,0x3c,0x66,0x18,0x3c,0x66,0x0,0x24,0x28,
  0x18,0x7e,0x18,0x28,0x24,0x0,0x8c,0x92,0x92,0xe2,0x0,0x82,0x64,0x38,0xc6,0x0,
  0xe,0xf0,0x70,0xe,0x0,0x80,0xf2,0x8e,0x2,0x0,0x14,0x6b,0x49,0x49,0x4e,0x0,
  0x7f,0x1,0x1,0x7f,0x1,0x1,0x7e,0x0,0x7,0x78,0x1c,0x7,0x78,0x1c,0x3,0x0,
  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  0x3,0x0,0x3,0x0,0x1,0x0,0x1,0x0,0x0,0x3,0x0,0x3,0x0,0x0,0x3,0x0,
  0x3,0x0,0x1,0x0,0x0,0x0,0x1,0x1,0x1,0x1,0x0,0x0,0x1,0x0,0x0,0x1,
  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
  0x0,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x0,
};

FontSmallFonts::Glyph FontSmallFonts::BitstreamVeraSans14px::GetGlyph(Unicode::Codepoint cp)
{
  switch(cp.GetRaw())
  {
  case 32: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 4, 16  },
      .bitmap_rect   = { 0, 0, 0, 0 }
    };
  case 33: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 1, 40, 2, 50 }
    };
  case 34: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 16, 94, 19, 98 }
    };
  case 35: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 31, 26, 41, 36 }
    };
  case 36: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 28, 0, 35, 13 }
    };
  case 37: return { 
      .width         = 13,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 103, 26, 115, 36 }
    };
  case 38: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 20, 26, 30, 36 }
    };
  case 39: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 9, 9, 10, 13 }
    };
  case 40: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 115, 0, 118, 12 }
    };
  case 41: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 123, 0, 126, 12 }
    };
  case 42: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 78, 89, 85, 95 }
    };
  case 43: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 91, 59, 100, 68 }
    };
  case 44: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 10  },
      .bitmap_rect   = { 8, 11, 10, 14 }
    };
  case 45: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 8  },
      .bitmap_rect   = { 57, 11, 61, 12 }
    };
  case 46: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 10  },
      .bitmap_rect   = { 9, 0, 10, 2 }
    };
  case 47: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 104, 0, 109, 12 }
    };
  case 48: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 40, 48, 47, 58 }
    };
  case 49: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 19, 49, 24, 59 }
    };
  case 50: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 25, 49, 31, 59 }
    };
  case 51: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 3, 40, 10, 50 }
    };
  case 52: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 80, 48, 87, 58 }
    };
  case 53: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 56, 48, 63, 58 }
    };
  case 54: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 72, 48, 79, 58 }
    };
  case 55: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 11, 38, 18, 48 }
    };
  case 56: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 104, 48, 111, 58 }
    };
  case 57: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 32, 48, 39, 58 }
    };
  case 58: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 4, 74, 5, 81 }
    };
  case 59: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 5, 82, 7, 90 }
    };
  case 60: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 61, 80, 70, 88 }
    };
  case 61: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 0, 91, 9, 95 }
    };
  case 62: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 71, 80, 80, 88 }
    };
  case 63: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 56, 59, 61, 69 }
    };
  case 64: return { 
      .width         = 14,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 52, 0, 64, 12 }
    };
  case 65: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 75, 2, 84, 12 }
    };
  case 66: return { 
      .width         = 10,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 86, 37, 94, 47 }
    };
  case 67: return { 
      .width         = 10,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 104, 37, 112, 47 }
    };
  case 68: return { 
      .width         = 11,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 0, 29, 9, 39 }
    };
  case 69: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 64, 48, 71, 58 }
    };
  case 70: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 119, 48, 125, 58 }
    };
  case 71: return { 
      .width         = 11,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 10, 27, 19, 37 }
    };
  case 72: return { 
      .width         = 10,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 95, 37, 103, 47 }
    };
  case 73: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 9, 3, 10, 13 }
    };
  case 74: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 48, 0, 51, 13 }
    };
  case 75: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 48, 48, 55, 58 }
    };
  case 76: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 112, 48, 118, 58 }
    };
  case 77: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 116, 26, 126, 36 }
    };
  case 78: return { 
      .width         = 10,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 28, 37, 36, 47 }
    };
  case 79: return { 
      .width         = 11,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 65, 2, 74, 12 }
    };
  case 80: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 96, 48, 103, 58 }
    };
  case 81: return { 
      .width         = 11,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 85, 0, 94, 12 }
    };
  case 82: return { 
      .width         = 10,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 19, 38, 27, 48 }
    };
  case 83: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 88, 48, 95, 58 }
    };
  case 84: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 46, 37, 55, 47 }
    };
  case 85: return { 
      .width         = 10,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 95, 2, 103, 12 }
    };
  case 86: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 76, 37, 85, 47 }
    };
  case 87: return { 
      .width         = 13,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 42, 26, 55, 36 }
    };
  case 88: return { 
      .width         = 11,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 56, 37, 65, 47 }
    };
  case 89: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 66, 37, 75, 47 }
    };
  case 90: return { 
      .width         = 10,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 113, 37, 121, 47 }
    };
  case 91: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 120, 13, 123, 25 }
    };
  case 93: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 124, 13, 127, 25 }
    };
  case 94: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 18, 89, 27, 93 }
    };
  case 95: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 14  },
      .bitmap_rect   = { 86, 46, 93, 47 }
    };
  case 96: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 0  },
      .bitmap_rect   = { 0, 0, 6, 16 }
    };
  case 97: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 71, 28, 77, 36 }
    };
  case 98: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 40, 14, 47, 25 }
    };
  case 99: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 16, 80, 22, 88 }
    };
  case 100: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 80, 13, 87, 24 }
    };
  case 101: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 37, 80, 44, 88 }
    };
  case 102: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 96, 25, 100, 36 }
    };
  case 103: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 24, 14, 31, 25 }
    };
  case 104: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 8, 15, 15, 26 }
    };
  case 105: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 101, 25, 102, 36 }
    };
  case 106: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 7, 0, 10, 14 }
    };
  case 107: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 64, 25, 70, 36 }
    };
  case 108: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 11, 3, 12, 14 }
    };
  case 109: return { 
      .width         = 13,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 106, 79, 117, 87 }
    };
  case 110: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 8, 18, 15, 26 }
    };
  case 111: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 32, 17, 39, 25 }
    };
  case 112: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 0, 17, 7, 28 }
    };
  case 113: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 16, 14, 23, 25 }
    };
  case 114: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 11, 82, 15, 90 }
    };
  case 115: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 30, 80, 36, 88 }
    };
  case 116: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 1, 51, 6, 61 }
    };
  case 117: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 48, 17, 55, 25 }
    };
  case 118: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 1, 4  },
      .bitmap_rect   = { 53, 80, 60, 88 }
    };
  case 119: return { 
      .width         = 11,
      .height        = 16,
      .render_offset = { 1, 4  },
      .bitmap_rect   = { 118, 79, 127, 87 }
    };
  case 120: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 1, 4  },
      .bitmap_rect   = { 45, 80, 52, 88 }
    };
  case 121: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 1, 4  },
      .bitmap_rect   = { 72, 13, 79, 24 }
    };
  case 122: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 23, 80, 29, 88 }
    };
  case 123: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 42, 0, 47, 13 }
    };
  case 124: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 11, 0, 12, 14 }
    };
  case 125: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 36, 0, 41, 13 }
    };
  case 126: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 7  },
      .bitmap_rect   = { 34, 95, 43, 97 }
    };
  case 171: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 71, 89, 77, 95 }
    };
  case 181: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 4  },
      .bitmap_rect   = { 88, 13, 96, 24 }
    };
  case 187: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 64, 89, 70, 95 }
    };
  case 196: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 0  },
      .bitmap_rect   = { 75, 0, 84, 12 }
    };
  case 214: return { 
      .width         = 11,
      .height        = 16,
      .render_offset = { 0, 0  },
      .bitmap_rect   = { 65, 0, 74, 12 }
    };
  case 220: return { 
      .width         = 10,
      .height        = 16,
      .render_offset = { 0, 0  },
      .bitmap_rect   = { 95, 0, 103, 12 }
    };
  case 223: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 56, 13, 63, 24 }
    };
  case 228: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 71, 25, 77, 36 }
    };
  case 246: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 32, 14, 39, 25 }
    };
  case 252: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 48, 14, 55, 25 }
    };
  case 8211: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 8  },
      .bitmap_rect   = { 86, 46, 93, 47 }
    };
  case 8212: return { 
      .width         = 14,
      .height        = 16,
      .render_offset = { 0, 8  },
      .bitmap_rect   = { 113, 96, 127, 97 }
    };
  case 8220: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 25, 95, 29, 98 }
    };
  case 8221: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 20, 94, 24, 97 }
    };
  default:
  case 65533: return { 
      .width         = 14,
      .height        = 16,
      .render_offset = { 0, 0  },
      .bitmap_rect   = { 13, 0, 27, 13 }
    };
  }
}

int_fast8_t FontSmallFonts::BitstreamVeraSans14px::GetKerning(Unicode::Codepoint first, Unicode::Codepoint second)
{
  switch(first.GetRaw())
  {
  case 45:
    switch (second.GetRaw())
    {
    case 84: return -2;
    case 89: return -2;
    }
    break;
  case 65:
    switch (second.GetRaw())
    {
    case 84: return -2;
    case 89: return -2;
    case 8220: return -2;
    case 8221: return -2;
    }
    break;
  case 70:
    switch (second.GetRaw())
    {
    case 46: return -2;
    case 58: return -2;
    case 65: return -2;
    case 97: return -2;
    case 105: return -1;
    case 114: return -1;
    case 121: return -2;
    case 196: return -2;
    case 228: return -2;
    }
    break;
  case 75:
    switch (second.GetRaw())
    {
    case 45: return -2;
    case 84: return -2;
    case 121: return -1;
    }
    break;
  case 76:
    switch (second.GetRaw())
    {
    case 84: return -2;
    case 86: return -2;
    case 87: return -2;
    case 89: return -2;
    case 121: return -2;
    case 8220: return -2;
    case 8221: return -4;
    }
    break;
  case 80:
    switch (second.GetRaw())
    {
    case 46: return -2;
    }
    break;
  case 82:
    switch (second.GetRaw())
    {
    case 84: return -1;
    case 8220: return -1;
    }
    break;
  case 84:
    switch (second.GetRaw())
    {
    case 45: return -2;
    case 46: return -2;
    case 58: return -2;
    case 65: return -2;
    case 97: return -2;
    case 99: return -2;
    case 101: return -2;
    case 111: return -2;
    case 114: return -2;
    case 115: return -2;
    case 117: return -2;
    case 119: return -2;
    case 121: return -2;
    case 171: return -2;
    case 196: return -2;
    case 228: return -2;
    case 246: return -2;
    case 252: return -2;
    }
    break;
  case 86:
    switch (second.GetRaw())
    {
    case 46: return -2;
    case 58: return -2;
    case 97: return -2;
    case 101: return -2;
    case 111: return -2;
    case 171: return -2;
    case 228: return -2;
    case 246: return -2;
    }
    break;
  case 87:
    switch (second.GetRaw())
    {
    case 46: return -2;
    }
    break;
  case 88:
    switch (second.GetRaw())
    {
    case 67: return -1;
    case 8220: return -2;
    }
    break;
  case 89:
    switch (second.GetRaw())
    {
    case 45: return -2;
    case 46: return -2;
    case 58: return -2;
    case 65: return -2;
    case 97: return -2;
    case 101: return -2;
    case 111: return -2;
    case 117: return -2;
    case 171: return -2;
    case 187: return -1;
    case 196: return -2;
    case 228: return -2;
    case 246: return -2;
    case 252: return -2;
    }
    break;
  case 102:
    switch (second.GetRaw())
    {
    case 46: return -1;
    }
    break;
  case 110:
    switch (second.GetRaw())
    {
    case 8220: return -1;
    }
    break;
  case 111:
    switch (second.GetRaw())
    {
    case 8220: return -1;
    }
    break;
  case 114:
    switch (second.GetRaw())
    {
    case 46: return -2;
    }
    break;
  case 118:
    switch (second.GetRaw())
    {
    case 46: return -2;
    }
    break;
  case 119:
    switch (second.GetRaw())
    {
    case 46: return -2;
    }
    break;
  case 121:
    switch (second.GetRaw())
    {
    case 46: return -2;
    case 58: return -1;
    }
    break;
  case 171:
    switch (second.GetRaw())
    {
    case 89: return -1;
    }
    break;
  case 187:
    switch (second.GetRaw())
    {
    case 84: return -2;
    case 86: return -2;
    case 89: return -2;
    }
    break;
  case 196:
    switch (second.GetRaw())
    {
    case 84: return -2;
    case 89: return -2;
    case 8220: return -2;
    case 8221: return -2;
    }
    break;
  case 246:
    switch (second.GetRaw())
    {
    case 8220: return -1;
    }
    break;
  case 8220:
    switch (second.GetRaw())
    {
    case 65: return -2;
    case 111: return -1;
    case 196: return -2;
    case 246: return -1;
    }
    break;
  }

  return 0;
}

FontSmallFonts::Glyph FontSmallFonts::NotoSansCondensedThin9pt::GetGlyph(Unicode::Codepoint cp)
{
  switch(cp.GetRaw())
  {
  case 32: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 3, 16  },
      .bitmap_rect   = { 0, 0, 0, 0 }
    };
  case 33: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 1, 3  },
      .bitmap_rect   = { 126, 48, 127, 57 }
    };
  case 34: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 37, 66, 40, 69 }
    };
  case 35: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 23, 60, 29, 69 }
    };
  case 36: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 90, 25, 95, 36 }
    };
  case 37: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 101, 59, 108, 68 }
    };
  case 38: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 16, 60, 22, 69 }
    };
  case 39: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 9, 10, 10, 13 }
    };
  case 40: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 116, 13, 119, 25 }
    };
  case 41: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 123, 0, 126, 12 }
    };
  case 42: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 10, 91, 14, 95 }
    };
  case 43: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 52, 89, 57, 95 }
    };
  case 44: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 0, 11  },
      .bitmap_rect   = { 8, 11, 10, 14 }
    };
  case 45: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 8  },
      .bitmap_rect   = { 56, 8, 59, 9 }
    };
  case 46: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 1, 11  },
      .bitmap_rect   = { 2, 2, 3, 3 }
    };
  case 47: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 81, 79, 85, 88 }
    };
  case 48: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 51, 70, 56, 79 }
    };
  case 49: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 125, 59, 127, 68 }
    };
  case 50: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 117, 69, 122, 78 }
    };
  case 51: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 10, 62, 15, 71 }
    };
  case 52: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 87, 69, 92, 78 }
    };
  case 53: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 105, 69, 110, 78 }
    };
  case 54: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 4, 62, 9, 71 }
    };
  case 55: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 1, 3  },
      .bitmap_rect   = { 101, 79, 105, 88 }
    };
  case 56: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 81, 69, 86, 78 }
    };
  case 57: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 99, 69, 104, 78 }
    };
  case 58: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 1, 5  },
      .bitmap_rect   = { 42, 72, 43, 79 }
    };
  case 59: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 8, 82, 10, 90 }
    };
  case 60: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 46, 89, 51, 94 }
    };
  case 61: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 4, 91, 9, 95 }
    };
  case 62: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 58, 89, 63, 95 }
    };
  case 63: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 86, 79, 90, 88 }
    };
  case 64: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 37, 37, 45, 47 }
    };
  case 65: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 84, 27, 89, 36 }
    };
  case 66: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 111, 69, 116, 78 }
    };
  case 67: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 30, 60, 36, 69 }
    };
  case 68: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 93, 69, 98, 78 }
    };
  case 69: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 91, 79, 95, 88 }
    };
  case 70: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 16, 70, 20, 79 }
    };
  case 71: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 117, 59, 124, 68 }
    };
  case 72: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 69, 70, 74, 79 }
    };
  case 73: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 1, 3  },
      .bitmap_rect   = { 9, 4, 10, 13 }
    };
  case 74: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 48, 1, 51, 13 }
    };
  case 75: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 123, 69, 127, 78 }
    };
  case 76: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 120, 16, 123, 25 }
    };
  case 77: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 109, 59, 116, 68 }
    };
  case 78: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 27, 70, 32, 79 }
    };
  case 79: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 56, 27, 63, 36 }
    };
  case 80: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 6, 72, 10, 81 }
    };
  case 81: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 64, 13, 71, 24 }
    };
  case 82: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 1, 72, 5, 81 }
    };
  case 83: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 33, 70, 38, 79 }
    };
  case 84: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 75, 70, 80, 79 }
    };
  case 85: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 1, 3  },
      .bitmap_rect   = { 78, 27, 83, 36 }
    };
  case 86: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 63, 70, 68, 79 }
    };
  case 87: return { 
      .width         = 9,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 81, 59, 90, 68 }
    };
  case 88: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 57, 70, 62, 79 }
    };
  case 89: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 45, 70, 50, 79 }
    };
  case 90: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 39, 70, 44, 79 }
    };
  case 91: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 110, 13, 112, 25 }
    };
  case 93: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 124, 13, 127, 25 }
    };
  case 94: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 40, 89, 45, 94 }
    };
  case 95: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 1, 13  },
      .bitmap_rect   = { 56, 11, 61, 12 }
    };
  case 96: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 125, 75, 127, 78 }
    };
  case 97: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 11, 74, 15, 81 }
    };
  case 98: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 1, 2  },
      .bitmap_rect   = { 46, 59, 50, 69 }
    };
  case 99: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 0, 82, 4, 89 }
    };
  case 100: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 122, 37, 127, 47 }
    };
  case 101: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 106, 88, 111, 95 }
    };
  case 102: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 77, 59, 80, 69 }
    };
  case 103: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 13, 49, 18, 59 }
    };
  case 104: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 1, 2  },
      .bitmap_rect   = { 72, 59, 76, 69 }
    };
  case 105: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 1, 3  },
      .bitmap_rect   = { 14, 72, 15, 81 }
    };
  case 106: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 1, 3  },
      .bitmap_rect   = { 113, 13, 115, 25 }
    };
  case 107: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 1, 2  },
      .bitmap_rect   = { 37, 59, 40, 69 }
    };
  case 108: return { 
      .width         = 2,
      .height        = 16,
      .render_offset = { 1, 2  },
      .bitmap_rect   = { 9, 3, 10, 13 }
    };
  case 109: return { 
      .width         = 8,
      .height        = 16,
      .render_offset = { 1, 5  },
      .bitmap_rect   = { 112, 88, 119, 95 }
    };
  case 110: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 1, 5  },
      .bitmap_rect   = { 72, 62, 76, 69 }
    };
  case 111: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 21, 72, 26, 79 }
    };
  case 112: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 1, 5  },
      .bitmap_rect   = { 41, 59, 45, 69 }
    };
  case 113: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 7, 51, 12, 61 }
    };
  case 114: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 1, 5  },
      .bitmap_rect   = { 8, 19, 10, 26 }
    };
  case 115: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 86, 89, 90, 96 }
    };
  case 116: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 0, 62, 3, 71 }
    };
  case 117: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 1, 5  },
      .bitmap_rect   = { 96, 81, 100, 88 }
    };
  case 118: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 96, 89, 100, 96 }
    };
  case 119: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 120, 88, 127, 95 }
    };
  case 120: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 91, 89, 95, 96 }
    };
  case 121: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 67, 59, 71, 69 }
    };
  case 122: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 5  },
      .bitmap_rect   = { 101, 89, 105, 96 }
    };
  case 123: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 110, 0, 114, 12 }
    };
  case 124: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 2  },
      .bitmap_rect   = { 11, 1, 12, 14 }
    };
  case 125: return { 
      .width         = 4,
      .height        = 16,
      .render_offset = { 1, 3  },
      .bitmap_rect   = { 119, 0, 122, 12 }
    };
  case 126: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 6  },
      .bitmap_rect   = { 44, 95, 49, 97 }
    };
  case 171: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 6  },
      .bitmap_rect   = { 34, 89, 39, 94 }
    };
  case 181: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 1, 5  },
      .bitmap_rect   = { 51, 59, 55, 69 }
    };
  case 187: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 6  },
      .bitmap_rect   = { 28, 89, 33, 94 }
    };
  case 196: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 84, 25, 89, 36 }
    };
  case 214: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 0, 1  },
      .bitmap_rect   = { 56, 25, 63, 36 }
    };
  case 220: return { 
      .width         = 7,
      .height        = 16,
      .render_offset = { 1, 1  },
      .bitmap_rect   = { 78, 25, 83, 36 }
    };
  case 223: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 1, 2  },
      .bitmap_rect   = { 62, 59, 66, 69 }
    };
  case 228: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 11, 72, 15, 81 }
    };
  case 246: return { 
      .width         = 5,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 21, 70, 26, 79 }
    };
  case 252: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 1, 3  },
      .bitmap_rect   = { 96, 79, 100, 88 }
    };
  case 8211: return { 
      .width         = 6,
      .height        = 16,
      .render_offset = { 0, 8  },
      .bitmap_rect   = { 56, 11, 61, 12 }
    };
  case 8212: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 8  },
      .bitmap_rect   = { 116, 96, 127, 97 }
    };
  case 8220: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 30, 95, 33, 98 }
    };
  case 8221: return { 
      .width         = 3,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 30, 95, 33, 98 }
    };
  default:
  case 65533: return { 
      .width         = 12,
      .height        = 16,
      .render_offset = { 0, 3  },
      .bitmap_rect   = { 97, 13, 109, 24 }
    };
  }
}

int_fast8_t FontSmallFonts::NotoSansCondensedThin9pt::GetKerning(Unicode::Codepoint first, Unicode::Codepoint second)
{
  switch(first.GetRaw())
  {
  case 34:
    switch (second.GetRaw())
    {
    case 65: return -1;
    case 99: return -1;
    case 100: return -1;
    case 101: return -1;
    case 111: return -1;
    case 113: return -1;
    case 196: return -1;
    case 246: return -1;
    }
    break;
  case 39:
    switch (second.GetRaw())
    {
    case 65: return -1;
    case 99: return -1;
    case 100: return -1;
    case 101: return -1;
    case 111: return -1;
    case 113: return -1;
    case 196: return -1;
    case 246: return -1;
    }
    break;
  case 40:
    switch (second.GetRaw())
    {
    case 74: return 1;
    }
    break;
  case 44:
    switch (second.GetRaw())
    {
    case 84: return -1;
    case 86: return -1;
    case 87: return -1;
    case 89: return -1;
    }
    break;
  case 46:
    switch (second.GetRaw())
    {
    case 84: return -1;
    case 86: return -1;
    case 87: return -1;
    case 89: return -1;
    }
    break;
  case 65:
    switch (second.GetRaw())
    {
    case 34: return -1;
    case 39: return -1;
    case 8221: return -1;
    }
    break;
  case 69:
    switch (second.GetRaw())
    {
    case 74: return 1;
    }
    break;
  case 70:
    switch (second.GetRaw())
    {
    case 44: return -1;
    case 46: return -1;
    }
    break;
  case 76:
    switch (second.GetRaw())
    {
    case 34: return -1;
    case 39: return -1;
    case 8221: return -1;
    }
    break;
  case 80:
    switch (second.GetRaw())
    {
    case 44: return -1;
    case 46: return -1;
    }
    break;
  case 84:
    switch (second.GetRaw())
    {
    case 44: return -1;
    case 46: return -1;
    case 97: return -1;
    case 99: return -1;
    case 100: return -1;
    case 101: return -1;
    case 103: return -1;
    case 111: return -1;
    case 113: return -1;
    case 115: return -1;
    case 171: return -1;
    case 228: return -1;
    case 246: return -1;
    }
    break;
  case 89:
    switch (second.GetRaw())
    {
    case 44: return -1;
    case 46: return -1;
    case 171: return -1;
    }
    break;
  case 91:
    switch (second.GetRaw())
    {
    case 74: return 1;
    }
    break;
  case 102:
    switch (second.GetRaw())
    {
    case 34: return 1;
    case 39: return 1;
    case 8221: return 1;
    }
    break;
  case 114:
    switch (second.GetRaw())
    {
    case 44: return -1;
    case 46: return -1;
    }
    break;
  case 123:
    switch (second.GetRaw())
    {
    case 74: return 1;
    }
    break;
  case 187:
    switch (second.GetRaw())
    {
    case 84: return -1;
    case 89: return -1;
    }
    break;
  case 196:
    switch (second.GetRaw())
    {
    case 34: return -1;
    case 39: return -1;
    case 8221: return -1;
    }
    break;
  case 8220:
    switch (second.GetRaw())
    {
    case 65: return -1;
    case 99: return -1;
    case 100: return -1;
    case 101: return -1;
    case 111: return -1;
    case 113: return -1;
    case 196: return -1;
    case 246: return -1;
    }
    break;
  case 8221:
    switch (second.GetRaw())
    {
    case 65: return -1;
    case 99: return -1;
    case 100: return -1;
    case 101: return -1;
    case 111: return -1;
    case 113: return -1;
    case 196: return -1;
    case 246: return -1;
    }
    break;
  }

  return 0;
}

