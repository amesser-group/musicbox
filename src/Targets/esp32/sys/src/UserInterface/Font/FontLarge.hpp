
/* Auto generated C++ Font implementation */
#ifndef MUSICBOX_USERINTERFACE_FONTS_FONTLARGE_HPP_
#define MUSICBOX_USERINTERFACE_FONTS_FONTLARGE_HPP_
#include "ecpp/Graphics/BitmapFont.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"
#include "ecpp/Graphics/Monochrome/PagedBufferProxy.hpp"

namespace MusicBox::UserInterface::Fonts
{
  class FontLargeFonts
  {
  protected:
    using Unicode = ::ecpp::StringEncodings::Unicode;
    using Glyph   = ::ecpp::Graphics::BitmapFont::Glyph<uint_least8_t>;
    using Texture = ::ecpp::Graphics::Monochrome::PagedBufferProxy<176, const uint8_t>;

    static const uint8_t kTexture[176 * 176 / 8];
  public:
    class BitstreamVeraSans28px : protected ::ecpp::Graphics::BitmapFont
    {
    public:
      template<typename Target>
      static constexpr ::ecpp::Graphics::FontRenderer<Target,BitstreamVeraSans28px>  CreateRenderer(Target target) { return { target }; }

      static constexpr Texture          GetTexture() { return { kTexture, 0, 0, 176, 176 }; }
      static FontLargeFonts::Glyph GetGlyph(Unicode::Codepoint cp);
      static int_fast8_t                GetKerning(Unicode::Codepoint first, Unicode::Codepoint second);
      static constexpr auto             GetHeight() { return 33; }
    };
  };
}
#endif