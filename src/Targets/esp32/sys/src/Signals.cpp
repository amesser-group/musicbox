/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "System.hpp"

using namespace MusicBox;

void
::signals::Complete(ecpp::Drivers::ESP32::TwiRequestBlock &trb)
{
  auto & bsp = ::sys::bsp();
  auto & g = ::sys::globals();

  if(bsp.pcal6408_trb_.IsSame(trb))
    return;
  else if( bsp.max98089_trb_.IsSame(trb))
    g.max98089_driver_.notifyTwiFinished();
  else if( bsp.si468x_transaction_.IsSame(trb))
    bsp.si468x_transaction_.RequestComplete.Enqueue();
}

void
::signals::TransferDone(::ecpp::Drivers::ESP32::SpiRequestBlock& srb)
{
  ::sys::bsp().Notify(::sys::Bsp::PeripheralEvent::DisplayTransferDoneEvent);
}

void
::signals::MusicBox::UserTimeout(MusicBoxApp& app)
{
  ::sys::globals().snoozecontroller_.HandleUserTimeout();
}

void
::signals::UserShutdown()
{
  auto & g = ::sys::globals();

  g.GoToStandBy();
  g.snoozecontroller_.HandleUserShutdown();
}