#include "ecpp/SystemTick.hpp"
#include "System.hpp"
#include "freertos/task.h"
#include <stdio.h>

extern "C" void app_main(void);

using namespace sys;

void app_main(void)
{
  auto & g = globals();

  sys::Initialize();

  sys::Start();
  
  while(true)
  {
    auto  next_timeout = ::ecpp::GetSystemTick() + MilliSecond<>(1000);
    Job   *job = nullptr;

    job = globals().timer_queue_.CheckNextTimeout(next_timeout);

    if (nullptr == job)
      job = g.job_queue_.GetNext();

    if(nullptr != job)
    {
      job->run();
      bsp().Poll();
    }
    else
    {
      bsp().Poll(next_timeout);
    }
  }
}
