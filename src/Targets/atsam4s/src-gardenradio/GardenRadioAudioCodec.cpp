/*  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "GardenRadioAudioCodec.hpp"
#include "Bsp.hpp"
#include "System.hpp"

using sys::Bsp;
using ecpp::Units::MilliSecond;

namespace MusicBox::Targets::ATSAM4S
{
  using MusicBox::Drivers::MAX98089::Max98089Defs;
  using MusicBox::Drivers::MAX98089::Max98089CommonSequences;

  static const ::std::array<uint8_t, 13> kMax98089RegisterSequence_Lineout 
  {  
    Max98089Defs::REG_DAC_MIXER,
    0x84,             /* Connect DAC with DAI1*/
    0x00, 0x00,       /* Input mixers all off */
    0x00, 0x00, 0x00, /* Headphone Mixers all off */
    0x01, 0x01, 0x00, /* Lineout Mixers connect with DACf  */
    0x00, 0x00, 0x00  /* Speaker Mixers all off */
  };

  /* Power management */
  const ::std::array<uint8_t, 6> kMax98089RegisterSequence_OutputLineout =
  {
    Max98089Defs::REG_OUTPUT_ENABLE,
    0x0F, /* Enable Lineout */
    0xF0, /* Enable Bandgap, Regulator, Common Mode & Bias */
    0x00, /* Disable Low Power mode */
    0x0D, /* DAI1,2  Input Dither + DAI1 Clock Enable */
    0xC0, /* Unshutdown */
  };

  void
  GardenRadioAudioCodec::SetMute(bool mute)
  {
    if(mute)
      requested_volume_level_ |=  Max98089Defs::kRegMask_Volume_Mute;
    else
      requested_volume_level_ &= ~Max98089Defs::kRegMask_Volume_Mute;

    PollTwiComplete.Enqueue();
  }

  void
  GardenRadioAudioCodec::ShutdownAudio()
  {
    if(std::holds_alternative<Shutdown>(current_profile_))
      return;

    current_profile_.emplace<Shutdown>();
    PollTwiComplete.Enqueue();
  }

  void
  GardenRadioAudioCodec::PrepareRadioAudio()
  {
    if(std::holds_alternative<RadioMode>(current_profile_))
      return;

    current_profile_.emplace<RadioMode>();
    PollTwiComplete.Enqueue();
  }

  ECPP_JOB_DEF(GardenRadioAudioCodec, PollTwiComplete)
  {
    auto bsp = Bsp::instance();

    if(bsp.IsClockEnabled(*this))
    {
      if(not bsp.GetTwiRequestBlock(*this).IsActive())
        ::std::visit([&](auto && arg){arg.TwiReady(*this);}, current_profile_);
    }
    else
    {
      PollTwiComplete.Start(MilliSecond<>(50));
    }
  }

  bool
  GardenRadioAudioCodec::WriteVolumeRegs()
  {
    uint8_t level = requested_volume_level_;

    std::array<uint8_t, 5> a = {
      Max98089Defs::REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL, 
      level | 0x1F, level | 0x1F, /* Line Out always runs at full level */
      0 ,0
    };

    if(WriteRegisters(a))
    {
      current_volume_level_ = requested_volume_level_;
      return true;
    }
    else
    {
      return false;
    }
  }

  void 
  GardenRadioAudioCodec::RadioMode::TwiReady(GardenRadioAudioCodec& codec)
  {
    bool next_state = false;

    switch(state_)
    {
    case 0: next_state = codec.WriteRegisters(Max98089CommonSequences::kShutdown);
    /* Setup for 24.576 MHz Crystal */
    case 1: next_state = codec.WriteRegisters(RegisterSequences::kMasterClockControl); break;
    /* Setup DAI 1 */
    case 2: next_state = codec.WriteRegisters(kMax98089RegisterSequence_DAI1_Si468x); break;
    /* setup mixers for lineout play */
    case 3: next_state = codec.WriteRegisters(kMax98089RegisterSequence_Lineout); break;
    /* setup power management section */
    case 4: 
      next_state = codec.WriteRegisters(kMax98089RegisterSequence_OutputLineout); 
      codec.active_ = next_state;
      break;
    case 5: 
      next_state = codec.WriteVolumeRegs(); 
      break;
    case 6: 
      if(codec.current_volume_level_ != codec.requested_volume_level_)
        codec.WriteVolumeRegs(); 
      break;
    }

    if(next_state)
      state_++;
  }
}
