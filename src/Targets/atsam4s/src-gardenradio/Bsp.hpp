/*
 *  Copyright 2018-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_TARGETGARDENRADIO_BSP_HPP_
#define MUSICBOX_TARGETGARDENRADIO_BSP_HPP_

#include "ecpp/System/TickTimer.hpp"

#include "sys/application.hpp"
#include "ecpp/Drivers/EA/EA_OLEDM204.hpp"
#include "ecpp/Drivers/BufferedDisplayDriver.hpp"

#include "Psp.hpp"
#include "GardenRadioAudioCodec.hpp"

#include "spi.h"
#include "pio.h"

#undef max
#undef min

namespace sys
{
  using ecpp::Drivers::EAOLEDM204::EAOLEDM204;
  using MusicBox::Targets::ATSAM4S::GardenRadioAudioCodec;
  
  using ::ecpp::Units::MilliSecond;


  class BspGardenRadio : public Psp
  {
  public:
    struct UserInput;

    using Display = MusicBox::UserInterface::Text::TextDisplayDriverFactory<EAOLEDM204<'A'> >;

    static constexpr ioport_pin_t kIOPortPinMaskKey[] = {PIO_PA10, PIO_PA11, PIO_PA12};

    static constexpr ioport_pin_t kIOPortPinDisplayReset = IOPORT_CREATE_PIN(PIOA, 15);

    static constexpr uint32_t kIOPortPinMaskSPI_A = (PIO_PA13A_MOSI | PIO_PA14A_SPCK);
    static constexpr uint32_t kIOPortPinMaskSPI_B = ( PIO_PA9B_NPCS1);

    static constexpr uint_least8_t kSPIChipSelectDisplay = 1;

    Display display_;

    static constexpr BspGardenRadio & getInstance() { return instance_; }
    static constexpr BspGardenRadio & instance()    { return instance_; }

    void Init();
    ::ecpp::System::FlexibleTimeout HandleUserInput(Application & app);

    using Psp::GetConfiguration;

    void PrepareRadioAudio();
    void ShutdownRadio();

    GardenRadioAudioCodec audio_codec_;

    void ChangeVolume(int_fast8_t change);
  private:
    ::ecpp::System::TickTimer key_change_tmr_;

    ButtonState key_state_to_report_[3];
    ButtonState key_state_reported_[3];
    ButtonState key_state_debounced_[3];

    static BspGardenRadio instance_;
    
    ButtonState GetCurrentKeyState(unsigned int idx) const { return (((PIOA->PIO_PDSR) & kIOPortPinMaskKey[idx])) ? ButtonState::Idle : ButtonState::Pressed; }
    bool        CheckKeyDebounced() const                  { return key_change_tmr_.IsExpired(MilliSecond<>(20)); }

    friend void ::PIOA_Handler(void);
  };

  struct BspGardenRadio::UserInput
  {
    enum Request : uint_least8_t { kNone, kClicked, kUp, kDown, kVolUp=kNone,kVolDown=kNone };

    constexpr UserInput(Request k) : key{k} {}
    constexpr operator Request() const { return key; }

    const Request key;
  };


  using Bsp = BspGardenRadio;

}
#endif  /* MUSICBOX_TARGETGARDENRADIO_BSP_HPP_ */