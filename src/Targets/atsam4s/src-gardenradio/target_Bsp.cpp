#include "Bsp.hpp"
#include "System.hpp"
#include "ecpp/Target/Bsp.hpp"
#include "ecpp/Units/Time.hpp"
#include "ecpp/Datatypes.hpp"
#include "pio.h"
#include "delay.h"
#include "app.hpp"
#include "compiler.h"
#include "conf_clock.h"
//#include "sysclk.h"
#include "ioport.h"
#include "adc.h"
#include "pio.h"
#include "delay.h"
#include "config.hpp"

using namespace sys;
using namespace ecpp;
using namespace ecpp::Units;
using namespace ::MusicBox;
using Max98089Defs = ::MusicBox::RegDef::Max98089;


BspGardenRadio BspGardenRadio::instance_;


void BspGardenRadio::Init(void)
{
  Psp::Init();

  /* setup keyboard */
  ioport_set_port_dir(IOPORT_PIOA, kIOPortPinMaskKey[0] | kIOPortPinMaskKey[1] | kIOPortPinMaskKey[2], IOPORT_DIR_INPUT);
  ioport_set_port_mode(IOPORT_PIOA, kIOPortPinMaskKey[0] | kIOPortPinMaskKey[1] | kIOPortPinMaskKey[2], IOPORT_MODE_PULLUP);
  ioport_set_port_sense_mode(IOPORT_PIOA, kIOPortPinMaskKey[0] | kIOPortPinMaskKey[1] | kIOPortPinMaskKey[2], IOPORT_SENSE_BOTHEDGES);

  /* enable interrups for all keyboard pios */
  PIOA->PIO_IER = kIOPortPinMaskKey[0] | kIOPortPinMaskKey[1] | kIOPortPinMaskKey[2];

  /* pull reset pin of display */
  ioport_set_pin_level(kIOPortPinDisplayReset, 0);
  ioport_set_pin_dir(kIOPortPinDisplayReset, IOPORT_DIR_OUTPUT);
  ioport_enable_pin(kIOPortPinDisplayReset);

  /* switch pins to spi mode */
  ioport_set_port_mode(IOPORT_PIOA, kIOPortPinMaskSPI_A, IOPORT_MODE_MUX_A);
  ioport_set_port_mode(IOPORT_PIOA, kIOPortPinMaskSPI_B, IOPORT_MODE_MUX_B);
  ioport_disable_port(IOPORT_PIOA, kIOPortPinMaskSPI_A | kIOPortPinMaskSPI_B);

  /* setup SPI */
  {
    const auto peripheral_clock = sysclk_get_peripheral_hz();

    spi_enable_clock(SPI);
    spi_disable(SPI);
    spi_reset(SPI);
    spi_set_lastxfer(SPI);
    spi_set_master_mode(SPI);
    spi_disable_mode_fault_detect(SPI);
    spi_set_fixed_peripheral_select(SPI);

    spi_set_clock_polarity(SPI, kSPIChipSelectDisplay, 1);
    spi_set_clock_phase(SPI, kSPIChipSelectDisplay, 0);
    spi_set_bits_per_transfer(SPI, kSPIChipSelectDisplay, SPI_CSR_BITS_8_BIT);
    spi_configure_cs_behavior(SPI, kSPIChipSelectDisplay, SPI_CS_KEEP_LOW);

    spi_set_baudrate_div(SPI, kSPIChipSelectDisplay, (peripheral_clock / 1000000));
    spi_set_transfer_delay(SPI, kSPIChipSelectDisplay,
                          peripheral_clock / (int)(1000000 / 0.5),
                          peripheral_clock / (int)(1000000 * 32 / 1.3));
  }

  spi_enable(SPI);

  display_.Init();

  /* Configure and enable interrupt of PIO. */
  NVIC_ClearPendingIRQ(PIOA_IRQn);
  NVIC_SetPriority(PIOA_IRQn, 4);
  NVIC_EnableIRQ(PIOA_IRQn);
}

::ecpp::System::FlexibleTimeout
BspGardenRadio::HandleUserInput(Application &app)
{
  ::ecpp::System::FlexibleTimeout timeout {0};
  unsigned int i;

  for(i = 0; i < ElementCount(kIOPortPinMaskKey); ++i)
  {
    ButtonState s = key_state_reported_[i];

    if ( (key_state_reported_[i] == ButtonState::Idle) && (key_state_to_report_[i] == ButtonState::Pressed))
    {
      key_state_to_report_[i] = ButtonState::Idle;
      key_state_reported_[i]  = ButtonState::Pressed;
      s = ButtonState::Down;
    }
    else if (CheckKeyDebounced())
    {
      /* get current state */
      key_state_debounced_[i] = GetCurrentKeyState(i);

      if (key_state_reported_[i] != key_state_debounced_[i])
      {
        if(key_state_debounced_[i] == ButtonState::Pressed)
          s = ButtonState::Down;
        else
          s = ButtonState::Up;

        key_state_reported_[i] = key_state_debounced_[i];
      }
    }
    else
    {
      timeout = MilliSecond<>(20);
    }

    if(ButtonState::Down == s)
    {
      timeout = MilliSecond<>(20);

      switch(i)
      {
      case 0: app.HandleKey(events::KeyEvent::kConfirm); break;
      case 1: app.HandleKey(events::KeyEvent::kNext);    break;
      case 2: app.HandleKey(events::KeyEvent::kPrev);    break;
      }
    }
  }

  return timeout;
}

void PIOA_Handler(void)
{
  auto & bsp = BspGardenRadio::instance();
  bool poll_ui = false;
  uint32_t reg;
  unsigned int i;

  /* get & clear PIO controller sirq status */
  pio_get_interrupt_status(PIOA);

  /* read all pio pins at once */
  reg = (PIOA->PIO_PDSR);

  if (bsp.CheckKeyDebounced())
  {
    for(i = 0; i < ElementCount(bsp.kIOPortPinMaskKey); ++i)
    {
      ButtonState s = ((reg & bsp.kIOPortPinMaskKey[i])) ? ButtonState::Idle : ButtonState::Pressed;

      if( bsp.key_state_debounced_[i] != s)
      {
        bsp.key_state_debounced_[i] = s;
        bsp.key_change_tmr_.Start();

        if ( (s == ButtonState::Pressed) && bsp.key_state_to_report_[i] == ButtonState::Idle)
        {
          bsp.key_state_to_report_[i] = ButtonState::Pressed;
          poll_ui = true;
        }
      }
    }
  }

  if(poll_ui)
    ::signals::UserInput();
}

namespace ecpp::Target::Bsp
{
  void
  ResetDisplay(DisplayDriver& inst)
  {
    ioport_set_pin_level(BspGardenRadio::kIOPortPinDisplayReset, false);
    delay_ms(1);
    ioport_set_pin_level(BspGardenRadio::kIOPortPinDisplayReset, true);
    delay_ms(1);
  }

  void SendSPI(DisplayDriver &inst, const uint8_t *buffer, size_t buffer_len)
  {
    spi_set_peripheral_chip_select_value(SPI, BspGardenRadio::kSPIChipSelectDisplay);

    while(buffer_len--)
    {
      /* Wait to inject new data */
      while ((spi_read_status(SPI) & SPI_SR_TDRE) == 0);
      spi_write(SPI, *(buffer++), 0 , 0);
    }

    /* wait for transfer to finish */
    while ((spi_read_status(SPI) & SPI_SR_TXEMPTY) == 0);

    /* deselect display */
    spi_set_peripheral_chip_select_value(SPI, 0xF);
  }
}

void 
BspGardenRadio::ShutdownRadio()
{
  audio_codec_.ShutdownAudio();
}