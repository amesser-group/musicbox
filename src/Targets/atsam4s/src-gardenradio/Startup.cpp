/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ecpp/HAL/Atmel/ATSAM4S.hpp"

void Reset_Handler()
{
  /* enable peripheral clock for pio a */
  PMC->PMC_PCER0 =  (1 << ID_PIOA);

  /* configure PA10, the middle button */
  PIOA->PIO_ODR   = PIO_PA10;
  PIOA->PIO_PUER  = PIO_PA10;
  PIOA->PIO_PPDDR = PIO_PA10;
  PIOA->PIO_IFER  = PIO_PA10;
  PIOA->PIO_PER   = PIO_PA10;

  /** When middle button was pressed during power-on
   *  Go to SAM-BA monitor mode */
  if(0 == (PIOA->PIO_PDSR & PIO_PA10))
  {
    volatile uint32_t *pulRom = (uint32_t*)0x00800000;
    int val;

    __asm volatile (
        "ldr %0, [%1] \n"
        "msr msp, %0  \n"
        "ldr pc, [%1, #4] \n"
        : "=r" (val), "+r" (pulRom)
    );
  }

  DefaultReset_Handler();
}




