/*  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_GARDENRADIO_AUDIO_CODEC_HPP_
#define MUSICBOX_GARDENRADIO_AUDIO_CODEC_HPP_
#include <variant>

#include <ecpp/Execution/JobTimer.hpp>
#include "ATSAM4SAudioCodec.hpp"


namespace MusicBox::Targets::ATSAM4S
{
  class GardenRadioAudioCodec : public MusicBox::Targets::ATSAM4S::AudioCodec
  {
  public:
    class RadioMode
    {
    public:
      void TwiReady(GardenRadioAudioCodec& codec);
      constexpr static bool IsShutdown() { return false; }

    protected:
      uint_least8_t state_;
    };

    typedef ::std::variant<Shutdown, RadioMode> ProfileContainer;

    void SetMute(bool muted);

    void PrepareRadioAudio();
    void ShutdownAudio();

    void Poll() {PollTwiComplete.Enqueue();}
  protected:
    bool WriteVolumeRegs();

    ECPP_JOB_TIMER_DECL(PollTwiComplete);

    ProfileContainer current_profile_;
  };

}

#endif