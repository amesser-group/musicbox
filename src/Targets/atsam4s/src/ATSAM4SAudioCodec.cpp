/*  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ATSAM4SAudioCodec.hpp"
#include "System.hpp"

using namespace MusicBox::Targets::ATSAM4S;


using MusicBox::Drivers::MAX98089::Max98089CommonSequences;
using MusicBox::Drivers::MAX98089::Max98089Defs;

const ::std::array<uint8_t, 9> AudioCodec::kMax98089RegisterSequence_DAI1_Si468x
{
  Max98089Defs::REG_DAI1_CLOCK_MODE,
  0x80,                                  /* 48 kHz Samplerate for ALC */
  (0x6000 >> 8) & 0x7F, 0x6000  & 0xFE,  /* Setup PLL for 48 khz samplerate from 24.576 Mhz Crystal and DHF=0 */
                                         /* DAI1 Configuration */
  0x90, /* I2S: Master, Second rising edge */
  0x02, /* I2S: BCLK = 48*LRCLK = 2.304 MHz-> suitable for 24bit output of si468x */// todo
  0x41, /* DAI1: Use Port S1, Playback enable */
  0x00,
  0x80 /* Set mode 1 for music mode */
};



void
AudioCodec::Shutdown::TwiReady(AudioCodec& codec)
{
  if(codec.active_)
  {
    if(written_)
      codec.active_ = false;
    else
      written_ = codec.WriteRegisters(Max98089CommonSequences::kShutdown);
  }
}
