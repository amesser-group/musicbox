#ifndef MUSICBOX_ECPPCONFIG_HPP_
#define MUSICBOX_ECPPCONFIG_HPP_

#include "ecpp/Units/Ticks.hpp"
#include "ecpp/HAL/Atmel/ATSAM4S/Exceptions.hpp"

namespace ecpp::Config
{  
  using SystemTick = ::ecpp::Units::Tick<uint32_t, ::ecpp::Units::DecimalScale<-2> >;

  extern volatile SystemTick systick_;

  static inline SystemTick GetSystemTick()
  { 
    return SystemTick(systick_) ;
  }

}

#endif