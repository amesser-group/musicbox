/*
 *  Copyright 2019 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the MusicBox project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef BSP_HSMCIDRIVER_HPP_
#define BSP_HSMCIDRIVER_HPP_

#include "Hsmci/HsmciHal.hpp"
#include "Platform.hpp"
#include "Bsp.hpp"

namespace MusicBox
{
  class HsmciDriver;

  class HsmciDriverState
  {
  public:
    HsmciDriverState();

  protected:
    /* poll for pending actions */
    virtual void poll(HsmciState state);
    virtual void timeout();

    friend class HsmciDriver;
  };

  class HsmciDriverStatePowerOn : public HsmciDriverState
  {
  protected:
    void poll(HsmciState state) override;
  };

  class HsmciDriverStateWaitInsertCard : public HsmciDriverState
  {
  public:
    HsmciDriverStateWaitInsertCard();

  protected:
    void timeout() override;
  };

  class HsmciDriverStatePowerupCard : public HsmciDriverState
  {
  public:
    HsmciDriverStatePowerupCard();

  protected:
    void poll(HsmciState state) override;
    void timeout() override;
  };

  enum class SDCardState : uint_least8_t
  {
    NotPlugged = 0,
    Initializing,
    Ready,
    Error,
  };

  struct HsmciCardState
  {
    SDCardState    CardState   {SDCardState::NotPlugged};

    uint_least8_t  CardVersion   {0};
    uint_least8_t  HighCapacity  {0};
    uint_least32_t BlockCount    {0};
    uint_least32_t RCA           {0};
  };

  class HsmciDriverStateInitCard_SendClocks : public HsmciDriverState
  {
  public:
    HsmciDriverStateInitCard_SendClocks();
  protected:
    void poll(HsmciState state) override;
  };

  class HsmciDriverStateInitCard_GoIdle : public HsmciDriverState
  {
  public:
    HsmciDriverStateInitCard_GoIdle();
  protected:
    void poll(HsmciState state) override;
  };

  class HsmciDriverStateInitCard_SendIfCond : public HsmciDriverState
  {
  public:
    HsmciDriverStateInitCard_SendIfCond();
  protected:
    void poll(HsmciState state) override;
  };

  class HsmciDriverStateInitCard_SendOpCond : public HsmciDriverState
  {
  public:
    HsmciDriverStateInitCard_SendOpCond();
  protected:
    void poll(HsmciState state) override;
    void timeout() override;
  private:
    void sendCommand();
  };

  class HsmciDriverStateInitCard_AllSendCid : public HsmciDriverState
  {
  public:
    HsmciDriverStateInitCard_AllSendCid();
  protected:
    void poll(HsmciState state) override;
  private:
    void processResponse() const;
  };

  class HsmciDriverStateInitCard_SendRCA : public HsmciDriverState
  {
  public:
    HsmciDriverStateInitCard_SendRCA();
  protected:
    void poll(HsmciState state) override;
  };

  class HsmciDriverStateInitCard_ReadCSD : public HsmciDriverState
  {
  public:
    HsmciDriverStateInitCard_ReadCSD();
  protected:
    void poll(HsmciState state) override;
  private:
    void processResponse();
  };

  class HsmciDriverStateIdle : public HsmciDriverState
  {
  public:
    HsmciDriverStateIdle();
  };

  class HsmciDriverStateError : public HsmciDriverState
  {
  public:
    HsmciDriverStateError();
  protected:
    void poll(HsmciState state) override;
    void timeout() override;
  };

  class HsmciDriver : public HsmciHal
  {
  public:
    void start() { pollAsync(); }

    constexpr const HsmciCardState & getCardState() const { return CardState; }

    bool writeBlock(uint32_t sector, const void* buf, uint32_t num_sect);
    bool readBlock(uint32_t sector, void* buf);

  protected:
    void pollAsync();

    HsmciDriverState & getState() { return *reinterpret_cast<HsmciDriverState*>(&State);}

    template<typename STATE, typename... ARGS>
    void changeState(ARGS ... args) {
      Timeout.Stop();
      new (& State) STATE(args...);
      pollAsync();
    }

    void startTimerMs(unsigned long timeout);
    void stopTimer();

    /* union of all states to share memory */
    union {
      HsmciDriverState                     Common;
      HsmciDriverStatePowerOn              PowerOn;
      HsmciDriverStateWaitInsertCard       WaitInsertCard;
      HsmciDriverStatePowerupCard          PowerupCard;
      HsmciDriverStateInitCard_SendClocks  InitCard_SendClocks;
      HsmciDriverStateInitCard_GoIdle      InitCard_GoIdle;
      HsmciDriverStateInitCard_SendIfCond  InitCard_SendIfCond;
      HsmciDriverStateInitCard_SendOpCond  InitCard_SendOpCond;
      HsmciDriverStateInitCard_SendRCA     InitCard_SendRCA;
      HsmciDriverStateInitCard_ReadCSD     InitCard_ReadCSD;
      HsmciDriverStateInitCard_AllSendCid  InitCard_AllSendCid;
      HsmciDriverStateIdle                 Idle;
      HsmciDriverStateError                Error;
    } State { .PowerOn = {} };

    ECPP_JOB_DECL(Poll);
    ECPP_JOB_TIMER_DECL(Timeout);

    HsmciCardState   CardState;

    friend class HsmciDriverState;
    friend class HsmciDriverStatePowerOn;
    friend class HsmciDriverStateWaitInsertCard;
    friend class HsmciDriverStatePowerupCard;
    friend class HsmciDriverStateInitCard_SendClocks;
    friend class HsmciDriverStateInitCard_GoIdle;
    friend class HsmciDriverStateInitCard_SendIfCond;
    friend class HsmciDriverStateInitCard_SendOpCond;
    friend class HsmciDriverStateInitCard_AllSendCid;
    friend class HsmciDriverStateInitCard_SendRCA;
    friend class HsmciDriverStateIdle;
    friend class HsmciDriverStateInitCard_ReadCSD;
    friend class HsmciDriverStateError;

    friend void ::HSMCI_Handler(void);
  };
}

namespace signals
{
  void Changed(const ::MusicBox::HsmciDriver& driver);
}

#endif