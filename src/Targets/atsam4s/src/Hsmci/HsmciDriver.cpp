/*
 *  Copyright 2019 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the MusicBox project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "Hsmci/HsmciDriver.hpp"
#include "Bsp.hpp"
#include "System.hpp"

#include "hsmci.h"
#include "pmc.h"

using namespace MusicBox;

void
HsmciDriver::pollAsync()
{
  Poll.Enqueue();
}

ECPP_JOB_DEF(HsmciDriver, Poll)
{ /* initialize the hsmci hardware for use */
  getState().poll(getHsmciState());
}

ECPP_JOB_DEF(HsmciDriver, Timeout)
{
  getState().timeout();
}

void
HsmciDriver::startTimerMs(unsigned long timeout)
{
  Timeout.Start(MilliSecond<>(timeout));
}

void
HsmciDriver::stopTimer()
{
  Timeout.Stop();
}


HsmciDriverState::HsmciDriverState()
{
  ::signals::Changed(container_of(HsmciDriver, State.Common, *this));
}

void HsmciDriverState::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.Common, *this);

  if( HsmciState::CommandFailed == state )
    m.changeState<HsmciDriverStateError>();
}

void HsmciDriverState::timeout()
{
  auto & m = container_of(HsmciDriver, State.WaitInsertCard, *this);
  auto & bsp = ::sys::bsp();

  if (!bsp.IsCardPlugged())
    m.changeState<HsmciDriverStateWaitInsertCard>();
}

void
HsmciDriverStatePowerOn::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.WaitInsertCard, *this);

  hsmci_init();
  m.changeState<HsmciDriverStateWaitInsertCard>();
}

HsmciDriverStateWaitInsertCard::HsmciDriverStateWaitInsertCard() : HsmciDriverState()
{
  auto & bsp = ::sys::bsp();
  HsmciDriver & m = container_of(HsmciDriver, State.PowerupCard, *this);

  bsp.SetCardPowerState( false);

  /* initialize card state to poweron values */
  new (&m.CardState) decltype(m.CardState)();

  m.startTimerMs(1000);
}

void
HsmciDriverStateWaitInsertCard::timeout()
{
  auto & bsp = ::sys::bsp();
  HsmciDriver & m = container_of(HsmciDriver, State.WaitInsertCard, *this);

  if (bsp.IsCardPlugged())
    m.changeState<HsmciDriverStatePowerupCard>();
  else
    m.startTimerMs(1000);
}

HsmciDriverStatePowerupCard::HsmciDriverStatePowerupCard() : HsmciDriverState()
{
  auto & bsp = ::sys::bsp();
  HsmciDriver & m = container_of(HsmciDriver, State.PowerupCard, *this);

  bsp.SetCardPowerState(true);
  m.startTimerMs(100);
}

void
HsmciDriverStatePowerupCard::poll(HsmciState state)
{
  auto & bsp = ::sys::bsp();
  HsmciDriver & m = container_of(HsmciDriver, State.PowerupCard, *this);

  if (!bsp.IsCardPlugged())
    m.changeState<HsmciDriverStateWaitInsertCard>();
}

void
HsmciDriverStatePowerupCard::timeout()
{
  auto & bsp = ::sys::bsp();
  HsmciDriver & m = container_of(HsmciDriver, State.PowerupCard, *this);

  if (bsp.IsCardPlugged())
    m.changeState<HsmciDriverStateInitCard_SendClocks>();
  else
    m.changeState<HsmciDriverStateWaitInsertCard>();
}

HsmciDriverStateInitCard_SendClocks::HsmciDriverStateInitCard_SendClocks() : HsmciDriverState()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendClocks, *this);
  auto & bsp = ::sys::bsp();

  m.CardState.CardState = SDCardState::Initializing;

  bsp.SetCardSpeed(200000);
  m.startSendClocks();
}

void HsmciDriverStateInitCard_SendClocks::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendClocks, *this);

  if(m.isReady())
      m.changeState<HsmciDriverStateInitCard_GoIdle>();
}

HsmciDriverStateInitCard_GoIdle::HsmciDriverStateInitCard_GoIdle() : HsmciDriverState()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_GoIdle, *this);
  m.startCommand(SDMMC_MCI_CMD0_GO_IDLE_STATE, 0);
}

void HsmciDriverStateInitCard_GoIdle::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendClocks, *this);

  if (HsmciState::Success == state)
    m.changeState<HsmciDriverStateInitCard_SendIfCond>();
  else
    HsmciDriverState::poll(state);
}

HsmciDriverStateInitCard_SendIfCond::HsmciDriverStateInitCard_SendIfCond() : HsmciDriverState()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendIfCond, *this);
  m.startCommand(SD_CMD8_SEND_IF_COND, SD_CMD8_PATTERN | SD_CMD8_HIGH_VOLTAGE);
}

void HsmciDriverStateInitCard_SendIfCond::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendIfCond, *this);

  if (HsmciState::Success == state)
  {
    uint32_t res;
    res = hsmci_get_response();

    if (res == 0xFFFFFFFF)
    {
      m.CardState.CardVersion = 1;
    }
    else
    {
      m.CardState.CardVersion = 2;

      /* check if card supports high voltage operation */
      if ((res & (SD_CMD8_MASK_PATTERN | SD_CMD8_MASK_VOLTAGE)) != (SD_CMD8_PATTERN | SD_CMD8_HIGH_VOLTAGE))
        m.changeState<HsmciDriverStateError>();
      else
        m.changeState<HsmciDriverStateInitCard_SendOpCond>();
    }
  }
  else
  {
    HsmciDriverState::poll(state);
  }

}


HsmciDriverStateInitCard_SendOpCond::HsmciDriverStateInitCard_SendOpCond() : HsmciDriverState()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendOpCond, *this);

  /* we must disable power save during acmd41, otherwise the card init might fail
   * if we do not send acmd41 in 50ms intervals */
  HSMCI->HSMCI_CR = HSMCI_CR_PWSDIS;

  sendCommand();
  m.startTimerMs(1000);
}

void HsmciDriverStateInitCard_SendOpCond::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendOpCond, *this);

  if(HsmciState::Success == state)
  {
    uint32_t res;
    res = hsmci_get_response();

    if (res & OCR_POWER_UP_BUSY)
    {
      if (res & OCR_CCS)
          m.CardState.HighCapacity = 1;

      /* re-enable power save mode */
      HSMCI->HSMCI_CR = HSMCI_CR_PWSEN;

      m.changeState<HsmciDriverStateInitCard_AllSendCid>();
    }
    else
    {
      sendCommand();
    }
  }
  else
  {
    HsmciDriverState::poll(state);
  }
}


void
HsmciDriverStateInitCard_SendOpCond::timeout()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendOpCond, *this);
  m.changeState<HsmciDriverStateError>();
}

void
HsmciDriverStateInitCard_SendOpCond::sendCommand()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendOpCond, *this);
  uint32_t arg;

  arg = OCR_VDD_32_33;

  if (m.CardState.CardVersion == 2)
    arg |= SD_ACMD41_HCS;

  m.startAppCommand(SD_MCI_ACMD41_SD_SEND_OP_COND, arg);
}

HsmciDriverStateInitCard_AllSendCid::HsmciDriverStateInitCard_AllSendCid() : HsmciDriverState()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_AllSendCid, *this);
  m.startCommand(SDMMC_CMD2_ALL_SEND_CID, 0);
}

void
HsmciDriverStateInitCard_AllSendCid::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_AllSendCid, *this);

  if(HsmciState::Success == state)
  {
    processResponse();
    m.changeState<HsmciDriverStateInitCard_SendRCA>();
  }
  else
  {
    HsmciDriverState::poll(state);
  }
}

void
HsmciDriverStateInitCard_AllSendCid::processResponse() const
{
  //HsmciDriver & m = container_of(HsmciDriver, State.InitCard_AllSendCid, *this);
  uint8_t buf[16];

  hsmci_get_response_128(buf);
}

HsmciDriverStateInitCard_SendRCA::HsmciDriverStateInitCard_SendRCA() : HsmciDriverState()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendRCA, *this);
  m.startCommand(SD_CMD3_SEND_RELATIVE_ADDR, 0);
}

void HsmciDriverStateInitCard_SendRCA::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_SendRCA, *this);
  uint32_t res;

  if (HsmciState::Success == state)
  {
    res = hsmci_get_response();
    m.CardState.RCA = (res & 0xFFFF0000);
    m.changeState<HsmciDriverStateInitCard_ReadCSD>();
  }
  else
  {
    HsmciDriverState::poll(state);
  }
}

HsmciDriverStateInitCard_ReadCSD::HsmciDriverStateInitCard_ReadCSD() : HsmciDriverState()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_ReadCSD, *this);
  m.startCommand(SDMMC_MCI_CMD9_SEND_CSD, m.CardState.RCA);
}

void HsmciDriverStateInitCard_ReadCSD::poll(HsmciState state)
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_ReadCSD, *this);

  if (HsmciState::Success == state)
  {
    processResponse();
    m.changeState<HsmciDriverStateIdle>();
  }
  else
  {
    HsmciDriverState::poll(state);
  }
}

void
HsmciDriverStateInitCard_ReadCSD::processResponse()
{
  HsmciDriver & m = container_of(HsmciDriver, State.InitCard_ReadCSD, *this);

  uint8_t csd[16];
  uint8_t version;

  hsmci_get_response_128(csd);

  version = csd[0] >> 6;

  if (version  == SD_CSD_VER_1_0)
  {
    uint16_t csize, csize_mult;
    uint8_t read_bl_len;

    read_bl_len = csd[5] & 0x0F;

    csize =          0x3 &  csd[6];
    csize = (csize << 8) |  csd[7];
    csize = (csize << 2) | (csd[8] >> 6);

    csize_mult = 0x3 & csd[9];
    csize_mult = (csize_mult << 1) | (csd[10] >> 7);

    m.CardState.BlockCount = (csize+1) * (1 << (csize_mult + 2)) / (1 << (read_bl_len - 9));
  }
  else if (version  == SD_CSD_VER_2_0)
  {
    uint32_t csize;

    csize =         0x3F & csd[7];
    csize = (csize << 8) | csd[8];
    csize = (csize << 8) | csd[9];

    m.CardState.BlockCount    = (csize + 1) * 1024;
  }
}

HsmciDriverStateIdle::HsmciDriverStateIdle() : HsmciDriverState()
{
  auto & m = container_of(HsmciDriver, State.Idle, *this);
  auto & bsp = ::sys::bsp();

  /* switch speed to 1mhz */
  bsp.SetCardSpeed(1000000);

  m.CardState.CardState = SDCardState::Ready;

  /* periodically poll for card state */
  m.startTimerMs(1000);
}


HsmciDriverStateError::HsmciDriverStateError() : HsmciDriverState()
{
  auto & bsp = ::sys::bsp();
  HsmciDriver & m = container_of(HsmciDriver, State.Error, *this);

  m.CardState.CardState = SDCardState::Error;
  bsp.SetCardPowerState( false);
  m.startTimerMs(1000);
}

void HsmciDriverStateError::poll(HsmciState state)
{

}

void HsmciDriverStateError::timeout()
{
  HsmciDriver & m = container_of(HsmciDriver, State.Error, *this);
  auto & bsp = ::sys::bsp();

  if (!bsp.IsCardPlugged())
    m.changeState<HsmciDriverStateWaitInsertCard>();
  else
    m.startTimerMs(1000);

}


extern "C" void HSMCI_Handler(void)
{
  auto & g = ::sys::globals();

  /* disable all interrupts */
  HSMCI->HSMCI_IDR = 0xFFFFFFFF;

  g.sdcard_driver_.pollAsync();
}

bool
HsmciDriver::readBlock(uint32_t sector, void* buf)
{
  HsmciState state;

  if (SDCardState::Ready != CardState.CardState)
    return false;

  if (sector >= CardState.BlockCount)
    return false;

  /* select card */
  startCommand(SDMMC_CMD7_SELECT_CARD_CMD, CardState.RCA);

  while(HsmciState::CommandPending == (state = getHsmciState()))
    pmc_enable_sleepmode(0);

  if (HsmciState::CommandFailed != state)
  {
    /* send read command  */
    if (CardState.HighCapacity)
      startDMACommand(SDMMC_CMD17_READ_SINGLE_BLOCK, sector, buf, 1);
    else
      startDMACommand(SDMMC_CMD17_READ_SINGLE_BLOCK, sector * 512, buf, 1);

    do {
      pmc_enable_sleepmode(0);
      state = getHsmciState();
    } while((HsmciState::CommandPending == state) || (HsmciState::TransferPending == state));
  }

  /* deselect card, dont expect response */
  startCommand(7 | SDMMC_CMD_NO_RESP, 0);

  while(HsmciState::CommandPending == getHsmciState())
    pmc_enable_sleepmode(0);

  return HsmciState::Success == state;
}

bool
HsmciDriver::writeBlock(uint32_t sector, const void* buf, uint32_t num_sect)
{
  HsmciState state;

  if (SDCardState::Ready != CardState.CardState)
    return false;

  if (sector >= CardState.BlockCount)
    return false;

  /* select card */
  startCommand(SDMMC_CMD7_SELECT_CARD_CMD, CardState.RCA);

  while(HsmciState::CommandPending == (state = getHsmciState()))
    pmc_enable_sleepmode(0);

  if (HsmciState::CommandFailed != state)
  {
    startCommand(SDMMC_CMD16_SET_BLOCKLEN, 512);

    while(HsmciState::CommandPending == (state = getHsmciState()))
      pmc_enable_sleepmode(0);
  }

  if (HsmciState::CommandFailed != state)
  {
    const uint8_t *bbuf = reinterpret_cast<const uint8_t*>(buf);

    for(; num_sect>0; --num_sect, bbuf+=512)
    {
      /* send write command  */
      if (CardState.HighCapacity)
        startDMACommand(SDMMC_CMD24_WRITE_BLOCK, sector, const_cast<uint8_t*>(bbuf), 1);
      else
        startDMACommand(SDMMC_CMD24_WRITE_BLOCK, sector * 512, const_cast<uint8_t*>(bbuf), 1);

      do {
        pmc_enable_sleepmode(0);
        state = getHsmciState();
      } while((HsmciState::CommandPending == state) || (HsmciState::TransferPending == state));
    }
  }

  /* deselect card, dont expect response */
  startCommand(7 | SDMMC_CMD_NO_RESP, 0);

  while(HsmciState::CommandPending == getHsmciState())
    pmc_enable_sleepmode(0);

  return HsmciState::Success == state;
}