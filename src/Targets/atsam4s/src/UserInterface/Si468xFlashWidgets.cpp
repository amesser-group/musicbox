/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "UserInterface/Si468xFlashWidgets.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface;

using MusicBox::Drivers::SI468x::Actions::Si468xUpdateFirmwareAction;
using MusicBox::Drivers::SI468x::Si468xOperationMode;

Si468xSelectFirmwareWidget::Si468xSelectFirmwareWidget()
  : WidgetEnvironment::Widget {}, Dir {}
{
  auto & v = sys::GetVolumeManager(*this);

  v.mountVolume();

  Dir = ::std::move(ElmChanDirectory("/"));
  v.unmountVolume();

  findNextFirmwareFile();
}

void
Si468xSelectFirmwareWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  painter[0].CenterText("Radio Update");

  if (File.getSize() > 0)
  {
    painter[1].IPrintF("Datei: %s", DirEnt.fname);
    painter[2].CenterText("Einspielen");
  }
  else
  {
    painter[2].CenterText("Zurück");
  }

}

void
Si468xSelectFirmwareWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  switch(user_input)
  {
  case events::KeyEvent::kDown:
    findNextFirmwareFile();
    break;
  case events::KeyEvent::kClicked:
    if (File.getSize() > 0)
    {
      auto a = ::sys::globals().si468x_actions_.Action<Si468xUpdateFirmwareAction>();

      if ( 0 == strncasecmp(DirEnt.fname, "dab", 3))
      {
        if( a.Aquire(File, ::sys::bsp().kSi468x_FlashAddr_DABFirmware) != nullptr)
          ctx.EmplaceWidget<Si468xShowFlashProgressWidget>();
      }
      else if ( 0 == strncasecmp(DirEnt.fname, "fmhd", 4))
      {
        if( a.Aquire(File, ::sys::bsp().kSi468x_FlashAddr_FMFirmware) != nullptr)
          ctx.EmplaceWidget<Si468xShowFlashProgressWidget>();
      }
    }
    else
    {
      ctx.PopWidget();
    }
    break;
  default:
    WidgetEnvironment::Widget::HandleEvent(user_input, ctx);
    break;
  }
}

void
Si468xSelectFirmwareWidget::findNextFirmwareFile()
{
  bool found;

  File.close();

  while( (found = Dir.read(DirEnt) ))
  {
    auto len = strnlen(DirEnt.fname, sizeof(DirEnt.fname));
    char buf[1 + 12 + 1];

    if ( (0 != strncasecmp(DirEnt.fname + len - 4, ".bin", 4)) &&
          (0 != strncasecmp(DirEnt.fname + len - 4, ".bif", 4)) )
      continue;

    if ( (0 != strncasecmp(DirEnt.fname, "dab", 3)) &&
          (0 != strncasecmp(DirEnt.fname, "fmhd", 4)) )
      continue;

    /* build pathname */
    strncpy(buf + 1, DirEnt.fname, 12);
    buf[0] = '/'; buf[13] = '\0';

    ElmChanFile f(buf);

    if(f.getSize() <= 0)
      continue;

    /* found firmware file canditate */
    File = ::std::move(f);
    break;
  }

  if(!found)
    Dir.rewind();
}

Si468xShowFlashProgressWidget::~Si468xShowFlashProgressWidget()
{
  auto a = ::sys::globals().si468x_actions_.Action<Si468xUpdateFirmwareAction>();
  a.Release();
}

void
Si468xShowFlashProgressWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  auto p = ::sys::globals().si468x_actions_.Action<Si468xUpdateFirmwareAction>().Get();

  painter[0].CenterText("Schreibe FW");

  if (p != nullptr)
  {
    auto progress = p->GetProgress();

    if(progress.num >= progress.denom)
    {
      painter[2].CenterText("Zurück");
      painter[3].IPrintF(" %6lu Byte", progress.denom);
    }
    else
    {
      painter[2].DrawProgress(progress);
      painter[3].IPrintF(" %6lu/%lu", progress.num, progress.denom);

      /* keep the widget until finished */
      ::sys::globals().user_interface_.ResetInteractionTime();
    }
  }
  else
  {
    painter[2].CenterText("Zurück");
  }
}

void
Si468xShowFlashProgressWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  switch(user_input)
  {
  case events::KeyEvent::kClicked:
    {
      auto p = ::sys::globals().si468x_actions_.Action<Si468xUpdateFirmwareAction>().Get();

      if ( (p == nullptr) || (p->GetProgress().num >= p->GetProgress().denom))
        ctx.PopWidget();
    }
    break;
  default:
    WidgetEnvironment::Widget::HandleEvent(user_input, ctx);
    break;
  }
}