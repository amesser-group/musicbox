/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_USERINTERFACE_TARGET_HPP_
#define MUSICBOX_USERINTERFACE_TARGET_HPP_

#include "UserInterface/Text/TextPainter.hpp"
#include "UserInterface/events.hpp"
#include "Bsp.hpp"
#include <variant>

namespace MusicBox::UserInterface
{
  class WidgetEnvironment
  {
  public:
    using Painter = ::sys::Bsp::Display::Painter;

    class Event;

    class Context;

    class Widget;

    constexpr Painter CreatePainter() const { return ::sys::Bsp::instance().display_.CreatePainter(); }
    

  protected:
    typedef ::std::variant<events::Refresh, 
                           events::KeyEvent, 
                           events::UserInteractionTimeout, 
                           events::StatusChange,
                           events::OperationModeChanged, 
                           events::DatabaseChanged> EventValue;
  };

  namespace Model
  {
    class ContextTargetMixin
    {
    public:
      void SpawnMainMenu()     const;
      void SpawnSettingsMenu() const;
    };
  };

};

#endif 