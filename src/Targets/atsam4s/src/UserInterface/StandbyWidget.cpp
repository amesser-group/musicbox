/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the musicbox firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "UserInterface/Widgets.hpp"
#include "System.hpp"

using namespace MusicBox::UserInterface::Model;

using ::ecpp::ArrayString;

void
StandByWidget::Draw(WidgetEnvironment::Painter &painter) const
{  
  if (sys::globals().user_interface_.IsUserIdle())
  {
#if MUSICBOX_IDLEDISPLAY
    ArrayString<ecpp::StringEncodings::Utf8, 16> buffer;
    auto t = ::ecpp_sys::Chrono::LocalClock::now().time();

    buffer.IPrintF("%02d:%02d", t.hours(), t.minutes());

    painter[1].CenterText(buffer);

    {
      const Alarm::Setpoint sp = ::sys::globals().wakealarms_.FindNextAlarm();

      if(sp.IsEnabled())
      {
        uint_fast8_t offset = 16;
        for(uint8_t mask = (sp.mask & 0x7F); mask != 0; mask>>= 1, offset-=2);

        buffer.IPrintF("((%.2s, %02d:%02d))", kDayNames+offset, sp.hour, sp.minute);

        painter[3].CenterText(buffer);
      }
    }
#endif
  }
  else
  {
    painter[1].Draw((*actions_.GetSelectedItem()), true);
  }
}
