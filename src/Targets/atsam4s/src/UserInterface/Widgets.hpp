/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_UI_WIDGETS_HPP_
#define MUSICBOX_UI_WIDGETS_HPP_

#include "UserInterface/Model/Widgets.hpp"

#include "UserInterface/TargetWidgets.hpp"
#include "UserInterface/Si468xFlashWidgets.hpp"

namespace MusicBox::UserInterface
{
  using WidgetStackItem = ::std::variant<Model::DummyWidget, Model::IdleWidget, 
                                         Model::ActionListWidget, Model::FilterableActionListWidget,
                                         Model::ServiceListWidget,
                                         SDCardInfoWidget, FileSystemWidget, 
                                         Model::FMTuningWidget,
                                         Model::DABChannelScanWidget, 
                                         Model::Si468xAutoAntCapTuningWidget, Model::Si468xManualAntCapTuningWidget,
                                         Si468xSelectFirmwareWidget, Si468xShowFlashProgressWidget,
                                         Model::AlarmListWidget,
                                         Model::WakeAlarmSettingsWidget,
                                         Model::StandByWidget>;

}

#endif