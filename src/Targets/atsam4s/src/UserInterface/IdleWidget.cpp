/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <type_traits>

#include "ecpp/ArrayString.hpp"
#include "ecpp_sys/Chrono.hpp"
#include "System.hpp"
#include "UserInterface/UserInterface.hpp"


using namespace ::MusicBox::UserInterface::Model;

using ::ecpp::ArrayString;
using ::MusicBox::Drivers::SI468x::Si468xOperationMode;

void
IdleWidget::Draw(WidgetEnvironment::Painter &painter) const
{  
  const auto & mgr = sys::GetSi468xDriver(painter);
  // const auto & fm = mgr.getStatusManager();
  const auto &  s = mgr.GetCurrentService();
  const auto mode = mgr.getMode();
  auto user_idle = sys::globals().user_interface_.IsUserIdle();

  if(mode == Si468xOperationMode::FMRadio)
  {
    const auto & rds = mgr.getRDSState();
    const auto freq  = mgr.getRadioManager().getTunedFreq();

    ArrayString<ecpp::StringEncodings::Utf8, 16> rds_name = rds.program_service_name;

    painter[0].IPrintF(u8"FM %3u.%02u MHz %2d dBµV", static_cast<unsigned>(freq/1000), static_cast<unsigned>(freq%1000/10),
                          static_cast<int>(mgr.getRadioManager().getRssi().raw().value));

    if ( s.Name.CountCharacters() > 0)
    {
      painter[1].CenterText(s.Name);

      if (rds.isProgramServiceNameValid())
      {
        if (s.Name != rds_name.Trim())
          painter[2].CenterText(rds_name);
      }
    }
    else if (rds.isProgramServiceNameValid())
    {
      painter[1].CenterText(rds_name);
    }
    else
    {
      painter[1].CenterText("<Unbekannt>");
    }
  }
  else if(mode == Si468xOperationMode::DABRadio)
  {
    const auto freq  = mgr.getRadioManager().getTunedFreq();

    painter[0].IPrintF("DAB     %2d dBµV", static_cast<int>(mgr.getRadioManager().getRssi().raw().value));

    if (s.Name.CountCharacters() > 0)
      painter[1].CenterText(s.Name);
    else
      painter[1].CenterText("<Unbekannt>");

    painter[2].IPrintF("%3d.%03d MHz", freq/1000, freq%1000);
  }
  else
  {
    painter[1].CenterText("<Kein Kanal>");
  }

  if (!user_idle)
  {
    painter[3].Draw(*quick_actions_.GetSelectedItem(), true);
  }
  else if (sys::globals().operating_state() != OperatingState::kStandBy)
  {
    auto lt  = ::ecpp_sys::Chrono::LocalClock::now().time();
    ::ecpp::Chrono::Timepoint<::ecpp::Chrono::UtcClockTag, ::ecpp::Chrono::PosixBrokenDownTime> gmt {::ecpp_sys::Chrono::SystemClock::now()};

    painter[3].IPrintF(u8"%02d:%02d (%02d:%02d GMT)", lt.hours(), lt.minutes(), gmt.time().hours(), gmt.time().minutes());
  }
}

