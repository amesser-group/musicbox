#ifndef MUSIBOX_WIDGETS_HPP_
#define MUSIBOX_WIDGETS_HPP_

#include "UserInterface/WidgetEnvironment.hpp"
#include "ecpp/Ui/Text/ListWidget.hpp"
#include "ecpp/Ui/Text/ActionItem.hpp"
#include "ecpp/UserInterface/Text/SettingListWidget.hpp"

#include <array>
#include <vector>
#include <functional>

namespace MusicBox::UserInterface
{
  template<typename SettingListModel>
  using SettingListWidget = ::ecpp::UserInterface::Text::SettingListWidget<SettingListModel, WidgetEnvironment>;

  class SDCardInfoWidget : public WidgetEnvironment::Widget
  {
  public:
    SDCardInfoWidget();
    SDCardInfoWidget(const char* title) : SDCardInfoWidget() {}

    ~SDCardInfoWidget();

    void Draw(WidgetEnvironment::Painter &painter) const override;
    void HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx) override;

  private:
    uint_least8_t Selection      {0};
    uint_fast8_t  updater_found_ {false};
  };

  class FileSystemWidget : public WidgetEnvironment::Widget
  {
  public:
    FileSystemWidget();
    FileSystemWidget(const char* title) : FileSystemWidget() {}

    ~FileSystemWidget() override;

    void Draw(WidgetEnvironment::Painter &painter) const override;
    void HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx) override;
  };
};

#endif