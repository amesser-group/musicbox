
#include "UserInterface/UserInterface.hpp"
#include "System.hpp"
#include "ecpp/String.hpp"
#include "ecpp/StringOperations.hpp"
#include "updater/UpdaterRunner.hpp"

#include <string.h>
#include <experimental/array>
#include <array>
#include <type_traits>


using MusicBox::Drivers::SI468x::Si468xOperationMode;
using MusicBox::Drivers::SI468x::TunerStateType;

using namespace MusicBox::Database;
using namespace MusicBox::UserInterface;

using namespace ecpp;
using namespace std;


SDCardInfoWidget::SDCardInfoWidget() : WidgetEnvironment::Widget()
{
  auto & vmgr = ::sys::GetVolumeManager(*this);
  vmgr.mountVolume();
  updater_found_ = (vmgr.openFile("/updater.bin").getSize() > 0);
}

SDCardInfoWidget::~SDCardInfoWidget()
{
  auto & vmgr = ::sys::GetVolumeManager(*this);
  vmgr.unmountVolume();
}

void
SDCardInfoWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  auto & cs = ::sys::globals().sdcard_driver_.getCardState();

  char buf[20];

  painter[0].CenterText("SD Karte");

  snprintf(buf, sizeof(buf), "Größe: %5lu MB", cs.BlockCount / 2 / 1024);
  painter[1].PutText(buf);

  painter[2].CreateField( {1,0,18,1}).PutText("Zurück");

  if (updater_found_)
    painter[3].CreateField({1, 0, 18, 1 }).PutText("Fw-Update");

  if(Selection < 2)
  {
    painter[2 + Selection][0]  = '[';
    painter[2 + Selection][19] = ']';
  }

}

void
SDCardInfoWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  switch(user_input)
  {
  case events::KeyEvent::kUp:
    if (Selection > 0)
      Selection--;

    if((Selection == 1) && (!updater_found_))
      Selection = 0;
    break;
  case events::KeyEvent::kDown:
    if (Selection < 1)
      Selection++;

    if((Selection == 1) && (!updater_found_))
      Selection = 0;
    break;
  case events::KeyEvent::kClicked:
    if (Selection == 0)
      ctx.PopWidget();
    else if (Selection == 1)
      UpdaterRunner().run();
    break;
  default:
    WidgetEnvironment::Widget::HandleEvent(user_input, ctx);
    break;
  }

}

FileSystemWidget::FileSystemWidget() : WidgetEnvironment::Widget()
{
  auto & vmgr = ::sys::GetVolumeManager(*this);
  vmgr.mountVolume();
}

FileSystemWidget::~FileSystemWidget()
{
  auto & vmgr = ::sys::GetVolumeManager(*this);
  vmgr.unmountVolume();
}

void
FileSystemWidget::Draw(WidgetEnvironment::Painter &painter) const
{
  auto & vmgr = ::sys::GetVolumeManager(*this);

  painter[0].CenterText("Dateien");

  {
    auto dir = vmgr.openDir("/");
    decltype(dir)::DirEntType ent;
    int r;

    for(r=1; (r < 4) && dir.read(ent); ++r)
    {
      painter[r].PutText(ent.fname);
    }
  }
}

void
FileSystemWidget::HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx)
{
  switch(user_input)
  {
  case events::KeyEvent::kClicked:
    ctx.PopWidget();
  default:
    WidgetEnvironment::Widget::HandleEvent(user_input, ctx);
    break;
  }
}
