
/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_SI468XFLASHWIDGET_HPP_
#define MUSICBOX_SI468XFLASHWIDGET_HPP_

#include "UserInterface/WidgetEnvironment.hpp"
#include "FileSystem.hpp"

namespace MusicBox::UserInterface
{
  class Si468xSelectFirmwareWidget : public WidgetEnvironment::Widget
  {
  public:
    Si468xSelectFirmwareWidget();

    void Draw(WidgetEnvironment::Painter &painter) const override;
    void HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx) override;

  private:
    ElmChanDirectory      Dir  {};
    ElmChanFile           File {};
    ElmChanFs::DirEntType DirEnt;

    void findNextFirmwareFile();
  };

  class Si468xShowFlashProgressWidget : public WidgetEnvironment::Widget
  {
  public:
    constexpr Si468xShowFlashProgressWidget() : WidgetEnvironment::Widget {} {}
    ~Si468xShowFlashProgressWidget();

    void Draw(WidgetEnvironment::Painter &painter) const override;
    void HandleEvent(events::KeyEvent user_input, const WidgetEnvironment::Context &ctx) override;
  };
}

#endif