/*  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_TARGET_ATSAM4S_INTERNAL_FLASH_STORAGE_HPP_
#define MUSICBOX_TARGET_ATSAM4S_INTERNAL_FLASH_STORAGE_HPP_

#include "Storage/BlockStorage.hpp"
extern "C"
{
#include "map.h"
#include "nand.h"
}

namespace MusicBox::Targets::ATSAM4S
{
  class InternalFlashStorage
  {
  public:
    /** One flash page has size of 512 Byte */
    union  FlashPage {
      uint8_t  data8[512];
      uint16_t data16[256];
      uint32_t data32[128];
    };

    static constexpr size_t kPageSize = sizeof(FlashPage);

    struct EraseBlock;

    void Initialize();

    bool ReadSector(int sector, uint8_t (&data)[kPageSize]);
    bool WriteSector(int sector, const uint8_t (&data)[kPageSize]);
    void Sync();
    void TrimUntilEnd(int start_sector);

  private:
    struct dhara_nand  dhara_nand_;
    struct dhara_map   dhara_map_;
    FlashPage          dhara_pagebuf_;

#if 0
        static StorageManager & getInstance(const struct dhara_nand* n)
    {
      return container_of(StorageManager, m_Nand, n);
    }
#endif

  };

  struct InternalFlashStorage::EraseBlock
  {
    static constexpr unsigned int kPageCount = 8;
    FlashPage pages[kPageCount];
  };
};


#endif