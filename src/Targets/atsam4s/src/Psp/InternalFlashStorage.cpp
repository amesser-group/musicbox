/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Psp/InternalFlashStorage.hpp"
#include "ecpp/Drivers/Atmel/ATSAM4S/EEFC.hpp"
#include "ecpp/Datatypes.hpp"
#include "ecpp/Meta/Math.hpp"
#include <string.h>

using namespace MusicBox::Targets::ATSAM4S;
using ecpp::Drivers::Atmel::ATSAM4S::EEFC;

/** Area for permanent storage 16*4kB = 64kB
 *  Locatin defined with linkerscript */
struct StorageArea
{
  static constexpr unsigned int kBlockCount = 16;
  InternalFlashStorage::EraseBlock blocks[kBlockCount];
};

extern StorageArea g_Storage;

void
InternalFlashStorage::Initialize()
{
  dhara_error_t err;

  dhara_nand_.log2_page_size = ecpp::MetaLog<2,    sizeof(FlashPage)>::Result;
  dhara_nand_.log2_ppb       = ecpp::MetaLog<2,    EraseBlock::kPageCount>::Result;
  dhara_nand_.num_blocks     = StorageArea::kBlockCount;

  dhara_map_init(&dhara_map_, &dhara_nand_, dhara_pagebuf_.data8, 16);

  dhara_map_resume(&dhara_map_, &err);
}

/** Read block from flash */
bool
InternalFlashStorage::ReadSector(int sector, uint8_t (&data)[512])
{
  dhara_error_t err;
  return 0 == dhara_map_read(&dhara_map_, sector, data, &err);
}

bool
InternalFlashStorage::WriteSector(int sector, const uint8_t (&data)[512])
{
  bool write_needed;
	dhara_error_t err;

#if INTERNALSTORAGE_NO_WRITE_OPTIMIZE
  FlashPage tmp;
  if( 0 != dhara_map_read(&dhara_map_, sector, tmp.data8, &err))
    return false;

  write_needed = (memcmp(tmp.data8, data, sizeof(tmp.data8)) != 0);
#else
	dhara_page_t p;

	if (dhara_map_find(&dhara_map_, sector, &p, &err) < 0) 
  {
		if (err != DHARA_E_NOT_FOUND)
		  return false;

    write_needed = false;
    for(uint8_t c : data)
    {
      if (c != 0xFF)
      {
        write_needed = true;
        break;
      }
    }
  }
  else if (p < (g_Storage.kBlockCount * g_Storage.blocks[0].kPageCount))
  {
    unsigned int block, page;

    /* translate from dhara address to flash address */
    block = p / g_Storage.blocks[0].kPageCount;
    page  = p % g_Storage.blocks[0].kPageCount;

    write_needed = (memcmp(data, g_Storage.blocks[block].pages[page].data8, sizeof(data)) != 0);
  }
  else
  { /* should never happen */
    return false;
  }
#endif  
  if (write_needed)
    return 0 == dhara_map_write(&dhara_map_, sector, data, &err);
  else
    return true;
}

void
InternalFlashStorage::Sync()
{
  dhara_error_t err;
  /* synchronize write access */
  dhara_map_sync(&dhara_map_, &err);
}

void
InternalFlashStorage::TrimUntilEnd(int start_sector)
{
  int stop_sect = dhara_map_.journal.nand->num_blocks << dhara_map_.journal.nand->log2_ppb;
  dhara_error_t err;

  /** trim all unused sectors */
  for(; start_sector < stop_sect; ++start_sector)
  {
    if(0 != dhara_map_trim(&dhara_map_, start_sector, &err))
      break;
  }

  Sync();
}



int dhara_nand_is_bad(const struct dhara_nand *n, dhara_block_t b)
{
  return false;
}

void dhara_nand_mark_bad(const struct dhara_nand *n, dhara_block_t b)
{
}

int dhara_nand_erase(const struct dhara_nand *n, dhara_block_t b,
         dhara_error_t *err)
{
  auto &per = EEFC::getInstance(0);
  unsigned int i;

  if (b >= g_Storage.kBlockCount)
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  for(i = 0; i < ecpp::ElementCount(g_Storage.blocks[b].pages); ++i)
  {
    if (!dhara_nand_is_free(n, b * g_Storage.blocks[b].kPageCount + i))
      break;
  }

  if(i < ecpp::ElementCount(g_Storage.blocks[b].pages))
  {
    per.eraseSectors(g_Storage.blocks[b].kPageCount, g_Storage.blocks[b].pages[0].data32);

    for(i = 0; i < ecpp::ElementCount(g_Storage.blocks[b].pages); ++i)
    {
      if (!dhara_nand_is_free(n, b * g_Storage.blocks[b].kPageCount + i))
      {
        dhara_set_error(err, DHARA_E_BAD_BLOCK);
        return -1;
      }
    }
  }

  return 0;
}

int
dhara_nand_prog(const struct dhara_nand *n, dhara_page_t p,
        const uint8_t *data, dhara_error_t *err)
{
  auto &per = EEFC::getInstance(0);
  unsigned int block, page;

  if (p >= (g_Storage.kBlockCount * g_Storage.blocks[0].kPageCount))
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  /* translate from dhara address to flash address */
  block = p / g_Storage.blocks[0].kPageCount;
  page  = p % g_Storage.blocks[0].kPageCount;

  per.writePages(1, g_Storage.blocks[block].pages[page].data32, data);

  if(0 != memcmp(g_Storage.blocks[block].pages[page].data8, data,
                sizeof(g_Storage.blocks[block].pages[page].data8)))
  {
    dhara_set_error(err, DHARA_E_BAD_BLOCK);
    return -1;
  }

  return 0;
}

int
dhara_nand_is_free(const struct dhara_nand *n, dhara_page_t p)
{
  unsigned int block, page;
  unsigned int i;

  if (p >= g_Storage.kBlockCount * g_Storage.blocks[0].kPageCount)
    return 0;

  /* translate from dhara address to flash address */
  block = p / g_Storage.blocks[0].kPageCount;
  page  = p % g_Storage.blocks[0].kPageCount;

  for(i = 0; i < ecpp::ElementCount(g_Storage.blocks[block].pages[page].data32); ++i)
  {
    if (0xFFFFFFFF != g_Storage.blocks[block].pages[page].data32[i])
      return 0;
  }

  return 1;
}

int dhara_nand_read(const struct dhara_nand *n, dhara_page_t p,
      size_t offset, size_t length, uint8_t *data, dhara_error_t *err)
{
  unsigned int block, page;

  if (p >= g_Storage.kBlockCount * g_Storage.blocks[0].kPageCount)
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  if ((length + offset) > sizeof(g_Storage.blocks[0].pages[0].data8))
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  /* translate from dhara address to flash address */
  block = p / g_Storage.blocks[0].kPageCount;
  page  = p % g_Storage.blocks[0].kPageCount;

  memcpy(data, &(g_Storage.blocks[block].pages[page].data8[offset]), length);

  return 0;
}

int dhara_nand_copy(const struct dhara_nand *n,
    dhara_page_t src, dhara_page_t dst, dhara_error_t *err)
{
  unsigned int block, page;

  if (src >= (g_Storage.kBlockCount * g_Storage.blocks[0].kPageCount))
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  if (dst >= (g_Storage.kBlockCount * g_Storage.blocks[0].kPageCount))
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  /* translate from dhara address to flash address */
  block = src / g_Storage.blocks[0].kPageCount;
  page  = src % g_Storage.blocks[0].kPageCount;

  return dhara_nand_prog(n, dst, g_Storage.blocks[block].pages[page].data8, err);
}