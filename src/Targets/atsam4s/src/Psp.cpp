/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Devices/MAX98089_Registers.hpp"
#include "Devices/SI4688_Registers.hpp"
#include "System.hpp"
#include "ecpp/Target/Bsp.hpp"
#include "Psp.hpp"
#include "delay.h"
#include "hsmci.h"

#define _COMPILING_NEWLIB 1

#include <sys/time.h>

#undef min
#undef max

using namespace sys;

using Max98089Defs = MusicBox::RegDef::Max98089;
using Si4688Defs = MusicBox::RegDef::Si4688;

using ::ecpp::Config::SystemTick;

const uint16_t
Psp::ksi4688_board_properties_[] =
{
  Si4688Defs::PROP_DIGITAL_IO_OUTPUT_SELECT,      0x0000,
  Si4688Defs::PROP_PIN_CONFIG_ENABLE,             0x0002,
  0xFFFF
};

const ::std::array<uint8_t, 8> Psp::kMax98089RegisterValues_DAI_Off
{
  0x00, 
  0x00,
  0x00,
  0x00,
  0x00,
  0x00,
  0x00 
};


const ::std::array<uint8_t, 9> Psp::kMax98089RegisterSequence_DAI2_I2S
{
  Max98089Defs::REG_DAI2_CLOCK_MODE,
  0x80,                                  /* 48 kHz Samplerate for ALC */
  (0x6000 >> 8) & 0x7F, 0x6000  & 0xFE,  /* Setup PLL for 48 khz samplerate from 24.576 Mhz Crystal and DHF=0 */
  /*DAI2 Configuration */
  0x90, /* I2S: Master, Second rising edge */
  0x02, /* I2S: BCLK = 48*LRCLK = 2.xxx MHz-> suitable for 16bit output of si468x */// todo
  0x81, /* DAI2: Use Port S2, Playback enable */
  0x00,
  0x00,
};

void
Psp::Init()
{
#ifndef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
  /* Disable the watchdog */
  WDT->WDT_MR = WDT_MR_WDDIS;
#endif

  SysTick_Config(sysclk_get_cpu_hz() / ::ecpp::Units::DecimalScale<0>::Rescale(1, ::ecpp::System::Tick::Scale()));

  /* GPIO has been deprecated, the old code just keeps it for compatibility.
    * In new designs IOPORT is used instead.
    * Here IOPORT must be initialized for others to use before setting up IO.
    */
  ioport_init();

  /* setup twi */
  ioport_set_port_mode(IOPORT_PIOA,
                       PIO_PA3A_TWD0 | PIO_PA4A_TWCK0,
                       IOPORT_MODE_MUX_A);

  ioport_disable_port(IOPORT_PIOA,
                       PIO_PA3A_TWD0 | PIO_PA4A_TWCK0);

  ioport_set_pin_level(kIOPortPinDabReset,0);
  ioport_set_pin_dir(kIOPortPinDabReset, IOPORT_DIR_OUTPUT);

  /* prepare ioport for hsmci */
  ioport_set_port_mode(IOPORT_PIOA, kIOPortPinMaskHSMCI,
                       IOPORT_MODE_MUX_C);

  /* switch hsmci pins to input */
  ioport_enable_port(IOPORT_PIOA, kIOPortPinMaskHSMCI);

  hsmci_init();

  /* configure sd card control pins */
  ioport_enable_pin(kIOPortPinSDPowerEn);
  ioport_set_pin_level(kIOPortPinSDPowerEn,1);
  ioport_set_pin_dir(kIOPortPinSDPowerEn, IOPORT_DIR_OUTPUT);

  ioport_enable_pin(kIOPortPinSDDetect);
  ioport_set_pin_dir(kIOPortPinSDDetect, IOPORT_DIR_INPUT);

  /** configure i2s pins */
  ioport_set_port_mode(IOPORT_PIOA, kIOPortPinMaskI2S,
                       IOPORT_MODE_MUX_A);

  ioport_set_port_mode(IOPORT_PIOA, kIOPortPinMaskI2S & PIO_PA17A_TD,
                       IOPORT_MODE_MUX_A);

  ioport_disable_port(IOPORT_PIOA, kIOPortPinMaskI2S);

  ecpp::HAL::ATSAM::SyncSerialController::SetupI2SSlave(16);

  /* configure interrupts */
  NVIC_SetPriority(SSC_IRQn, 4);
  NVIC_EnableIRQ(SSC_IRQn);

  NVIC_SetPriority(SysTick_IRQn, 5);

  NVIC_SetPriority(HSMCI_IRQn, 6);
  NVIC_EnableIRQ(HSMCI_IRQn);

  /* assume si468x is still booting */
  twi_manager_.Initialize();
  twi_manager_.Suspend();

  DeassertReset( globals().si468x_driver_);

  /* init storage */
  storage_provider_.Initialize();
}


void
Psp::TwiManager::Submit(Psp::TwiManager::Request &req)
{
  auto & twi_manager = sys::bsp().twi_manager_;

  twi_manager.PushBack(req);
  twi_manager.Process.Enqueue();
}


void
Psp::TwiManager::PushBack(Psp::TwiManager::Request &req)
{
  LockGuard lock(*this);
  Request **it = &pending_requests_;

  while(*it != nullptr)
  {
    if(*it == &req)
      {
        asm volatile ("bkpt");
      }
    it = &((*it)->next_request_);
  }

  *it = &req;
}

ECPP_JOB_DEF(Psp::TwiManager, Process)
{
  Request *req = nullptr;

  if(suspended_)
    return;

  req = PopFront();

  if(req != nullptr)
  {
    HandleRequest(req);
    ::signals::Complete(*req);

    Process.Enqueue();
  }
}

Psp::TwiManager::Request*
Psp::TwiManager::PopFront()
{
  LockGuard lock(*this);
  Request *req = pending_requests_;

  if( req != nullptr)
  {
    pending_requests_ = req->next_request_;
    req->next_request_ = nullptr;
  }

  return req;
}

Si468xDriver &
Psp::Si4688RequestBlock::GetDriver() const
{
  return globals().si468x_driver_;
}

ECPP_JOB_DEF(Psp, UnblockTwi)
{
  hardware_settled_ = true;

  twi_manager_.Resume();

  sys::bsp().audio_codec_.Poll();
  sys::globals().si468x_driver_.pollAsync();
}

bool
Psp::AssertReset(const Si468xDriver&)
{
  auto & audio_codec = ::sys::bsp().audio_codec_;

  if (trb_max98089_.IsActive())
    return false;

  if (not audio_codec.IsShutdown())
  {
    audio_codec.ShutdownAudio();
    return false;
  }
  else
  {
    twi_manager_.Suspend();
    ioport_set_pin_level(kIOPortPinDabReset, 0);
    return true;
  }
}

void
Psp::DeassertReset(const Si468xDriver&)
{
  ioport_set_pin_level(kIOPortPinDabReset, 1);
  UnblockTwi.Start(MilliSecond<>(100));
}

bool
Psp::IsCardPlugged()
{
  if(ioport_get_pin_level(kIOPortPinSDDetect))
    return false;
  else
    return true;
}

void
Psp::SetCardPowerState(bool poweron)
{
  /* detach hsmci from pins */
  ioport_enable_port(IOPORT_PIOA, kIOPortPinMaskHSMCI);

  if(poweron)
    ioport_set_pin_level(kIOPortPinSDPowerEn ,0);
  else
    ioport_set_pin_level(kIOPortPinSDPowerEn,1);
}

void
Psp::SetCardSpeed(uint32_t freq)
{
  hsmci_select_device(0, freq, 1, 0);

  /* attach hsmci to port  */
  ioport_disable_port(IOPORT_PIOA, kIOPortPinMaskHSMCI);
}

ECPP_JOB_DEF(Psp, Poll)
{
  if(clock_timer_.IsExpired())
  {
    clock_timer_.Continue(MilliSecond<>(1000));
    clock_sec_++;
  }
}

void 
Psp::HandleSystemTick()
{
  if(clock_timer_.IsExpired())
    Poll.Enqueue();
}

namespace ecpp::Config
{
  volatile ::ecpp::System::Tick systick_;
}

void SysTick_Handler(void)
{
  ::ecpp::Config::systick_ += ecpp::Units::TimeSpan<unsigned int, SystemTick::Scale>(1);
  Bsp::instance().HandleSystemTick();
}

namespace ecpp::Target::Bsp
{

  template<>
  void delay(MilliSecond<> timeout)
  {
    delay_ms(timeout.RoundTo(MilliSecond<>::ScaleType()));
  }
}

int _gettimeofday( struct timeval *tv, void *tzvp )
{
    tv->tv_sec  = Bsp::instance().clock_sec();
    tv->tv_usec = 0;
    return 0;
}

int settimeofday( const struct timeval *tv, const struct timezone *)
{
    Bsp::instance().set_clock_sec(tv->tv_sec);
    return 0;
}
