/*
 *  Copyright 2020-2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TARGET_PSP_HPP_
#define TARGET_PSP_HPP_

#include "ecpp/HAL/Atmel/ATSAM4S.hpp"
#include "ecpp/HAL/ATSAM/SSC.hpp"
#include "ecpp/Peripherals/ATSAM4S/Twi.hpp"
#include "ioport.h"
/* these macros are set through atmel headers */
#undef max
#undef min

#include "Drivers/SI468x/Driver.hpp"
#include "Drivers/SI468x/Transaction.hpp"
#include "Drivers/MAX98089.hpp"
#include "Storage/BlockStorage.hpp"

#include "Psp/InternalFlashStorage.hpp"
#include "Platform.hpp"

#include "ecpp/DateTime/Clock.hpp"
#include "ecpp/DateTime/DateTime.hpp"

#include "UserInterface/Text/TextDisplayDriverFactory.hpp"

#include "ATSAM4SAudioCodec.hpp"

#include <cstdbool>


namespace sys
{
  enum class ButtonState : uint_least8_t
  {
    Idle    = 0,
    Down    = 1,
    Pressed = 2,
    Up      = 3,
  };

  
  using MusicBox::Drivers::SI468x::Si468xTransaction;


  class Psp
  {
  public:
    using Si468xDriver              = MusicBox::Drivers::SI468x::Si468xDriver;

    using DateTime        = ::ecpp::DateTime::DateTime;
    using Clock           = ::ecpp::DateTime::Clock<DateTime>;

    static constexpr ioport_pin_t kIOPortPinDabReset   = IOPORT_CREATE_PIN(PIOA, 2);
    static constexpr ioport_pin_t kIOPortPinSDPowerEn  = IOPORT_CREATE_PIN(PIOA, 5);
    static constexpr ioport_pin_t kIOPortPinSDDetect   = IOPORT_CREATE_PIN(PIOA, 6);

    static constexpr uint32_t kIOPortPinMaskHSMCI      = (PIO_PA28C_MCCDA | PIO_PA29C_MCCK | PIO_PA30C_MCDA0 | PIO_PA31C_MCDA1 | PIO_PA26C_MCDA2 | PIO_PA27C_MCDA3);
    static constexpr uint32_t kIOPortPinMaskI2S        = (PIO_PA17A_TD | PIO_PA18A_RD | PIO_PA19A_RK | PIO_PA20A_RF);

    static constexpr uint32_t kSi468x_FlashAddr_RomPatch    = 0x04000;
    static constexpr uint32_t kSi468x_FlashAddr_FMFirmware  = 0x08000;
    static constexpr uint32_t kSi468x_FlashAddr_DABFirmware = 0xa0000;

    using StorageWriter = ::MusicBox::Storage::BlockStorage::Writer;
    using StorageReader = ::MusicBox::Storage::BlockStorage::Reader;

    using AudioCodec = MusicBox::Targets::ATSAM4S::AudioCodec;

    void           EnableClock(const AudioCodec&)    {};
    constexpr bool IsClockEnabled(const AudioCodec&) { return hardware_settled_; }

    void ConnectI2C(const Si468xDriver&) {};
    bool AssertReset(const Si468xDriver&);
    void DeassertReset(const Si468xDriver&);

    const uint16_t* GetConfiguration(const Si468xDriver&) const {return ksi4688_board_properties_;}

    bool IsCardPlugged();
    void SetCardPowerState(bool poweron);
    void SetCardSpeed(uint32_t freq);

    class TwiManager : public ::ecpp::Peripherals::ATSAM4S::Twi, public InterruptLockable
    {
    public:
      class Request : public ::ecpp::Peripherals::ATSAM4S::Twi::RequestBlockBase 
      {
      public:
        void Execute();
        void Submit(uint_fast16_t write_len, uint_fast16_t read_len) {
          state_     = State::Queued;
          write_len_ = write_len;
          read_len_  = read_len;

          TwiManager::Submit(*this); 
        }

        constexpr bool IsSame(const Request& other) { return this == &other; }
      private:
        Request *next_request_;

        friend class Psp::TwiManager;
      };

      template<size_t buffer_size>
      class RequestBlock : public ::ecpp::Peripherals::ATSAM4S::Twi::RequestBlock<buffer_size, Psp::TwiManager::Request>
      {
      public:
        using Psp::TwiManager::Request::Submit;
      };


      void        Suspend() { suspended_ = true; }
      void        Resume()  { suspended_ = false; Process.Enqueue(); }
    private:
      static void Submit(Request &trb);

      void                       PushBack(Psp::TwiManager::Request &req);
      Psp::TwiManager::Request * PopFront();

      ECPP_JOB_DECL(Process);

      Request             *pending_requests_ {nullptr};
      uint_least8_t        suspended_        {false};
    };

    class Si4688RequestBlock : public TwiManager::RequestBlock<16 + 512>
    {
    protected:
      void             SetUp()     { TwiManager::RequestBlock<16 + 512>::SetUp(kI2CAddrSi468x); }
      Si468xDriver &   GetDriver() const;
    private:
      
    };

    ::std::time_t clock_sec() const { return clock_sec_;}
    void          set_clock_sec(::std::time_t val) { clock_sec_ = val;}

    ECPP_JOB_DECL(Poll);

    MusicBox::Targets::ATSAM4S::InternalFlashStorage storage_provider_;

    TwiManager::RequestBlock<16 >                    trb_max98089_;
    Si468xTransaction<Si4688RequestBlock >           si4688_transaction_;


    constexpr auto & GetTwiRequestBlock(const MusicBox::Drivers::MAX98089::Max98089Driver&)  { return trb_max98089_; }

  protected:
    static constexpr uint8_t kI2CAddrSi468x   = 0x64;

    static const ::std::array<uint8_t, 8> kMax98089RegisterValues_DAI_Off;
    static const ::std::array<uint8_t, 9> kMax98089RegisterSequence_DAI2_I2S;

    void Init();
    void HandleSystemTick();

    TwiManager         twi_manager_;

    ECPP_JOB_TIMER_DECL(UnblockTwi);

    uint_least8_t           hardware_settled_;

    volatile ::std::time_t        clock_sec_;
    ::ecpp::System::DeadlineTimer clock_timer_;


    static const uint16_t ksi4688_board_properties_[];

    friend void ::SysTick_Handler();
  };


};

namespace signals
{
  void Complete(::sys::Psp::TwiManager::Request &req);
  void UserInput();
}

#endif