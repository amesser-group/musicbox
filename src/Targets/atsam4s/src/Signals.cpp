#include "System.hpp"

using namespace MusicBox;

using ::MusicBox::Drivers::SI468x::Si468xDriver;

void
::signals::Changed(const ::MusicBox::HsmciDriver &)
{
  ::sys::globals().user_interface_.DispatchEventValue(UserInterface::events::StatusChange());
}

void
::signals::Complete(::sys::Psp::TwiManager::Request &req)
{
  auto & g = ::sys::globals();
  auto & b = ::sys::bsp();

  if(b.trb_max98089_.IsSame(req))
    b.audio_codec_.Poll();
  else if(b.si4688_transaction_.IsSame(req))
    b.si4688_transaction_.RequestComplete.Enqueue();

}

void
::signals::UserInput()
{
  ::sys::globals().user_interface_.HandleUserInput();
}

void
::signals::MusicBox::UserTimeout(MusicBoxApp& app)
{
  ::sys::globals().snoozecontroller_.HandleUserTimeout();
}

void
::signals::UserShutdown()
{
  auto & g = ::sys::globals();

  g.GoToStandBy();
  g.snoozecontroller_.HandleUserShutdown();
}