
#include "System.hpp"
#include "ecpp/Chrono/Timezones.hpp"

using namespace sys;
using namespace MusicBox;

using MusicBox::Drivers::SI468x::Si468xOperationMode;

GlobalsNew GlobalsNew::instance_;


extern "C"
{
  extern char **environ;

  /* hard regsister timezone string */
  const char* envs [] =
  {
    ::ecpp::Chrono::Timezones::Europe::Berlin::tz_env_,
    NULL,
  };
}

namespace sys
{
  void Init()
  {
    auto & g = globals();

    environ = (char**)envs;
    
    bsp().Init();

    g.Initialize();
  }

  void 
  Start()
  {
    auto & g = globals();

    g.Start();

    g.sdcard_driver_.start();
  }

  Platform & platform() 
  { 
    return GlobalsNew::instance(); 
  }
}