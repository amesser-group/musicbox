/*  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_ATSAM4S_AUDIOCODEC_HPP_
#define MUSICBOX_ATSAM4S_AUDIOCODEC_HPP_

#include "Drivers/MAX98089.hpp"

namespace MusicBox::Targets::ATSAM4S
{
  using MusicBox::Drivers::MAX98089::Max98089RegisterSequences;

  class AudioCodec : protected MusicBox::Drivers::MAX98089::Max98089Driver
  {
  public:
    class Shutdown;

    Volume GetCurrentVolume() const { return Volume::FromLevel(current_volume_level_); }

    bool IsShutdown() const { return not active_;}

  protected:

    static constexpr unsigned long kExternalCrystalFreq = 24576000;

    using RegisterSequences = Max98089RegisterSequences<kExternalCrystalFreq>;
    static const ::std::array<uint8_t, 9> kMax98089RegisterSequence_DAI1_Si468x;


    /** requested volume, negative value means muted */
    uint_least8_t requested_volume_level_;
    /** current volume level */
    uint_least8_t current_volume_level_;
  };

  class AudioCodec::Shutdown
  {
  public:
    void TwiReady(AudioCodec& codec);
    
  protected:
    uint_least8_t written_;
  };
}
#endif
