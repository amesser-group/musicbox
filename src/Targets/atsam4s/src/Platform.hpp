/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PLATFORM_HPP_
#define PLATFORM_HPP_

#include "ecpp/Execution/LockGuard.hpp"
#include "ecpp/Execution/ContextManager.hpp"
#include "ecpp/Execution/Job.hpp"
#include "ecpp/Execution/JobTimer.hpp"
#include "ecpp/Units/Ticks.hpp"
#include "ecpp/HAL/ARM/NVIC.hpp"

#include <mutex>

namespace sys
{
  using ecpp::Execution::ContextManager;

  using ::ecpp::Execution::Job;

  class InterruptLockable
  {
  public:
    using Lockable  = InterruptLockable;
    using LockGuard = ::ecpp::Execution::LockGuard<InterruptLockable>;
  protected:
    class Locker : public ::ecpp::HAL::ARM::InterruptLock
    {
    public: 
      constexpr Locker(InterruptLockable&) : ::ecpp::HAL::ARM::InterruptLock() {}; 
    };

    friend class ::ecpp::Execution::LockGuard<InterruptLockable>;
  };


  class JobQueue : protected ecpp::Execution::JobQueue, public InterruptLockable
  {
  public:
    void Enqueue(Job & job)
    {
      ecpp::Execution::JobQueue::Enqueue(*this, job);
    }

    Job* GetNext()
    {
      return ecpp::Execution::JobQueue::GetNext(*this);
    }
  };


  class TimerQueue : public ::ecpp::Execution::TimerQueue, public InterruptLockable
  {
  public:
    Job* CheckNextTimeout(DeadlineType &next_timeout) 
    { 
      return ::ecpp::Execution::TimerQueue::CheckNextTimeout(*this, next_timeout); 
    } 
    
    friend class TimerQueue::Timer;
  };
  
  class Platform
  {
  public:
    JobQueue        jobs_;
    TimerQueue      timer_queue_;

    Job*  CheckNextTimeout(TimerQueue::DeadlineType & next_timeout)  
    { 
      return timer_queue_.CheckNextTimeout(next_timeout); 
    }
    
    Job*  GetNextJob()                                { return jobs_.GetNext(); }
  };

  Platform & platform(void);

  template<typename C>
  static void Enqueue(const C& owner, Job & job) { platform().jobs_.Enqueue(job); }

  template<typename C>
  static void Enqueue(const C& owner, Job & job, Job::Handler handler) { job.SetHandler(handler); platform().jobs_.Enqueue(job); }
}

#endif /* CONFIG_SYSTEM_HPP_ */
