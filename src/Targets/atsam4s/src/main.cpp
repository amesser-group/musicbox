/*
 * main.cpp
 *
 *  Created on: 04.08.2016
 *      Author: andi
 */

#include "System.hpp"
#include "Bsp.hpp"

#include "pmc.h"
#include "spi.h"
#include "compiler.h"
#include "conf_clock.h"
#include "sysclk.h"
#include "ioport.h"
#include "pio.h"
#include "delay.h"

using namespace sys;


// static uint32_t gs_ul_spi_clock = 25000;

extern "C" void _init() {};

int main()
{

  SystemInit();

  sysclk_init();

  sys::Init();

  Enable_global_interrupt();

  sys::Start();

  //::ecpp::HAL::ATSAM::SyncSerialController::Start();

  while(1)
  {
    auto & g = globals();
    
    ecpp::System::Tick next_timeout;
    Job   *job;

    job = g.timer_queue_.CheckNextTimeout(next_timeout);

    if(nullptr == job)
      job = g.jobs_.GetNext();

    if(nullptr != job)
      job->run();
    else
      pmc_enable_sleepmode(0);
  }
}
