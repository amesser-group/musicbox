
#ifndef SYSTEM_HPP_
#define SYSTEM_HPP_

#include "sys/application.hpp"
#include "Platform.hpp"
#include "ecpp/Peripherals/ATSAM4S/Twi.hpp"

#include "MusicBox.hpp"
#include "Drivers/MAX98089.hpp"
#include "Drivers/SI468x/ActionContainer.hpp"
#include "Drivers/SI468x/Actions/UpdateFirmware.hpp"
#include "Drivers/SI468x/Actions/MeasureAntCap.hpp"
#include "Drivers/SI468x/Actions/ScanDAB.hpp"
#include "Drivers/SI468x/Driver.hpp"
#include "Drivers/SI468x/Transaction.hpp"
#include "Hsmci/HsmciDriver.hpp"

#include "Bsp.hpp"
#include "Database/ServiceDatabase.hpp"
#include "FileSystem.hpp"
#include "SnoozeController.hpp"

#include "ui_4x20_rot/DrawContext.hpp"

namespace sys
{
  using MusicBox::Drivers::MAX98089::Max98089Manager;
  using MusicBox::Drivers::MAX98089::Max98089Configuration;  
  using MusicBox::Lcd4x20DrawContext;
  using MusicBox::HsmciDriver;

  namespace ATSAM4S = ::ecpp::Peripherals::ATSAM4S;
  using ::MusicBox::Drivers::MAX98089::Max98089Manager;
  using ::MusicBox::Drivers::SI468x::Si4688Status;
  using ::MusicBox::Drivers::SI468x::Si468xTransaction;
  using ::MusicBox::Drivers::SI468x::Si468xDriver;
  using ::MusicBox::Database::ServiceDatabase;
  using ::MusicBox::Database::ServiceValueIterator;
  using ::MusicBox::ElmChanVolumeManager;
  using namespace ::MusicBox::Drivers::SI468x::Actions;
  
  class GlobalsNew;
  static inline constexpr GlobalsNew & globals(void);

  static inline constexpr Bsp        & bsp()     { return Bsp::instance(); }


  class GlobalsNew : public Platform, public MusicBox::MusicBoxApp, public Application
  {
  public:
    HsmciDriver                                         sdcard_driver_;
    ElmChanVolumeManager                                volumemanager_;
    MusicBox::SnoozeController                          snoozecontroller_;

    static constexpr GlobalsNew & instance() { return instance_ ;}
  private:
    static GlobalsNew instance_;

    static constexpr GlobalsNew & GetGlobals(Application &app) {return static_cast<GlobalsNew &>(app);}

    friend class Application;
  };

  static inline constexpr GlobalsNew & globals()  { return GlobalsNew::instance(); }

  template<typename OBJ>
  constexpr auto & GetApp(const OBJ&) { return GlobalsNew::instance(); }

  constexpr TimerQueue & GetTimerQueue(const ::ecpp::Execution::TimerQueue::Timer&) { return globals().timer_queue_; }
  
  template <typename OBJ> 
  void Complete(OBJ &obj);

  template <typename OBJ> 
  void Complete(OBJ *obj);

  template<typename Object>
  auto & GetSi468xDriver(const Object *)            { return globals().si468x_driver_; }

  template<typename Object>
  auto & GetSi468xDriver(const Object &)            { return globals().si468x_driver_; }

  constexpr auto & GetSi468xTransaction(const Si468xDriver&)  { return bsp().si4688_transaction_; }
  constexpr auto & GetSi468xActions(const Si468xDriver&)      { return globals().si468x_actions_; }

  template<typename Object>
  auto & GetStorage(const Object &)                 { return globals().storage_manager_; }

  template<typename Object>
  auto & GetChannelDatabase(const Object *)         { return ServiceDatabase::instance(); }

  template<typename Object>
  auto & GetChannelDatabase(const Object &)         { return ServiceDatabase::instance(); }

  template<typename Object>
  auto & GetVolumeManager(const Object *)           { return globals().volumemanager_; }

  template<typename Object>
  auto & GetVolumeManager(const Object &)           { return globals().volumemanager_; }

  void Init();
  void Start();
};

#endif