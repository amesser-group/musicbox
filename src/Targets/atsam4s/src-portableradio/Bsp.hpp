/*
 * board.h
 *
 *  Created on: 11.08.2016
 *      Author: andi
 */

#ifndef FIRMWARE_APP_SRC_BSP_H_
#define FIRMWARE_APP_SRC_BSP_H_

#include <cstdbool>
#include <cstdio>

#include "ecpp/Drivers/NHD/NHD_0420DZW.hpp"
#include "ecpp/Drivers/Raystar/RC2004A.hpp"
#include "ecpp/Drivers/BufferedDisplayDriver.hpp"

#include "ecpp/Ui/Widget/Text/DrawContext.hpp"

#include "ecpp/Units/Time.hpp"
#include "ecpp/Units/Ticks.hpp"

#include "sys/application.hpp"
#include "Platform.hpp"
#include "Psp.hpp"
#include "Drivers/SI468x/Driver.hpp"
#include "PortableRadioAudioCodec.hpp"

#include "conf_board.h"
#include "sam4s.h"

namespace sys
{
  using namespace ::ecpp::Drivers::Display;
  using MusicBox::Targets::ATSAM4S::PortableRadioAudioCodec;

  using ::ecpp::Units::MilliSecond;

  template<unsigned FREQ, typename VALUETYPE = unsigned>
  class TickDelta
  {
  public:
    typedef VALUETYPE ValueType;

    static constexpr unsigned Frequency = FREQ;

    constexpr TickDelta() {};
    constexpr TickDelta(ValueType init) : Delta(init) {};

    template<typename T>
    constexpr bool operator > (const MilliSecond<T> & rhs) const
    {
      return Delta > (rhs.Interval + (1000 / FREQ) - 1) / (1000 / FREQ);
    }

  private:
    ValueType Delta {0};
  };


  template<unsigned int SAMPLES, typename VALUETYPE>
  class SmoothedAnalogValue
  {
  public:
    typedef VALUETYPE ValueType;

    void sample(ValueType sample)
    {
      SampleSum = SampleSum - SampleSum / SAMPLES + sample;
    }

    ValueType operator * ()
    {
      ValueType v = (SampleSum + RoundAdd) / SAMPLES;

      if (v < LastValue)
        RoundAdd = SAMPLES / 4;
      else if (v > LastValue)
        RoundAdd = SAMPLES / 4 + SAMPLES / 2;
      LastValue = v;

      return v;
    }

    /** Return value further downscaled */
    template<typename RHS>
    ValueType operator / (RHS rhs)
    {
      ValueType r = (SampleSum + RoundAdd * rhs) / (SAMPLES * rhs);

      if ((r * rhs) < LastValue)
        RoundAdd = SAMPLES / 4;
      else if ((r * rhs) > LastValue)
        RoundAdd = SAMPLES / 4 + SAMPLES / 2;

      LastValue = r*rhs;
      return r;
    }

  private:
    ValueType  SampleSum {0};
    ValueType  LastValue {0};
    ValueType  RoundAdd  {SAMPLES / 4};
  };



  using namespace ::ecpp::Drivers;

  class BspPortableRadio : public ::sys::Psp
  {    
  public:
    struct UserInput;


#if MUSICBOX_DISPLAY_NHD0420DZW
    using DisplayDriver = NHD0420DZW::Driver<NHD0420DZW::Parallel4Bit, NHD0420DZW::EnglishWesternFontTable> ;
#elif MUSICBOX_DISPLAY_RC2004A
    using DisplayDriver = RC2004A::Driver<RC2004A::Parallel4Bit>;
#else
#error must specify display type
    using DisplayDriver = RC2004A::Driver<RC2004A::Parallel4Bit>;
#endif
    using Display = MusicBox::UserInterface::Text::TextDisplayDriverFactory<DisplayDriver>;

    typedef int_least16_t                     EncoderPos;
    typedef SmoothedAnalogValue<4, int16_t>   VolumeLevelType;

    static constexpr BspPortableRadio & getInstance() { return instance_; }
    static constexpr BspPortableRadio & instance()    { return instance_; }

    void Init();
    
    ::ecpp::System::FlexibleTimeout HandleUserInput(Application & app);

    ButtonState   getButtonState();

    void PrepareRadioAudio();
    void ShutdownRadio();

    Display                 display_;
    PortableRadioAudioCodec audio_codec_;

    using Psp::GetConfiguration;
  private:
    static BspPortableRadio instance_;


    ::ecpp::System::TickTimer button_change_tmr_;
    ::ecpp::System::TickTimer encoder_change_tmr_;

    ButtonState    RealButtonState;
    ButtonState    ReportedButtonState;

    uint_least8_t   LastEncoderState;
    int_least8_t    LastEncoderDelta;

    EncoderPos      encoder_pos_real_;
    EncoderPos      encoder_pos_handled_;

    VolumeLevelType VolumeLevel;

    void   HandleSystemTick();
    void   InitMax98089();

    friend void ::PIOA_Handler(void);
    friend void ::ADC_Handler(void);
    friend void ::SysTick_Handler();
  };

  struct BspPortableRadio::UserInput
  {
    enum Request : uint_least8_t { kNone, kClicked, kUp, kDown, kVolUp=kNone, kVolDown=kNone};

    constexpr UserInput(Request k) : key{k} {}
    constexpr operator Request() const { return key; }
    const Request key;
  };


  using Bsp = BspPortableRadio;

}

#endif /* FIRMWARE_APP_SRC_BSP_H_ */
