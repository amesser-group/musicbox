/**
 * \file
 *
 * \brief Board configuration.
 *
 * Copyright (c) 2011-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef CONF_BOARD_H_INCLUDED
#define CONF_BOARD_H_INCLUDED


/** SPI slave select MACRO definition */
#define CONF_BOARD_SPI_NPCS0

/** Spi Hw ID . */
#define SPI_ID          ID_SPI

/** SPI base address for SPI master mode*/
#define SPI_MASTER_BASE      SPI
/** SPI base address for SPI slave mode, (on different board) */
#define SPI_SLAVE_BASE       SPI



#define ENCODER_A_GPIO                 (PIO_PA9_IDX)
#define ENCODER_A_FLAGS                (PIO_TYPE_PIO_INPUT)
#define ENCODER_B_GPIO                 (PIO_PA15_IDX)
#define ENCODER_B_FLAGS                (PIO_TYPE_PIO_INPUT)
#define ENCODER_BUTTON_GPIO            (PIO_PA10_IDX)
#define ENCODER_BUTTON_FLAGS           (PIO_TYPE_PIO_INPUT)


#define DAB_RESET_GPIO                 (PIO_PA2_IDX)
#define DAB_RESET_FLAGS                (PIO_TYPE_PIO_OUTPUT_0)


#define TWI_IRQn    TWI0_IRQn

#define SPI_LCD_CHIP_SEL 0
#define SPI_LCD_CHIP_PCS spi_get_pcs(SPI_LCD_CHIP_SEL)
#define SPI_LCD_CLK_POLARITY 0
#define SPI_LCD_CLK_PHASE 0
#define SPI_LCD_DLYBS 0x40
#define SPI_LCD_DLYBCT 0x10

#endif /* CONF_BOARD_H_INCLUDED */
