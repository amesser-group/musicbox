/*  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PortableRadioAudioCodec.hpp"
#include "Bsp.hpp"

using sys::Bsp;
using ecpp::Units::MilliSecond;

namespace MusicBox::Targets::ATSAM4S
{
  using MusicBox::Drivers::MAX98089::Max98089Defs;
  using MusicBox::Drivers::MAX98089::Max98089CommonSequences;

  /* Equalizer Settings 
  * Equalizer params for Visaton FR10-4 in 1.1l case backwards open
  * (no br). These values are only valid for fixed Samplerate */

  static const ::std::array<uint8_t, 10> kMax98089RegisterValues_EQBand1 {
    EQ_PARAM(0x7C8A), EQ_PARAM(0xC001), EQ_PARAM(0x3FC1), EQ_PARAM(0x00A5), EQ_PARAM(0x0599)
  };

  static const ::std::array<uint8_t, 10> kMax98089RegisterValues_EQBand2 {
    EQ_PARAM(0x1B3B), EQ_PARAM(0xC009), EQ_PARAM(0x3EF9), EQ_PARAM(0x021B), EQ_PARAM(0x0B66)
  };

  static const ::std::array<uint8_t, 10> kMax98089RegisterValues_EQBand3 {
    EQ_PARAM(0x175E), EQ_PARAM(0xC08E), EQ_PARAM(0x3938), EQ_PARAM(0x0868), EQ_PARAM(0x1CA9)
  };

  static const ::std::array<uint8_t, 10> kMax98089RegisterValues_EQBand4 {
    EQ_PARAM(0x12BD), EQ_PARAM(0xCAB5), EQ_PARAM(0x00C7), EQ_PARAM(0x236F), EQ_PARAM(0x3FFE)
  };

  static const ::std::array<uint8_t, 10> kMax98089RegisterValues_EQBand5 {
    EQ_PARAM(0x2FB2), EQ_PARAM(0x3FDA), EQ_PARAM(0xFFF9), EQ_PARAM(0x044D), EQ_PARAM(0x3FFF)
  };

  static const ::std::array<uint8_t, 13> kMax98089RegisterSequence_Speaker {  
    Max98089Defs::REG_DAC_MIXER,
    0x84,             /* Connect DAC with DAI1*/
    0x00, 0x00,       /* Input mixers all off */
    0x00, 0x00, 0x00, /* Headphone Mixers all off */
    0x00, 0x00, 0x00, /* Lineout Mixers all off  */
    0x01, 0x01, 0x00  /* Speaker Mixers connect with DAC, Gain 0dB */
  };

  static const ::std::array<uint8_t, 2> kMax98089RegisterSequence_LevelControl =
  {
    /* Enable EQ1 */
    Max98089Defs::REG_LEVEL_CONTROL, 0x01
  };

  static const ::std::array<uint8_t, 6> kMax98089RegisterSequence_OutputSpeaker {
    Max98089Defs::REG_OUTPUT_ENABLE,
    0x33, /* Enable Speaker + DAC */
    0xF0, /* Enable Bandgap, Regulator, Common Mode & Bias */
    0x00, /* Disable Low Power mode */
    0x05, /* DAI1 Input Dither + DAI1 Clock Enable */
    0xC0  /* Unshutdown */
  };

  void
  PortableRadioAudioCodec::SetMute(bool mute)
  {
    if(mute)
      requested_volume_level_ |=  Max98089Defs::kRegMask_Volume_Mute;
    else
      requested_volume_level_ &= ~Max98089Defs::kRegMask_Volume_Mute;

    PollTwiComplete.Enqueue();
  }

  void 
  PortableRadioAudioCodec::SetVolumeLevel(uint_fast8_t level)
  {
    if (level > 0x1F)
      level = 0x1F;

    requested_volume_level_ = level | (requested_volume_level_ & Max98089Defs::kRegMask_Volume_Mute);

    PollTwiComplete.Enqueue();
  }

  void
  PortableRadioAudioCodec::ShutdownAudio()
  {
    if(std::holds_alternative<Shutdown>(current_profile_))
      return;

    current_profile_.emplace<Shutdown>();
    PollTwiComplete.Enqueue();
  }

  void
  PortableRadioAudioCodec::PrepareRadioAudio()
  {
    if(std::holds_alternative<RadioMode>(current_profile_))
      return;

    current_profile_.emplace<RadioMode>();
    PollTwiComplete.Enqueue();
  }


  ECPP_JOB_DEF(PortableRadioAudioCodec, PollTwiComplete)
  {
    auto bsp = Bsp::instance();

    if(bsp.IsClockEnabled(*this))
    {
      if(not bsp.GetTwiRequestBlock(*this).IsActive())
        ::std::visit([&](auto && arg){arg.TwiReady(*this);}, current_profile_);
    }
    else
    {
      PollTwiComplete.Start(MilliSecond<>(50));
    }
  }

  bool
  PortableRadioAudioCodec::WriteVolumeRegs()
  {
    uint8_t level = requested_volume_level_;

    std::array<uint8_t, 5> a = {Max98089Defs::REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL, 0, 0, level, level};

    if(WriteRegisters(a))
    {
      current_volume_level_ = requested_volume_level_;
      return true;
    }
    else
    {
      return false;
    }
  }

  void 
  PortableRadioAudioCodec::RadioMode::TwiReady(PortableRadioAudioCodec& codec)
  {
    bool next_state = false;

    switch(state_)
    {
    case 0: next_state = codec.WriteRegisters(Max98089CommonSequences::kShutdown); break;
    /* Setup for 24.576 MHz Crystal */
    case 1: next_state = codec.WriteRegisters(RegisterSequences::kMasterClockControl); break;
    /* Setup DAI 1 */
    case 2: next_state = codec.WriteRegisters(kMax98089RegisterSequence_DAI1_Si468x); break;
    /* setup mixers for speaker amplifier play */
    case 3: next_state = codec.WriteRegisters(kMax98089RegisterSequence_Speaker); break;
    /** Enables EQ on DAI1 */
    case 4: next_state = codec.WriteRegisters(kMax98089RegisterSequence_LevelControl); break;
    /* DAI1 uses fixed samplerate so we can easily preconfigure EQ for now */
    case 5: next_state  = codec.WriteRegisters(Max98089Defs::REG_EQ_DAI1_BAND1, kMax98089RegisterValues_EQBand1); break;
    case 6: next_state  = codec.WriteRegisters(Max98089Defs::REG_EQ_DAI1_BAND2, kMax98089RegisterValues_EQBand2); break;
    case 7: next_state  = codec.WriteRegisters(Max98089Defs::REG_EQ_DAI1_BAND3, kMax98089RegisterValues_EQBand3); break;
    case 8: next_state  = codec.WriteRegisters(Max98089Defs::REG_EQ_DAI1_BAND4, kMax98089RegisterValues_EQBand4); break;
    case 9: next_state  = codec.WriteRegisters(Max98089Defs::REG_EQ_DAI1_BAND5, kMax98089RegisterValues_EQBand5); break;
    case 10: next_state = codec.WriteRegisters(kMax98089RegisterSequence_OutputSpeaker);
      codec.active_ = next_state;
      break;
    case 11: 
      next_state = codec.WriteVolumeRegs(); 
      break;
    case 12: 
      if(codec.current_volume_level_ != codec.requested_volume_level_)
        codec.WriteVolumeRegs(); 
      break;
    }

    if(next_state)
      state_++;
  }
}
