#include "Bsp.hpp"
#include "System.hpp"

#include "ecpp/Target/Bsp.hpp"
#include "ecpp/Units/Time.hpp"
#include "pio.h"
#include "delay.h"
#include "app.hpp"
#include "compiler.h"
#include "conf_clock.h"
//#include "sysclk.h"
#include "ioport.h"
#include "adc.h"
#include "pio.h"
#include "delay.h"
#include "config.hpp"

#define LCD_PAR_PIO     PIOA

#define LCD_PAR_DB (PIO_PA15 |\
                    PIO_PA14 |\
                    PIO_PA13 |\
                    PIO_PA12)


#define LCD_PAR_PINMASK (PIO_PA15 |\
                         PIO_PA14 |\
                         PIO_PA13 |\
                         PIO_PA12 |\
                         PIO_PA11 |\
                         PIO_PA10 |\
                         PIO_PA9)

#define LCD_PAR_RW      PIO_PA9
#define LCD_PAR_E       PIO_PA10
#define LCD_PAR_RS      PIO_PA11

using namespace ecpp::Units;

namespace ecpp::Target::Bsp
{
  void
  pulseLcdEnable()
  {
    delay_us(1);
    pio_set(LCD_PAR_PIO, LCD_PAR_E);
    delay_us(1);
    pio_clear(LCD_PAR_PIO,   LCD_PAR_E);
    delay_us(1);
  }

  void
  ResetDisplay(DisplayDriver& inst)
  {
    pio_clear(LCD_PAR_PIO, LCD_PAR_PINMASK);
    pio_set_output(LCD_PAR_PIO, LCD_PAR_PINMASK, LOW, DISABLE, DISABLE);

    delay(MilliSecond<>(50));
  }

  void
  writeLcdNibble(DisplayDriver& inst, uint8_t nibble)
  {
    pio_clear(LCD_PAR_PIO, LCD_PAR_RW);

    pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
    pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

    pulseLcdEnable();
  }

  void
  writeLcdByte(DisplayDriver& inst, uint8_t byte)
  {
    uint8_t nibble;

    nibble = (byte & 0xF0) >> 4;

    pio_clear(LCD_PAR_PIO, LCD_PAR_RW);

    pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
    pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

    pulseLcdEnable();

    nibble = (byte & 0x0F) >> 0;

    pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
    pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

    pulseLcdEnable();
    delay_us(50);
  }

  void
  setLcdRS(DisplayDriver& inst)
  {
    pio_set (LCD_PAR_PIO, LCD_PAR_RS);
  }

  void
  clearLcdRS(DisplayDriver& inst)
  {
    pio_clear (LCD_PAR_PIO, LCD_PAR_RS);
  }
}

using Max98089Defs = ::MusicBox::RegDef::Max98089;

using namespace ::MusicBox;
using namespace ::sys;

BspPortableRadio BspPortableRadio::instance_;



void BspPortableRadio::HandleSystemTick()
{
  Psp::HandleSystemTick();

  /* update volume from potentiometer */
  audio_codec_.SetVolumeLevel(VolumeLevel / 32);

  if(encoder_change_tmr_.IsExpired(MilliSecond<>(1000)))
  { /* clear encoder pos */
    if (LastEncoderDelta < 0)
      encoder_pos_real_ = (encoder_pos_real_ + 2) & 0xFFFC;
    else
      encoder_pos_real_ = (encoder_pos_real_ + 1) & 0xFFFC;
  }

  adc_start(ADC);
}


static constexpr int_least8_t BspEncoderDeltaMissed = -2;

static const int_least8_t BspEncoderStateMap[][4] = {
  {  0, -1,  1, BspEncoderDeltaMissed},
  {  1,  0, BspEncoderDeltaMissed, -1},
  { -1, BspEncoderDeltaMissed,  0,  1},
  { BspEncoderDeltaMissed,  1, -1,  0}
};

void PIOA_Handler(void)
{
  auto & bsp = Bsp::getInstance();

  unsigned int state = 0;
  ButtonState button;
  int_fast8_t delta;
  uint32_t reg;

  /* get & clear PIO controller sirq status */
  pio_get_interrupt_status(PIOA);

  /* read all pio pins at once */
  reg = ~(PIOA->PIO_PDSR);

  state = 0;
  if(reg & PIO_PA22)
    state += 1;

  if(reg & PIO_PA23)
    state += 2;

  if(reg & PIO_PA24)
    button = ButtonState::Pressed;
  else
    button = ButtonState::Idle;

  if(button != bsp.RealButtonState)
    bsp.RealButtonState = button;
    
  delta = BspEncoderStateMap[bsp.LastEncoderState][state];
  bsp.LastEncoderState = state;

  if(delta == BspEncoderDeltaMissed)
  {
    delta = bsp.LastEncoderDelta;
    bsp.LastEncoderDelta = 0;
  }
  else
  {
    bsp.LastEncoderDelta = delta;
  }

  if(delta != 0)
    bsp.encoder_change_tmr_.Start();

  bsp.encoder_pos_real_ += (int_least16_t)delta;

  ::signals::UserInput();
}

extern "C" void ADC_Handler(void)
{
  auto & bsp = Bsp::getInstance();
  uint32_t status = adc_get_status(ADC);

  // Check the ADC conversion status
  if (ADC_ISR_EOC8 & status)
  {
    // Get latest digital data value from ADC and can be used by application
    bsp.VolumeLevel.sample(adc_get_channel_value(ADC, ADC_CHANNEL_8));
  }
}


void BspPortableRadio::Init(void)
{
  Psp::Init();

  /* set encoder to be an input */
  pio_set_input(PIOA, PIO_PA24 | PIO_PA23 | PIO_PA22,
                PIO_PULLUP | PIO_DEGLITCH);

  pio_set_debounce_filter(PIOA, PIO_PA24 | PIO_PA23 | PIO_PA22, 500);

  /* enable pin change interrupts on all encoder pins */
  pio_configure_interrupt(PIOA, PIO_PA24 | PIO_PA23 | PIO_PA22, 0);

  /* Disable all PIOA I/O line interrupt. */
  pio_disable_interrupt(PIOA, 0xFFFFFFFF);
  pio_enable_interrupt(PIOA, PIO_PA24 | PIO_PA23 | PIO_PA22);

  /* enable ADC for volume knob */
  sysclk_enable_peripheral_clock(ID_ADC);

  adc_init(ADC, sysclk_get_main_hz(), 2000000, ADC_STARTUP_TIME_8);
  adc_set_bias_current(ADC, 0);

  adc_configure_timing(ADC, 15, ADC_SETTLING_TIME_3, 2);
  adc_set_resolution(ADC, ADC_10_BITS);

  adc_enable_channel(ADC, ADC_CHANNEL_8);

  adc_enable_interrupt(ADC, ADC_IER_EOC8);

  adc_configure_trigger(ADC, ADC_TRIG_SW, 0);

  /* Configure and enable interrupt of PIO. */
  NVIC_ClearPendingIRQ(PIOA_IRQn);
  NVIC_SetPriority(PIOA_IRQn, 4);
  NVIC_EnableIRQ(PIOA_IRQn);

  NVIC_SetPriority(ADC_IRQn, 6);
  NVIC_EnableIRQ(ADC_IRQn);

  display_.Init();
}

ButtonState
BspPortableRadio::getButtonState()
{
  ButtonState r = RealButtonState;

  if(r == ReportedButtonState)
    return ReportedButtonState;

  if (button_change_tmr_.IsExpired(MilliSecond<>(50)))
  {
    ReportedButtonState = r;

    button_change_tmr_.Start();

    if (r == ButtonState::Pressed)
      return ButtonState::Down;
    else
      return ButtonState::Up;
  }

  return ReportedButtonState;
}

::ecpp::System::FlexibleTimeout
BspPortableRadio::HandleUserInput(Application & app)
{
  auto buttonState = getButtonState();

  if(buttonState == ButtonState::Down)
  {
    if(::sys::globals().user_interface_.IsUserIdle())
      app.HandleKey(events::KeyEvent::kSnooze);
    else
      app.HandleKey(events::KeyEvent::kConfirm);

    return MilliSecond<>(20);
  }
  else if(encoder_pos_real_ != encoder_pos_handled_)
  {
    auto change = encoder_pos_real_ - encoder_pos_handled_;

    if(change > 2)
    {
      encoder_pos_handled_ += 4;
      change = encoder_pos_real_ - encoder_pos_handled_;
      app.HandleKey(events::KeyEvent::kNext);
    }
    else if (change < -2)
    {
      encoder_pos_handled_ -= 4;
      change = encoder_pos_real_ - encoder_pos_handled_;
      app.HandleKey(events::KeyEvent::kPrev);
    }

    return MilliSecond<>(20);
  }
  else
  {
    return MilliSecond<>(0);
  }
}

void 
BspPortableRadio::ShutdownRadio()
{
  audio_codec_.ShutdownAudio();
}