
define run
  monitor reset halt
  flushregs
  thb app_main
  continue
end

target remote :3333
set remote hardware-watchpoint-limit 2

run
