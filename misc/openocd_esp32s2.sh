#!/bin/sh

SELF=`readlink -f $0`
BASEDIR=`dirname $SELF` 

exec openocd --search "${BASEDIR}/openocd" $@ -f esp32s2.cfg
