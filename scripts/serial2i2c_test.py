import serial
import struct
import time
import logging
import sys
import binascii

class I2CError(Exception):
    pass

class Timeout(Exception):
    pass

class SerialI2CAdapter(object):
    CMD_START_REQ     = 0x01
    CMD_START_CNF     = 0x02
    CMD_RESTART_REQ   = 0x03
    CMD_RESTART_CNF   = 0x04
    CMD_STOP_REQ      = 0x05
    CMD_STOP_CNF      = 0x06
    CMD_READ_REQ      = 0x07
    CMD_READ_CNF      = 0x08
    CMD_WRITE_REQ     = 0x09
    CMD_WRITE_CNF     = 0x0A

    CMD_RESETCFG_REQ  = 0x11
    CMD_RESETCFG_CNF  = 0x12

    class SerialDumper(object):
        def __init__(self, serial):
            self.serial = serial
            self.setTimeout = serial.setTimeout
            
        def write(self, data):
            print('Sending %d byte: %s' %(len(data), data.encode('hex')))
            return self.serial.write(data)

        def read(self, size):
            data = self.serial.read(size)
            print('Received %d/%d byte: %s' %(len(data), size, data.encode('hex')))
            return data

    def __init__(self, path):
        self.serial = serial.Serial(path, 1000000, timeout=0)
        #self.serial = self.SerialDumper(self.serial)
        self.serial.read(size=40)
        self.serial.setTimeout(10)
        
        
        try:
            self.stop()
        except:
            pass
        
        self.resetcfg(0)

        
    def sendRequest(self, cmd, payload):
        self.serial.write(struct.pack('<BB', cmd, len(payload)) + payload)
        
    def recvResponse(self, cmd):
        cmd_, length = struct.unpack('<BB', self.serial.read(size=2))
        
        if length > 0:
            tmp = self.serial.read(size=length)
            
            status, = struct.unpack('<B', tmp[0])
            payload = tmp[1:]
        else:
            status = -43

        if cmd_ != cmd:
            raise ValueError('Unexpected response command %#04x' % cmd_)
        
        if cmd_ != self.CMD_READ_CNF and status != 0:
            raise I2CError('I2C command failed with %d' % status)
            
        return payload
        
    def start(self, address, read = True):
        payload = struct.pack('<BB', address, 1 if read else 0)
        self.sendRequest(self.CMD_START_REQ, payload)
        self.recvResponse(self.CMD_START_CNF)

    def restart(self, address, read = True):
        payload = struct.pack('<BB', address, 1 if read else 0)
        self.sendRequest(self.CMD_RESTART_REQ, payload)
        self.recvResponse(self.CMD_RESTART_CNF)

    def stop(self):
        self.sendRequest(self.CMD_STOP_REQ, "")
        self.recvResponse(self.CMD_STOP_CNF)
        
    def read(self, length):
        response = ''
        
        for x in range(0, length, 32):
            payload = struct.pack('<B', min(32, length - x))
            
            self.sendRequest(self.CMD_READ_REQ, payload)
            payload = self.recvResponse(self.CMD_READ_CNF)

            l, = struct.unpack('<B', payload[0])

            assert(len(payload) == (l + 1))
            response += payload[1:]

        assert(len(response) == length)

        return response

    def write(self, data):
        for x in range(0,len(data),32):
            chunk = data[x:x+32]
            
            payload = struct.pack('<B', len(chunk)) + chunk
            self.sendRequest(self.CMD_WRITE_REQ, payload)
    
            payload = self.recvResponse(self.CMD_WRITE_CNF)
            length, = struct.unpack('<B', payload)
            
            if length != len(chunk):
                break

        return x + length

    def resetcfg(self, value):
        self.sendRequest(self.CMD_RESETCFG_REQ, struct.pack('<B', value))
        self.recvResponse(self.CMD_RESETCFG_CNF)
        
        self.resetstatus = value
        time.sleep(0.1);
        
    def reset_dab(self):
        self.resetcfg(self.resetstatus & (~0x01))
        self.resetcfg(self.resetstatus | 0x01)
        
    

class Si4688(object):
    addr = 0x64
    
    CMD_RD_REPLY           = 0x00
    CMD_POWER_UP           = 0x01
    CMD_HOST_LOAD          = 0x04
    CMD_FLASH_PASS_THROUGH = 0x05
    CMD_LOAD_INIT          = 0x06
    CMD_BOOT               = 0x07
    CMD_GET_SYS_STATE      = 0x09
    CMD_SET_PROPERTY       = 0x13
    
    CMD_FM_SEEK_START      = 0x31
    CMD_FM_RSQ_STATUS      = 0x32

    CMD_GET_DIGITAL_SERVICE_LIST = 0x80
    CMD_START_DIGITAL_SERVICE    = 0x81
    CMD_DAB_TUNE_FREQ            = 0xB0
    CMD_DAB_DIGRAD_STATUS        = 0xB2
    CMD_DAB_SET_FREQ_LIST        = 0xB8
    
    MSK_STATUS0_CTS = 0x80
    MSK_STATUS0_ERR = 0x40
    
    PROP_DIGITAL_IO_OUTPUT_SELECT      = 0x0200
    PROP_DIGITAL_IO_OUTPUT_SAMPLE_RATE = 0x0201
    PROP_DIGITAL_IO_OUTPUT_FORMAT      = 0x0202
    PROP_PIN_CONFIG_ENABLE             = 0x0800
    
    def __init__(self, i2cadapter):
        self.i2c   = i2cadapter
        self.log = logging.getLogger(u'Si4688')
        
    def reset(self):
        self.log.info(u'Resetting')
        self.i2c.reset_dab()

    def write_command(self, cmd, data):
        self.log.info(u'Sending command {0:#04x}'.format(cmd))
        
        i2c = self.i2c
        
        i2c.start(self.addr, False)
        try:
            i2c.write(struct.pack('<B', cmd) + data)
        finally:
            i2c.stop()
            
    def log_status(self, status):
        msg = u''
        
        if status[0] & self.MSK_STATUS0_ERR:
            msg = msg + u' ERR'
            
        self.log.info(u'Status: {0:02x} {1:02x} {2:02x} {3:02x}'.format(*status) + msg)

    def read_reply(self, lenresponse):
        #self.write_command(self.CMD_RD_REPLY, '')

        i2c = self.i2c
        
        i2c.start(self.addr, False)
        try:
            i2c.write(struct.pack('<B', self.CMD_RD_REPLY))
            i2c.restart(self.addr, True)
            
            data = i2c.read(4)

            status = struct.unpack('<4B', data)

            self.log_status(status)

            if status[0] & self.MSK_STATUS0_CTS:
                if callable(lenresponse):
                    response = ''
                    len_to_read = True
                    
                    while len_to_read:
                        len_to_read = lenresponse(response)

                        if len_to_read > 0:
                            response += i2c.read(len_to_read)
                else:
                    response = i2c.read(lenresponse)
            else:
                response = None
        finally:
            i2c.stop()
            
        return status, response

    def poll_ready(self, timeout = 1, lenresponse = 0):
        tsend = time.time() + timeout

        status = (0,)

        while 0 == (status[0] & self.MSK_STATUS0_CTS):
            status, response = self.read_reply(lenresponse)
            
            if tsend < time.time():
                break
            
        if 0 == (status[0] & self.MSK_STATUS0_CTS):
            msg = u'Not ready within {0:f} seconds'.format(timeout)
            raise Timeout(msg)

        return status, response

    def command(self, cmd, data, lenresponse, timeout=1):
        
        self.poll_ready(timeout)
        self.write_command(cmd, data)

        return self.poll_ready(timeout, lenresponse)

    
    def load_firmware(self, path):
        self.log.info(u'Loading firmware image {0}'.format(path))
        content = open(path,'rb').read()
        
        self.command(self.CMD_LOAD_INIT, '\x00', 0)
        time.sleep(0.004)
        
        for x in range(0,len(content), 2048):
            blob = content[x:x+2048]
            self.command(self.CMD_HOST_LOAD, '\x00\x00\x00' + blob, 0)
            progress = (x + len(blob)) * 100 / len(content)
            self.log.info(u'Loading firmware image {0}: {1}%'.format(path, int(progress)))
            time.sleep(0.001)

    def load_firmware_flash(self, address):
        self.command(self.CMD_LOAD_INIT, '\x00', 0)
        time.sleep(0.004)

        data = struct.pack('<3x L 4x', address)
        self.command(self.CMD_FLASH_PASS_THROUGH, data, 0, timeout=2)

    def boot(self):
        self.command(self.CMD_BOOT, '\x00', 0)
        
    def get_sys_state(self):
        status, response = self.command(self.CMD_GET_SYS_STATE, '\x00', 2)
        print(response.encode('hex'))
        
    def set_property(self, propid, value):
        self.command(self.CMD_SET_PROPERTY, struct.pack('<BHH',0,propid,value), 0)
        
    def fm_seek(self):
        args = struct.pack('<BBBH',0x00,0x01, 0,0)
        self.command(self.CMD_FM_SEEK_START, args, 0)
        
    def fm_status(self):
        args = struct.pack('<B',0x00)
        status, resp = self.command(self.CMD_FM_RSQ_STATUS, args, 16)
        
        resp4, resp5, readfreq, freqoff, rssi, snr, mult, readantcap, hdlevel = \
          struct.unpack('<BBHBBBBHxB4x', resp)
          
        self.log.info(u'Freq: {0}, Snr: {1}, Valid: {2}'.format(readfreq, snr, (resp5 & 0x01) != 0))
        
    def powerup(self):
        self.reset()
        
        clk_mode = 1
        tr_size  = 0x7
        ibias    = 40
        ibias_run = ibias #40
        freq     = 19200000
        ctun     = 20
        arg1  = 0
        arg2  = (clk_mode << 4) | tr_size
        arg3  = 0x7f & ibias
        arg4  = freq
        arg8  = 0x3f & ctun
        arg9  = 0x10
        arg13 = 0
        
        data = struct.pack('<3BL8B', arg1,arg2,arg3,arg4, arg8, arg9, 0, 0, 0, arg13, 0, 0)
        
        self.command(self.CMD_POWER_UP, data, 0)
        
    def activate_fm(self):
        self.powerup()
        
        self.load_firmware('rom00_patch.016.bin')
        self.load_firmware_flash(2*512*1024);
        #self.load_firmware('fmhd_radio_4_0_12.bif')
        self.boot()
        
        self.get_sys_state()
        
        self.set_property(self.PROP_DIGITAL_IO_OUTPUT_SELECT,     0x0000)
        self.set_property(self.PROP_DIGITAL_IO_OUTPUT_SAMPLE_RATE, 48000)
        self.set_property(self.PROP_DIGITAL_IO_OUTPUT_FORMAT,     0x1000)
        self.set_property(self.PROP_PIN_CONFIG_ENABLE,            0x0002)
        
    def activate_dab(self):
        self.powerup()
        
        self.load_firmware('rom00_patch.016.bin')
        
        self.load_firmware_flash(3*512*1024);
        #self.load_firmware('dab_radio_4_0_5.bif')
        self.boot()
        
        self.get_sys_state()
        
        self.set_property(self.PROP_DIGITAL_IO_OUTPUT_SELECT,     0x0000)
        self.set_property(self.PROP_DIGITAL_IO_OUTPUT_SAMPLE_RATE, 48000)
        self.set_property(self.PROP_DIGITAL_IO_OUTPUT_FORMAT,     0x1000)
        self.set_property(self.PROP_PIN_CONFIG_ENABLE,            0x0002)
        
    
    SUBCMD_FLASH_ERASE_CHIP = 0xFF
    SUBCMD_FLASH_WRITE_BLOCK = 0xF0
    SUBCMD_FLASH_WRITE_BLOCK_PACKET_VERIFY = 0xF2
    SUBCMD_FLASH_WRITE_BLOCK_READBACK_AND_PACKET_VERIFY = 0xF3
    
    def flash_erase(self):
        data = struct.pack('<B BB', self.SUBCMD_FLASH_ERASE_CHIP, 0xDE, 0xC0)
        self.command(self.CMD_FLASH_PASS_THROUGH, data, 0, timeout=60)

    def flash_write_block(self, address, blob):
        crc = binascii.crc32(blob) & 0xFFFFFFFF
        data = struct.pack('<B BB L L L', self.SUBCMD_FLASH_WRITE_BLOCK_READBACK_AND_PACKET_VERIFY, 
                           0x0C, 0xED, crc, address, len(blob)) + blob

        data = struct.pack('<B BB l L L', self.SUBCMD_FLASH_WRITE_BLOCK, 
                           0x0C, 0xED, 0 , address, len(blob)) + blob
                           
        self.command(self.CMD_FLASH_PASS_THROUGH, data, 0, timeout=60)
        time.sleep(0.001)

    def flash_write_firmware(self, address, path):
        self.log.info(u'Writing firmware image {0} at {1:#08x}'.format(path, address))
        content = open(path,'rb').read()
        
        for x in range(0,len(content), 2048):
            blob = content[x:x+2048]

            self.log.info(u'Writing from {0:#08x}-{1:#08x}'.format(address+x, address+x+len(blob)-1))
            self.flash_write_block(address + x, blob);


    def flash_firmwares(self):
        self.powerup()

        # patch required according manual
        self.load_firmware('rom00_patch.016.bin')
        self.flash_erase()

        self.flash_write_firmware(2 * 512*1024, 'fmhd_radio_4_0_12.bif');
        self.flash_write_firmware(3 * 512*1024, 'dab_radio_4_0_5.bif');

    def dab_set_freq_list(self, frequencies):
        x = '<B2x%dL' % len(frequencies)
        data = struct.pack(x, len(frequencies), *frequencies)

        self.command(self.CMD_DAB_SET_FREQ_LIST, data, 0)
        
    def dab_tune_freq(self, idx):
        data = struct.pack('<xBxH', idx, 0)
        self.command(self.CMD_DAB_TUNE_FREQ, data, 0)
        
    def get_digital_service_list(self, type = 0):
        data = struct.pack('<B', type & 0x3)
        
        def calc_response_len(response):
            if len(response) < 2:
                len_to_read =  2 - len(response)
            else:
                len_to_read, = struct.unpack('<H', response[0:2])
                len_to_read =  2 + len_to_read - len(response)
                
            return len_to_read
        
        status, response = self.command(self.CMD_GET_DIGITAL_SERVICE_LIST, data, calc_response_len)
        self.log.info('Service List: ' + response.encode('hex'))
        
        if len(response) >= 8:
            offset = 2

            version, numservices = struct.unpack('<HB3x', response[offset:offset+6])
            offset += 6
            
            self.log.info('Found {0} services'.format(numservices))
            
            for s in range(numservices):
                service_id, sinfo1, sinfo2, sinfo3, label = struct.unpack('<LBBBx16s', response[offset:offset+24])
                offset += 24
                
                self.log.info('Service {0} ID {1}'.format(label, service_id))
                
                numcomponents = sinfo2 & 0x0F
                
                for c in range(numcomponents):
                    component_id, cinfo, vflags = struct.unpack('<HBB', response[offset:offset+4])
                    offset += 4
                    self.log.info('Component ID {0}'.format(component_id))
        
    def start_digital_service(self, service_id, component_id):
        data = struct.pack('<B2xLL', 0, service_id, component_id)
        self.command(self.CMD_START_DIGITAL_SERVICE, data, 0)
        
    def dab_digrad_status(self):
        data = struct.pack('<B', 0x5)
        status,response = self.command(self.CMD_DAB_DIGRAD_STATUS, data, 18)
        
        resp4,resp5,rssi,snr,fiq_quality, cnr, fib_error_count, tune_freq, tune_index,fft_offset, readantcap,culevel = \
          struct.unpack('<6BHL2BHH', response)
          
        if resp5 & 0x10:
            self.log.warning('DAB Hard Mute')

        self.log.info('DAB SNR: {0}'.format(snr))
        

class Max98089(object):
    addr = 0x10
    
    def __init__(self, i2cadapter):
        self.i2c   = i2cadapter
        self.log = logging.getLogger(u'Max98089')
        
    def write_registers(self, addr, data):
        if isinstance(data,int):
            data = struct.pack('<B',data)
        
        if len(data) > 1:
            self.log.info(u'Writing registers {0:#04x} to {1:#04x}'.format(addr, addr + len(data) - 1))
        else:
            self.log.info(u'Writing register {0:#04x}'.format(addr))

        i2c = self.i2c

        i2c.start(self.addr, False)
        try:
            i2c.write(struct.pack('<B', addr) + data)
        finally:
            i2c.stop()

    def read_registers(self, addr, count):
        if count > 1:
            self.log.info(u'Reading registers {0:#04x} to {1:#04x}'.format(addr, addr + count - 1))
        else:
            self.log.info(u'Reading register {0:#04x}'.format(addr))

        i2c = self.i2c

        i2c.start(self.addr, False)
        try:
            i2c.write(struct.pack('<B', addr))
            
            i2c.restart(self.addr,True)
            
            data = i2c.read(count)
        finally:
            i2c.stop()
            
        return data
    
    REG_MASTER_CLOCK                   = 0x10
    REG_DAI1_CLOCK_MODE                = 0x11
    REG_DAI1_ANY_CLOCK_CONTROL1        = 0x12
    REG_DAI1_ANY_CLOCK_CONTROL2        = 0x13
    REG_DAI1_FORMAT                    = 0x14
    REG_DAI1_CLOCK                     = 0x15
    REG_DAI1_IO_CONFIGURATION          = 0x16
    REG_DAI1_TIME_DIVISION_MULTIPLEX   = 0x17
    REG_DAC_MIXER                      = 0x22
    REG_LEFT_RECEIVER_AMPLIFIER_MIXER  = 0x28
    REG_RIGHT_RECEIVER_AMPLIFIER_MIXER = 0x29
    REG_RECEIVER_AMPLIFIER_MIXER_CONTROL = 0x2A
    REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL   = 0x3B
    REG_RIGHT_RECEIVER_AMPLIFIER_VOLUME_CONTROL  = 0x3C
    REG_OUTPUT_ENABLE                            = 0x4D
    REG_POWER_MANAGEMENT_TOPLEVEL_BIAS_CONTROL   = 0x4E
    REG_DAC_LOW_POWER_MODE2                      = 0x50
    REG_SYSTEM_SHUTDOWN                          = 0x51
    
    def test(self):
        self.write_registers(self.REG_SYSTEM_SHUTDOWN, 0x00)
        
        self.write_registers(self.REG_MASTER_CLOCK,                     0x20)
        self.write_registers(self.REG_DAI1_CLOCK_MODE,                  0x80)
        self.write_registers(self.REG_DAI1_ANY_CLOCK_CONTROL1,          (0x6000 >> 8) & 0x7F)
        self.write_registers(self.REG_DAI1_ANY_CLOCK_CONTROL2,          0x6000  & 0xFE)
        self.write_registers(self.REG_DAI1_FORMAT,                      0x90)
        self.write_registers(self.REG_DAI1_CLOCK,                       0x02) # todo
        self.write_registers(self.REG_DAI1_IO_CONFIGURATION,            0x41)
        self.write_registers(self.REG_DAI1_TIME_DIVISION_MULTIPLEX,     0x00)
        self.write_registers(self.REG_DAC_LOW_POWER_MODE2,              0x05)
        self.write_registers(self.REG_DAC_MIXER,                        0x84)
        self.write_registers(self.REG_LEFT_RECEIVER_AMPLIFIER_MIXER,    0x01)
        self.write_registers(self.REG_RIGHT_RECEIVER_AMPLIFIER_MIXER,   0x01)
        self.write_registers(self.REG_RECEIVER_AMPLIFIER_MIXER_CONTROL, 0x80)
        self.write_registers(self.REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL, 0x12)
        self.write_registers(self.REG_RIGHT_RECEIVER_AMPLIFIER_VOLUME_CONTROL, 0x12)

        self.write_registers(0x2F,   0x00)
        
        self.write_registers(self.REG_OUTPUT_ENABLE,   0x0F)
        self.write_registers(self.REG_POWER_MANAGEMENT_TOPLEVEL_BIAS_CONTROL,  0xF0)
        self.write_registers(self.REG_SYSTEM_SHUTDOWN, 0x80)
    
    
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    adapter = SerialI2CAdapter(sys.argv[1])

    
    try:
        adapter.read(1)
    except:
        pass
    
    try:
        adapter.stop()
    except:
        pass
    
    si    = Si4688(adapter)
    codec = Max98089(adapter)

    if 1:
        si.activate_fm()
        
        codec.test()
        
        s = codec.read_registers(0,4)
        
        print s.encode('hex')
    
        for i in range(6):
            si.fm_seek()
            
            tend = time.time() + 10
            while tend > time.time():
                si.fm_status()
    
        s = codec.read_registers(0,4)
        print s.encode('hex')
    elif 0:
        si.activate_dab()
        codec.test()
        
        si.dab_set_freq_list([190640, 220352])
        
        si.dab_tune_freq(0)

        for i in range(5):
            time.sleep(1)
            si.get_digital_service_list()
            
        service_id, = struct.unpack('<L', 'c\xd3\x00\x00')
            
        si.start_digital_service(54115, 2)

        for i in range(5):
            time.sleep(1)
            si.start_digital_service(54115, 2)
            si.dab_digrad_status()
    else:
        si.flash_firmwares()

    exit(0)
    
    
    
    
    for i in xrange(10):
        si.send_command(0, '')
        time.sleep(0.1)
        si.recv_response(4)
    
    si.send_command(6, '\x00')
    
    for i in xrange(10):
        time.sleep(0.1)
        si.recv_response(4)
    
    si.send_command(7, '\x00')
    si.recv_response(4)
    
    for i in xrange(10):
        time.sleep(0.1)
        si.send_command(0, '')
        si.recv_response(4)

    #for x in xrange(100):
    #    adapter.start(0x10)
    #    try:
    #        adapter.read(1)
    #    finally:
    #        adapter.stop()
    #        
    #    adapter.start(0x64)
    #    try:
    #        adapter.read(1)
    #    finally:
    #        adapter.stop()