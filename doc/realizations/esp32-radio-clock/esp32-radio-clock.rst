ESP 32 based radio clock
========================

.. figure:: esp32-radio-clock_finish.jpg
   :width: 80%
   :align: center

Powered by music box software, I have made an radio clock based on
my elf designed `Bastelino ESP32-S2`_ base board and `Radio shield`_. 
The radio clock uses a 128x64 graphics LCD for display and a 
wooden housing. Its powered by an USB charge adaptor for smartphones 
and has integrated 3xAAA failsafe battery.

.. _Bastelino ESP32-S2: https://gitlab.com/amesser-group/electronic-devices/bastelino-esp32-s2

.. _Radio Shield: https://gitlab.com/amesser-group/electronic-devices/radio-shield

Status
------

Radio operation and alarm clock function are finished. The hardware 
is also capable of WLAN and has SD-Card slot. However, this is not
yet implemented in music box software.


Here are some images for reference:

.. figure:: esp32-radio-clock_inner.jpg
   :width: 80%
   :align: center

.. figure:: esp32-radio-clock_assembled.jpg
   :width: 80%
   :align: center

.. figure:: esp32-radio-clock_closed.jpg
   :width: 80%
   :align: center

References
----------

.. target-notes::