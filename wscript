#! /usr/bin/env python
# encoding: utf-8
top = '.'
out = './build_waf'

def options(opt):
    opt.recurse(['components/ecpp-core'])

def configure(conf):
    conf.recurse(['components/ecpp-core','src'])
  
def build(ctx):
    ctx.recurse(['components/ecpp-core','src'])